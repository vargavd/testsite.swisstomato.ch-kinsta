<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ST_Base_Theme
 */

get_header();

$page_404_background_image = get_field('404_page_background_image', 'option');
$page_404_mobile_image = get_field('404_page_mobile_image', 'option');
$page_404_title = get_field('404_page_title', 'option');
$page_404_text = get_field('404_page_text', 'option');
// $page_404_links = get_field('404_page_links', 'option');
?>

<div class="stbase-page page-404">
  <div class="container" style="background-image: url(<?=$page_404_background_image?>);">
    <div class="page-404__text">
      <h1><?=$page_404_title?></h1>
      <?=$page_404_text?>
      <img src="<?=$page_404_mobile_image?>" class="mobile" />
    </div>
  </div>
</div>

<?php
get_footer();
