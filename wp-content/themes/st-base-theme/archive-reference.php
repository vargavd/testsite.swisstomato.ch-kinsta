<?php
get_header();


?>

	<div class="stbase-references-page stbase-page container">
    
    <?php 
      $options = array(
        'like-archive' => array(
          'display_buttons' => false,
          'title' => get_field('references_page_title', 'option'),
          'intro' => get_field('references_page_intro', 'option')
        )
      );

      masonry_references_list_section_func($options);
    ?>

	</div>

<?php
get_footer();
