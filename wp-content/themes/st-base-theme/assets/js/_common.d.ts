
// TYPE DEFINITIONS
declare var swisstInfos: {
  baseUrl: string,
  privacyPolicyUrl: string,
  language: string
  no_references_text: string,
  minWidthForAnimations: number
}
