jQuery(function ($) {
  let 
    // DOM 
    $body = $('body'),

    // main funcs
    setTranslationPreferencesOnFieldGroups = () => {
      $('.acf-field-object').each(function (index) {
        let 
          // DOM
          $acfFieldPanel = $(this),
          $panelName = $acfFieldPanel.find('.handle li.li-field-label a.edit-field').first(),
          $liFieldType = $acfFieldPanel.find('.handle li.li-field-type').first(),
          $translationPreferenceLabels = $acfFieldPanel
            .find('tr.acf-field-setting-wpml_cf_preferences')
            .first()
            .find('input[name*="wpml_cf_preferences"]')
            .closest('label'),
          
          // misc
          panelName = $panelName.text(),
          liFieldType = $liFieldType.text().trim();
        
        switch (liFieldType) {
          case 'Tab':
          case 'Repeater':
          case 'Group':
            $translationPreferenceLabels.find('input[value=3]').click();
            console.log(`> "Copy Once" - ${liFieldType} - ${panelName}`);
            break;
          case 'Text':
          case 'Text Area':
          case 'Image':
          case 'File':
          case 'Email':
          case 'Url':
          case 'Button Group':
          case 'Link':
          case 'Number':
          case 'Wysiwyg Editor':
          case 'True / False':
          case 'Message':
          case 'Taxonomy':
          case 'Post Object':
          case 'Date Picker':
            $translationPreferenceLabels.find('input[value=2]').click();
            console.log(`> "Translate" - ${liFieldType} - ${panelName}`);
            break;
          default:
            console.error(`Unknown field type: ${liFieldType}`);
        };
      });
    },
    checkTranslationPreferencesOnFieldGroups = () => {
      jQuery('tr.acf-field-setting-wpml_cf_preferences .acf-radio-list label.selected').each(function () {
        let 
          // DOM
          $selectedLabel = jQuery(this),
          $acfFieldPanel = $selectedLabel.closest('.acf-field-object'),
          $liFieldType = $acfFieldPanel.find('li.li-field-type').first(),
          
          // misc
          selectedLabel = $selectedLabel.text(),
          liFieldType = $liFieldType.text().trim();
        
        console.log(`${selectedLabel} - ${liFieldType}`);
      });
    };

    if (!$body.is('.post-type-acf-field-group')) {
      return;
    }

  // setTranslationPreferencesOnFieldGroups();
  checkTranslationPreferencesOnFieldGroups();
});