jQuery(function () {
  const accordionHeaders = document.querySelectorAll(".accordion-header");

  accordionHeaders.forEach((header) => {
    header.addEventListener("click", () => {
      const content = header.nextElementSibling;
      const svgPath = header.querySelector("svg path");
      if (content.style.display === "block") {
        content.style.display = "none";
        svgPath.setAttribute(
          "d",
          "M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"
        );
        header.classList.remove("open");
      } else {
        content.style.display = "block";
        svgPath.setAttribute(
          "d",
          "M432 256c0 17.7-14.3 32-32 32L48 288c-17.7 0-32-14.3-32-32s14.3-32 32-32l352 0c17.7 0 32 14.3 32 32z"
        );
        header.classList.add("open");
      }
    });
  });
});
