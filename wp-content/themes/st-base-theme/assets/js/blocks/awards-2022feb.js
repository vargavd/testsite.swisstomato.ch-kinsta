jQuery(function ($) {
  let
    // DOM
    $awardsSection = $('.awards-2022feb-section'),
    $awardsList = $awardsSection.find('.awards'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($awardsSection, 'scrolled-to', 350, $awardsList, true);
      // $awardsList.addClass('no-scroll');
    };

  initAnimations();
});