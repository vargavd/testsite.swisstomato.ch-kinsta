jQuery(function ($) {
  let
    // DOM
    $awardsSection = $('.stbase-awards-block'),
    $awardsList = $awardsSection.find('.awards'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($awardsSection, 'scrolled-to', 350, $awardsList, true);
      // $awardsList.addClass('no-scroll');
    };

  initAnimations();
});