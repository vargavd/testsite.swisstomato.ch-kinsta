jQuery(function ($) {
    var $collapsibleAdvantages = $('.fact-advantages.collapsible'), clickOnShowMoreLink = function () {
        var $link = $(this), $wrapper = $link.closest('.fact-advantages'), $advantages = $wrapper.find('.advantage');
        $advantages.each(function (_i, elem) {
            var $advantage = $(elem), fullHeight = $advantage.attr('data-full-height'), closedHeight = $advantage.attr('data-closed-height');
            if ($link.hasClass('opened')) {
                $advantage.animate({ 'height': closedHeight });
            }
            else {
                $advantage.animate({ 'height': fullHeight });
            }
            $link.toggleClass('opened');
        });
    }, setUpAdvantage = function (_, elem) {
        var $advantage = $(elem), height = $advantage.height(), lineHeightPx = getComputedStyle($advantage[0]).lineHeight, closedHeight = (+lineHeightPx.replace('px', '')) * 3;
        $advantage
            .attr({ 'data-full-height': height, 'data-closed-height': closedHeight })
            .animate({ 'height': closedHeight });
    };
    $collapsibleAdvantages.each(function (_i, elem) {
        var $advantagesWrapper = $(elem), $advantages = $advantagesWrapper.find('.advantage'), $showMoreLink = $advantagesWrapper.find('.advantages-show-more-link');
        $advantages.each(setUpAdvantage);
        $showMoreLink.on('click', clickOnShowMoreLink);
    });
});
//# sourceMappingURL=show-more-advantages.js.map