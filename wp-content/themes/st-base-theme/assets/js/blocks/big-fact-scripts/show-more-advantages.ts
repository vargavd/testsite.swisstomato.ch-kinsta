jQuery(function ($) {
  let
      // DOM
      $collapsibleAdvantages = $('.fact-advantages.collapsible'),
      
      // funcs
      clickOnShowMoreLink = function () {
        let
          // DOM
          $link             = $(this),
          $wrapper          = $link.closest('.fact-advantages'),
          $advantages       = $wrapper.find('.advantage');

        $advantages.each((_i, elem:HTMLElement) => {
          let
            // DOM
            $advantage = $(elem),

            // misc
            fullHeight   = $advantage.attr('data-full-height'),
            closedHeight = $advantage.attr('data-closed-height');

          if ($link.hasClass('opened')) {
            $advantage.animate({ 'height': closedHeight });
          } else {
            $advantage.animate({'height': fullHeight});
          }

          $link.toggleClass('opened');
        });
      },

      // main funcs
      setUpAdvantage = (_, elem) => {
        let
          // DOM
          $advantage = $(elem),

          // misc
          height       = $advantage.height(),
          lineHeightPx = getComputedStyle($advantage[0]).lineHeight,
          closedHeight = (+lineHeightPx.replace('px', ''))*3;

        $advantage
          .attr({ 'data-full-height': height, 'data-closed-height': closedHeight })
          .animate({'height': closedHeight});
      };

    $collapsibleAdvantages.each((_i, elem:HTMLElement) => {
      let
        // DOM
        $advantagesWrapper = $(elem),
        $advantages        = $advantagesWrapper.find('.advantage'),
        $showMoreLink      = $advantagesWrapper.find('.advantages-show-more-link');

      $advantages.each(setUpAdvantage);
      $showMoreLink.on('click', clickOnShowMoreLink);
    });
});
