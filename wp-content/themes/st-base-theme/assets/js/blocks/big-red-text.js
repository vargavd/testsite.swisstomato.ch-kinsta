jQuery(function ($) {
  let
    // DOM
    $bigRedTextSection = $('.stbase-big-red-text-block'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($bigRedTextSection, 'seen-first', -50);
      swisstScrollTriggerer($bigRedTextSection, 'seen-second', 100);
    };

  initAnimations();
});