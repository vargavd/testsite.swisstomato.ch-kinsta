jQuery(function ($) {
  let
    // DOM
    $clientsSection = $('.stbase-clients-block'),
    $clientsList = $clientsSection.find('.client-list'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($clientsList, 'scrolled-to', -300);
      // $clientsList.addClass('no-scroll');
    };

  initAnimations();
});