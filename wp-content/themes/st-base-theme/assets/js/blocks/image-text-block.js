jQuery(function ($) {
  let
    // DOM
    $imageTextBlocks = $('.stbase-image-text-block'),
    $containers = $imageTextBlocks.find('.container'),
    
    // init funcs
    initAnimations = function () {
      $imageTextBlocks.each(function () {
        swisstScrollTriggerer($(this).find('.container'), 'scrolled-to');
      });
    };

  initAnimations();
});