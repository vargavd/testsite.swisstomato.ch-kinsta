jQuery(document).ready($ => {
  let $testimonialsBlock = $('.stbase-testimonials-block');
  let blockContent = $testimonialsBlock.data('testimonials-var');

  if (!!window.IntersectionObserver) {
    let observer = new IntersectionObserver(
      (entries, observer) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            $testimonialsBlock.html(window[blockContent]);

            const swiper = new Swiper('.swiper', {
              slidesPerView: 1,
              loop: true,
              autoplay: false,
              grabCursor: true,
              paginationClickable: true,
              lazy: true,
              pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true,
              },
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
              breakpoints: {
                800: {
                  slidesPerView: 2,
                  spaceBetween: 30,
                }
              },
              updateOnWindowResize: true,
            });

            swiper.autoplay.start()
            observer.unobserve(entry.target);
          }
        });
      }, {
      rootMargin: "0px 0px -200px 0px"
    }
    );
    observer.observe($testimonialsBlock[0]);
  } else {
    // You can use the polyfill or just start the autoplay
  }
});