jQuery(function ($) {
    var $cookieWrapper = $('#cookie'), $cookieButton = $cookieWrapper.find('.cookie__btn'), lsName = 'ST Base Cookie', lsValue = localStorage.getItem(lsName), clickOnAcceptButton = function () {
        localStorage.setItem(lsName, '1');
        $cookieWrapper.removeClass('active');
        cl('Close cookie message');
    };
    $cookieButton.on('click', clickOnAcceptButton);
    if (lsValue === null) {
        $cookieWrapper.addClass('active');
    }
});
//# sourceMappingURL=cookies.js.map