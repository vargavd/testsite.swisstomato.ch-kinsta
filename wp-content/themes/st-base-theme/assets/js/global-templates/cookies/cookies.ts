/// <reference path="../../utils/common.ts" />

jQuery(function ($) {
  let
    // DOM
    $cookieWrapper = $('#cookie'),
    $cookieButton = $cookieWrapper.find('.cookie__btn'),

    // misc
    lsName = 'ST Base Cookie',
    lsValue = localStorage.getItem(lsName),

    // events
    clickOnAcceptButton = function (this: any) {
      localStorage.setItem(lsName, '1');
      $cookieWrapper.removeClass('active');

      cl('Close cookie message');
    };

  $cookieButton.on('click', clickOnAcceptButton);

  if (lsValue === null) {
    $cookieWrapper.addClass('active');
  }
});