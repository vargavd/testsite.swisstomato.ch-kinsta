/// <reference path="../../_common.d.ts" />

jQuery(function ($) {
  let
    // global DOM
    $body = $('body'),
    $masonryReferenceListSections = $('.masonry-references-list-section') as JQuery<HTMLDivElement>;

  $masonryReferenceListSections.each(function () {
    let
      // global DOM
      $masonryReferenceListSection = $(this),
      $referenceList = $masonryReferenceListSection.find('.masonry-references-list') as JQuery<HTMLDivElement>,
      $template = $masonryReferenceListSection.find('.template').first().detach().removeClass('template') as JQuery<HTMLDivElement>,
      $moreReferencesButton = $masonryReferenceListSection.find('a.more-references') as JQuery<HTMLButtonElement>,

      // global variables
      bottomPosOfLastReference = 0,
      isArchive = $body.hasClass('post-type-archive'),

      // helper funcs
      removeAnimationsFromNonExistentReferences: {():void }[] = [],
      
      // init
      initLayout = function () {
        let
          // DOM
          $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>,

          // style variables
          referenceListComputedStyle = window.getComputedStyle($referenceList[0]),
          referenceListGridAutoRowsValue = parseInt(referenceListComputedStyle.getPropertyValue('grid-auto-rows')),
          referenceListDisplayStyle = referenceListComputedStyle.getPropertyValue('display'),

          // masonry helper variables
          nextFreeRows = [3, 1],
          mobileCurrentTop = 0,
          indexOfLastReference = $references.length -1,
          
          // helper funcs
          alignReferenceToMasonry = (index: number, elem: HTMLElement) => {
            let
              // DOM
              $reference = $(elem),

              // misc
              imgHeight = +$reference!.attr('data-height'),
              rowSpanValue = Math.floor(imgHeight / referenceListGridAutoRowsValue),
              currentColumn = index%2;

            // last post should go to the smaller column
            if (indexOfLastReference === index) {
              currentColumn = nextFreeRows[0] < nextFreeRows[1] ? 0 : 1;
            }

            if (801 < window.innerWidth && window.innerWidth < 990) {
              rowSpanValue = Math.floor(2*rowSpanValue/3);
            }

            if (651 < window.innerWidth && window.innerWidth < 800) {
              rowSpanValue = Math.floor(rowSpanValue/2);
            }

            $reference.css({
              // grid-row: grid-row-start / grid-row-end
              gridRow: `${nextFreeRows[currentColumn]} / span ${rowSpanValue}`,
              gridColumnStart: `${currentColumn+1}`
            });

            // the top attr is needed for animation later
            $reference.attr('data-top', nextFreeRows[currentColumn]*referenceListGridAutoRowsValue);

            nextFreeRows[currentColumn] += rowSpanValue;

            $reference.attr('data-index', index);
          },
          alignReferenceToMobile = (_: number, elem: HTMLElement) => {
            let
              // DOM
              $reference = $(elem),
              
              referenceHeight = $reference.height();

            $reference.attr('data-top', mobileCurrentTop - 50);

            mobileCurrentTop += referenceHeight;
          };

        // click event
        $references.on('click', event => location.href = $(event.currentTarget).attr('data-url'));

        // align references into masonry
        if (referenceListDisplayStyle === 'grid') {
          $references.each(alignReferenceToMasonry);
        } else {
          $references.each(alignReferenceToMobile);
        }
        

        // display the list
        $referenceList.css('opacity', 1);

        // set bottom post - for animation
        bottomPosOfLastReference = Math.max(...nextFreeRows)*referenceListGridAutoRowsValue;
      },
      initFiltering = function () {
        let
          // references DOM
          $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>,

          // filter DOM
          $filterSection = $('#filter-section') as JQuery<HTMLDivElement>,
          $categoryFilters = $filterSection.find('.filter-box') as JQuery<HTMLDivElement>,

          // misc
          baseRestUrl = swisstInfos.baseUrl + '/wp-json/wp/v2/reference?_embed&acf_format=standard',
          currentCategory = location.hash.includes('platform=') ? location.hash.split('platform=')[1] : '',

          // events
          loadReferences = (categorySlug: string) => {
            let 
              // DOM
              $categoryFilter = categorySlug ? 
                $categoryFilters.filter(`[data-slug="${categorySlug}"]`) : 
                $categoryFilters.filter(':not([data-slug])'),
      
              // misc
              categoryId = $categoryFilter.attr('data-id'),
              constructedRestUrl = baseRestUrl;
      
      
            // state classes
            $categoryFilters.removeClass('selected');
            $categoryFilter.addClass('selected');
            $filterSection.addClass('loading');
            $masonryReferenceListSection.addClass('loading');

            // construct rest url
            if (typeof categoryId !== 'undefined') {
              constructedRestUrl += `&reference_category=${categoryId}`;
              if (isArchive) {
                location.hash = `platform=${categorySlug}`;
              }
            } else {
              if (isArchive) {
                location.hash = '';
              }
            }
      
            // ajax call
            $.get(constructedRestUrl, {}, references => {
              // remove references from DOM
              $references.remove();
              
              // add references to DOM
              references.forEach(reference => {
                let 
                  // DOM
                  $reference = $template.clone(),
      
                  // misc
                  imageUrl = `${location.origin}/wp-content/themes/st-base-theme/imgs/gray-624x460.jpg`,
                  imageWidth = 624,
                  imageHeight = 460;
      
                if (reference.acf.masonry_thumbnail_image) {
                  imageUrl = reference.acf.masonry_thumbnail_image.url;
                  imageWidth = reference.acf.masonry_thumbnail_image.width;
                  imageHeight = reference.acf.masonry_thumbnail_image.height;
                }

                // class - black layer
                if (reference.acf.masonry_black_layer_below_title) {
                  $reference.addClass('black-layer');
                }

                // class - mobile black layer
                if (reference.acf.masonry_black_layer_below_title_mobile) {
                  $reference.addClass('mobile-black-layer');
                }
      
                // data attributes
                $reference.attr({
                  'data-width': imageWidth, 
                  'data-height': imageHeight,
                  'data-url': reference.link,
                  'data-id': reference.id
                });
      
                // title
                $reference.find('h3')
                  .attr('data-text-slide-from-up', reference.acf.masonry_reference_title)
                  .html(`<span class="text-slide-down">${reference.acf.masonry_reference_title}</span>`);
      
                // text
                $reference.find('p')
                  .attr('data-text-slide-from-up', reference.acf.masonry_reference_text)
                  .html(`<span class="text-slide-down">${reference.acf.masonry_reference_text}</span>`);
      
                // image
                $reference.find('img').attr({
                  src: imageUrl,
                  alt: reference.acf.masonry_reference_title
                });

                // category
                $reference.find('.reference__content__category').text(categorySlug ? $categoryFilter.text().trim() : reference.category_name);
      
                $referenceList.append($reference);
      
                $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>;
              });
      
              // remove loading status from DOM
              $filterSection.removeClass('loading');
              $masonryReferenceListSection.removeClass('loading');

              // reinit Masonry and animations
              initLayout();
              initAnimation();
            });
          },
          clickOnFilter = (event: JQuery.ClickEvent) => {
            let 
              // DOM
              $categoryFilter = $(event.currentTarget),
      
              // misc
              categorySlug = $categoryFilter.attr('data-slug'),
              buttonText = $categoryFilter.attr('data-button-text'),
              buttonLink = $categoryFilter.attr('data-button-link');
      
            if ($filterSection.hasClass('loading')) {
              return;
            }

            $moreReferencesButton.attr('href', buttonLink).text(buttonText);

            loadReferences(categorySlug);
          };

        if ($filterSection.length === 0) {
          return;
        }

        baseRestUrl += isArchive ? '&per_page=100' : '&per_page=7';

        $categoryFilters.on('click', clickOnFilter);

        if (currentCategory) {
          loadReferences(currentCategory);
        }
      },
      initAnimation = function () {
        let
          // DOM
          $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>,

          // helper funcs
          animateReference = (_, referenceDOM) => {
            let
              // DOM
              $reference = $(referenceDOM),
      
              // misc
              top = +$reference.attr('data-top');
      
            removeAnimationsFromNonExistentReferences.push(swisstScrollTriggerer.addSection($referenceList, 'scrolled-to', top+200, $reference, true, true));
            // $reference.addClass('no-scroll');
          };

        // remove old animations
        removeAnimationsFromNonExistentReferences.forEach(removeAnim => removeAnim());

        // animation
        $references.each(animateReference);

        // animate the button
        swisstScrollTriggerer.addSection($masonryReferenceListSection, 'scrolled-to', bottomPosOfLastReference + 200, $moreReferencesButton, true);
        $masonryReferenceListSection.addClass('no-scroll');

        // recalculate trigger points with the animation component
        swisstScrollTriggerer.recalculateTriggerPoints();
        console.log('recalculation completed');
      };

    

    initLayout();

    initAnimation();

    initFiltering();

    // setTimeout(() => $('.filter-box:nth-child(2)').trigger('click'));

    // console.log('Masonry layout done');
  });
});