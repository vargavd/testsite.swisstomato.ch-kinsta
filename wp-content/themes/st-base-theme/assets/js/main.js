jQuery(function ($) {
    var $window = $(window), $header = $('#masthead'), $htmlBody = $('html, body'), initMisc = function () {
        var $stbaseJQueryUITabs = $('.stbase-jqueryui-tabs'), $nextBlockLinks = $('.next-block-link'), scrollEvent = function () {
            $header.toggleClass('addshadow', window.scrollY > 50);
        }, clickOnNextBlockLink = function () {
            var $link = $(this), $parentBlock = $link.closest('.stbase-block'), $nextBlock = $parentBlock.next('.stbase-block');
            $htmlBody.animate({
                scrollTop: $nextBlock.position().top - 50,
            }, 700, 'swing');
        };
        $window.on('scroll', scrollEvent);
        $stbaseJQueryUITabs.tabs();
        $nextBlockLinks.on('click', clickOnNextBlockLink);
    }, initContactSectionAndForm = function () {
        var $contactUsSections = $('.contact-us-section'), $contactInputs = $contactUsSections.find('input[type=text], input[type=tel], textarea, input[type=email]'), $contactFormWrapper = $('.wpcf7'), focusOnContactInput = function () {
            var $input = $(this), $fieldGroup = $input.closest('.field-group');
            $fieldGroup.toggleClass('focus');
            if ($fieldGroup.hasClass('filled') && $input.val() === '') {
                $fieldGroup.removeClass('filled');
            }
            else {
                $fieldGroup.addClass('filled');
            }
        }, contactFormEmailSent = function () {
            $contactInputs.closest('.field-group').removeClass('filled');
        };
        $contactInputs.on('focus', focusOnContactInput);
        $contactInputs.on('focusout', focusOnContactInput);
        $contactFormWrapper.on('wpcf7submit', contactFormEmailSent);
    }, initMenu = function () {
        var $openCloseMobileMenu = $header.find('.menu-toggle'), $siteNavigation = $header.find('#site-navigation'), $parentLis = $header.find('li.menu-item-has-children'), clickOnMobileMenuParentItem = function (e) {
            var $a = $(e.target), $parentLi = $a.closest('li'), $subMenuUl = $parentLi.find('ul.sub-menu');
            if ($subMenuUl.length === 0 || !$openCloseMobileMenu.hasClass('toggled')) {
                return;
            }
            e.preventDefault();
            if (!$parentLi.hasClass('expanded')) {
                $subMenuUl.show(200, function () {
                    $parentLi.addClass('expanded');
                });
            }
            else {
                $subMenuUl.hide(200, function () {
                    $parentLi.removeClass('expanded');
                });
            }
        }, openCloseMobileMenu = function () {
            if ($openCloseMobileMenu.hasClass('toggled')) {
                $openCloseMobileMenu.removeClass('toggled');
                $siteNavigation.removeClass('toggled');
                $siteNavigation.hide(300, function () {
                    $openCloseMobileMenu.attr('aria-expanded', 'true');
                    $siteNavigation.attr('aria-expanded', 'true');
                });
            }
            else {
                $openCloseMobileMenu.addClass('toggled');
                $siteNavigation.addClass('toggled');
                $siteNavigation.show(300, function () {
                    $openCloseMobileMenu.attr('aria-expanded', 'false');
                    $siteNavigation.attr('aria-expanded', 'false');
                });
            }
        };
        $openCloseMobileMenu.on('click', openCloseMobileMenu);
        $parentLis.on('click', clickOnMobileMenuParentItem);
    }, initCookies = function () {
        var $cookieWrapper = $('#cookie'), $cookieButton = $cookieWrapper.find('.st-button'), lsName = 'ST Base Cookie', lsValue = localStorage.getItem(lsName), clickOnAcceptButton = function () {
            localStorage.setItem(lsName, '1');
            $cookieWrapper.animate({ opacity: 0 }, 200, function () {
                $cookieWrapper.remove();
            });
        };
        $cookieButton.on('click', clickOnAcceptButton);
        if (lsValue === null) {
            $cookieWrapper.addClass('active');
        }
    };
    initMisc();
    initContactSectionAndForm();
    initMenu();
    initCookies();
});
//# sourceMappingURL=main.js.map