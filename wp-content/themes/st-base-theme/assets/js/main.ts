// declare var AOS: any;

jQuery(function ($) {
  let
    // DOM 
    $window = $(window),
    $header = $('#masthead'),
    $htmlBody = $('html, body'),
    
    // init functions
    initMisc = function () {
      let
        // DOM
        $stbaseJQueryUITabs = $('.stbase-jqueryui-tabs'),
        $nextBlockLinks = $('.next-block-link'),

        // events
        scrollEvent = function () {
          $header.toggleClass('addshadow', window.scrollY > 50);
        },
        clickOnNextBlockLink = function () {
          let
            // DOM
            $link = $(this),
            $parentBlock = $link.closest('.stbase-block'),
            $nextBlock = $parentBlock.next('.stbase-block');

          $htmlBody.animate({
            scrollTop: $nextBlock.position().top - 50,
          }, 700, 'swing');
        };

      $window.on('scroll', scrollEvent);

      // init jquery ui tabs
      $stbaseJQueryUITabs.tabs();

      // init AOS animation lib
      // AOS.init();

      // clicking on next block
      $nextBlockLinks.on('click', clickOnNextBlockLink);
    },
    initContactSectionAndForm = function () {
      let
        // DOM
        $contactUsSections = $('.contact-us-section'),
        $contactInputs = $contactUsSections.find('input[type=text], input[type=tel], textarea, input[type=email]'),
        $contactFormWrapper = $('.wpcf7'),

        // events
        focusOnContactInput = function (this: HTMLInputElement) {
          var
              // DOM
              $input = $(this),
              $fieldGroup = $input.closest('.field-group');
    
          $fieldGroup.toggleClass('focus');
    
          if ($fieldGroup.hasClass('filled') && $input.val() === '') {
              $fieldGroup.removeClass('filled');
          } else {
              $fieldGroup.addClass('filled');
          }
        },
        contactFormEmailSent = function () {
          $contactInputs.closest('.field-group').removeClass('filled');
        };
      
      $contactInputs.on('focus', focusOnContactInput);
      $contactInputs.on('focusout', focusOnContactInput);
      $contactFormWrapper.on('wpcf7submit', contactFormEmailSent);

      // cl($contactUsSections);
    },
    initMenu = function () {
      let
        // DOM
        $openCloseMobileMenu = $header.find('.menu-toggle'),
        $siteNavigation = $header.find('#site-navigation'),
        $parentLis = $header.find('li.menu-item-has-children'),

        // menu events
        clickOnMobileMenuParentItem = function (e: JQuery.ClickEvent) {
          var
              // DOM
              $a = $(e.target),
              $parentLi = $a.closest('li'),
              $subMenuUl = $parentLi.find('ul.sub-menu');

          if ($subMenuUl.length === 0 || !$openCloseMobileMenu.hasClass('toggled')) {
              return;
          }

          e.preventDefault();

          if (!$parentLi.hasClass('expanded')) {
              $subMenuUl.show(200, function () {
                  $parentLi.addClass('expanded');
              });
          } else {
              $subMenuUl.hide(200, function () {
                  $parentLi.removeClass('expanded');
              });
          }
        },
        openCloseMobileMenu = function () {
          if ($openCloseMobileMenu.hasClass('toggled')) {
            $openCloseMobileMenu.removeClass('toggled');
            $siteNavigation.removeClass('toggled');

            $siteNavigation.hide(300, function () {
              $openCloseMobileMenu.attr('aria-expanded', 'true');
              $siteNavigation.attr('aria-expanded', 'true');
            });

          } else {
            $openCloseMobileMenu.addClass('toggled');
            $siteNavigation.addClass('toggled');

            $siteNavigation.show(300, function () {
              $openCloseMobileMenu.attr('aria-expanded', 'false');
              $siteNavigation.attr('aria-expanded', 'false');
            });

          }
        };

      $openCloseMobileMenu.on('click', openCloseMobileMenu);
      $parentLis.on('click', clickOnMobileMenuParentItem);
    },
    initCookies = function () {
      let
        // DOM
        $cookieWrapper = $('#cookie'),
        $cookieButton = $cookieWrapper.find('.st-button'),

        // misc
        lsName = 'ST Base Cookie',
        lsValue = localStorage.getItem(lsName),

        // events
        clickOnAcceptButton = function (this: any) {
          localStorage.setItem(lsName, '1');

          $cookieWrapper.animate({ opacity: 0 }, 200, () => {
            $cookieWrapper.remove();
          });
        };

      $cookieButton.on('click', clickOnAcceptButton);

      if (lsValue === null) {
        $cookieWrapper.addClass('active');
      }
    };

  initMisc();
  initContactSectionAndForm();
  initMenu();
  initCookies();

  // debug
  // openCloseMobileMenu();
});