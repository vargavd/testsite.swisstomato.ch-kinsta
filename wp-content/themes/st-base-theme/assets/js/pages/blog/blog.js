jQuery(function ($) {
    var $blogList = $('.blog-post-list'), $postTemplate = $('.post-template').detach(), $loadMoreWrapper = $('.load-more-wrapper'), $loadMoreButton = $loadMoreWrapper.find('a.load-more'), restBaseUrl = "".concat(swisstInfos.baseUrl, "/wp-json/wp/v2/posts?"), loadFirstPosts = true, postsPerPage = 6, currentPage = 1, fixPostsLayout = function () {
        var numberOfPosts = $blogList.find('.post').length;
        $blogList.toggleClass('extra-post-after', ((numberOfPosts - 1) % 3 === 2));
    }, setLoadingState = function (state) {
        $blogList.toggleClass('loading', state);
    }, createPostDOM = function (post, index) {
        var $post = $postTemplate.clone().removeClass('post-template');
        if (loadFirstPosts && index === 0) {
            $post
                .addClass('first-post')
                .css('background-image', "url(".concat(post.featured_image_big_url, ")"))
                .find('.post-image').remove();
        }
        else {
            $post.find('a.post-image')
                .css('background-image', "url(".concat(post.featured_image_url, ")"))
                .attr('href', post.link);
        }
        $post.find('.post-title a')
            .attr('href', post.link)
            .text(stConvertHTMLEntitesToChar(post.title.rendered));
        $post.find('.post-date').text(stGetMonthName(post.month_number) + post.date_formatted);
        $post.find('.post-text').append(post.excerpt.rendered);
        $blogList.append($post);
    }, loadPosts = function (emptyBloglist) {
        if (emptyBloglist === void 0) { emptyBloglist = false; }
        var currentRestUrl = restBaseUrl, restParams = [], addPagingParams = function () {
            if (loadFirstPosts) {
                restParams.push('per_page=' + (+postsPerPage + 1));
            }
            else {
                restParams.push("per_page=".concat(postsPerPage));
                restParams.push("offset=".concat(1 + 6 * (currentPage - 1)));
            }
        };
        setLoadingState(true);
        addPagingParams();
        currentRestUrl += restParams.join('&');
        $.get(currentRestUrl, {}, function (postsData, _, request) {
            if (emptyBloglist) {
                $blogList.empty();
            }
            postsData.forEach(createPostDOM);
            setLoadingState(false);
            fixPostsLayout();
            $loadMoreWrapper.toggle($blogList.find('.post').length != +request.getResponseHeader('x-wp-total'));
        });
    }, loadMoreClicked = function () {
        currentPage++;
        loadFirstPosts = false;
        loadPosts();
    };
    $loadMoreButton.on('click', loadMoreClicked);
});
//# sourceMappingURL=blog.js.map