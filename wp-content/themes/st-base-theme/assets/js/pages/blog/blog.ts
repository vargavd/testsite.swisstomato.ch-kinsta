jQuery(function ($) {
  let
    // DOM
    $blogList = $('.blog-post-list'),
    $postTemplate = $('.post-template').detach(),
    $loadMoreWrapper = $('.load-more-wrapper'),
    $loadMoreButton = $loadMoreWrapper.find('a.load-more'),

    // misc
    restBaseUrl = `${swisstInfos.baseUrl}/wp-json/wp/v2/posts?`,
    loadFirstPosts = true,
    postsPerPage = 6,
    currentPage = 1,

    // helper funcs
    fixPostsLayout = function () {
      let
        // misc
        numberOfPosts = $blogList.find('.post').length;

      $blogList.toggleClass('extra-post-after', ((numberOfPosts - 1) % 3 === 2));
    },
    setLoadingState = function (state: boolean) {
      $blogList.toggleClass('loading', state)
    },

    // main func
    createPostDOM = (post: any, index: number) => {
      let
        // DOM
        $post = $postTemplate.clone().removeClass('post-template');

      if (loadFirstPosts && index === 0) {
        $post
          .addClass('first-post')
          .css('background-image', `url(${post.featured_image_big_url})`)
          .find('.post-image').remove();
      } else {
        $post.find('a.post-image')
          .css('background-image', `url(${post.featured_image_url})`)
          .attr('href', post.link);
      }

      // cl(post);

      // title
      $post.find('.post-title a')
        .attr('href', post.link)
        .text(stConvertHTMLEntitesToChar(post.title.rendered));

      // date
      $post.find('.post-date').text(stGetMonthName(post.month_number) + post.date_formatted);

      // text
      $post.find('.post-text').append(post.excerpt.rendered);

      $blogList.append($post);
    },
    loadPosts = (emptyBloglist: boolean = false) => {
      let
        // misc
        currentRestUrl = restBaseUrl,
        restParams: Array<string> = [],

        // helper funcs
        addPagingParams = () => {
          if (loadFirstPosts) {
            restParams.push('per_page=' + (+postsPerPage + 1));
          } else {
            restParams.push(`per_page=${postsPerPage}`);
            restParams.push(`offset=${1 + 6 * (currentPage - 1)}`);
          }
        };

      // loading state
      setLoadingState(true);

      // add params to rest url
      addPagingParams();
      currentRestUrl += restParams.join('&');

      // get posts
      $.get(currentRestUrl, {}, function (postsData: Array<any>, _, request: JQueryXHR) {
        if (emptyBloglist) {
          $blogList.empty();
        }

        postsData.forEach(createPostDOM);

        setLoadingState(false);

        fixPostsLayout();

        $loadMoreWrapper.toggle($blogList.find('.post').length != +request.getResponseHeader('x-wp-total')!);
      });
    },

    // events
    loadMoreClicked = () => {
      currentPage++;
      loadFirstPosts = false;

      loadPosts();
    };

  // events
  $loadMoreButton.on('click', loadMoreClicked);

  // demo: click on load more
  // $loadMoreButton.trigger('click');

  // setLoadingState(true);
});