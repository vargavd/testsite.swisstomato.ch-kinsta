'use strict';

jQuery(document).ready(function ($) {
  var
    // DOM
    $videoCover = $('.video-cover'),
    $videoPopupWrapper = $('#video-popup-wrapper'),
    $videoContent = $('.video-content'),

    // misc
    lightboxTopPos = (window.innerWidth > 450) ? 50 : 100,

    // events
    clickOnVideoCover = function () {
      var
        // misc
        width = _.min([$videoContent.width(), window.innerWidth - 20]),
        height = width * 0.5625;

      console.log($videoPopupWrapper, $videoCover);

      $videoPopupWrapper
        .html($videoContent.html())
        .css({
          'padding-top': (window.innerHeight - height) / 2 - 50,
        })
        .addClass('opened');
    },
    closeVideoPopup = function () {
      $videoPopupWrapper.html('').removeClass('opened');
    },
    
    init = function () {
      var
        initVideos = function () {
          $videoCover.click(clickOnVideoCover);
          $videoPopupWrapper.click(closeVideoPopup);
        },
        initLightbox = function () {
          lightbox.option({
            positionFromTop: lightboxTopPos,
            disableScrolling: true,
            albumLabel: '',
            resizeDuration: 400,
            alwaysShowNavOnTouchDevices: true
          });
        };

      initVideos();
      initLightbox();
    };

  init();
});
