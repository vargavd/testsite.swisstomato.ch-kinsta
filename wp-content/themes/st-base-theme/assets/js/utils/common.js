var cl = console.log;
var isTouchDevice = function () { return ('ontouchstart' in window) || (navigator.maxTouchPoints > 0); };
var stGetMonthName = function (monthNumber) {
    switch (swisstInfos.language) {
        case 'fr_FR':
            return ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'][monthNumber - 1];
        case 'de-DE':
            return ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'][monthNumber - 1];
        default:
            return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][monthNumber - 1];
    }
    ;
};
var stConvertHTMLEntitesToChar = function (text) {
    var tempSpan = document.createElement('span');
    tempSpan.innerHTML = text;
    var string = tempSpan.textContent || tempSpan.innerText;
    tempSpan = null;
    return string;
};
//# sourceMappingURL=common.js.map