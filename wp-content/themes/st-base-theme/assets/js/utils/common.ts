// public variables
declare var Swiper: any;
declare var stInfos: { baseUrl: string };

// TYPE DEFINITATIONS
type SwiperOption = {
  loop?: boolean,
  spaceBetween?: number
  effect?: string,
  allowTouchMove?: boolean,
  navigation?: boolean | {
    nextEl: null | string | HTMLElement,
    prevEl: null | string | HTMLElement
  },
  pagination?: boolean | {
    el: null | string | HTMLElementEventMap,
    clickable: boolean
  },
  autoplay?: boolean | {
    delay: number,
    disableOnInteraction: boolean
  },
  slidesPerView?: number | "auto"
}

// helper functions
const cl = console.log;
const isTouchDevice = () => ('ontouchstart' in window) || (navigator.maxTouchPoints > 0);
const stGetMonthName = (monthNumber:number) => {
  switch (swisstInfos.language) {
    case 'fr_FR':
      return ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'][monthNumber - 1];
    case 'de-DE': 
      return ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'][monthNumber - 1];

    default: // english
      return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][monthNumber - 1];
  };  
};
const stConvertHTMLEntitesToChar = (text:string) => {
  let tempSpan:HTMLSpanElement|null = document.createElement('span');
  tempSpan.innerHTML = text;

  let string = tempSpan.textContent || tempSpan.innerText;
  tempSpan = null;

  return string;
}
