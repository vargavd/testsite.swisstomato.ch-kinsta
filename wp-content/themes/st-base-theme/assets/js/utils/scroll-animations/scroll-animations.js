jQuery(function ($) {
    var $servicesBlock = $('.stbase-services-block'), $highlightedBigRedTextBlock = $('.stbase-big-red-text-block.highlighted'), $clientsBlockList = $('.stbase-clients-block .client-list'), $awardsBlockList = $('.stbase-awards-block'), $checkListBlockList = $('.stbase-check-list-block .check-list'), $factLinksBlockList = $('.stbase-fact-links-block .fact-links'), $zoomInPictureSections = $('.zoom-in-on-scroll'), $zoomOutPictureSections = $('.zoom-out-on-scroll'), $factAdvantagesBlockList = $('.stbase-fact-advantages-block .fact-advantages'), $factProcessTimingBlockList = $('.stbase-fact-process-timing-block .process-timing-steps'), $factTeamBlockList = $('.stbase-fact-team-block .team'), $factComplexityBlockList = $('.stbase-fact-complexity-block .complexity-items'), $renderBlocksWithJS = $('[data-var]');
    swisstScrollTriggerer.addSection($servicesBlock, 'scrolled-to', -170);
    swisstScrollTriggerer.addSection($highlightedBigRedTextBlock, 'seen-first', -50);
    swisstScrollTriggerer.addSection($highlightedBigRedTextBlock, 'seen-second', 100);
    swisstScrollTriggerer.addSection($clientsBlockList, 'scrolled-to', -300);
    swisstScrollTriggerer.addSection($awardsBlockList, 'scrolled-to', -100);
    swisstScrollTriggerer.addSection($checkListBlockList, 'scrolled-to', -100);
    swisstScrollTriggerer.addSection($factLinksBlockList, 'scrolled-to', -100);
    swisstScrollTriggerer.addSection($factAdvantagesBlockList, 'scrolled-to', -100);
    $zoomInPictureSections.each(function (_i, elem) {
        swisstScrollTriggerer.addSection($(elem), 'scrolled-to');
    });
    $zoomOutPictureSections.each(function (_i, elem) {
        swisstScrollTriggerer.addSection($(elem), 'scrolled-to');
    });
    swisstScrollTriggerer.addSection($factProcessTimingBlockList, 'scrolled-to', -200);
    swisstScrollTriggerer.addSection($factTeamBlockList, 'scrolled-to', -200);
    swisstScrollTriggerer.addSection($factComplexityBlockList, 'scrolled-to', -200);
    $renderBlocksWithJS.each(function (_i, elem) {
        var contentKeyInWindow = $(elem).data('var');
        swisstScrollTriggerer.addSection($(elem), function () {
            $(elem).html(window[contentKeyInWindow]);
        }, 0, null, false, true);
    });
});
//# sourceMappingURL=scroll-animations.js.map