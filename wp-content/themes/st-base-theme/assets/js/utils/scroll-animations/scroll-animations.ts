/**
 * THIS FILE CONTAINS ALL THE ANIMATIONS THAT ARE TRIGGERED BY SCROLL
 */

jQuery(function ($) {
  let
    // DOM blocks
    $servicesBlock = $('.stbase-services-block'),
    $highlightedBigRedTextBlock = $('.stbase-big-red-text-block.highlighted'),
    $clientsBlockList = $('.stbase-clients-block .client-list'),
    $awardsBlockList = $('.stbase-awards-block'),
    $checkListBlockList = $('.stbase-check-list-block .check-list'),
    $factLinksBlockList = $('.stbase-fact-links-block .fact-links'),
    $zoomInPictureSections = $('.zoom-in-on-scroll'),
    $zoomOutPictureSections = $('.zoom-out-on-scroll'),
    $factAdvantagesBlockList = $('.stbase-fact-advantages-block .fact-advantages'),
    $factProcessTimingBlockList = $('.stbase-fact-process-timing-block .process-timing-steps'),
    $factTeamBlockList = $('.stbase-fact-team-block .team'),
    $factComplexityBlockList = $('.stbase-fact-complexity-block .complexity-items'),
    $renderBlocksWithJS = $('[data-var]');

  // SERVICES BLOCK
  swisstScrollTriggerer.addSection($servicesBlock, 'scrolled-to', -170);

  // // HIGHLIGHTED BIG RED TEXT BLOCK
  swisstScrollTriggerer.addSection($highlightedBigRedTextBlock, 'seen-first', -50);
  swisstScrollTriggerer.addSection($highlightedBigRedTextBlock, 'seen-second', 100);

  // // CLIENTS BLOCK
  swisstScrollTriggerer.addSection($clientsBlockList, 'scrolled-to', -300);

  // AWARDS LIST
  swisstScrollTriggerer.addSection($awardsBlockList, 'scrolled-to', -100);

  // CHECK LIST
  swisstScrollTriggerer.addSection($checkListBlockList, 'scrolled-to', -100);

  // FACT LINKS
  swisstScrollTriggerer.addSection($factLinksBlockList, 'scrolled-to', -100);

  // FACT LINKS
  swisstScrollTriggerer.addSection($factAdvantagesBlockList, 'scrolled-to', -100);

  // ZOOM IN SECTIONS
  $zoomInPictureSections.each((_i: number, elem: HTMLElement) => {
    swisstScrollTriggerer.addSection($(elem), 'scrolled-to');
  });

  // ZOOM OUT SECTIONS
  $zoomOutPictureSections.each((_i: number, elem: HTMLElement) => {
    swisstScrollTriggerer.addSection($(elem), 'scrolled-to');
  });

  // FACT PROCESS STEPS
  swisstScrollTriggerer.addSection($factProcessTimingBlockList, 'scrolled-to', -200);

  // FACT TEAM LIST
  swisstScrollTriggerer.addSection($factTeamBlockList, 'scrolled-to', -200);

  // FACT COMPLEXITY LIST
  swisstScrollTriggerer.addSection($factComplexityBlockList, 'scrolled-to', -200);

  // RENDER BLOCKS WITH JS
  $renderBlocksWithJS.each((_i: number, elem: HTMLElement) => {
    let contentKeyInWindow: string = $(elem).data('var');

    swisstScrollTriggerer.addSection($(elem), () => {
      $(elem).html(window[contentKeyInWindow]);
    }, 0, null, false, true);
  });

  // console.log('Animations set done');
});