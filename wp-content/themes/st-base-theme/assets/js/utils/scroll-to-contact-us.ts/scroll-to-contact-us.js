jQuery(function ($) {
    var $htmlBody = $('html,body'), $mainHeader = $('#masthead'), $contactUsLinks = $('a[href="#contact-us"]'), $contactUsSection = $('#contact-us'), clickOnContactUsLinks = function () {
        $htmlBody.animate({
            scrollTop: $contactUsSection.first().position().top - $mainHeader.height(),
        }, 700, 'swing');
    };
    $contactUsLinks.on('click', clickOnContactUsLinks);
});
//# sourceMappingURL=scroll-to-contact-us.js.map