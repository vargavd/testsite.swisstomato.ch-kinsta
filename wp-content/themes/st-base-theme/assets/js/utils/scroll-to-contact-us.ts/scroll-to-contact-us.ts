/// <reference path="../common.ts" />

jQuery(function ($) {
  let
    // DOM
    $htmlBody = $('html,body'),
    $mainHeader = $('#masthead')!,
    $contactUsLinks = $('a[href="#contact-us"]'),
    $contactUsSection = $('#contact-us'),

    // events
    clickOnContactUsLinks = function (this: any) {
      $htmlBody.animate({
        scrollTop: $contactUsSection.first().position().top - $mainHeader.height()!,
      }, 700, 'swing');
    };

  $contactUsLinks.on('click', clickOnContactUsLinks);
});