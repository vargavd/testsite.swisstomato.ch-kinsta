jQuery(function ($) {
    var $htmlBody = $('html,body'), $header = $('#masthead'), $contactUsLinks = $('a[href="#contact-us"]'), $contactUsSection = $('#contact-us'), $footerWrapper = $('#footer-wrapper'), clickOnContactUsLinks = function () {
        $htmlBody.animate({
            scrollTop: $htmlBody.innerHeight() - $header.innerHeight() - $contactUsSection.innerHeight() - $footerWrapper.innerHeight(),
        }, 700, 'swing');
        cl($htmlBody.innerHeight(), $contactUsSection.innerHeight(), $footerWrapper.innerHeight());
    };
    $contactUsLinks.on('click', clickOnContactUsLinks);
});
//# sourceMappingURL=scroll-to-contact-us.js.map