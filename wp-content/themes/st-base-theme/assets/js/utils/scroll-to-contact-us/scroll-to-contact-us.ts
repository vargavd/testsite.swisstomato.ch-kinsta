/// <reference path="../common.ts" />

jQuery(function ($) {
  let
    // DOM
    $htmlBody = $('html,body'),
    $header = $('#masthead'),
    $contactUsLinks = $('a[href="#contact-us"]'),
    $contactUsSection = $('#contact-us'),
    $footerWrapper = $('#footer-wrapper'),

    // events
    clickOnContactUsLinks = function (this: any) {
      $htmlBody.animate({
        scrollTop: $htmlBody.innerHeight() - $header.innerHeight() - $contactUsSection.innerHeight() - $footerWrapper.innerHeight(),
      }, 700, 'swing');

      cl($htmlBody.innerHeight(), $contactUsSection.innerHeight(), $footerWrapper.innerHeight());
    };

  $contactUsLinks.on('click', clickOnContactUsLinks);
});