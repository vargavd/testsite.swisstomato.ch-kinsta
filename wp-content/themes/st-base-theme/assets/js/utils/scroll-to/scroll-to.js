jQuery(function ($) {
    var $htmlBody = $('html,body'), $mainHeader = $('#masthead'), $scrollToLinks = $('[data-scroll-to]'), clickOnScrollToLink = function () {
        var $link = $(this), $target, hashLink = $link.attr('data-scroll-to');
        hashLink = hashLink.indexOf('#') === -1 ? "#".concat(hashLink) : hashLink;
        $target = $(hashLink);
        $htmlBody.animate({
            scrollTop: $target.position().top - $mainHeader.height(),
        }, 700, 'swing');
    };
    $scrollToLinks.on('click', clickOnScrollToLink);
});
//# sourceMappingURL=scroll-to.js.map