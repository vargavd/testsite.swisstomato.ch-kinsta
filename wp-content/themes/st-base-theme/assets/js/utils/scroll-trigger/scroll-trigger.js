var swisstScrollTriggerer = (function ($) {
    var $window = $(window), sections = [], viewportHeight = $(window).height(), calculateTriggerPoint = function ($section, offset, startOfSection) {
        var elementHeight = $section.height(), elementTop = $section.position() && $section.position().top, triggerPointTop = elementTop + offset;
        if (!startOfSection) {
            triggerPointTop += 0.66 * elementHeight;
        }
        return triggerPointTop;
    }, browserScrolled = function () {
        var browserTopScroll = $window.scrollTop(), browserBottom = browserTopScroll + viewportHeight;
        sections.forEach(function (section) {
            if (section.triggered) {
                return;
            }
            if (browserBottom > section.triggerPointTop) {
                section.triggered = true;
                if (typeof section.callbackOrClassName === 'string') {
                    if (section.$target) {
                        section.$target.addClass(section.callbackOrClassName);
                    }
                    else {
                        section.$section.addClass(section.callbackOrClassName);
                    }
                }
                else {
                    section.callbackOrClassName();
                }
            }
        });
    }, addSection = function ($section, callbackOrClassName, offset, $target, startOfSection, mobile) {
        var _a;
        if (offset === void 0) { offset = 0; }
        if ($target === void 0) { $target = null; }
        if (startOfSection === void 0) { startOfSection = false; }
        if (mobile === void 0) { mobile = false; }
        var triggerPointTop = 0, minWidthForAnim = (_a = swisstInfos.minWidthForAnimations) !== null && _a !== void 0 ? _a : 768, animationId = (new Date()).getTime(), removeThisAnimation = function () {
            sections.splice(sections.indexOf(sections.find(function (s) { return s.id === animationId; })), 1);
        };
        if (!mobile && window.innerWidth < minWidthForAnim) {
            if ($target) {
                $target.addClass('no-scroll');
            }
            else {
                $section.addClass('no-scroll');
            }
            return function () { };
        }
        triggerPointTop = calculateTriggerPoint($section, offset, startOfSection);
        sections.push({
            id: animationId,
            $target: $target,
            $section: $section,
            triggerPointTop: triggerPointTop,
            callbackOrClassName: callbackOrClassName,
            triggered: false,
            offset: offset,
            startOfSection: startOfSection
        });
        browserScrolled();
        return removeThisAnimation;
    }, recalculateTriggerPoints = function () {
        sections.forEach(function (section) {
            var $section = section.$section, offset = section.offset, startOfSection = section.startOfSection;
            section.triggerPointTop = calculateTriggerPoint($section, offset, startOfSection);
        });
    };
    $window.on('scroll', browserScrolled);
    return { addSection: addSection, recalculateTriggerPoints: recalculateTriggerPoints };
}(jQuery));
var swisstSplitTextAnimation = function (element, delay) {
    if (delay === void 0) { delay = 0.02; }
    var textInside = element.innerText, words = textInside.split(" "), wordsLength = words.length, newElements = [];
    element.style.opacity = '1';
    element.innerHTML = "";
    var animatedCharCounter = 0;
    for (var i = 0; i < wordsLength; i++) {
        animatedCharCounter++;
        var newElement = document.createElement("span"), wordText = words[i], characters = [];
        var whiteSpace = void 0;
        for (var j = 0; j < words[i].length; j++) {
            animatedCharCounter++;
            characters[j] = document.createElement("span");
            characters[j].innerHTML = words[i][j];
            characters[j].classList.add("split__char");
            characters[j].setAttribute("aria-hidden", "true");
            characters[j].style.animationDelay = delay * animatedCharCounter + "s";
            newElement.appendChild(characters[j]);
        }
        newElement.classList.add("split__word");
        newElement.setAttribute("aria-label", wordText);
        newElements.push(newElement);
        if (i < wordsLength - 1) {
            whiteSpace = document.createElement("span");
            whiteSpace.innerHTML = " ";
            whiteSpace.classList.add("split__whitespace");
            newElements.push(whiteSpace);
        }
    }
    newElements.forEach(function (arrElement) {
        element.appendChild(arrElement);
    });
};
//# sourceMappingURL=scroll-trigger.js.map