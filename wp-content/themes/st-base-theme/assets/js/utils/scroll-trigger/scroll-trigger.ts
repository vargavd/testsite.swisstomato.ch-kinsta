/// <reference path="../../_common.d.ts" />

type SectionType = {
  id: number,
  $target: JQuery<HTMLElement> | null,
  $section: JQuery<HTMLElement>,
  triggerPointTop: number,
  callbackOrClassName: Function | string,
  triggered: boolean,
  offset: number,
  startOfSection: boolean
};

const swisstScrollTriggerer = (function ($) {
  let
    // DOM
    $window = $(window),

    // misc
    sections: SectionType[] = [],
    viewportHeight = $(window).height()!,

    // helper funcs
    calculateTriggerPoint = (
      $section: JQuery<HTMLElement>,
      offset: number,
      startOfSection: boolean
    ) => {
      let
        // misc
        elementHeight = $section.height()!,
        elementTop = $section.position() && $section.position().top,
        triggerPointTop = elementTop + offset;

      if (!startOfSection) {
        triggerPointTop += 0.66 * elementHeight;
      }

      return triggerPointTop;
    },

    // events
    browserScrolled = () => {
      let
        browserTopScroll = $window.scrollTop()!,
        browserBottom = browserTopScroll + viewportHeight;

      sections.forEach((section: SectionType) => {
        if (section.triggered) {
          return;
        }

        if (browserBottom > section.triggerPointTop) {
          section.triggered = true;

          if (typeof section.callbackOrClassName === 'string') {
            if (section.$target) {
              section.$target.addClass(section.callbackOrClassName);
            } else {
              section.$section.addClass(section.callbackOrClassName);
            }
          } else {
            section.callbackOrClassName();
          }
        }
      });
    },

    // main functions
    addSection = (
      $section: JQuery<HTMLElement>,
      callbackOrClassName: any,
      offset: number = 0,
      $target: JQuery<HTMLElement> | null = null,
      startOfSection: boolean = false,
      mobile: boolean = false
    ) => {
      let
        // misc
        triggerPointTop = 0,
        minWidthForAnim = swisstInfos.minWidthForAnimations ?? 768,
        animationId = (new Date()).getTime(),

        removeThisAnimation = () => {
          sections.splice(sections.indexOf(sections.find(s => s.id === animationId)!), 1);
        };

      if (!mobile && window.innerWidth < minWidthForAnim) {
        if ($target) {
          $target.addClass('no-scroll');
        } else {
          $section.addClass('no-scroll');
        }

        return () => { };
      }

      triggerPointTop = calculateTriggerPoint($section, offset, startOfSection);

      sections.push({
        id: animationId,
        $target: $target,
        $section: $section,
        triggerPointTop,
        callbackOrClassName,
        triggered: false,
        offset, startOfSection
      });

      // console.log(sections);

      browserScrolled();

      // return a functions which removes that section from animation
      return removeThisAnimation;
    },
    recalculateTriggerPoints = () => {
      sections.forEach(section => {
        const { $section, offset, startOfSection } = section;

        section.triggerPointTop = calculateTriggerPoint($section, offset, startOfSection);
      });
    };

  $window.on('scroll', browserScrolled);

  return { addSection, recalculateTriggerPoints };
}(jQuery));


const swisstSplitTextAnimation = (element: HTMLElement, delay = 0.02) => {
  const
    textInside = element.innerText,
    words = textInside.split(" "),
    wordsLength = words.length,
    newElements = [];

  element.style.opacity = '1';

  element.innerHTML = ""; // delete inital text
  let animatedCharCounter = 0;
  for (let i = 0; i < wordsLength; i++) {
    animatedCharCounter++;

    const newElement = document.createElement("span"),
      wordText = words[i],
      characters = [];
    let whiteSpace;

    for (let j = 0; j < words[i].length; j++) {
      animatedCharCounter++;
      characters[j] = document.createElement("span");
      characters[j].innerHTML = words[i][j];
      characters[j].classList.add("split__char");

      // Make it invisible for screen-readers.
      characters[j].setAttribute("aria-hidden", "true");

      // set animation delays
      characters[j].style.animationDelay = delay * animatedCharCounter + "s";

      newElement.appendChild(characters[j]);
    }

    newElement.classList.add("split__word");

    // Word for screenreaders
    newElement.setAttribute("aria-label", wordText);
    newElements.push(newElement);

    if (i < wordsLength - 1) {
      whiteSpace = document.createElement("span");
      whiteSpace.innerHTML = " ";
      whiteSpace.classList.add("split__whitespace");
      newElements.push(whiteSpace);
    }
  }

  newElements.forEach(function (arrElement) {
    element.appendChild(arrElement);
  });
}
