jQuery(function ($) {
    var $showMoreTextItems = $('.show-more-text-item'), $showMoreBeforeAfterTextItems = $('.show-more-before-after-text-item'), clickOnShowMoreLink = function () {
        var $link = $(this), $showMoreTextItem = $link.closest('.show-more-text-item'), $showMoreText = $showMoreTextItem.find('.show-more-text'), fullHeight = $showMoreText.attr('data-full-height'), closedHeight = $showMoreText.attr('data-closed-height');
        if ($showMoreText.hasClass('opened')) {
            $showMoreText
                .removeClass('opened')
                .animate({ 'height': closedHeight });
        }
        else {
            $showMoreText
                .addClass('opened')
                .animate({ 'height': fullHeight });
        }
        setTimeout(function () {
            swisstScrollTriggerer.recalculateTriggerPoints();
        }, 400);
    }, clickOnShowSecondTextLink = function () {
        var $link = $(this), $item = $link.closest('.show-more-before-after-text-item'), $secondText = $item.find('.show-more-second-text'), secondTextHeight = $secondText.attr('data-height');
        $secondText.animate({ height: ($secondText.height() == 0) ? secondTextHeight : 0 });
        setTimeout(function () {
            swisstScrollTriggerer.recalculateTriggerPoints();
        }, 400);
    }, setUpShowMoreTextItems = function (_, elem) {
        var $showMoreTextItem = $(elem), $showMoreText = $showMoreTextItem.find('.show-more-text'), $showMoreLink = $showMoreTextItem.find('.show-more-link'), height = $showMoreText.height(), lineHeightPx = getComputedStyle($showMoreText[0]).lineHeight, visibleRowsInClosed = $showMoreText.attr('data-rows-in-collapsed') ? +$showMoreText.attr('data-rows-in-collapsed') : 3, closedHeight = (+lineHeightPx.replace('px', '')) * visibleRowsInClosed;
        if (height < closedHeight) {
            $showMoreLink.remove();
            return;
        }
        $showMoreText
            .attr({ 'data-full-height': height, 'data-closed-height': closedHeight })
            .animate({ 'height': closedHeight });
        $showMoreLink.on('click', clickOnShowMoreLink);
    }, setUpShowMoreBeforeAfterTextItems = function (_, elem) {
        var $beforeAfterTextItem = $(elem), $showMoreLink = $beforeAfterTextItem.find('.show-more-link'), $secondText = $beforeAfterTextItem.find('.show-more-second-text'), secondTextHeight = $secondText.height();
        ;
        $secondText
            .attr('data-height', secondTextHeight)
            .animate({ height: 0 });
        $showMoreLink.on('click', clickOnShowSecondTextLink);
    };
    $showMoreTextItems.each(setUpShowMoreTextItems);
    $showMoreBeforeAfterTextItems.each(setUpShowMoreBeforeAfterTextItems);
    setTimeout(function () {
        swisstScrollTriggerer.recalculateTriggerPoints();
    }, 400);
});
//# sourceMappingURL=show-more-texts.js.map