jQuery(function ($) {
  let
      // DOM
      $showMoreTextItems = $('.show-more-text-item'),
      $showMoreBeforeAfterTextItems = $('.show-more-before-after-text-item'),
      
      // funcs
      clickOnShowMoreLink = function () {
        let
          // DOM
          $link             = $(this),
          $showMoreTextItem = $link.closest('.show-more-text-item'),
          $showMoreText     = $showMoreTextItem.find('.show-more-text'),
          
          // misc
          fullHeight   = $showMoreText.attr('data-full-height'),
          closedHeight = $showMoreText.attr('data-closed-height');

        if ($showMoreText.hasClass('opened')) {
          $showMoreText
            .removeClass('opened')
            .animate({ 'height': closedHeight });
        } else {
          $showMoreText
            .addClass('opened')
            .animate({'height': fullHeight});
        }

        setTimeout(() => {
          swisstScrollTriggerer.recalculateTriggerPoints();
        }, 400);
      },
      clickOnShowSecondTextLink = function () {
        let
          // DOM
          $link = $(this),
          $item = $link.closest('.show-more-before-after-text-item'),
          $secondText = $item.find('.show-more-second-text'),

          // misc
          secondTextHeight = $secondText.attr('data-height');

        $secondText.animate({ height: ($secondText.height() == 0) ? secondTextHeight : 0 });

        setTimeout(() => {
          swisstScrollTriggerer.recalculateTriggerPoints();
        }, 400);
      },

      // main funcs
      setUpShowMoreTextItems = function (_, elem) {
        let
          // DOM
          $showMoreTextItem = $(elem),
          $showMoreText = $showMoreTextItem.find('.show-more-text'),
          $showMoreLink = $showMoreTextItem.find('.show-more-link'),

          // misc
          height              = $showMoreText.height(),
          lineHeightPx        = getComputedStyle($showMoreText[0]).lineHeight,
          visibleRowsInClosed = $showMoreText.attr('data-rows-in-collapsed') ? +$showMoreText.attr('data-rows-in-collapsed') : 3,
          closedHeight        = (+lineHeightPx.replace('px', ''))*visibleRowsInClosed;

        if (height < closedHeight) {
          $showMoreLink.remove();
          return;
        }

        $showMoreText
          .attr({ 'data-full-height': height, 'data-closed-height': closedHeight })
          .animate({'height': closedHeight});

        $showMoreLink.on('click', clickOnShowMoreLink);
      },
      setUpShowMoreBeforeAfterTextItems = function (_ , elem) {
        let
          // DOM
          $beforeAfterTextItem = $(elem),
          $showMoreLink = $beforeAfterTextItem.find('.show-more-link'),
          $secondText = $beforeAfterTextItem.find('.show-more-second-text'),
          
          // misc
          secondTextHeight = $secondText.height();;

        $secondText
          .attr('data-height', secondTextHeight)
          .animate({ height: 0 });

        $showMoreLink.on('click', clickOnShowSecondTextLink);
      };

    $showMoreTextItems.each(setUpShowMoreTextItems);
    $showMoreBeforeAfterTextItems.each(setUpShowMoreBeforeAfterTextItems);

    setTimeout(() => {
      swisstScrollTriggerer.recalculateTriggerPoints();
    }, 400);
});
