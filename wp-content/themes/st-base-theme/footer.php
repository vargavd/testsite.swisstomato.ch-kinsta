<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ST_Base_Theme
 */

$current_lang = get_bloginfo('language');

?>

<div id="video-popup-wrapper">
  <div id="video-popup">
  </div>
</div>

<?php contact_us_section_func(true) ?>

<div id="footer-wrapper">
  <footer id="colophon" class="site-footer container <?=get_bloginfo('language')?>">
    <div id="footer-logo-wrapper">
      <?=get_img(get_field('footer_logo', 'option'), 'Footer Logo')?>
    </div>
    <div id="footer-pivacy-policy-wrapper">
      <a href="<?=get_privacy_policy_url()?>" id="footer-privacy-policy-link">
        <?=get_field('footer_privacy_policy_text', 'option')?>
      </a>
    </div>
      <div id="footer-text">
        <?=get_field('footer_text', 'option')?>
      </div>
      <div id="footer-social-links">
        <a target="_blank" rel="noopener" href="<?=get_field('facebook_link', 'option')?>">
          <i class="fab fa-facebook"></i>
        </a>
        <!-- <a target="_blank" href="<?=get_field('twitter_link', 'option')?>">
          <i class="fab fa-twitter"></i>
        </a> -->
        <a target="_blank" rel="noopener" href="<?=get_field('linkedin_link', 'option')?>">
          <i class="fab fa-linkedin"></i>
        </a>
      </div>
  </footer><!-- footer#colophon -->
</div>
</div><!-- #page -->

<?php include "global-templates/_cookies.php"; ?>

<?php wp_footer(); ?>

<script>
  jQuery(document).ready(function ($) {

    $('a[href="#contact-us"], #contact-popup-trigger').click(function () {
      console.log('Event:', 'CONTACT US CLICKED');
    });

    $('#contact-us .wpcf7').on('wpcf7mailsent', function () {
      console.log('Event:', 'ELMENT A FORM!!!');

      dataLayer.push({
        event: "SWISSTOMATO_FORM_SUBMIT",
        category: 'Send',
        label: 'Successfull'
      });
    });

    $('#contact-us a[href*="tel:"], #colophon a[href*="tel:"]').click(function () {
      console.log('Event:', 'PHONE CLICKED');
    });

    $('#contact-us a[href*="mailto:"], #colophon a[href*="mailto:"]').click(function () {
      console.log('Event:', 'EMAIL CLICKED');
    });

  });
</script>

</body>
</html>
