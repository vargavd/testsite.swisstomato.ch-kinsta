<?php

/**
 * ST Base Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ST_Base_Theme
 */

include "inc/helper.php";
include "inc/enqueue.php";
include "inc/acf-gutenberg.php";
include "inc/setup.php";
include "inc/image-sizes.php";
include "inc/reference-content-type.php";
include "inc/redirects.php";
include "inc/reference-reorder-endpoint.php";

// global templates funcs
include "global-templates/_hero-function.php";
include "global-templates/_masonry-references-section-func.php";
include "global-templates/_contact-us-section-func.php";


// disable feed links https://kinsta.com/knowledgebase/wordpress-disable-rss-feed/#disable-rss-feed-code
function stbase_disable_feed()
{
  wp_die(__('No feed available, please visit the <a href="' . esc_url(home_url('/')) . '">homepage</a>!'));
}
add_action('do_feed', 'stbase_disable_feed', 1);
add_action('do_feed_rdf', 'stbase_disable_feed', 1);
add_action('do_feed_rss', 'stbase_disable_feed', 1);
add_action('do_feed_rss2', 'stbase_disable_feed', 1);
add_action('do_feed_atom', 'stbase_disable_feed', 1);
add_action('do_feed_rss2_comments', 'stbase_disable_feed', 1);
add_action('do_feed_atom_comments', 'stbase_disable_feed', 1);

// remove feed links from header
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);




// disable caching sitemap (https://rankmath.com/kb/fix-sitemap-issues/)
add_filter('rank_math/sitemap/enable_caching', '__return_false');


// allow unfiltered uploads temporary (specifically for .ico)
// define('ALLOW_UNFILTERED_UPLOADS', true);


// dont store ip addresses in contact form 7
add_filter('wpcf7_remote_ip_addr', 'wpcf7_anonymize_ip_addr');


// removing rest api header link due to a w3c error (https://wordpress.stackexchange.com/questions/211817/how-to-remove-rest-api-link-in-http-headers)
remove_action('template_redirect', 'rest_output_link_header', 11);


// references order by reference_order on archive
add_action('pre_get_posts', function ($query) {
  if (
    is_admin() ||
    !isset($query->query['post_type']) ||
    $query->query['post_type'] !== 'reference' ||
    (!is_front_page() && !is_post_type_archive('reference'))
  ) {
    return $query;
  }

  // ORDERING
  $query->set('orderby', 'meta_value_num');
  $query->set('meta_key', 'no_category_order');
  $query->set('order', 'ASC');

  // ONLY REFERENCES WHERE MASONRY IMAGE IS FILLED AND NOT HIDDEN IN ARCHIVE
  $query->set('meta_query', array(
    'relation' => 'AND',
    array(
      'key' => 'masonry_thumbnail_image',
      'compare' => 'NOT IN',
      'value' => ''
    ),
    array(
      'relation' => 'OR',
      array(
        'key' => 'hide_in_archive',
        'compare' => '!=',
        'value' => '1'
      ),
      array(
        'key' => 'hide_in_archive',
        'compare' => 'NOT EXISTS',
      ),
    ),
  ));
}, 1);

// referemces order by reference order in rest queries
add_filter('rest_reference_query', function ($query_vars) {
  // ORDERING
  $query_vars["orderby"] = "meta_value_num";
  $query_vars["order"] = "ASC";
  $query_vars["meta_key"] = "no_category_order";

  if ($_GET['reference_category'] === '20' || $_GET['reference_category'] === '21' || $_GET['reference_category'] === '22') {
    $query_vars["meta_key"] = "app_category_order";
  }

  if ($_GET['reference_category'] === '23' || $_GET['reference_category'] === '24' || $_GET['reference_category'] === '25') {
    $query_vars["meta_key"] = "arvr_category_order";
  }

  if ($_GET['reference_category'] === '17' || $_GET['reference_category'] === '18' || $_GET['reference_category'] === '19') {
    $query_vars["meta_key"] = "web_category_order";
  }

  // ONLY REFERENCES WHERE MASONRY IMAGE IS FILLED AND NOT HIDDEN IN ARCHIVE
  $query_vars['meta_query'] = array(
    'relation' => 'AND',
    array(
      'key' => 'masonry_thumbnail_image',
      'compare' => 'NOT IN',
      'value' => ''
    ),
    array(
      'relation' => 'OR',
      array(
        'key' => 'hide_in_archive',
        'compare' => '!=',
        'value' => '1'
      ),
      array(
        'key' => 'hide_in_archive',
        'compare' => 'NOT EXISTS',
      ),
    ),
  );

  return $query_vars;
}, 10, 2);




// Signs to mobile menu
add_filter('wp_nav_menu_items', function ($items, $args) {
  if ($args->theme_location == 'primary-menu') {
    $items = str_replace('</a', '<i class="fas fa-plus"></i><i class="fas fa-minus"></i></a', $items);
  }
  return $items;
}, 10, 2);


// post archive params
add_action('pre_get_posts', function ($query) {
  if (is_admin() || !$query->is_main_query()) {
    return;
  }

  if (is_home()) {
    $query->set('posts_per_page', 7);
  }
}, 1);



// add fields to rest
add_filter('rest_prepare_post', function ($data) {
  // ADD featured image url to restapi
  $featured_image_id = $data->data['featured_media'];
  $featured_image_big_url = wp_get_attachment_image_src($featured_image_id, 'post-thumbnail-1300x565');
  $featured_image_url = wp_get_attachment_image_src($featured_image_id, 'post-thumbnail-395x240');

  if ($featured_image_big_url) {
    $data->data['featured_image_big_url'] = $featured_image_big_url[0];
  } else {
    $data->data['featured_image_big_url'] = get_stylesheet_directory_uri() . '/imgs/sample-hero.jpg';
  }

  if ($featured_image_url) {
    $data->data['featured_image_url'] = $featured_image_url[0];
  } else {
    $data->data['featured_image_url'] = get_stylesheet_directory_uri() . '/imgs/sample-hero.jpg';
  }


  $data->data['date_formatted'] = date(' d, o', strtotime($data->data['date']));
  $data->data['month_number'] = date('n', strtotime($data->data['date']));

  return $data;
});



if (!function_exists('stbase_setup')) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function stbase_setup()
  {
    /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
    add_theme_support('title-tag');

    /*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(
      array(
        'primary-menu' => esc_html__('Primary', 'stbase'),
        'secondary-menu' => esc_html__('Secondary', 'stbase')
      )
    );

    /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
    add_theme_support(
      'html5',
      array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'style',
        'script',
      )
    );

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support(
      'custom-logo',
      array(
        'height'      => 250,
        'width'       => 250,
        'flex-width'  => true,
        'flex-height' => true,
      )
    );
  }
endif;
add_action('after_setup_theme', 'stbase_setup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function stbase_widgets_init()
{
  register_sidebar(
    array(
      'name'          => esc_html__('Sidebar', 'stbase'),
      'id'            => 'sidebar-1',
      'description'   => esc_html__('Add widgets here.', 'stbase'),
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    )
  );
}
add_action('widgets_init', 'stbase_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function stbase_scripts()
{
  stbase_enqueue_style_with_filetime('stbase-style', '/style.css', NULL);

  // dequeue gutenberg block library css for custom templates
  if (strpos(get_page_template(), 'page.php') === false && strpos(get_page_template(), 'single.php') === false) {
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
  }

  // font awesome
  // wp_enqueue_style("font-awesome", "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css", NULL);
  // stbase_enqueue_style_with_filetime("font-awesome", "/assets/fonts/fontawesome-pro-5.15.4-web/css/all.min.css", NULL);

  // OpenSans font from google
  wp_enqueue_style("google-open-sans", "https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap", NULL);

  // LODASH
  wp_register_script('st-lodash', 'https://cdn.jsdelivr.net/npm/lodash@4.17.19/lodash.min.js', NULL);

  // AOS animation library
  // wp_enqueue_style("aos-style", "https://unpkg.com/aos@2.3.1/dist/aos.css", NULL);
  // wp_enqueue_script('aos-script', 'https://unpkg.com/aos@2.3.1/dist/aos.js', NULL);

  // Swiper
  // stbase_enqueue_style_with_filetime('st-swiper-style', '/assets/js/plugins/swiper/swiper-bundle.min.css', NULL);
  // stbase_enqueue_script_with_filetime('st-swiper-script', '/assets/js/plugins/swiper/swiper-bundle.min.js', NULL);

  // common, helper JS 
  stbase_enqueue_script_with_filetime('st-common-script', '/assets/js/utils/common.js', NULL);

  // lottie js
  // stbase_enqueue_script_with_filetime('st-lottie-player-js', '/assets/js/plugins/lottie-player/lottie-player.js', array('jquery'));


  // MAIN JS
  stbase_enqueue_script_with_filetime('st-main-script', '/assets/js/main.js', array('jquery', 'jquery-ui-tabs'));
  wp_localize_script('st-main-script', 'swisstInfos', array(
    'baseUrl' => get_home_url(),
    'privacyPolicyUrl'   => get_privacy_policy_url(),
    'language'        => get_locale(),
    'no_references_text' => __('There are no exact matches for your search criteria but check out these project.', 'swisst2'),
    'minWidthForAnimations' => 768,
    'reference_archive_link' => get_post_type_archive_link('reference')
  ));

  // utils
  stbase_enqueue_script_with_filetime('st-scroll-to-script', '/assets/js/utils/scroll-to/scroll-to.js', NULL);
  stbase_enqueue_script_with_filetime('st-scroll-to-contact-us', '/assets/js/utils/scroll-to-contact-us/scroll-to-contact-us.js', NULL);
  stbase_enqueue_script_with_filetime('st-scroll-trigger-script', '/assets/js/utils/scroll-trigger/scroll-trigger.js', NULL);
  stbase_enqueue_script_with_filetime('st-show-more-texts-script', '/assets/js/utils/show-more-texts/show-more-texts.js', array('st-scroll-trigger-script'));

  // masonry list
  stbase_enqueue_script_with_filetime('st-reference-masonry-list', '/assets/js/global-templates/reference-list-masonry/reference-list-masonry-section.js', array('jquery', 'st-scroll-trigger-script'));

  // animations
  stbase_enqueue_script_with_filetime('st-scroll-animations-script', '/assets/js/utils/scroll-animations/scroll-animations.js', array('st-scroll-trigger-script'));

  // --- BLOG PAGE ---
  if (is_home()) {
    stbase_enqueue_script_with_filetime('st-blog-page', '/assets/js/pages/blog/blog.js', array('jquery'));
    stbase_enqueue_style_with_filetime('st-blog-page', '/assets/css/pages/blog.css', NULL);
  }

  // --- BLOGPOST (plus styles for the special blogposts) ---
  if (is_singular('post')) {
    stbase_enqueue_style_with_filetime('st-blogpost', '/assets/css/pages/blogpost.css', NULL);
    stbase_enqueue_style_with_filetime('st-blogpost-app-cost', '/assets/css/pages/blogpost-app-cost.css', NULL);
    stbase_enqueue_style_with_filetime('st-blogpost-web-cost', '/assets/css/pages/blogpost-web-cost.css', NULL);
  }

  // --- REFERENCES ARCHIVE ---
  if (is_post_type_archive('reference')) {
    // wp_enqueue_script('st-reference-masonry-list');
    stbase_enqueue_style_with_filetime('st-references', '/assets/css/pages/references.css', NULL);
  }

  // --- REFERENCE ---
  if (is_singular('reference')) {
    stbase_enqueue_style_with_filetime('st-lightbox-styles', '/assets/js/plugins/lightbox/lightbox.min.css', NULL);
    stbase_enqueue_script_with_filetime('st-lightbox-script', '/assets/js/plugins/lightbox/lightbox.min.js', ['jquery']);
    stbase_enqueue_script_with_filetime('st-reference-page', '/assets/js/pages/reference.js', array('jquery', 'st-lodash', 'st-lightbox-script'));

    stbase_enqueue_style_with_filetime('st-reference', '/assets/css/pages/reference.css', NULL);
  }

  // --- 404 page ---
  if (is_404()) {
    stbase_enqueue_style_with_filetime('st-404-page', '/assets/css/pages/404.css', NULL);
  }

  // --- HOMEPAGE ---
  if (strpos(get_page_template(), 'page-home') !== false) {
    stbase_enqueue_style_with_filetime('st-home-page', '/assets/css/pages/home-page.css', NULL);
  }

  // --- ANDROID PAGE ---
  if (strpos(get_page_template(), 'page-android') !== false) {
    stbase_enqueue_style_with_filetime('st-android-page', '/assets/css/pages/android-page.css', NULL);
  }

  // --- APPDEV PAGE ---
  if (strpos(get_page_template(), 'page-app-dev') !== false) {
    stbase_enqueue_style_with_filetime('st-app-dev-page', '/assets/css/pages/app-dev-page.css', NULL);
  }

  // --- ARVR PAGE ---
  if (strpos(get_page_template(), 'page-arvr') !== false) {
    stbase_enqueue_style_with_filetime('st-arvr-page', '/assets/css/pages/arvr-page.css', NULL);
  }

  // --- DRUPAL PAGE ---
  if (strpos(get_page_template(), 'page-drupal') !== false) {
    stbase_enqueue_style_with_filetime('st-drupal-page', '/assets/css/pages/drupal-page.css', NULL);
  }

  // --- FLUTTER PAGE ---
  if (strpos(get_page_template(), 'page-flutter') !== false) {
    stbase_enqueue_style_with_filetime('st-flutter-page', '/assets/css/pages/flutter-page.css', NULL);
  }

  // --- IPHONE PAGE ---
  if (strpos(get_page_template(), 'page-iphone') !== false) {
    stbase_enqueue_style_with_filetime('st-iphone-page', '/assets/css/pages/iphone-page.css', NULL);
  }

  // --- IPHONE PAGE ---
  if (strpos(get_page_template(), 'page-iphone') !== false) {
    stbase_enqueue_style_with_filetime('st-iphone-page', '/assets/css/pages/iphone-page.css', NULL);
  }

  // --- WEB DEV PAGE ---
  if (strpos(get_page_template(), 'page-web-dev') !== false) {
    stbase_enqueue_style_with_filetime('st-web-dev-page', '/assets/css/pages/web-dev-page.css', NULL);
  }

  // --- CREATION SITE PAGE ---
  if (strpos(get_page_template(), 'page-creation-site') !== false) {
    stbase_enqueue_style_with_filetime('st-creation-site-page', '/assets/css/pages/creation-site-page.css', NULL);
  }

  // --- WEBAPP PAGE ---
  if (strpos(get_page_template(), 'page-webapp') !== false) {
    stbase_enqueue_style_with_filetime('st-webapp-page', '/assets/css/pages/webapp-page.css', NULL);
  }

  // --- WORDPRESS PAGE ---
  if (strpos(get_page_template(), 'page-wordpress') !== false) {
    stbase_enqueue_style_with_filetime('st-wordpress-page', '/assets/css/pages/wordpress-page.css', NULL);
  }

  // --- STARTUP PAGE ---
  if (strpos(get_page_template(), 'page-startup') !== false) {
    stbase_enqueue_style_with_filetime('st-startup-page', '/assets/css/pages/startup-page.css', NULL);
  }

  // --- NGO PAGE ---
  if (strpos(get_page_template(), 'page-ngo') !== false) {
    stbase_enqueue_style_with_filetime('st-ngo-page', '/assets/css/pages/ngo-page.css', NULL);
  }

  // --- DISCOVERY PROCESS PAGE ---
  if (strpos(get_page_template(), 'page-discovery-process') !== false) {
    stbase_enqueue_style_with_filetime('st-discovery-process-page', '/assets/css/pages/discovery-process-page.css', NULL);
  }

  // --- REFERENCE REORDER PAGE ---
  if (strpos(get_page_template(), 'page-references-reorder') !== false) {
    wp_enqueue_style('jquery-ui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', NULL);
    stbase_enqueue_style_with_filetime('swisst2-references-reorder', '/assets/css/pages/references-reorder.css', NULL);
    stbase_enqueue_script_with_filetime('swisst2-references-reorder', '/assets/js/pages/references-reorder.js', array('jquery', 'jquery-ui-sortable'));
    wp_localize_script('swisst2-references-reorder', 'wpApiSettings', array(
      'nonce' => wp_create_nonce('wp_rest')
    ));
  }
}
add_action('wp_enqueue_scripts', 'stbase_scripts');


/* ADMIN STYLES */
add_action('admin_enqueue_scripts', function () {
  stbase_enqueue_style_with_filetime('stbase-admin-styles', '/stbase-admin-styles.css', NULL);

  stbase_enqueue_script_with_filetime('stbase-vdani-admin-scripts', '/assets/js/admin/check-acf-fields-scripts.js', NULL);
});


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/* DISPLAY REFERENCE TEMPLATE */
// Add custom column to the reference post type admin screen
function custom_reference_columns($columns)
{
  $columns['template'] = 'Template';
  return $columns;
}
add_filter('manage_reference_posts_columns', 'custom_reference_columns');

// Display the template name in the custom column
function display_custom_reference_columns($column, $post_id)
{
  if ($column === 'template') {
    $template = get_page_template_slug($post_id);
    if (!empty($template)) {
      echo '<span style="color: #616161;background: #f5cfd1;padding: 5px 10px;border-radius: 4px;">Case Study</span>';
    } else {
      echo '<span style="color: #616161;background: #dedede;padding: 5px 10px;border-radius: 4px;">Default</span>';
    }
  }
}
add_action('manage_reference_posts_custom_column', 'display_custom_reference_columns', 10, 2);

