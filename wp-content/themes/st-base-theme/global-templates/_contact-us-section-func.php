<?php function contact_us_section_func($with_id = false) { ?>
  <?php
  $current_lang = get_bloginfo('language');
  ?>

  <div class="contact-us-section" <?= $with_id ? 'id="contact-us"' : '' ?>>
    <?php if ($current_lang === 'hu-HU') : ?>
      <div class="container mod-on-de">
        <div class="contact-column-1">
          <h2>
            <?= get_field('contact_section_title', 'option') ?>
          </h2>
          <div class="contact-us-text">
            <?= get_field('contact_section_text', 'option') ?>
          </div>
          <?php
          if (get_bloginfo('language') == 'en-US') {
            echo do_shortcode('[contact-form-7 id="86" title="Contact form EN"]');
          } elseif (get_bloginfo('language') == 'de-DE') {
            echo do_shortcode('[contact-form-7 id="87" title="Contact form DE"]');
          } else {
            echo do_shortcode('[contact-form-7 id="72" title="Contact form FR"]');
          }
          ?>
        </div>

        <div class="contact-column-2" style="background-image: url(<?= get_img_url_webp_if_possible(get_field('contact_section_german_background', 'option')) ?>);">
          <?= get_img(get_field('contact_section_image', 'option'), 'Contact Us Image') ?>
          <div id="contact-informations">
            <!-- <div id="city-list">
                  <a data-target="#contact-geneva">Geneva</a>
                  <a data-target="#contact-zurich">Zurich</a>
                  <a data-target="#contact-london">London</a>
                  <a data-target="#contact-budapest">Budapest</a>
              </div> -->

            <?php if (get_bloginfo('language') == 'de-DE') : ?>
              <div class="contact-info-text" id="contact-zurich">
                <h3><?= __('Zurich', 'swisst2') ?></h3>
                <?= get_field('contact_section_zurich_text', 'option') ?>
              </div>
            <?php else : ?>
              <div class="contact-info-text" id="contact-geneva">
                <h3><?= __('Geneva', 'swisst2') ?></h3>
                <?= get_field('contact_section_geneva_text', 'option') ?>
              </div>
              <div class="contact-info-text" id="contact-zurich">
                <h3><?= __('Zurich', 'swisst2') ?></h3>
                <?= get_field('contact_section_zurich_text', 'option') ?>
              </div>
            <?php endif; ?>

            <!-- <div class="contact-info-text" id="contact-london">
                  <?= get_field('contact_section_london_text', 'option') ?>
              </div>
              <div class="contact-info-text" id="contact-budapest">
                  <?= get_field('contact_section_budapest_text', 'option') ?>
              </div> -->

          </div>

          <a target="_blank" class="contact-social-icon" href="<?= get_field('facebook_link', 'option') ?>">
            <i class="fab fa-facebook"></i>
          </a>

          <!-- <a target="_blank" class="contact-social-icon" href="<?= get_field('twitter_link', 'option') ?>">
                <i class="fab fa-twitter"></i>
            </a> -->

          <a target="_blank" class="contact-social-icon" href="<?= get_field('linkedin_link', 'option') ?>">
            <i class="fab fa-linkedin"></i>
          </a>
        </div>
      </div>
    <?php else : ?>

      <div class="container">
        <div class="contact-column-1">
          <h2>
            <?= get_field('contact_section_title', 'option') ?>
          </h2>
          <div class="contact-us-text">
            <?= get_field('contact_section_text', 'option') ?>
          </div>
          <?php
          if (get_bloginfo('language') == 'en-US') {
            echo do_shortcode('[contact-form-7 id="86" title="Contact form EN"]');
          } elseif (get_bloginfo('language') == 'de-DE') {
            echo do_shortcode('[contact-form-7 id="87" title="Contact form DE"]');
          } else {
            echo do_shortcode('[contact-form-7 id="72" title="Contact form FR"]');
          }
          ?>
        </div>

        <div class="contact-column-2">
          <div class="contact-cup-animation">
            <!-- <lottie-player 
              src="<?= get_stylesheet_directory_uri() ?>/assets/js/plugins/lottie-player/footer-cup.json" 
              speed="1" 
              style="width: 600px; height: 600px;" 
              loop autoplay></lottie-player> -->
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/Tomato-juice.gif" alt="Tomato Juice Animation" loading="lazy" />
          </div>

          <div id="contact-informations">

            <div class="contact-info-text">
              <?= get_field('contact_info_text', 'option') ?>
            </div>

          </div>

          <!-- <a target="_blank" class="contact-social-icon" rel="noopener" href="<?= get_field('facebook_link', 'option') ?>">
            <i class="fab fa-facebook"></i>
          </a>

          <a target="_blank" class="contact-social-icon" rel="noopener" href="<?= get_field('linkedin_link', 'option') ?>">
            <i class="fab fa-linkedin"></i>
          </a> -->
        </div>
      </div>

    <?php endif; ?>
  </div>
  <?php 
} 

function contact_us_compact_section_func($with_id = false) { ?>
  <div class="contact-us-section compact" <?= $with_id ? 'id="contact-us"' : '' ?>>
    <div class="container">
      <div class="contact-column-1">
        <h2>
          <?= get_field('contact_section_title', 'option') ?>
        </h2>
        <div class="contact-us-text">
          <?= get_field('contact_section_text', 'option') ?>
        </div>
        <a href="#contact-us" class="st-button-link">
          <i class="far fa-arrow-right" aria-hidden="true"></i>
          <?= get_field('contact_link_text', 'option') ?>
        </a>
      </div>

      <div class="contact-column-2">
        <div class="contact-cup-animation">
          <!-- <lottie-player 
            src="<?= get_stylesheet_directory_uri() ?>/assets/js/plugins/lottie-player/footer-cup.json" 
            speed="1" 
            style="width: 600px; height: 600px;" 
            loop autoplay></lottie-player> -->
          <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/Tomato-juice.gif" alt="Tomato Juice Animation" loading="lazy" />
        </div>
      </div>
    </div>
  </div>
<?php 
} 
?>