<?php
  $cookie_text = get_field('cookie_text', 'option');
  $cookie_link = get_field('cookie_link', 'option');
  $cookie_button_text = get_field('cookie_button_text', 'option');

  if (empty($cookie_text)) {
    $cookie_text = __('We are using cookies to give you the best experience on our website.', 'stbase');
  }

  if (empty($cookie_button_text)) {
    $cookie_button_text = __('Accept', 'stbase');
  }

  if ($cookie_link) {
		$cookie_link_url = $cookie_link['url'] ? $cookie_link['url'] : get_privacy_policy_url();
		$cookie_link_title = trim($cookie_link['title']) ? trim($cookie_link['title']) : __('Privacy Policy', 'swisst2');
		$cookie_link_target = $cookie_link['target'] ? $cookie_link['target'] : '_self';
	}
?>

<div id="cookie" class="cookie">
  <p class="cookie__text"><?= $cookie_text ?></p>
  
  <a href="<?= $cookie_link_url ?>" target="<?= $cookie_link_target ?>" class="cookie__url"><?= $cookie_link_title ?></a>
  
  <button class="st-button"><?= $cookie_button_text ?></button>
</div>

