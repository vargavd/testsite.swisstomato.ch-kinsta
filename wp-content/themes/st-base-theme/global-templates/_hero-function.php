<?php function print_stbase_hero_section($img_url, $title, $text = '') { ?>
  <div class="stbase-hero">
    <div class="container">
      <div class="hero-slide" style="background-image: url(<?=$img_url?>);">
        <div class="dark-layer">
          <h1><?=$title?></h1>
          <?=$text?>
        </div>
      </div>
    </div>
  </div>
<?php }

