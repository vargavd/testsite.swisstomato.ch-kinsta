<?php 
  /**
   * Display references in a masonry list
   * @param type_options: array(
   *  archive_like => (
   *    display_buttons => boolean
   *  ),
   *  one_category => (
   *    title => string
   *    category => WP Category Object
   *  )
   * )
   */
  function masonry_references_list_section_func($type_options) {
    $is_archive_like = array_key_exists('like-archive', $type_options);
    $is_one_category = array_key_exists('one-category', $type_options);
    $is_specific_references = array_key_exists('specific-references', $type_options);
    
    if ($is_archive_like) {
      $archive_like_options = $type_options['like-archive'];
    } elseif ($is_one_category) {
      $one_category_options = $type_options['one-category'];

      $category = $one_category_options['category'];
    } else {
      $specific_references_options = $type_options['specific-references'];
    }
?>

  <div class="masonry-references-list-section">

    <!-- intro and filter part for archive like block -->
    <?php if ($is_archive_like): ?>
      <?php
        $platforms = get_terms(array(
          'taxonomy' => 'reference_category',
          'hide_empty' => false,
          'orderby' => 'id',
        ));
      ?>

      <div class="masonry-references-intro center-1117">
        <?php if (is_post_type_archive('reference')): ?>
          <h1 class="intro-title"><?=$archive_like_options['title']?></h1>
        <?php else: ?>
          <h2 class="intro-title"><?=$archive_like_options['title']?></h2>
        <?php endif; ?>

        <div id="references-intro-text">
          <?=$archive_like_options['intro']?>
        </div>

        <div id="filter-section">
          <div class="filter-area">    
            <div class="filter-boxes">
              <div 
                class="filter-box selected" 
                data-button-text="More references"
              >
                <?=__('All projects', 'swisst2')?>
              </div>

              <?php foreach ($platforms as $platform): 
              ?>
                <div class="filter-box" 
                  data-id="<?=$platform->term_id?>" 
                  data-slug="<?=$platform->slug?>"
                  data-button-text="<?=get_field('button_text_more_references', 'reference_category_' . $platform->term_id)?>"
                  data-button-link="<?=st_get_references_link($platform->slug)?>"
                >
                  <?=get_field('filter_button_text', $platform->taxonomy . '_' . $platform->term_id)?>
                </div>
              <?php endforeach; ?>

              <i class="fas fa-spinner"></i>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <!-- h2 title for regular one category blocks -->
    <?php if ($is_one_category): ?>
      <h2 class="no-intro"><?=$one_category_options['title']?></h2>
    <?php endif; ?>

    <!-- h2 title for specific references blocks -->
    <?php if ($is_specific_references): ?>
      <h2 class="no-intro"><?=$specific_references_options['title']?></h2>
    <?php endif; ?>

    <div class="masonry-references-list container">
      <!-- reference template -->
      <article class="reference template" data-width="" data-height="" data-url="">
        <div class="reference__image-wrapper">
          <img src="<?=get_stylesheet_directory_uri()?>/assets/imgs/gray-624x460.jpg" alt="Reference"/>
        </div>

        <div class="reference__content">
          <h3></h3>

          <p></p>

          <div class="reference__content__category"></div>
        </div>
      </article>


      <?php 
        if (is_post_type_archive('reference')) {
          // *** ON ARCHIVE PAGE ***
          while ( have_posts() ) { 
            the_post(); 
            masonry_reference_template(); 
          }
        } else {
          // *** IN A BLOCK ***
          $args = array(  
            'post_type' => 'reference',
            'post_status' => 'publish',
            'posts_per_page' => 7,
            'orderby' => 'meta_value_num',
            'meta_key' => 'no_category_order',
            'order' => 'ASC',
            'meta_query' => array (
              'key' => 'masonry_thumbnail_image',
              'compare' => 'NOT IN',
              'value' => ''
            )
          );

          if ($is_archive_like) {
            $args['meta_query'] = array(
              'relation' => 'AND',
              array(
                'key' => 'masonry_thumbnail_image',
                'compare' => 'NOT IN',
                'value' => ''
              ),
              array(
                'relation' => 'OR',
                array(
                  'key' => 'hide_in_archive',
                  'compare' => '!=',
                  'value' => '1'
                ),
                array(
                  'key' => 'hide_in_archive',
                  'compare' => 'NOT EXISTS',
                ),
              ),
            );
          } elseif ($is_one_category) {
            $args['tax_query'] = array(
              array(
                'taxonomy' => 'reference_category',
                'field'    => 'slug',
                'terms'    => $category->slug,
              ),
            );

            if (strpos($category->slug, 'web') !== false) {
              $args['meta_key'] = 'web_category_order';
            }

            if (strpos($category->slug, 'app') !== false) {
              $args['meta_key'] = 'app_category_order';
            }

            if (strpos($category->slug, 'reality') !== false) {
              $args['meta_key'] = 'arvr_category_order';
            }
          } elseif ($is_specific_references) {
            $args['post__in'] = $specific_references_options['references'];
            $args['orderby'] = 'post__in';
          }

          $loop = new WP_Query( $args ); 
              
          while ( $loop->have_posts()) {
            $loop->the_post();
            masonry_reference_template();
          }
      
          wp_reset_postdata(); 
        }       
        
      ?>

    </div>

    <?php 
      if ($is_archive_like) {
        if (!is_archive()) {
          masonry_reference_list_button($archive_like_options['button_text'], st_get_references_link());
        }
      } 
      
      if ($is_one_category) {
        masonry_reference_list_button(
          get_field('button_text_more_references', "reference_category_" . $category->term_id), 
          st_get_references_link($category->slug)
        );
      }

      if ($is_specific_references) {
        $button_link_to_category = $specific_references_options['button_link_to_category'];
        masonry_reference_list_button($specific_references_options['button_text'], st_get_references_link($button_link_to_category->slug));
      }
    ?>
  </div>

<?php } ?>
<?php function masonry_reference_template() { ?>

  <?php
    $ID = get_the_ID();
    $reference_thumbnail = get_field('masonry_thumbnail_image', $ID);

    $thumbnail_type = gettype($reference_thumbnail);
    $thumbnail_src = '';

    if ($thumbnail_type === 'string') {
      $thumbnail_infos = wp_get_attachment_image_src(intval($reference_thumbnail), 'full');
      $thumbnail_src = $thumbnail_infos[0];
      $thumbnail_width = $thumbnail_infos[1];
      $thumbnail_height = $thumbnail_infos[2];
    }

    if ($thumbnail_type === 'array') {
      $thumbnail_width = $reference_thumbnail['width'];
      $thumbnail_height = $reference_thumbnail['height'];
      $thumbnail_src = $reference_thumbnail['url'];
    }

    $link = get_permalink($ID);
    $title = get_field('masonry_reference_title', $ID);
    $text = get_field('masonry_reference_text', $ID);

    if (empty($thumbnail_src)) {
      // print "<a href='$link' target='_blank' data-type='$thumbnail_type'>" . get_the_title($ID) . "</a><br />";
      $thumbnail_src = get_stylesheet_directory_uri() . '/imgs/gray-624x460.jpg';
      $thumbnail_width = 624;
      $thumbnail_height = 460;
    }

    if (empty($title)) {
      $title = get_the_title();
    }

    $ref_categories = get_the_terms($ID, 'reference_category');

    $ref_class = 'reference';

    if (get_field('masonry_black_layer_below_title', $ID)) {
      $ref_class .= ' black-layer';
    }

    if (get_field('masonry_black_layer_below_title_mobile', $ID)) {
      $ref_class .= ' mobile-black-layer';
    }

    $hide_archive = get_field('hide_in_archive', $ID);
  ?>

  <article
    class="<?=$ref_class?>" 
    data-width="<?=$thumbnail_width?>" 
    data-height="<?=$thumbnail_height?>" 
    data-url="<?=$link?>"
    data-id="<?=$ID?>"
    data-hide-archive="<?=$hide_archive?>"
  >
    <div class="reference__image-wrapper">
      <?=get_img($thumbnail_src, $title)?>
      <!-- <img src="<?=$thumbnail_src?>" alt="<?=$title?>" /> -->
    </div>

    <div class="reference__content">
      <h3 data-text-slide-from-up="<?=$title?>">
        <span class="text-slide-down"><?=$title?></span>
      </h3>

      <p data-text-slide-from-up="<?=$text?>">
        <span class="text-slide-down"><?=$text?></span>
      </p>

      <?php if (is_array($ref_categories) && sizeof($ref_categories) > 0): ?>
        <div class="reference__content__category">
          <?=get_field('filter_text', $ref_categories[0])?>
        </div>
      <?php endif; ?>
    </div>
  </article>

<?php } ?>
<?php function masonry_reference_list_button($button_text, $button_link) { ?>
  <div class="text-center">
    <a href="<?=$button_link?>" class="st-button-link more-references">
      <i class="fas fa-angle-right"></i>
      <?=$button_text?>
    </a>
  </div>
<?php } ?>