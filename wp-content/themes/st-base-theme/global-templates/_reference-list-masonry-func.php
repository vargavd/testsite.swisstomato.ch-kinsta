<?php function reference_list_masonry_section($title, $references, $category_slug = '', $button_text = '') { ?>
  <div class="masonry-reference-list-section">
    <div class="container">
      <?php if (!empty($title)): ?>
        <h2 id="masonry-reference-list-title"><?=$title?></h2>
      <?php endif; ?>
  
      <div class="masonry-reference-list">
        <?php foreach ($references as $index => $reference): ?>
            <?php
              $reference_thumbnail = get_field('masonry_thumbnail_image', $reference);
              $width=$reference_thumbnail['width'];
              $height=$reference_thumbnail['height'];

              $ref_class = 'reference';
              
              if (get_field('masonry_black_layer_below_title', $reference)) {
                $ref_class .= ' black-layer';
              }

              if (get_field('masonry_black_layer_below_title_mobile', $reference)) {
                $ref_class .= ' mobile-black-layer';
              }

              $title = get_field('masonry_reference_title', $reference);
              $text = get_field('masonry_reference_text', $reference);
            ?>
            <article 
              class="<?=$ref_class?>" 
              data-width="<?=$width?>" 
              data-height="<?=$height?>"
              data-url="<?=get_permalink($reference)?>"
              data-id="<?=$reference?>"
            >
              <div class="reference__image-wrapper">
                <?=get_img($reference_thumbnail['url'], get_the_title($reference))?>
                <!-- <img 
                  src="<?=$reference_thumbnail['url']?>"
                  alt="<?=get_the_title($reference)?>"
                /> -->
              </div>
  
              <div class="reference__content">
                <h3 data-text-slide-from-up="<?=$title?>">
                  <span class="text-slide-down"><?=$title?></span>
                </h3>

                <p data-text-slide-from-up="<?=$text?>">
                  <span class="text-slide-down"><?=$text?></span>
                </p>
  
                <p><?=get_field('masonry_reference_text', $reference)?></p>
              </div>
            </article>
          <?php endforeach; ?>
      </div>

      <?php if (!empty($button_text)): ?>
        <div class="text-center">
          <a href="<?=swisst2_get_references_link($category_slug)?>" class="button-red more-references">
            <i class="fas fa-angle-right"></i>
            <?=$button_text?>
          </a>
        </div>
      <?php endif; ?>
    </div>
  </div>
    
<?php }

