<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ST_Base_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <!-- Google Tag Manager -->
  <!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-W965NLM');</script> -->
  <!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

  <!-- Fonts Preloading -->
	<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
	<link rel="stylesheet preload prefetch" href="https://fonts.googleapis.com/css?family=Rubik:wght@0,300;0,400;0,500;0,700&display=swap" as="style" type="text/css" crossorigin="anonymous">

  <!-- <script src="https://kit.fontawesome.com/4f24a40eba.js" crossorigin="anonymous"></script> -->

  <?php include "inc/preload-in-header.php"; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(get_bloginfo('language')); ?>>

<!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W965NLM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->

<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div class="container">
      <div class="site-branding">
        <?php the_custom_logo(); ?>
      </div>

      <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
        <span class="bar1"></span>
        <span class="bar2"></span>
        <span class="bar3"></span>
      </button>
      
      <nav id="site-navigation" class="main-navigation">
        <?php
        wp_nav_menu(array(
          'theme_location' => 'secondary-menu',
          'menu_id'        => 'secondary-menu',
          'container_id' => 'secondary-menu-container',
        ));
        wp_nav_menu(array(
          'theme_location' => 'primary-menu',
          'menu_id'        => 'primary-menu',
          'container_id' => 'primary-menu-container',
        ));
        ?>
      </nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

  <a href="#contact-us" id="contact-popup-trigger">
    <i class="far fa-envelope"></i>
    <span><?=get_field('contact_floating_button', 'option')?></span>
  </a>
