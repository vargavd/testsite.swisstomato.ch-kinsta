<?php

get_header();

$categories = get_categories();
$tags = get_tags();

?>

<div class="stbase-blogposts-page stbase-page">

  <div id="intro-section" class="container">
    <h1><?=get_the_title( get_option('page_for_posts', true) ) ?></h1>
  </div>

  <?php if ( have_posts() ) : ?>
    <div class="st-blog-posts container">  
      <div class="blog-post-list">
        <article class="post post-template">
            <a href="" class="post-image">
            </a>
            <div class="post-content">
                <h2 class="post-title">
                    <a href="">
                        
                    </a>
                </h2>
                <div class="post-date"></div>
                <div class="post-text"></div>
            </div>
        </article>
  
        <?php 
          $first_post = true;
          while ( have_posts() ) : 
            the_post();
  
            $ID = get_the_ID();
            $categories = get_the_category();
  
            $post_img = get_field($first_post ? 'first_post_thumbnail' : 'post_thumbnail', $ID);
        ?>
        
          <article 
            class="post <?=$first_post ? 'first-post' : ''?>" 
            style="background-image: url(<?=$first_post ? $post_img : ''?>);"
          >
            <a href="<?=get_permalink()?>" class="post-image" style="background-image: url(<?=$post_img?>);">
            </a>
  
            <div class="post-content">
              <div class="post-categories">
                <?php foreach ($categories as $category): ?>
                  <a href="<?=get_category_link($category)?>">
                    <?=$category->name?>
                  </a>
                <?php endforeach; ?>
              </div>
              <h2 class="post-title">
                <a href="<?=get_permalink()?>">
                  <?php the_title(); ?>
                </a>
              </h2>
              <div class="post-date"><?=get_the_date("F d, o")?></div>
              <div class="post-text"><?=the_excerpt()?></div>
            </div>
          </article>  
  
        <?php 
          $first_post = false;
          endwhile; 
        ?>
  
      </div><!-- /#blog-post-list -->
  
      <div class="load-more-wrapper text-center">
        <i class="fas fa-circle-notch spinning-icon"></i>
        
        <a class="load-more st-button-link center">
          <?=get_field('load_more_button_text', 'option')?>
        </a>
      </div>
  
      <div class="loading-popup">
        
      </div>
    </div>

  <?php else: echo "There are no posts yet.";  endif; ?>		

</div><!-- /#swisst-blogposts-page -->

<?php get_footer(); ?>
