<?php

// ADD BLOCK CATEGORIES
add_filter('block_categories', function ($categories, $post) {

  array_push($categories, array(
    'slug' => 'stbase-general-blocks',
    'title' => __('Swiss Tomato General Blocks'),
    'icon'  => 'menu',
  ));

  array_push($categories, array(
    'slug' => 'stbase-cs-blocks',
    'title' => __('Swiss Tomato Reference - Case Study Blocks'),
  ));

  return $categories;
}, 10, 2);


// REGISTER BLOCKS
add_action('acf/init', function () {
  if (!function_exists('acf_register_block_type')) {
    return;
  }

  #region GENERAL BLOCKS
  acf_register_block_type(array(
    'name'              => 'stbase-hero-block',
    'title'             => __('Hero Block'),
    'render_template'   => 'template-parts/hero-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'id',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-hero-style', '/assets/css/blocks/hero-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-intro-text-awards-block',
    'title'             => __('Intro Text Awards Block'),
    'render_template'   => 'template-parts/intro-text-awards-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'welcome-widgets-menus',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-intro-text-awards-style', '/assets/css/blocks/intro-text-awards.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-big-red-text-block',
    'title'             => __('Big Red Text Block'),
    'render_template'   => 'template-parts/big-red-text-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-aside',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-big-red-text-block-style', '/assets/css/blocks/big-red-text-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-our-story-block',
    'title'             => __('Our Story Block'),
    'render_template'   => 'template-parts/our-story-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'groups',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-our-story-style', '/assets/css/blocks/our-story.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-awards-block',
    'title'             => __('Awards Block'),
    'render_template'   => 'template-parts/awards-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'images-alt',
    'supports'          => array('anchor' => true),
    'enqueue_assets'    => function () {
      // stbase_enqueue_script_with_filetime('st-awards-script', '/assets/js/blocks/awards-block.js', array('jquery', 'st-scroll-trigger-script'));
      stbase_enqueue_style_with_filetime('st-awards-block-style', '/assets/css/blocks/awards-block.css', NULL);
    },
    'mode'              => 'auto'
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-clients-block',
    'title'             => __('Clients Block'),
    'render_template'   => 'template-parts/clients-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'screenoptions',
    'supports'          => array('anchor' => true),
    'enqueue_assets'    => function () {
      // stbase_enqueue_script_with_filetime('st-clients-script', '/assets/js/blocks/clients-block.js', array('jquery', 'st-scroll-trigger-script'));
      stbase_enqueue_style_with_filetime('st-clients-block-style', '/assets/css/blocks/clients-block.css', NULL);
    },
    'mode'              => 'auto'
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-image-text-block',
    'title'             => __('Image Text Block'),
    'render_template'   => 'template-parts/image-text-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'media-document',
    'supports'          => array('anchor' => true),
    'enqueue_assets'    => function () {
      // stbase_enqueue_script_with_filetime('st-image-text-script', '/assets/js/blocks/image-text-block.js', array('jquery', 'st-scroll-trigger-script'));
      stbase_enqueue_style_with_filetime('st-image-text-block-style', '/assets/css/blocks/image-text-block.css', NULL);
    },
    'mode'              => 'auto'
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-text-and-cta-block',
    'title'             => __('Text And CTA Block'),
    'render_template'   => 'template-parts/text-and-cta-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-aside',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-text-and-cta-block-style', '/assets/css/blocks/text-and-cta-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-why-us-block',
    'title'             => __('Why Us Block'),
    'render_template'   => 'template-parts/why-us-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'align-pull-left',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-why-us-block-style', '/assets/css/blocks/why-us-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-services-block',
    'title'             => __('Services Block'),
    'render_template'   => 'template-parts/services-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'columns',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-services-block-style', '/assets/css/blocks/services-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-big-bc-mt-block',
    'title'             => __('Big BC MT Block'),
    'render_template'   => 'template-parts/big-bc-mt-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'embed-photo',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-big-bc-mt-block-style', '/assets/css/blocks/big-bc-mt-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fields-of-exp-block',
    'title'             => __('Fields Of Experience Block'),
    'render_template'   => 'template-parts/fields-of-exp-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'calendar-alt',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      // stbase_enqueue_script_with_filetime('st-fields-of-exp-script', '/assets/js/blocks/fields-of-exp-block.js', array('jquery', 'st-scroll-trigger-script'));
      stbase_enqueue_style_with_filetime('st-fields-of-exp-block-style', '/assets/css/blocks/fields-of-exp-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-intro-text-video-block',
    'title'             => __('Intro Text Video Block'),
    'render_template'   => 'template-parts/intro-text-video-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'table-col-after',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-intro-text-video-style', '/assets/css/blocks/intro-text-video.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-masonry-references-block',
    'title'             => __('References (Masonry) Block'),
    'render_template'   => 'template-parts/masonry-references-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'editor-table',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      // stbase_enqueue_script_with_filetime('st-masonry-references-script', '/assets/js/global-templates/reference-list-masonry/reference-list-masonry-section.js', array('jquery', 'st-scroll-trigger-script'));
      stbase_enqueue_style_with_filetime('st-references-masonry-block-style', '/assets/css/blocks/references-masonry-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-icon-list-block',
    'title'             => __('Icon List Block'),
    'render_template'   => 'template-parts/icon-list-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'screenoptions',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-icon-list-block-style', '/assets/css/blocks/icon-list-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-check-list-block',
    'title'             => __('Check List Block'),
    'render_template'   => 'template-parts/check-list-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'yes',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-check-list-block-style', '/assets/css/blocks/check-list-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-big-facts-list-block',
    'title'             => __('Big Facts List Block'),
    'render_template'   => 'template-parts/big-facts-list-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'align-wide',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-big-facts-list-style', '/assets/css/blocks/big-facts-list.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-contact-us-block',
    'title'             => __('Contact us Block'),
    'render_template'   => 'template-parts/contact-us-section-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'screenoptions',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      // stbase_enqueue_style_with_filetime('st-ddd-style', '/assets/css/blocks/ddd.css', NULL);
    },
  ));

  // FACT BLOCKS
  acf_register_block_type(array(
    'name'              => 'stbase-fact-links-block',
    'title'             => __('Facts Links Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-links-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'align-wide',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-links-style', '/assets/css/blocks/fact-styles/fact-links.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-header-block',
    'title'             => __('Facts Header Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-header-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-image',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-header-style', '/assets/css/blocks/fact-styles/fact-header.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-text-block',
    'title'             => __('Facts Text Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-text-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'text',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-text-style', '/assets/css/blocks/fact-styles/fact-text.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-advantages-block',
    'title'             => __('Facts Advantages Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-advantages-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'columns',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_script_with_filetime('st-big-facts-show-more-advantages', '/assets/js/blocks/big-fact-scripts/show-more-advantages/show-more-advantages.js', array('jquery'));
      stbase_enqueue_style_with_filetime('st-fact-advantages-style', '/assets/css/blocks/fact-styles/fact-advantages.css', NULL);
    }
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-process-timing-block',
    'title'             => __('Facts Process Timing Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-process-timing-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'clock',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-process-timing-style', '/assets/css/blocks/fact-styles/fact-process-timing.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-team-block',
    'title'             => __('Facts Team Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-team-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'admin-users',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-team-style', '/assets/css/blocks/fact-styles/fact-team.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-complexity-block',
    'title'             => __('Facts Complexity Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-complexity-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'media-archive',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-complexity-style', '/assets/css/blocks/fact-styles/fact-complexity.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-text-list-block',
    'title'             => __('Facts Text List Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-text-list-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'excerpt-view',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-text-list-style', '/assets/css/blocks/fact-styles/fact-text-list.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-media-grid-block',
    'title'             => __('Media Grid Block'),
    'render_template'   => 'template-parts/media-grid-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'grid-view',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-media-grid-style', '/assets/css/blocks/media-grid.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-header-with-bc-block',
    'title'             => __('Header With BC Block'),
    'render_template'   => 'template-parts/header-with-bc-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-image',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-header-with-bc-style', '/assets/css/blocks/header-with-bc.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-application-fields-block',
    'title'             => __('Fact Application Fields Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-application-fields-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-image',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-application-fields-style', '/assets/css/blocks/fact-styles/fact-application-fields.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-image-text-boxes-block',
    'title'             => __('Fact Image Text Boxes Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-image-text-boxes-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-image',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-image-text-boxes-style', '/assets/css/blocks/fact-styles/fact-image-text-boxes.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-intro-text-image-block',
    'title'             => __('Intro Text Image Block'),
    'render_template'   => 'template-parts/intro-text-image-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'table-col-after',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-intro-text-image-style', '/assets/css/blocks/intro-text-image.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-cost-table-block',
    'title'             => __('Cost Table Block'),
    'render_template'   => 'template-parts/cost-table-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'excerpt-view',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-cost-table-block-style', '/assets/css/blocks/cost-table-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-gallery-block',
    'title'             => __('Gallery Block'),
    'render_template'   => 'template-parts/gallery-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'format-gallery',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-gallery-block-style', '/assets/css/blocks/gallery-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-testimonials-block',
    'title'             => __('Testimonials Block'),
    'render_template'   => 'template-parts/testimonials-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'testimonial',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-testimonials-block-style', '/assets/css/blocks/testimonials-block.css', NULL);
      stbase_enqueue_style_with_filetime('st-testimonials-block-swiper-style', '/assets/js/plugins/swiper/testimonial/swiper-bundle.min.css', NULL);
      stbase_enqueue_script_with_filetime('st-testimonials-block-swiper-js', '/assets/js/plugins/swiper/testimonial/swiper-bundle.min.js', NULL);
      stbase_enqueue_script_with_filetime('st-testimonials-script', '/assets/js/blocks/testimonials.js', array('st-testimonials-block-swiper-js'));
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-bg-text-block',
    'title'             => __('Background + Text Block'),
    'render_template'   => 'template-parts/bg-text-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'cover-image',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-bg-text-block-style', '/assets/css/blocks/bg-text-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-process-block',
    'title'             => __('Process Block'),
    'render_template'   => 'template-parts/process-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'editor-ol',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-process-block-style', '/assets/css/blocks/process-block.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-accordion-block',
    'title'             => __('Accordion Block'),
    'render_template'   => 'template-parts/accordion-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'menu-alt3',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-accordion-block-style', '/assets/css/blocks/accordion-block.css', NULL);
      stbase_enqueue_script_with_filetime('st-accordion-script', '/assets/js/blocks/accordion.js', array('jquery'));
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-fact-accordion-block',
    'title'             => __('Facts Accordion Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-accordion-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'menu-alt3',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-accordion-style', '/assets/css/blocks/fact-styles/fact-accordion.css', NULL);
      stbase_enqueue_script_with_filetime('st-accordion-script', '/assets/js/blocks/accordion.js', array('jquery'));
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-additional-parameters-block',
    'title'             => __('Facts Additional Parameters Block'),
    'render_template'   => 'template-parts/fact-blocks/fact-additional-parameters-block.php',
    'category'          => 'stbase-general-blocks',
    'icon'              => 'plus-alt',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-fact-additional-parameters-style', '/assets/css/blocks/fact-styles/fact-additional-parameters.css', NULL);
    },
  ));

  #endregion

  #region REFERENCE BLOCKS
  acf_register_block_type(array(
    'name'              => 'stbase-reference-introduction-block',
    'title'             => __('Reference (CS) Introduction Block'),
    'render_template'   => 'template-parts/reference-blocks/reference-introduction-block.php',
    'category'          => 'stbase-cs-blocks',
    'icon'              => 'welcome-write-blog',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-reference-introduction-style', '/assets/css/blocks/reference-styles/reference-introduction.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-reference-text-image-block',
    'title'             => __('Reference (CS) Text + Image Block'),
    'render_template'   => 'template-parts/reference-blocks/reference-text-image-block.php',
    'category'          => 'stbase-cs-blocks',
    'icon'              => 'cover-image',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-reference-text-image-style', '/assets/css/blocks/reference-styles/reference-text-image.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-reference-side-images-block',
    'title'             => __('Reference (CS) Text + Side Images Block'),
    'render_template'   => 'template-parts/reference-blocks/reference-side-images-block.php',
    'category'          => 'stbase-cs-blocks',
    'icon'              => 'image-flip-horizontal',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-reference-side-images-style', '/assets/css/blocks/reference-styles/reference-side-images.css', NULL);
    },
  ));

  acf_register_block_type(array(
    'name'              => 'stbase-reference-result-block',
    'title'             => __('Reference (CS) Result Block'),
    'render_template'   => 'template-parts/reference-blocks/reference-result-block.php',
    'category'          => 'stbase-cs-blocks',
    'icon'              => 'slides',
    'supports'          => array('anchor' => true),
    'mode'              => 'auto',
    'enqueue_assets'    => function () {
      stbase_enqueue_style_with_filetime('st-reference-result-style', '/assets/css/blocks/reference-styles/reference-result.css', NULL);
    },
  ));

  #endregion

});

// ALLOWED BLOCK TYPES
/*add_filter('allowed_block_types', function ($allowed_blocks, $post) {

  $allowed_blocks = [
    'core/freeform',
    'acf/stbase-hero-block',
    'acf/stbase-intro-text-awards-block',
    'acf/stbase-big-red-text-block',
    'acf/stbase-our-story-block',
    'acf/stbase-awards-block',
    'acf/stbase-clients-block',
    'acf/stbase-image-text-block',
    'acf/stbase-text-and-cta-block',
    'acf/stbase-why-us-block',
    'acf/stbase-services-block',
    'acf/stbase-big-bc-mt-block',
    'acf/stbase-fields-of-exp-block',
    'acf/stbase-intro-text-video-block',
    'acf/stbase-masonry-references-block',
    'acf/stbase-icon-list-block',
    'acf/stbase-check-list-block',
    'acf/stbase-big-facts-list-block',
    'acf/stbase-contact-us-block',
    'acf/stbase-fact-links-block',
    'acf/stbase-fact-header-block',
    'acf/stbase-fact-text-block',
    'acf/stbase-fact-advantages-block',
    'acf/stbase-fact-process-timing-block',
    'acf/stbase-fact-team-block',
    'acf/stbase-fact-complexity-block',
    'acf/stbase-fact-text-list-block',
    'acf/stbase-media-grid-block',
    'acf/stbase-header-with-bc-block',
    'acf/stbase-fact-application-fields-block',
    'acf/stbase-fact-image-text-boxes-block',
    'acf/stbase-intro-text-image-block',
    'acf/stbase-cost-table-block',
    'acf/stbase-gallery-block',
    'acf/stbase-testimonials-block'
  ];

  return $allowed_blocks;
}, 10, 2);*/
    

	// /*	Gutenberg | ACF Settings
	// -----------------------------------------------*/


	// // Custom Toolbar
	// add_filter( 'acf/fields/wysiwyg/toolbars' , 'custom_toolbars'  );

	// function custom_toolbars( $toolbars ) {

	// 	//pr1($toolbars);
	// 	unset($toolbars['Full'][1][10]); // wp_more
	// 	unset($toolbars['Full'][1][12]); // fullscreen
	// 	unset($toolbars['Full'][1][13]); // wp_adv

	// 	// Add a new toolbar called "Very Simple" - this toolbar has only 1 row of buttons 
	// 	/*$toolbars['Very Simple' ] = [];
	// 	$toolbars['Very Simple' ][1] = ['bold' , 'italic' , 'underline'];

	// 	// Edit the "Full" toolbar and remove 'code' - delete from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
	// 	if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false ) {
	// 		unset( $toolbars['Full' ][2][$key] );
	// 	}*/

	// 	// remove the 'Basic' toolbar completely
	// 	unset( $toolbars['Basic' ] );

	// 	return $toolbars;
	// }


	// // Custom Block Category: Webmernok
	// add_filter( 'block_categories', 'custom_block_categories', 10, 2 );

	// function custom_block_categories( $categories, $post ) {
	// 	if ( $post->post_type !== 'page' ) {
	// 		return $categories;
	// 	}
	// 	return array_merge(
	// 		$categories,
	// 		[
	// 			[
	// 			'slug' => 'webmernok',
	// 			'title' => __( 'Webmernok ' ),
	// 			'icon'  => 'wordpress',
	// 			],
	// 		]
	// 	);
	// }


	// // Custom Allowed Block Types
	// add_filter( 'allowed_block_types', 'allowed_block_types', 10, 2 );

	// function allowed_block_types( $allowed_blocks, $post  ) {

	// 	$allowed_blocks = [
	// 		'core/freeform',
	// 		'acf/hero',
	// 		'acf/news',
	// 		'acf/facts',
	// 		'acf/events',
	// 		'acf/map',
	// 		'acf/testimonials',
	// 		'acf/services',
	// 		'acf/contact',
	// 		'acf/sliders',
	// 		'acf/cards',
	// 		'acf/code',
	// 		'acf/accordion',
	// 		'acf/feedbacks',
	// 	];

	// 	return $allowed_blocks;
	// }


	// // Check if function exists and hook into setup.
	// if( function_exists('acf_register_block_type') ) {
	// 	add_action('acf/init', 'register_acf_blocks');
	// }

	// function register_acf_blocks() {

	// 	$theme_color = '#001a49';

	// 	// Register a Hero Block.
	// 	acf_register_block_type([
	// 		'name'              => 'hero',
	// 		'title'             => __('Hero', 'webmernok'),
	// 		'description'       => __('Hero block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'desktop',
	// 		],
	// 		'keywords'          => [ 'hero', 'fejléc', 'egyedi' ],
	// 		'supports'			=> [
	// 			'align' 		=> true,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);

	// 	// Register a News Block.
	// 	acf_register_block_type([
	// 		'name'              => 'news',
	// 		'title'             => __('News', 'webmernok'),
	// 		'description'       => __('News block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'megaphone',
	// 		],
	// 		'keywords'          => [ 'news', 'hírek', 'hír' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);

	// 	// Register a Facts Block.
	// 	acf_register_block_type([
	// 		'name'              => 'facts',
	// 		'title'             => __('Facts', 'webmernok'),
	// 		'description'       => __('Fact block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'marker',
	// 		],
	// 		'keywords'          => [ 'facts', 'tények', 'számok' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Events Block.
	// 	acf_register_block_type([
	// 		'name'              => 'events',
	// 		'title'             => __('Events', 'webmernok'),
	// 		'description'       => __('Events block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'groups',
	// 		],
	// 		'keywords'          => [ 'events', 'események' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Map Block.
	// 	acf_register_block_type([
	// 		'name'              => 'map',
	// 		'title'             => __('Map', 'webmernok'),
	// 		'description'       => __('Map block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'location',
	// 		],
	// 		'keywords'          => [ 'map', 'térkép', 'pin' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Testimonials Block.
	// 	acf_register_block_type([
	// 		'name'              => 'testimonials',
	// 		'title'             => __('Testimonials', 'webmernok'),
	// 		'description'       => __('Testimonials block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'format-status',
	// 		],
	// 		'keywords'          => [ 'testimonials', 'vélemények', 'review' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Services Block.
	// 	acf_register_block_type([
	// 		'name'              => 'services',
	// 		'title'             => __('Services', 'webmernok'),
	// 		'description'       => __('Services block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'admin-generic',
	// 		],
	// 		'keywords'          => [ 'services', 'szolgáltatások' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Contact Block.
	// 	acf_register_block_type([
	// 		'name'              => 'contact',
	// 		'title'             => __('Contact', 'webmernok'),
	// 		'description'       => __('Contact block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'phone',
	// 		],
	// 		'keywords'          => [ 'Contact', 'kapcsolat', 'elérhetőség' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Slider Block.
	// 	acf_register_block_type([
	// 		'name'              => 'sliders',
	// 		'title'             => __('Slider', 'webmernok'),
	// 		'description'       => __('Slider Block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'image-flip-horizontal',
	// 		],
	// 		'keywords'          => [ 'sliders', 'vetítés', 'csúszka' ],
	// 		'supports'			=> [
	// 			'align' 		=> true,
	// 			'mode' 			=> false,
	// 			'multiple'		=> true,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);

	// 	// Register a Cards Block.
	// 	acf_register_block_type([
	// 		'name'              => 'cards',
	// 		'title'             => __('Cards', 'webmernok'),
	// 		'description'       => __('Cards Block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'admin-page',
	// 		],
	// 		'keywords'      	=> [ 'cards', 'kártya', 'lista' ],
	// 		'supports'			=> [
	// 			'align' 		=> true,
	// 			'mode' 			=> false,
	// 			'multiple'		=> true,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// Register a Code Block.
	// 	acf_register_block_type([
	// 		'name'              => 'code',
	// 		'title'             => __('Code', 'webmernok'),
	// 		'description'       => __('Code Block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'editor-code',
	// 		],
	// 		'keywords'          => [ 'code', 'kód', 'shortcode' ],
	// 		'supports'			=> [
	// 			'align' 		=> true,
	// 			'mode' 			=> false,
	// 			'multiple'		=> true,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);

	// 	// Register a Team Block.
	// 	acf_register_block_type([
	// 		'name'              => 'team',
	// 		'title'             => __('Team', 'webmernok'),
	// 		'description'       => __('Team Block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'businessman',
	// 		],
	// 		'keywords'          => [ 'team', 'csapat', 'tag' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit',
	// 	]);


	// 	// Register an Accordion Block.
	// 	acf_register_block_type([
	// 		'name'              => 'accordion',
	// 		'title'             => __('Accordion'),
	// 		'description'       => __('Accordion Block'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'feedback',
	// 		],
	// 		'keywords'          => [ 'accordion', 'faq', 'gyik' ],
	// 		'supports'			=> [
	// 			'align' 		=> false,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);


	// 	// register a Feedback Block.
	// 	acf_register_block_type([
	// 		'name'              => 'feedbacks',
	// 		'title'             => __('Feedback', 'webmernok'),
	// 		'description'       => __('Feedback Block', 'webmernok'),
	// 		'render_callback'   => 'custom_acf_block_render_callback',
	// 		'category'          => 'webmernok',
	// 		'icon'              => [
	// 			'foreground' 	=> $theme_color,
	// 			'src'		 	=> 'format-status',
	// 		],
	// 		'keywords'          => [ 'visszajelzés', 'feedback', 'vélemény' ],
	// 		'supports'			=> [
	// 			'align' 		=> true,
	// 			'mode' 			=> false,
	// 			'multiple'		=> false,
	// 		],
	// 		'mode'				=> 'edit'
	// 	]);

	// }


	// function custom_acf_block_render_callback( $block ) {

	// 	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	// 	$slug = str_replace('acf/', '', $block['name']);

	// 	// include a template part from within the "partials/acf-blocks" folder
	// 	if( file_exists(STYLESHEETPATH . "/parts/blocks/core/{$slug}.php") ) {
	// 		include( STYLESHEETPATH . "/parts/blocks/core/{$slug}.php" );
	// 	}
	// }


	// /*	ACF Theme Options
	// -----------------------------------------------*/
    // add_action('acf/init', 'my_acf_init');
    
    // function my_acf_init() {
    
    //     if( function_exists('acf_add_options_page') ) {

	// 		if ( current_user_can( 'administrator' ) ) {

	// 			$parent = acf_add_options_page([
	// 				'page_title' 	=> 'Weboldal Beállítások',
	// 				'menu_title' 	=> 'Weboldal Beállítások',
	// 				'menu_slug' 	=> 'weboldal-beallitasok',
	// 				'capability'	=> 'edit_posts',
	// 				'redirect' 		=> false
	// 			]);
				
	// 		}
	// 	}
		
	// 	acf_update_setting('google_api_key', 'AIzaSyBSd0CdX3imQHJISyzLuyFIHaAbpDwJX5Y');
	// }