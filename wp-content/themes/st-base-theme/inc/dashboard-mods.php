<?php

	// Remove some dashboard menu for non administrators
	add_action('admin_menu', function () {
    if (current_user_can('administrator')) {
      return;
    }

		remove_menu_page('index.php'); 
		remove_menu_page( 'edit-comments.php' ); 
		//remove_menu_page( 'edit.php' ); 							
		//remove_menu_page( 'edit.php?post_type=page' ); 			
		remove_menu_page( 'themes.php' ); 		
		//remove_menu_page( 'profile.php' );		
		//remove_menu_page( 'upload.php' ); 		
		remove_menu_page('plugins.php');			
		//remove_menu_page( 'users.php' );		
		remove_menu_page('options-general.php');			
		remove_menu_page('tools.php');			
		remove_menu_page('rank-math');			
		remove_menu_page('edit.php?post_type=acf-field-group');
		remove_menu_page('duplicator-pro');
		remove_menu_page('postman');
		remove_menu_page('maintance');
	}, 999);
	