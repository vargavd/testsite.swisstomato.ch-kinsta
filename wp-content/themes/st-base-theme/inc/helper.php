<?php

// DEBUG
function vd1($var) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    var_dump($var);
    echo "\n</pre>\n";
}
function vd2($var1, $var2) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    var_dump($var1);
    var_dump($var2);
    echo "\n</pre>\n";
}
function vd3($var1, $var2, $var3) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    var_dump($var1);
    var_dump($var2);
    var_dump($var3);
    echo "\n</pre>\n";
}
function pr1($var) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    print_r($var);
    echo "\n</pre>\n";
    
}

function pr1_logged_in($var) {
  if (!is_user_logged_in()) {
    return;
  }
  
  echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
  print_r($var);
  echo "\n</pre>\n";
  
}
function pr2($var1, $var2) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    print_r($var1);
    echo "\n";
    print_r($var2);
    echo "\n</pre>\n";
}
function pr3($var1, $var2, $var3) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    print_r($var1);
    echo "\n";
    print_r($var2);
    echo "\n";
    print_r($var3);
    echo "\n</pre>\n";
}
function pr4($var1, $var2, $var3, $var4) {
    echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
    print_r($var1);
    echo "\n";
    print_r($var2);
    echo "\n";
    print_r($var3);
    echo "\n";
    print_r($var4);
    echo "\n</pre>\n";
}


function get_current_url() {
    return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}


function is_mobile()
{
	return (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
		'|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
		'|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);
}


function convert_string_to_kebab_case($s) {
  $s = str_replace(' ', '-', strtolower($s));

  $chars_to_change = [
    ['ö', 'o'],
    ['ó', 'o'],
    ['ő', 'o'],
    ['ô', 'o'],
    ['ü', 'u'],
    ['ú', 'u'],
    ['ù', 'u'],
    ['ű', 'u'],
    ['û', 'u'],
    ['é', 'e'],
    ['è', 'e'],
    ['ê', 'e'],
    ['ë', 'e'],
    ['á', 'a'],
    ['à', 'a'],
    ['ä', 'a'],
    ['â', 'a'],
    ['ß', 's'],
    ['í', 'i'],
    ['î', 'i'],
    ['ï', 'i'],
    ['ü', 'u'],
    ['ç', 'c']
  ];

  foreach ($chars_to_change as $char_array) {
    $s = str_replace($char_array[0], $char_array[1], $s);
  }

  return $s;
}


function acf_to_img_url($acf_img) {
  if (is_string($acf_img)) {
    return $acf_img;
  }

  if (is_integer($acf_img)) {
    return wp_get_attachment_url($acf_img);
  }

  if (is_array($acf_img) && array_key_exists('url', $acf_img)) {
    return $acf_img['url'];
  }

  return '';
}


function get_img_url_webp_if_possible($acf_img)
{
  $url = acf_to_img_url($acf_img);

	// WebP for Only non Safari browsers
	$webp = '.webp';
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	if (stripos($user_agent, 'Chrome') !== false) {
	} elseif (stripos($user_agent, 'Safari') !== false) {
		$webp = '';
	}

	return $url . $webp;
}


function get_img($img, $alt_param = '', $class = '', $lazy = true, $set_width = true) {
  if (is_integer($img)) {
		$img_id = $img;
	} else if (is_array($img)) {
		$img_id = $img['ID'];
	} else {
		$img_id = attachment_url_to_postid($img);
	}

  // ALT TEXT
  $alt = (is_array($img) && array_key_exists('alt', $img)) ? 
          $img['alt'] : 
          get_post_meta($img_id, '_wp_attachment_image_alt', TRUE);

  $alt = (!$alt) ? $alt_param : $alt;

  $alt = str_replace("'", "&#39;", $alt);

  // URL AND SIZE
  $img_info = wp_get_attachment_image_src($img_id, 'full');
	$url 	= $img_info[0];
	$width_attr = $set_width ? ("width='" . $img_info[1] . "'") : '';
	$height 	= $img_info[2];

  
  // if (stripos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== false && strpos($url, '.svg') === false) {
	// 	$url = "$url.webp";
	// }

  // webp
  $source_url = (stripos($url, '.svg') !== false) ? $url : "$url.webp";
  // $source_url = $url;

  // lazy loading
  $lazy = $lazy ? 'loading="lazy"' : '';

  // Srcset support
	$srcset = wp_get_attachment_image_srcset($img_id);

  return "<picture>
    <source srcset='$source_url' type=image/webp>
    <img $lazy src='$url' $width_attr  height='$height' alt='$alt' class='$class'>
  </picture>";
}

function render_acf_link($acf_link, $class = '', $icon_class = '') {
  // acf link stuff
  $url = $acf_link['url'];
  $target = $acf_link['target'];
  $title = $acf_link['title'];

  // add icon if defined
  $icon = empty($icon_class) ? '' : "<i class='$icon_class'></i>";


  return "<a href='$url' class='$class' target='$target'> $icon $title </a>";
}

function st_get_references_link($platform = '')
{
	$url = get_post_type_archive_link('reference');

	if (!empty($platform)) {
		$url .= '#platform=' . $platform;
	}

	return $url;
}