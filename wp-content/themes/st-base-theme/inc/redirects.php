<?php

/**
 * url_part_to_check: string, url part to check if the current url contains it
 * $only_if_anonymous: boolean (default: FALSE)
 */
function st_redirect_helper_func($url_part_to_check, $url_part_to_redirect, $only_if_anonymous = false)
{
  $current_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  if (strpos($current_url, $url_part_to_check) !== false && (!$only_if_anonymous || !is_user_logged_in())) {
    wp_redirect(str_replace($url_part_to_check, $url_part_to_redirect, $current_url), 301);
    exit;
  }
}

// adding custom redirects
add_action('template_redirect', function () {
  $current_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  $home_en_url = '/en/';
  $home_fr_url = '';
  $home_de_url = '/de/';

  $appdev_en_url = '/en/app-development-geneva-zurich/';
  $appdev_fr_url = '/creer-une-application/';
  $appdev_de_url = '/de/app-entwickeln-zurich/';

  $webdev_en_url = '/en/webdesign-geneva-zurich/';
  $webdev_fr_url = '/agence-web-geneve/';
  $webdev_de_url = '/de/webdesign-zurich/';

  $arvr_en_url = '/en/augmented-reality-virtual-reality-app-development/';
  $arvr_fr_url = '/applications-de-realite-augmentee-et-de-realite-virtuelle/';
  $arvr_de_url = '/de/app-entwicklung-augmented-reality-und-virtual-reality/';


  // redirect post list by query param
  //st_redirect_helper_func('?post_type=post', '');
  //st_redirect_helper_func('?post_type=page', '');
  //st_redirect_helper_func('?post_type=reference', '');


  /***404 REDIRECTS***/
  //to home page
  //st_redirect_helper_func('/en/2020/06/', $home_en_url);
  //st_redirect_helper_func('/2020/06/', $home_fr_url);
  //st_redirect_helper_func('/de/2020/06/', $home_de_url);

  //st_redirect_helper_func('/en/?post_type=reference&p=4895', $home_en_url);
  //st_redirect_helper_func('/digital-marketing-adwords-seo/', $home_en_url);


  //to webdev
  //st_redirect_helper_func('/en/references/aventure-linguistique-website-development-and-online-communication/', $webdev_en_url);
  //st_redirect_helper_func('/en/references/leica-adwords-campaign-management-webdesign/', $webdev_en_url);
  //st_redirect_helper_func('/en/references/braun-pg-banner-development/', $webdev_en_url);
  //st_redirect_helper_func('/en/portfolio/jobup-mobile-site-development/', $webdev_en_url);

  //st_redirect_helper_func('/creation-site-internet-suisse-lausanne-geneve/embed/', $webdev_fr_url);

  //st_redirect_helper_func('/de/referenzen/leica-adwords-kampagnen-management-website-entwicklung/', $webdev_de_url);
  //st_redirect_helper_func('/de/referenzen/braun-p-g-banner-website-entwicklung/', $webdev_de_url);
  //st_redirect_helper_func('/de/referenzen/entering-to-swiss-market-and-raise-brand-awareness/', $webdev_de_url);
  //st_redirect_helper_func('/de/referenzen/braun-p-g-banner-entwicklung/', $webdev_de_url);


  //to appdev
  //st_redirect_helper_func('/en/app-development-geneva-switzerland/google-adwords-search-marketing/', $appdev_en_url);
  //st_redirect_helper_func('/en/2020/06/11/app-development-cost-in-2020/', $appdev_en_url);

  //st_redirect_helper_func('/developper-une-application-combien-ca-coute/', $appdev_fr_url);
  //st_redirect_helper_func('/fr/developpement-application-geneve-suisse/', $appdev_fr_url);
  //st_redirect_helper_func('/references/pic2real-developpement-application-mobile/', $appdev_fr_url);

  //st_redirect_helper_func('/de/referenzen/pic2real-mobile-app-entwicklung/', $appdev_de_url);
  //st_redirect_helper_func('/de/portfolio/hafenfuhrer-schweiz-franzosische-riviera/', $appdev_de_url);


  //to arvr
  //st_redirect_helper_func('/de/web-ar/https://logan-5.de/web-arExpeditionen', $arvr_de_url);
  //st_redirect_helper_func('/de/app-entwicklung/social-media-ein-marketinginstrument-fur-kleine-und-mittlere-unternehmen/', $arvr_de_url);
  //st_redirect_helper_func('/de/app-development-zurich-switzerland/embed/', $arvr_de_url);




  /***NORMAL REDIRECTS***/
  //DIGITAL MARKETING STRATEGY EN
  //st_redirect_helper_func('/en/digital-marketing-strategy/', $home_en_url);

  //DIGITAL MARKETING STRATEGY FR
  //st_redirect_helper_func('/strategie-de-marketing-digital/', $home_fr_url);

  //DIGITAL MARKETING STRATEGY DE
  //st_redirect_helper_func('/de/digital-marketing-strategie/', $home_de_url);


  //DIGITAL MARKETING REDESIGN EN
  //st_redirect_helper_func('/en/digital-marketing-redesign/', $home_en_url);


  //DIGITAL MARKETING EN
  //st_redirect_helper_func('/en/digital-marketing/', $home_en_url);

  //DIGITAL MARKETING FR
  //st_redirect_helper_func('/marketing-digital/', $home_fr_url);

  //DIGITAL MARKETING DE
  //st_redirect_helper_func('/de/digitales-marketing/', $home_de_url);


  //EMAIL MARKETING EN
  //st_redirect_helper_func('/en/email-marketing/', $home_en_url);

  //EMAIL MARKETING FR
  //st_redirect_helper_func('/email-marketing/', $home_fr_url);

  //EMAIL MARKETING DE
  //st_redirect_helper_func('/de/email-marketing/', $home_de_url);


  //GOOGLE ADS EN
  //st_redirect_helper_func('/en/google-ads/', $home_en_url);

  //GOOGLE ADS FR
  //st_redirect_helper_func('/annonces-google/', $home_fr_url);

  //GOOGLE ADS DE
  //st_redirect_helper_func('/de/google-ads/', $home_de_url);


  // SEO EN
  //st_redirect_helper_func('/en/app-development-geneva-zurich/search-engine-optimisation-small-mid-sized-companies/', $webdev_en_url);

  // SEO FR
  //st_redirect_helper_func('/creer-une-application/le-referencement-seo-pour-les-pme/', $webdev_fr_url);

  // SEO DE
  //st_redirect_helper_func('/de/app-entwickeln-zurich/suchmaschinenoptimierung-fur-kleine-und-mittlere-unternehmen/', $webdev_de_url);


  // SMARTWATCH EN
  //st_redirect_helper_func('/en/app-development-geneva-zurich/smartwatch-app-development-agency/', $appdev_en_url);

  // SMARTWATCH FR
  //st_redirect_helper_func('/creer-une-application/agence-de-developpement-dapplications-pour-smartwatch/', $appdev_fr_url);

  // SMARTWATCH DE
  //st_redirect_helper_func('/de/app-entwickeln-zurich/agentur-fur-smartwatch-appentwicklung/', $appdev_de_url);


  // UNITY EN
  //st_redirect_helper_func('/en/app-development-geneva-zurich/unity-development-agency-services/', $appdev_en_url);

  // UNITY FR
  //st_redirect_helper_func('/creer-une-application/services-de-developpement-en-agence-avec-unity/', $appdev_fr_url);

  // UNITY DE
  //st_redirect_helper_func('/de/app-entwickeln-zurich/agentur-fuer-unity-entwicklung/', $appdev_de_url);


  // WEBDEV SMALL COMPANIES EN
  //st_redirect_helper_func('/en/webdesign-geneva-zurich/website-design-small-companies/', $webdev_en_url);

  // WEBDEV SMALL COMPANIES FR
  //st_redirect_helper_func('/agence-web-geneve/le-web-design-pour-les-petites-entreprises/', $webdev_fr_url);

  // WEBDEV SMALL COMPANIES DE
  //st_redirect_helper_func('/de/webdesign-zurich/webdesign-fur-kleine-unternehmen/', $webdev_de_url);


  // e-Commerce EN
  //st_redirect_helper_func('/en/webdesign-geneva-zurich/ecommerce-development-small-mid-sized-companies/', $webdev_en_url);

  // e-Commerce FR
  //st_redirect_helper_func('/agence-web-geneve/creation-de-boutiques-en-ligne-pour-les-pme/', $webdev_fr_url);

  // e-Commerce DE
  //st_redirect_helper_func('/de/webdesign-zurich/entwicklung-von-onlineshops-fur-kleine-und-mittelstandische-unternehmen/', $webdev_de_url);


  // Web AR EN
  //st_redirect_helper_func('/en/web-ar/', $arvr_en_url);

  // Web AR FR
  //st_redirect_helper_func('/web-ar/', $arvr_en_url);

  // Web AR DE
  //st_redirect_helper_func('/de/web-ar/', $arvr_en_url);


  // OLD ARVR (with the 2 in the link) TO NEW (without the 2)
  //st_redirect_helper_func('/applications-de-realite-augmentee-et-de-realite-virtuelle-2/', $arvr_fr_url);


  //   #region [colored] Web page 301 redirect (2021.07)

  //   $web_pages_redirections = array();

  //   // Web EN
  //   array_push($web_pages_redirections, array('https://swisstomato.ch/en/website-development-geneva-switzerland','https://swisstomato.ch/en/webdesign-geneva-zurich'));

  //   // Web FR
  //   array_push($web_pages_redirections, array('https://swisstomato.ch/creation-site-internet-suisse-lausanne-geneve','https://swisstomato.ch/agence-web-geneve'));

  //   // Web DE
  //   array_push($web_pages_redirections, array('https://swisstomato.ch/de/webdesign-genf-schweiz','https://swisstomato.ch/de/webdesign-zurich'));

  //   foreach ($web_pages_redirections as $redirection) {
  //       if ($redirection[0] === rtrim($current_url, '/')) {
  //           wp_redirect($redirection[1], 301);
  //           exit;
  //       }
  //   }

  //   #endregion

  //   #region [colored] App page 301 redirect (2021.07)

  //   $app_pages_redirections = array();

  //   // App EN
  //   array_push($app_pages_redirections, array('https://swisstomato.ch/en/app-development-geneva-switzerland', 'https://swisstomato.ch/en/app-development-geneva-zurich/'));

  //   // App FR
  //   array_push($app_pages_redirections, array('https://swisstomato.ch/developpement-application-geneve-suisse', 'https://swisstomato.ch/creer-une-application/'));

  //   // App DE
  //   array_push($app_pages_redirections, array('https://swisstomato.ch/de/app-development-zurich-switzerland', 'https://swisstomato.ch/de/app-entwickeln-zurich/'));

  //   foreach ($app_pages_redirections as $redirection) {
  //       if ($redirection[0] === rtrim($current_url, '/')) {
  //           wp_redirect($redirection[1], 301);
  //           exit;
  //       }
  //   }

  //   #endregion

  //   if (strpos($current_url, 'references-reorder') !== false && !is_user_logged_in()) {
  //     wp_redirect(get_home_url(), 301);
  //     exit;
  //   }

  //   if (strpos($current_url, 'portfolio-category/agence-web-geneve') !== false) {
  //       wp_redirect('https://swisstomato.ch', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, 'en/our-portfolio') !== false) {
  //       wp_redirect('https://swisstomato.ch/en/references', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, '/portfolio-fr') !== false) {
  //       wp_redirect('https://swisstomato.ch/references/', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, '/portfolio-de') !== false) {
  //       wp_redirect('https://swisstomato.ch/de/referenzen/', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, 'fr/developpement-dapplications-mobiles/utiliser-google-adwords-quand-on-est-une-pme-en-suisse/') !== false) {
  //       wp_redirect('https://swisstomato.ch/marketing-digital/', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, 'de/app-entwicklung/swisstomato-chdeapp-entwicklungapp-entwicklung-augmented-reality-und-virtual-reality/') !== false) {
  //       wp_redirect('https://swisstomato.ch/de/app-entwicklung/app-entwicklung-augmented-reality-und-virtual-reality/');
  //       exit;
  //   }

  //   if (strpos($current_url, 'de/app-entwicklung/swisstomato-chdeapp-entwicklungapp-entwicklung-augmented-reality-und-virtual-reality/') !== false) {
  //       wp_redirect('https://swisstomato.ch/de/app-entwicklung/app-entwicklung-augmented-reality-und-virtual-reality/');
  //       exit;
  //   }

  //   if (strpos($current_url, 'de/mobile-app-entwicklung/google-adwords-anzeigen-fuer-kleine-und-mittelstaendische-unternehmen-in-der-schweiz-nutzen/') !== false) {
  //       wp_redirect('https://swisstomato.ch/de/app-entwicklung/google-adwords-suchmaschinenmarketing/');
  //       exit;
  //   }

  //   if (strpos($current_url, 'de/mobile-app-entwicklung/suchmaschinenoptimierung-fuer-kleine-und-mittlere-unternehmen/') !== false) {
  //       wp_redirect('https://swisstomato.ch/de/app-entwicklung/suchmaschinenoptimierung-fur-kleine-und-mittlere-unternehmen/');
  //       exit;
  //   }

  //   if (strpos($current_url, 'uploads/2017/02/blog_bestswiss') !== false) {
  //       wp_redirect('https://swisstomato.ch/wp-content/uploads/2018/12/Swiss-tomato-logo.png');
  //       exit;
  //   }

  //   if (strpos($current_url, 'magic-cube-ar-app-android') !== false) {
  //       wp_redirect('https://play.google.com/store/apps/details?id=com.swisstomato.magiccubear', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, 'magic-cube-ar-app-ios') !== false) {
  //       wp_redirect('https://apps.apple.com/us/app/ar-magic-cube/id1469397965?l=hu&ls=1', 301);
  //       exit;
  //   }

  //   if (strpos($current_url, 'developpement-application-mobiles-suisse-lausanne-geneve/application-devenement') !== false) {
  //     // original url: https://swisstomato.ch/developpement-application-mobiles-suisse-lausanne-geneve/application-devenement/
  //     $current_url = str_replace('developpement-application-mobiles-suisse-lausanne-geneve/application-devenement', 'creer-une-application', $current_url);
  //     wp_redirect($current_url, 301);
  //     exit;
  //   }

  //   if (strpos($current_url, 'developpement-application-mobiles-suisse-lausanne-geneve/google-adwords-search-marketing') !== false) {
  //     // original url: https://swisstomato.ch/developpement-application-mobiles-suisse-lausanne-geneve/google-adwords-search-marketing/
  //     $current_url = str_replace('developpement-application-mobiles-suisse-lausanne-geneve/google-adwords-search-marketing', 'marketing-digital', $current_url);
  //     wp_redirect($current_url, 301);
  //     exit;
  //   }

  //   if (strpos($current_url, 'developpement-application-mobiles-suisse-lausanne-geneve/les-reseaux-sociaux-excellents-outils-de-marketing-pour-les-pme') !== false) {
  //     // original url: https://swisstomato.ch/developpement-application-mobiles-suisse-lausanne-geneve/les-reseaux-sociaux-excellents-outils-de-marketing-pour-les-pme/
  //     $current_url = str_replace('developpement-application-mobiles-suisse-lausanne-geneve/les-reseaux-sociaux-excellents-outils-de-marketing-pour-les-pme', 'medias-sociaux', $current_url);
  //     wp_redirect($current_url, 301);
  //     exit;
  //   }

  //   if (strpos($current_url, 'fr/developpement-application-mobiles-suisse-lausanne-geneve') !== false) {
  //     // original url: https://swisstomato.ch/fr/developpement-application-mobiles-suisse-lausanne-geneve/applications-de-realite-augmentee-et-de-realite-virtuelle-2/
  //     $current_url = str_replace('fr/developpement-application-mobiles-suisse-lausanne-geneve', 'creer-une-application', $current_url);
  //     $current_url = rtrim($current_url, '/') . '/';
  //     wp_redirect($current_url, 301);
  //     exit;
  // }

  //   if (strpos($current_url, 'developpement-application-mobiles-suisse-lausanne-geneve') !== false) {
  //       // original url: https://swisstomato.ch/developpement-application-mobiles-suisse-lausanne-geneve/applications-de-realite-augmentee-et-de-realite-virtuelle-2/
  //       $current_url = str_replace('developpement-application-mobiles-suisse-lausanne-geneve', 'creer-une-application', $current_url);
  //       $current_url = rtrim($current_url, '/') . '/';
  //       wp_redirect($current_url, 301);
  //       exit;
  //   }

  //   if (strpos($current_url, 'developpement-application-mobiles-suisse-lausanne-geneve/applications-de-realite-augmentee-et-de-realite-virtuelle-2') !== false) {
  //     // original url: developpement-application-mobiles-suisse-lausanne-geneve/applications-de-realite-augmentee-et-de-realite-virtuelle-2/
  //     $current_url = str_replace('developpement-application-mobiles-suisse-lausanne-geneve/applications-de-realite-augmentee-et-de-realite-virtuelle-2', 'applications-de-realite-augmentee-et-de-realite-virtuelle-2', $current_url);
  //     wp_redirect($current_url, 301);
  //     exit;
  //   }

  //   if ($current_url === 'https://swisstomato.ch/appdevelopment' || $current_url === 'https://swisstomato.ch/appdevelopment/') {
  //     // original url: https://swisstomato.ch/appdevelopment
  //     $current_url = str_replace('appdevelopment', 'creer-une-application', $current_url);
  //     wp_redirect($current_url);
  //     exit;
  //   }

  //   if ($current_url === 'https://swisstomato.ch/en/webdesign-genf-schweiz' || $current_url === 'https://swisstomato.ch/en/webdesign-genf-schweiz/') {
  //     // original url: https://swisstomato.ch/appdevelopment
  //     $current_url = str_replace('/en/webdesign-genf-schweiz', '/en/webdesign-geneva-zurich', $current_url);
  //     wp_redirect($current_url, 301);
  //     exit;
  //   }

  //   // if (strpos($current_url, 'de/app-entwicklung') !== false) {
  //   //     // original url: https://swisstomato.ch/de/app-entwicklung/
  //   //     $current_url = str_replace('de/app-entwicklung', 'de/app-development-zurich-switzerland', $current_url);
  //   //     wp_redirect($current_url);
  //   //     exit;
  //   // }

  if (is_category() || is_tag() || is_date() || is_tax() || is_author()) {
    wp_redirect(home_url(), 301);
    exit();
  }
});
