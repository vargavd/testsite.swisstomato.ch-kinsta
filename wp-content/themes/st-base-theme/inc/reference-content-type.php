<?php

// -----------------------------------------
// REGISTER REFERENCE POST TYPE AND TAXONOMIES
add_action( 'init', function () {
  register_taxonomy( 'reference_category', null, array(
    'labels' => array(
      'name'                       => 'Reference Categories',
      'singular_name'              => 'Reference Category',
      'search_items'               => 'Search Reference Categories',
      'all_items'                  => 'All Reference Categories',
      'parent_item'                => null,
      'parent_item_colon'          => null,
      'edit_item'                  => 'Edit Reference Category',
      'update_item'                => 'Update Reference Category',
      'add_new_item'               => 'Add New Reference Category',
      'new_item_name'              => 'New Reference Category Name',
      'separate_items_with_commas' => 'Separate reference category with commas',
      'add_or_remove_items'        => 'Add or remove reference category',
      'choose_from_most_used'      => 'Choose from the most used reference categories',
      'not_found'                  => 'No reference category found.',
      'menu_name'                  => 'Reference Categories',
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => false,
    //'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'reference_category' ),
    'show_in_rest'          => true
  ));

  register_taxonomy( 'reference_tag', null, array(
    'labels' => array(
      'name'                       => 'Reference Tags',
      'singular_name'              => 'Reference Tag',
      'search_items'               => 'Search Reference Tags',
      'all_items'                  => 'All Reference Tags',
      'parent_item'                => null,
      'parent_item_colon'          => null,
      'edit_item'                  => 'Edit Reference Tag',
      'update_item'                => 'Update Reference Tag',
      'add_new_item'               => 'Add New Reference Tag',
      'new_item_name'              => 'New Reference Tag Name',
      'separate_items_with_commas' => 'Separate reference tag with commas',
      'add_or_remove_items'        => 'Add or remove reference tag',
      'choose_from_most_used'      => 'Choose from the most used reference Tags',
      'not_found'                  => 'No reference tag found.',
      'menu_name'                  => 'Reference Tags',
    ),
    'hierarchical'          => true,
    'show_ui'               => true,
    'show_admin_column'     => false,
    //'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'reference_tag' ),
    'show_in_rest'          => true
  ));

  register_post_type( 'reference',
    array(
      'labels' => array(
        'name'               => 'References',
        'singular_name'      => 'Reference',
        'menu_name'          => 'References',
        'name_admin_bar'     => 'Reference', 
        'add_new'            => 'Add New', 'Reference',
        'add_new_item'       => 'Add New Reference',
        'new_item'           => 'New Reference',
        'edit_item'          => 'Edit Reference',
        'view_item'          => 'View Reference',
        'all_items'          => 'All References',
        'search_items'       => 'Search References',
        'parent_item_colon'  => 'Parent References',
        'not_found'          => 'No References found.',
        'not_found_in_trash' => 'No References found in Trash.',
      ),
      'public'         => true,
      'show_in_rest'  => true,
      'has_archive'    => true,
      'rewrite'        => array(
        'slug'       => 'references',
        'with_front' => true
      ),
      'query_var'      => 'casestudy',
      'taxonomies'     => array('reference_category', 'reference_tag'),
      'supports'       => array('title', 'editor', 'thumbnail'),
    )
  );
} );



// -----------------------------------------
// ADD COLUMNS TO REFERENCES ADMIN LIST
add_filter('manage_reference_posts_columns', function ($columns) {
  unset($columns['date']);
  unset($columns['rank_math_seo_details']);

  $columns['reference_category'] = 'Category';
  $columns['masonry_title'] = 'Masonry Title';
  $columns['masonry_text'] = 'Masonry Text';
  $columns['masonry_thumbnail'] = 'Masonry Thumbnail';

  return $columns;
});
add_action('manage_posts_custom_column', function ($column, $post_id) {
  switch ( $column ) {
    case 'reference_category':
      $category_objects = get_the_terms($ID, 'reference_category');
      $category_names = array();
      if (is_array($category_objects) && sizeof($category_objects) > 0) {
        foreach ($category_objects as $cat_object) {
          array_push($category_names, $cat_object->name);
        }

        echo implode(', ', $category_names);
      } else {
        echo "-";
      }
      break;
    case 'masonry_title':
      echo get_post_meta ( $post_id, 'masonry_reference_title', true );
      break;
    case 'masonry_text':
      echo get_post_meta ( $post_id, 'masonry_reference_text', true );
      break;
    case 'masonry_thumbnail':
      $img = get_field('masonry_thumbnail_image', $post_id);
      if (is_array($img)) {
        print "<img src='" . $img['url'] . "' alt='" . $img['title'] . "' style='max-width: 60px;'/>";
      }
      break;
  }
}, 10, 2 );



// -----------------------------------------
// ADD FIELDS TO REFERENCE REST ENDPOINT
add_filter( 'rest_prepare_reference', function ($data, $post, $context) {
  // // ADD featured image url to restapi
  $featured_image_id = $data->data['featured_media']; // get featured image id
  $featured_image_url = wp_get_attachment_image_src( $featured_image_id, 'original' ); // get url of the original size

  if( $featured_image_url ) {
      $data->data['featured_image_url'] = $featured_image_url[0];
  } else {
      $data->data['featured_image_url'] = get_stylesheet_directory_uri() . '/imgs/swisstomato.png';
  }


  // ADD category name (filter_text)
  $category_ids = $data->data['reference_category'];
  if (is_array($category_ids) && sizeof($category_ids) > 0) {
    $data->data['category_name'] = get_field('filter_text', "reference_category_" .$category_ids[0]);
  }

  
  // // ADD date string to restapi
  // $data->data['date_string'] = get_the_date("F d, o", $data->data['id']);

  return $data;
}, 10, 3 );



// -----------------------------------------
// UNLIMITED REFERENCES
add_action( 'pre_get_posts', function ( $query ) {
  if (is_admin() || $query->is_singular() || !$query->is_main_query()) {
      return;
  }

  if (is_post_type_archive( 'reference' )) {
      $query->set( 'posts_per_page', '-1' );
      return;
  }
});



// -----------------------------------------
// ALTER REFERENCE QUERY VARS
//   * add ordering
//   * allow only references with masonry image
add_action( 'pre_get_posts', function ( $query ) {
	if ( is_admin() || ! $query->is_main_query() || !isset($query->query['post_type']) || $query->query['post_type'] !== 'reference' || !is_post_type_archive('reference')) {
		return;
	}

  // ORDERING
  $query->set('orderby', 'meta_value_num');
  $query->set('meta_key', 'no_category_order');
  $query->set('order', 'ASC');

  // ONLY REFERENCES WHERE MASONRY IMAGE IS FILLED
  $query->set('meta_query', array(
    'key' => 'masonry_thumbnail_image',
    'compare' => 'NOT IN',
    'value' => ''
  ));
}, 1 );



// -----------------------------------------
// ALTER REFERENCE QUERY VARS FOR REST API
//   * add ordering
//   * allow only references with masonry image
add_filter( 'rest_reference_query', function ($query_vars) {
  // ORDERING
  $query_vars["orderby"] = "meta_value_num";
  $query_vars["order"] = "ASC";
  $query_vars["meta_key"] = "no_category_order";

  if (array_key_exists('reference_category', $_GET)) {
    if ($_GET['reference_category'] === '12' || $_GET['reference_category'] === '75' || $_GET['reference_category'] === '76') {
      $query_vars["meta_key"] = "app_category_order";
    }

    if ($_GET['reference_category'] === '14' || $_GET['reference_category'] === '79' || $_GET['reference_category'] === '80') {
      $query_vars["meta_key"] = "arvr_category_order";
    }

    if ($_GET['reference_category'] === '13' || $_GET['reference_category'] === '77' || $_GET['reference_category'] === '78') {
      $query_vars["meta_key"] = "web_category_order";
    }
  }

  // ONLY REFERENCES WHERE MASONRY IMAGE IS FILLED
  $query_vars['meta_query'] = array(
    'key' => 'masonry_thumbnail_image',
    'compare' => 'NOT IN',
    'value' => ''
  );

  return $query_vars;
}, 10, 2);