<?php

function update_reference_orders($req) {
  $response = array();

  $response['order_field'] = $req['order_field'];
  $response['reference_infos'] = $req['reference_infos'];

  foreach ($response['reference_infos'] as $reference_info) {
    update_field($response['order_field'], $reference_info['position'], $reference_info['id']);
  }
  

  return array(
    'original' => $response
  );
}

add_action('rest_api_init', function () {
  register_rest_route( 'references/v1', 'reorder', array(
      'methods' => 'POST',
      'callback' => 'update_reference_orders'
  ));
});
