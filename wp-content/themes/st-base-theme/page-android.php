<?php

/**
 * Template Name: Android Page
 *
 * Page template for Android.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

<div class="stbase-page stbase-android-page no-padding-top">
    <?php
    while (have_posts()) {
        the_post();

        the_content();
    }
    ?>
</div>

<?php
get_footer();
