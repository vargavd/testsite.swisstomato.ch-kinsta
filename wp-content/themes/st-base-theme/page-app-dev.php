<?php

/**
 * Template Name: App Dev
 *
 * Template for the app dev page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-appdev-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
