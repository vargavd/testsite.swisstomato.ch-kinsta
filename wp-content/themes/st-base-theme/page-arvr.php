<?php

/**
 * Template Name: ARVR Page
 *
 * Page template with the ARVR page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-arvr-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
