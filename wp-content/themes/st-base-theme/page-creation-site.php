<?php

/**
 * Template Name: Creation Site
 *
 * Template for the creation site page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-creation-site-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
