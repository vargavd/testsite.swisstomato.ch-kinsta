<?php

/**
 * Template Name: Discovery & Planning Process
 *
 * Template for the Discovery & Planning Process page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

<div class="stbase-page stbase-discovery-process-page new no-padding-top">
    <?php
    while (have_posts()) {
        the_post();

        the_content();
    }
    ?>
</div>

<?php
get_footer();