<?php

/**
 * Template Name: Drupal Page
 *
 * Page template for Drupa.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

<div class="stbase-page stbase-drupal-page no-padding-top">
    <?php
    while (have_posts()) {
        the_post();

        the_content();
    }
    ?>
</div>

<?php
get_footer();
