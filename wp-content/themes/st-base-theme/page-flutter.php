<?php

/**
 * Template Name: Flutter Page
 *
 * Page template for Flutter.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

<div class="stbase-page stbase-flutter-page no-padding-top">
    <?php
    while (have_posts()) {
        the_post();

        the_content();
    }
    ?>
</div>

<?php
get_footer();
