<?php

/**
 * Template Name: Home Page
 *
 * Page template with homepage.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-home-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
