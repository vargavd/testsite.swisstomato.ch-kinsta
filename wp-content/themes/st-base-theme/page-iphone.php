<?php

/**
 * Template Name: iPhone Page
 *
 * Page template for iPhone.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-iphone-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
