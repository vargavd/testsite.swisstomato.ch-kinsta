<?php

/**
 * Template Name: NGO
 *
 * Template for the NGO page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

<div class="stbase-page stbase-ngo-page new no-padding-top">
    <?php
    while (have_posts()) {
        the_post();

        the_content();
    }
    ?>
</div>

<?php
get_footer();
