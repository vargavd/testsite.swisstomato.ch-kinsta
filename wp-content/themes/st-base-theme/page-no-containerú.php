<?php

/**
 * Template Name: No Container Page
 *
 * Page template with no container.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-webdev-page">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
