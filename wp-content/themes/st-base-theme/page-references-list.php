<?php
/**
 * Template Name: References List Page
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$url = "http://$_SERVER[HTTP_HOST]" . strtok($_SERVER["REQUEST_URI"], '?');

$lang_slug = get_bloginfo('language');
?>

	<div class="swisst2-page container references-reorder">

    <h1><?php the_title(); ?></h1>

    <div class="references" data-order-field="<?=$current_order_field?>" style="margin-top: 60px;">
      <?php
        // pr2($current_order_field, $current_category_slug);
        
        $get_posts_args = array(
          'posts_per_page' => -1,
          'post_type' => 'reference',
          'suppress_filters' => 0,
          // 'meta_key'  => 'no_category_order',
          // 'orderby'   => 'meta_value_num',
          // 'order'     => 'ASC'
        );

        $references = get_posts($get_posts_args);

        foreach ($references as $reference):
          $image = get_field('masonry_thumbnail_image', $reference->ID);

          if (empty($image)) {
            $image = get_the_post_thumbnail_url($reference->ID, 'full');
          }

          $masonry_title = get_field('masonry_reference_title', $reference->ID);
          $masonry_text = get_field('masonry_reference_text', $reference->ID);

          $title = (empty($masonry_title)) ? 
            $reference->post_title : 
            "$masonry_title - <small>$masonry_text</small>";
      ?>
        <div class="reference" data-id="<?=$reference->ID?>">
          <div class="img-wrapper" style="max-width: 40px; max-height: 40px; float: left; margin-right: 10px;">
            <?=get_img($image)?>
          </div>

          <h2>
            <?=$title?>
          </h2>
        </div>
      <?php endforeach; ?>
    </div>
		
	</div><!-- #references-reorder-page -->

<?php
//get_sidebar();
get_footer();
