<?php

/**
 * Template Name: Startup
 *
 * Template for the startup page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

<div class="stbase-page stbase-startup-page new no-padding-top">
    <?php
    while (have_posts()) {
        the_post();

        the_content();
    }
    ?>
</div>

<?php
get_footer();
