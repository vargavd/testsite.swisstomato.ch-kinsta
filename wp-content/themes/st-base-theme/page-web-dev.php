<?php

/**
 * Template Name: Web Dev
 *
 * Template for the app dev page.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-webdev-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
