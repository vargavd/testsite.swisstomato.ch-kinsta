<?php

/**
 * Template Name: Webapp Page
 *
 * Page template for Webapp.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-webapp-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
