<?php

/**
 * Template Name: WordPress Page
 *
 * Page template for WordPress.
 *
 * @package ST_Base_Theme
 */

get_header();
?>

	<div class="stbase-page stbase-wordpress-page no-padding-top">
    <?php
      while ( have_posts() ) {
        the_post();

        the_content();
      }
    ?>
	</div>

<?php
get_footer();
