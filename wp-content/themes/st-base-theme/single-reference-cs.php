<?php

/**
 * Template Name: Reference - Case Study
 * Template Post Type: reference
 * The template for displaying referencers with case study layout
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Swisstomato2
 */

get_header();
?>

<div class="st-reference-page st-reference-cs-page stbase-page">

    <div id="page-content">
        <?php
        while (have_posts()) :
            the_post();

            $ID = get_the_ID();

            $featured_img_class = '';

            $categories = wp_get_post_terms($post->ID, 'reference_category');

            $tags = wp_get_post_terms($post->ID, 'reference_tag');

            $reference_archive_url = get_post_type_archive_link('reference');

            if (get_field('title_black', $ID)) {
                $featured_img_class = 'black-text';
            }

            $featured_img_url = get_the_post_thumbnail_url($post, 'full');
            $featured_mobile_img = get_field('mobile_hero_image', $ID);

            if (empty($featured_img_url)) {
                $featured_img_url = get_stylesheet_directory_uri() . "/imgs/swisstomato.png";
                $featured_img_class .= ' default-image';
            }

            if (empty($featured_mobile_img)) {
                $featured_mobile_img = $featured_img_url;
            }

            $logo_url = get_field('logo', $ID);

            $main_color = get_field('main_color', $ID);

            $shadow_type = get_field('shadow_type', $ID);

            $custom_shadow_color = get_field('custom_shadow_color', $ID);

        ?>

            <article id="swisst2-reference">
                <div class="reference-hero">
                    <div class="reference-hero-overlay <?= $shadow_type ?>" <?php if ($shadow_type == 'maincolor') { ?> style="background: linear-gradient(180deg, rgba(233, 174, 123, 0.00) 41.15%, <?= $main_color ?> 100%);" <?php } elseif ($shadow_type == 'custom') { ?> style="background: linear-gradient(180deg, rgba(233, 174, 123, 0.00) 41.15%, <?= $custom_shadow_color ?> 100%);" <?php } ?>>
                    </div>
                    <div id="reference-title-client" class="<?= empty($client_name) ? 'only-title' : '' ?>">
                        <?php
                        $overriden_title = get_field('title_displayed', $ID);

                        $displayed_title = !empty($overriden_title) ? $overriden_title : get_the_title();
                        ?>
                        <div class="reference-type"><?php _e('Case Study', 'stbase'); ?></div>
                        <h1 class="<?= strlen($displayed_title) > 40 ? 'over-40' : '' ?>">
                            <?= $displayed_title ?>
                        </h1>
                    </div>

                    <?= get_img($featured_img_url, 'Feature Reference Image', 'feature-reference-image') ?>
                    <?= get_img($featured_mobile_img, 'Feature Reference Image', 'feature-reference-image mobile') ?>
                </div>

                <div id="reference-content-row">
                    <div id="reference-body" class="reference-case-study">
                        <?php
                        the_content();
                        ?>
                    </div>
                </div>
            </article><!-- #post-<?php the_ID(); ?> -->


        <?php
        endwhile; // End of the loop.
        ?>
    </div> <!-- /#page-content -->

</div><!-- /#swisst2-blogpost-page -->

<?php
get_footer();
