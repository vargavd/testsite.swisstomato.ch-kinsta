<?php

/**
 * The template for displaying all single references
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Swisstomato2
 */

get_header();
?>

<div class="st-reference-page stbase-page container-1250">

  <div id="page-content">
    <?php
    while (have_posts()) :
      the_post();

      $ID = get_the_ID();

      $featured_img_class = '';

      $categories = wp_get_post_terms($post->ID, 'reference_category');

      $tags = wp_get_post_terms($post->ID, 'reference_tag');

      $reference_archive_url = get_post_type_archive_link('reference');

      if (get_field('title_black', $ID)) {
        $featured_img_class = 'black-text';
      }

      $featured_img_url = get_the_post_thumbnail_url($post, 'full');
      $featured_mobile_img = get_field('mobile_hero_image', $ID);

      if (empty($featured_img_url)) {
        $featured_img_url = get_stylesheet_directory_uri() . "/imgs/swisstomato.png";
        $featured_img_class .= ' default-image';
      }

      if (empty($featured_mobile_img)) {
        $featured_mobile_img = $featured_img_url;
      }

      $logo_url = get_field('logo', $ID);

      $client_name = get_field('client_name', $ID);

      $additional_content_bottom = get_field('additional_content_bottom', $ID);
    ?>

      <article id="swisst2-reference">
        <div id="reference-title-client" class="<?= empty($client_name) ? 'only-title' : '' ?>">
          <?php
          $overriden_title = get_field('title_displayed', $ID);

          $displayed_title = !empty($overriden_title) ? $overriden_title : get_the_title();
          ?>
          <h1 class="<?= strlen($displayed_title) > 40 ? 'over-40' : '' ?>">
            <?= $displayed_title ?>
          </h1>

          <?php if (!empty($client_name)) : ?>
            <div id="reference-client">
              <label><?= get_bloginfo('language') === 'de-DE' ? 'KLIENT' : 'CLIENT' ?></label>
              <h2><?= $client_name ?></h2>
            </div>
          <?php endif; ?>
        </div>

        <?= get_img($featured_img_url, 'Feature Reference Image', 'feature-reference-image') ?>
        <?= get_img($featured_mobile_img, 'Feature Reference Image', 'feature-reference-image mobile') ?>

        <div id="reference-content-row">
          <div id="reference-body">
            <?php
            the_content();
            ?>
          </div>

          <?php
          $right_content = get_field('right_content', $ID);
          $number_of_ps = substr_count($right_content, '</p>');
          $sidebar_classes = array();

          if ($number_of_ps > 1) {
            array_push($sidebar_classes, 'long');
          }

          if (($number_of_ps % 3) === 0) {
            array_push($sidebar_classes, 'plus-one-in-after');
          }
          ?>

          <div id="reference-sidebar" class="<?= implode(' ', $sidebar_classes) ?>">
            <p>
              <strong><?= __('Platform', 'swisst2') ?></strong><br />
              <?php foreach ($categories as $category) : ?>
                <a href="<?= $reference_archive_url . '#platform=' . $category->slug ?>">
                  <?= $category->name ?>
                </a>
              <?php endforeach; ?>
            </p>


            <!-- <p>
                <strong><?= __('Area', 'swisst2') ?></strong><br />
                <?php foreach ($tags as $tag) : ?>
                  <a href="<?= $reference_archive_url . '#area=' . $tag->slug ?>">
                    <?= $tag->name ?>
                  </a>
                <?php endforeach; ?>
              </p> -->


            <?= $right_content ?>
          </div>
        </div>

        <?php
        $media_row_1 = get_field('media_row_1', $ID);

        $row1image1 = get_field('media_row_1_image_1', $ID);
        $row1image1Lightbox = get_field('media_row_1_image_1_big_lightbox', $ID);
        $row1image2 = get_field('media_row_1_image_2', $ID);
        $row1image2Lightbox = get_field('media_row_1_image_2_big_lightbox', $ID);

        if (!$row1image1Lightbox) {
          $row1image1Lightbox = $row1image1;
        }

        if (!$row1image2Lightbox) {
          $row1image2Lightbox = $row1image2;
        }

        switch ($media_row_1) {
          case 'Image':
        ?>
            <div id="media-row-1" class="media-row image">
              <a href="<?= acf_to_img_url($row1image1Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row1image1, 'Media Row 1 Image 1') ?>
              </a>
            </div>
          <?php
            break;
          case 'Video Image':
          ?>
            <div id="media-row-1" class="media-row video-image">
              <div class="video">
                <div class="video-content">
                  <?= get_field('media_row_1_video', $ID) ?>
                </div>
                <?php
                $video_cover = get_field('media_row_1_video_cover', $ID);
                if (!empty($video_cover)) : ?>

                  <div class="video-cover">
                    <?= get_img($video_cover, 'Video Cover', 'mobile-image') ?>
                  </div>

                <?php endif; ?>
              </div>
              <a href="<?= acf_to_img_url($row1image2Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row1image2, 'Media Row 1 Image 2') ?>
              </a>
            </div>
          <?php
            break;
          case 'Image Image':
          ?>
            <div id="media-row-1" class="media-row image-image">
              <a href="<?= acf_to_img_url($row1image1Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row1image1, 'Media Row 1 Mobile Image 1') ?>
              </a>
              <a href="<?= acf_to_img_url($row1image2Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row1image2, 'Media Row 1 Mobile Image 2') ?>
              </a>
            </div>
          <?php
            break;
          case 'Video':
          ?>
            <div id="media-row-1" class="media-row video">
              <div class="video">
                <div class="video-content">
                  <?= get_field('media_row_1_video', $ID) ?>
                </div>
                <?php
                $video_cover = get_field('media_row_1_video_cover', $ID);
                if (!empty($video_cover)) : ?>

                  <div class="video-cover">
                    <?= get_img($video_cover, 'Video Cover') ?>
                  </div>

                <?php endif; ?>
              </div>
            </div>
        <?php
            break;
        }
        ?>

        <?php
        $media_row_2 = get_field('media_row_2', $ID);

        $row2image1 = get_field('media_row_2_image_1', $ID);
        $row2image1Lightbox = get_field('media_row_2_image_1_big_lightbox', $ID);
        $row2image2 = get_field('media_row_2_image_2', $ID);
        $row2image2Lightbox = get_field('media_row_2_image_2_big_lightbox', $ID);

        if (!$row2image1Lightbox) {
          $row2image1Lightbox = $row2image1;
        }

        if (!$row2image2Lightbox) {
          $row2image2Lightbox = $row2image2;
        }

        switch ($media_row_2) {
          case 'Image':
            $row2image1 = get_field('media_row_2_image_1', $ID);
        ?>
            <div id="media-row-2" class="media-row image">
              <a href="<?= acf_to_img_url($row2image1Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row2image1, 'Media Row 2 Mobile Image 1') ?>
              </a>
            </div>
          <?php
            break;
          case 'Image Image':
            $row2image1 = get_field('media_row_2_image_1', $ID);
            $row2image2 = get_field('media_row_2_image_2', $ID);
          ?>
            <div id="media-row-2" class="media-row image-image">
              <a href="<?= acf_to_img_url($row2image1Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row2image1, 'Media Row 2 Mobile Image 1') ?>
              </a>
              <a href="<?= acf_to_img_url($row2image2Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row2image2, 'Media Row 2 Mobile Image 2') ?>
              </a>
            </div>
          <?php
            break;
          case 'Quote Image':
            $row2image2 = get_field('media_row_2_image_2', $ID);
          ?>
            <div id="media-row-2" class="quote-image">
              <div class="quote">
                <div id="quote-content">
                  <?= get_field('media_row_2_quote_text', $ID) ?>
                </div>
                <?= get_img(get_field('media_row_2_quote_image', $ID), 'Quote Author Image') ?>
                <div id="quote-author-name"><?= get_field('media_row_2_quote_name', $ID) ?></div>
                <div id="quote-author-position"><?= get_field('media_row_2_quote_position', $ID) ?></div>
              </div>
              <div class="image" style="background-image: url(<?= get_field('media_row_2_image_2', $ID) ?>)" alt="Media Row 2 Image 2"></div>
              <a href="<?= acf_to_img_url($row2image2Lightbox) ?>" data-lightbox="reference-gallery">
                <?= get_img($row2image2, 'Media Row 2 Mobile Image 2') ?>
              </a>
            </div>
        <?php
            break;
        }
        ?>

        <?php if ($additional_content_bottom == "yes") {
            $text_column_text_1 = get_field('text_column_text_1', $ID);

            if ($text_column_text_1) :
            ?>
              <div class="reference-text-column">
                <?= $text_column_text_1 ?>
              </div>
            <?php endif; ?>

            <?php
            $text_column_image = get_field('text_column_image', $ID);
            $text_column_image_big_lightbox = get_field('text_column_image_big_lightbox', $ID);

            if ($text_column_image) :
            ?>
              <div id="reference-text-column-image">
                <a href="<?= $text_column_image_big_lightbox ?>" data-lightbox="reference-gallery">
                  <?= get_img($text_column_image, 'Text Column Image') ?>
                </a>
              </div>
            <?php endif; ?>

            <?php
            $text_column_text_2 = get_field('text_column_text_2', $ID);

            if ($text_column_text_2) :
            ?>
              <div class="reference-text-column">
                <?= $text_column_text_2 ?>
              </div>
            <?php endif; 
        } ?>

      </article><!-- #post-<?php the_ID(); ?> -->


    <?php
    endwhile; // End of the loop.
    ?>
  </div> <!-- /#page-content -->

</div><!-- /#swisst2-blogpost-page -->

<?php
get_footer();
