<?php function masonry_reference_list_section_func() { ?>

  <?php
    $platforms = get_terms(array(
      'taxonomy' => 'reference_category',
      'hide_empty' => false,
      'orderby' => 'id',
    ));
  ?>

  <div class="masonry-reference-list-section">

    <div class="masonry-references-intro center-1117">
      <h1 class="intro-title"><?=__('References', 'swisst2')?></h1>

      <div id="references-intro-text">
        <?=get_field('references_page_intro', 'option')?>
      </div>

      <div id="filter-section">
        <div class="filter-area">    
          <div class="filter-boxes">
            <div class="filter-box selected">
              <?=__('All projects', 'swisst2')?>
            </div>

            <?php foreach ($platforms as $platform): 
            ?>
              <div class="filter-box" data-id="<?=$platform->term_id?>" data-slug="<?=$platform->slug?>">
                <?=get_field('filter_text', $platform->taxonomy . '_' . $platform->term_id)?>
              </div>
            <?php endforeach; ?>

            <i class="fas fa-spinner"></i>
          </div>
        </div>
      </div>
    </div>

    <div class="masonry-reference-list container">
      <article class="reference template" data-width="" data-height="" data-url="">
        <div class="reference__image-wrapper">
          <img src="" alt="" />
        </div>

        <div class="reference__content">
          <h3></h3>

          <p></p>

          <div class="reference__content__category"></div>
        </div>
      </article>

      <?php while ( have_posts() ) : the_post(); ?>
        <?php
          $ID = get_the_ID();
          $reference_thumbnail = get_field('masonry_thumbnail_image', $ID);

          $thumbnail_type = gettype($reference_thumbnail);
          $thumbnail_src = '';

          if ($thumbnail_type === 'string') {
            $thumbnail_infos = wp_get_attachment_image_src(intval($reference_thumbnail), 'full');
            $thumbnail_src = $thumbnail_infos[0];
            $thumbnail_width = $thumbnail_infos[1];
            $thumbnail_height = $thumbnail_infos[2];
          }

          if ($thumbnail_type === 'array') {
            $thumbnail_width = $reference_thumbnail['width'];
            $thumbnail_height = $reference_thumbnail['height'];
            $thumbnail_src = $reference_thumbnail['url'];
          }

          $link = get_permalink($ID);
          $title = get_field('masonry_reference_title', $ID);
          $text = get_field('masonry_reference_text', $ID);

          if (empty($thumbnail_src)) {
            // print "<a href='$link' target='_blank' data-type='$thumbnail_type'>" . get_the_title($ID) . "</a><br />";
            $thumbnail_src = get_stylesheet_directory_uri() . '/imgs/gray-624x460.jpg';
            $thumbnail_width = 624;
            $thumbnail_height = 460;
          }

          if (empty($title)) {
            $title = get_the_title();
          }

          $ref_categories = get_the_terms($ID, 'reference_category');

          $ref_class = 'reference';

          if (get_field('masonry_black_layer_below_title', $ID)) {
            $ref_class .= ' black-layer';
          }

          if (get_field('masonry_black_layer_below_title_mobile', $ID)) {
            $ref_class .= ' mobile-black-layer';
          }

        ?>

        <article
          class="<?=$ref_class?>" 
          data-width="<?=$thumbnail_width?>" 
          data-height="<?=$thumbnail_height?>" 
          data-url="<?=$link?>"
          href="<?=$link?>"
          target="_blank"
          data-id="<?=$ID?>"
        >
          <div class="reference__image-wrapper">
            <img src="<?=$thumbnail_src?>" alt="<?=$title?>" />
          </div>

          <div class="reference__content">
            <h3 text-slide-from-up="<?=$title?>">
              <span class="text-slide-down"><?=$title?></span>
            </h3>

            <p text-slide-from-up="<?=$text?>">
              <span class="text-slide-down"><?=$text?></span>
            </p>

            <?php if (is_array($ref_categories) && sizeof($ref_categories) > 0): ?>
              <div class="reference__content__category">
                <?=get_field('filter_text', $ref_categories[0])?>
              </div>
            <?php endif; ?>
          </div>
        </article>

      <?php endwhile; ?>
    </div>

  </div>

<?php } ?>