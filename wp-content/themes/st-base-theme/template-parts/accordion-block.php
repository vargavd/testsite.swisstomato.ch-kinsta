<?php
  $title = get_field('title');
  $accordions = get_field('accordions');
  
  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-accordion-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Accordion Block</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
        <h2 class="title"><?= $title ?></h2>
        <?php if ( have_rows('accordions') ) { ?>
        <div class="accordions">
            <?php while( have_rows('accordions') ): the_row(); 
                $accordion_title = get_sub_field('accordion_title');
                $accordion_content = get_sub_field('accordion_content');
                ?>
                <div class="accordion-item">
                    <div class="accordion-header">
                        <h5><?=$accordion_title?></h5>
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"/></svg>
                        </span>
                    </div>
                    <div class="accordion-content">
                        <?=$accordion_content?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php } ?>
    </div>
  <?php endif; ?>
</div>