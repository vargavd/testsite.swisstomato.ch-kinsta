<?php 
  $title = get_field('title');
  $text = get_field('text');
  $awards = get_field('awards');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-awards-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Awards</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="center-1300">
      <div class="awards-text-column">
        <?php if ($title): ?>
          <h2 class="<?=$text ? '' : 'big'?>"><?=$title?></h2>
        <?php endif;?>
        
        <?php if ($text): ?>
          <p><?=$text?></p>
        <?php endif;?>
      </div>
      <div class="awards-column">
        <?php
          // pr1($awards);
        ?>
        <?php foreach ($awards as $award): ?>
          <div class="award">
            <?=get_img($award['image'], 'Award Image')?>

            <div class="award-content">

              <?php if ($award['title']): ?>
                <div class="award-title"><?=$award['title']?></div>
              <?php endif; ?>

              <?php if ($award['type'] === 'List'): ?>
                <ul>
                  <?php
                    $award_list_info = $award['list'];
                    foreach ($award_list_info as $list_row):
                  ?>
                    <li style="background-color: <?=$list_row['background_color']?>; color: <?=$list_row['color']?>">
                      <?=$list_row['text']?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              <?php else: ?>
                <p><?=$award['text']?></p>
              <?php endif; ?>
            </div>

          </div>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endif; ?>
</div>
