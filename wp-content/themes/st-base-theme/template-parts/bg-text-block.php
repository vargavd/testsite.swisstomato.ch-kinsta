<?php
  $background_image = get_field('background_image');
  $background_type = get_field('background_type');
  if (empty($background_type)) {
    $background_type = 'contained';
  }
  $title = get_field('title', false, false);
  $heading_type = get_field('heading_type');
  $description = get_field('description');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-bg-text-block stbase-block <?= $background_type ?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Background + Text Block</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: 
    if ($background_type == "contained") { ?>
      <div class="container" style="background-image: url('<?= $background_image ?>')">
        <div class="bg-overlay"></div>
    <?php } elseif ($background_type == "fullwidth") { ?>
      <div class="fullwidth" style="background-image: url('<?= $background_image ?>')">
        <div class="bg-overlay"></div>
        <div class="container">
    <?php } ?>
        <div class="content">
            <<?= $heading_type ?> class="title"><?= $title ?><<?= $heading_type ?>/>
            <?php if( get_field('description') ): ?>
                <div class="description">
                    <?= $description ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($background_type == "fullwidth") { echo '</div>';} ?>
    </div>
  <?php endif; ?>
</div>