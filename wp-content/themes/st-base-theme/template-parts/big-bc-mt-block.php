<?php
  $title = get_field('title');
  $text = get_field('text');
  $background_image = get_field('background_image');
  $more_link_type = get_field('more_link_type');
  $more_link_page = get_field('more_link_page');
  $more_link_text = get_field('more_link_text');
  $more_link_id = get_field('more_link_id');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-big-bc-mt-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Big BC MT Block</h2>
        <h3><?=empty($title) ? 'No title' : strip_tags($title)?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <?=get_img($background_image, $title, 'big-bc-img')?>
    <div class="big-bc-content">
      <div class="container">
        <h2><?=$title?></h2>
        <?=$text?>

        <?php if ($more_link_type === 'Page'): ?>
          <a class="read-more" href="<?=$more_link_page?>">
            <?=$more_link_text?>
          </a>
        <?php elseif ($more_link_type === 'More Text'): ?>
          <a class="read-more" target-id="#<?=$more_text_id?>">
            <i class="fas fa-angle-down"></i>
            <?=$more_link_text?>
          </a>
        <?php endif; ?>

      </div>
    </div>
  <?php endif; ?>
</div>