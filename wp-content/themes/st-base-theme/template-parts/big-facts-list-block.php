<?php
  $title = get_field('title');
  $facts = get_field('facts');
  $number_of_facts = sizeof($facts);

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-big-facts-list-block stbase-block <?=$is_preview && ('preview')?> top-block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Big Facts List Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title)) . " (<strong>$number_of_facts</strong> facts)"?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <!-- FACTS -->
    <div class="facts">
      <?php 
        for ($i = 0; $i < sizeof($facts); $i++): 
          $fact = $facts[$i];
          $type = $fact['type'];
          $title = $fact['title'];
          $header_image = $fact['header_image'];
      ?>
        <div class="fact <?=$type?>" id="fact-<?=$i?>">
          <?php if ($type !== 'big-red-without-img'): ?>
            <header>
              <?=get_img($fact['header_image'], $fact['title'])?>
              <div class="fact-header-content">
                <div class="fact-number"><?=$i+1?></div>
                <h3><?=$fact['title']?></h3>
              </div>
            </header>
          <?php endif; ?>

          <div class="fact-content">
            <!-- SHOW MORE ELEMENTS -->
            <?php if (strpos($type, 'show-more') !== false): ?>
              <?php
                $text = $fact['text'];
                $show_more_texts = $fact['show_more_texts'];
              ?>
              <?php if (!empty($text)): ?>
                <div class="first-text">
                  <?=$text?>
                </div>
              <?php endif; ?>

              <?php foreach ($show_more_texts as $show_more_text): ?>
                <div class="show-more-text-item">
                  <h4><?=$show_more_text['title']?></h4>
  
                  <div class="show-more-text">
                    <?=$show_more_text['text']?>
                  </div>
                  
                  <a class="show-more-link">
                    <i class="far fa-chevron-down"></i>
                    <span class="more"><?=__('Show more', 'stbase')?></span>
                    <span class="less"><?=__('Show less', 'stbase')?></span>
                  </a>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>
  

            <!-- ADVANTAGES ELEMENTS -->
            <?php if ($type === 'show-more-and-advantages'): ?>
              <?php
                $collapsible_advantages = $fact['collapsible_advantages'];
                $advantages_title = $fact['advantages_title'];
                $advantages = $fact['advantages'];
              ?>
              <div class="fact-advantages <?=$collapsible_advantages ? 'collapsible' : ''?>">
                <h4><?=$advantages_title?></h4>
  
                <?php foreach ($advantages as $advantage): ?>
                  <div class="advantage">
                    <div class="advantage-header">
                      <?php if (array_key_exists('title', $advantage)): ?>
                        <h5><?=$advantage['title']?></h5>
                      <?php endif; ?>
  
                      <?php 
                        if (array_key_exists('logo', $advantage) && !empty($advantage['logo'])) {
                          print get_img($advantage['logo'], $advantage['title']);
                        }
                      ?>
                    </div>
                      
                    <div class="advantage-text">
                      <?=$advantage['text']?>
                    </div>
                  </div>
                <?php endforeach; ?>

                <a class="advantages-show-more-link">
                  <i class="far fa-chevron-down"></i>
                  <span class="more"><?=__('Show more', 'stbase')?></span>
                  <span class="less"><?=__('Show less', 'stbase')?></span>
                </a>
              </div>
            <?php endif; ?>


            <!-- PROCESS TIMING -->
            <?php if ($type === 'process-timing'): ?>
              <?php
                $text = $fact['text'];
                $steps = $fact['process_timing_steps'];
                $collapsible_process_timing_steps = $fact['collapsible_process_timing_steps'];
              ?>
              <?php if (!empty($fact['text'])): ?>
                <div class="first-text">
                  <?=$text?>
                </div>
              <?php endif; ?>

              <div class="process-timing-section p-68">
                <div class="process-timing-step-list process-timing-steps no-scroll">
                  <?php foreach ($steps as $step): ?>
                    <div class="process-timing-step">
                      <div class="step-red-bc">
                          <div class="step-number"> 
                              <?=$step['number']?>
                          </div>
                          <div class="step-title">
                              <?=$step['title']?>
                          </div>
                          <div class="step-timing">
                              <?=$step['timing']?>
                          </div>

                          <?php if ($collapsible_process_timing_steps): ?>
                            <div class="show-more-text-item">            
                              <div class="show-more-text">
                                <?=$step['text']?>
                              </div>
                              
                              <a class="show-more-link">
                                <i class="far fa-chevron-down"></i>
                                <span class="more"><?=__('Show more', 'stbase')?></span>
                                <span class="less"><?=__('Show less', 'stbase')?></span>
                              </a>
                            </div>
                          <?php else: ?>
                            <p>
                              <?=$step['text']?>
                            </p>
                          <?php endif; ?>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>

              <?php if (!empty($fact['after_text'])): ?>
                <div class="after-text">
                  <?=$fact['after_text']?>
                </div>
              <?php endif; ?>
            <?php endif; ?>

            <?php if ($type === 'big-more-text'): ?>
              <div class="show-more-before-after-text-item">
                <div class="show-more-first-text">
                  <?=$fact['text']?>
                </div>
                
                <div class="show-more-second-text">
                  <?=$fact['big_more_text']?>
                </div>
                
                <a class="show-more-link">
                  <i class="far fa-chevron-down"></i>
                  <span class="more"><?=__('Show more', 'stbase')?></span>
                  <span class="less"><?=__('Show less', 'stbase')?></span>
                </a>
              </div>
            <?php endif; ?>

            <?php if ($type === 'big-red-without-img'): ?>
              <?=$fact['text']?>
            <?php endif; ?>
          </div>
        </div>
      <?php endfor; ?>
    </div>
  <?php endif; ?>
</div>