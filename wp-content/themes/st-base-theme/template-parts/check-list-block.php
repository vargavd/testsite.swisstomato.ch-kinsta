<?php
  /*
    Pages:
    - https://testsite.swisstomato.ch/
    - https://testsite.swisstomato.ch/creer-une-application/
    - https://testsite.swisstomato.ch/creer-une-application/agence-de-developpement-ios/
    - https://testsite.swisstomato.ch/creer-une-application/agence-de-developpement-dapplications-android/
    - https://testsite.swisstomato.ch/creer-une-application/developpement-application-flutter-geneve/
    - https://testsite.swisstomato.ch/creer-une-application/developpement-application-web-geneve/
    - https://testsite.swisstomato.ch/developpement-app-web-startup-pme-geneve/
    - https://testsite.swisstomato.ch/developpement-app-web-ong-nonprofit-geneve/
    - https://testsite.swisstomato.ch/agence-web-geneve/
    - https://testsite.swisstomato.ch/agence-web-geneve/services-de-developpement-wordpress/
    - https://testsite.swisstomato.ch/agence-web-geneve/agence-de-services-du-developpement-drupal/
  */
  $title = get_field('title');
  $heading_type = get_field('heading_type');
  $items = get_field('items');

  $is_it_odd_number_of_items = (sizeof($items) % 2) === 1;

  if (empty($heading_type)) {
    $heading_type = 'h2';
  }

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>


  <?php if ($is_preview) : ?>
    <div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-check-list-block stbase-block">
      <div class="stbase-block-preview">
        <div class="stbase-hero-title">
          <h2>Check List Block</h2>
          <h3><?= "(" . sizeof($items) . " items)" ?></h3>
        </div>

        <span class="dashicons dashicons-ellipsis"></span>
      </div>
    </div>
  <?php else : ?>

    <?php
      // START RENDERING TO STRING
      ob_start();
    ?>

    <div class="container">
      <?php if ($title) : ?>
        <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      <?php endif; ?>

      <div class="check-list <?= $is_it_odd_number_of_items ? 'odd' : '' ?>">
        <?php foreach ($items as $item) : ?>
          <div class="check">
            <!-- <i class="fal fa-check"></i> -->
            <img src="<?= get_template_directory_uri() ?>/assets/imgs/check-list-check.png" alt="check">
            <h3><?= $item['title'] ?></h3>
            <p><?= $item['text'] ?></p>
          </div>
        <?php endforeach; ?>
      </div>
    </div>

    <?php
      // END RENDERING TO STRING
      $output = ob_get_contents();
      ob_get_clean();
    ?>

    <script>
      window.checkListBlockContent = <?=json_encode($output)?>;
    </script>

    <div 
      <?= $id_attribute ? "id='$id_attribute'" : "" ?>
      data-var="checkListBlockContent"
      class="stbase-check-list-block stbase-block"
    >
    </div>
  <?php endif; ?>