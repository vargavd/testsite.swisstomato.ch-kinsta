<?php
$title = get_field('title');
$heading_type = get_field('heading_type');
$text = get_field('text');
$columns_to_display = get_field('columns_to_display');
$clients = get_field('clients');
$colour_shades_of_images = get_field('colour_shades_of_images');
$display_review_badge = get_field('display_review_badge');
$text_before_review_image = get_field('text_before_review_image');
$reviews_image = get_field('reviews_image');
$reviews_link = get_field('reviews_link');
$reviews_link_text = $reviews_link['title'];
$reviews_link_url = $reviews_link['url'];

if (empty($heading_type)) {
  $heading_type = 'h2';
}
if (empty($columns_to_display)) {
  $columns_to_display = '5';
}
if (empty($colour_shades_of_images)) {
  $colour_shades_of_images = 'original';
}
if (empty($display_review_badge)) {
  $display_review_badge = 'yes';
}

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>
<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-clients-block stbase-block block-margin">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Clients</h2>
        <h3><?= empty($title) ? 'No title' : $title ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <div class="container">
      <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      <?php if ($text) { ?>
        <div id="clients-text">
          <?= $text ?>
        </div>
      <?php } ?>
      <div class="client-list column-<?= $columns_to_display ?> <?= $colour_shades_of_images ?>">
        <?php foreach ($clients as $client) : ?>
          <div data-href="<?= in_array('reference_link', $client) ? $client['reference_link'] : '' ?>" class="client" title="<?= $client['name'] ?>">
            <?= get_img($client['image'], $client['name']) ?>
          </div>
        <?php endforeach; ?>
      </div>

      <?php if ($display_review_badge == 'yes') { ?>
        <?php if (!empty($reviews_image) && !empty($reviews_link_text) && !empty($reviews_link_url)) : ?>
          <div id="clients-reviews-wrapper">
            <!-- <?php if (!empty($text_before_review_image)) : ?>
              <p><?= $text_before_review_image ?></p>
            <?php endif; ?> -->

            <?php if (!empty($reviews_image)) : ?>
              <?= get_img($reviews_image, $reviews_link_text) ?>
            <?php endif; ?>

            <?php if (!empty($reviews_link_text) && !empty($reviews_link_url)) : ?>
              <a href="<?= $reviews_link_url ?>" target="_blank">
                <?= $reviews_link_text ?>
              </a>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      <?php } ?>

    </div> <!-- /#clients-block -->
  <?php endif; ?>
</div>