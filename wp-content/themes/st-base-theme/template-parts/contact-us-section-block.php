<?php
$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';

$compact = get_field('compact');
?>
<div id="<?= $block['id'] ?>" class="stbase-contact-us-block stbase-block block-margin">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Contact Us Section</h2>
        <h3><?php if ($compact == '1') {
          print 'Compact Contact Block';
        } else {
          print 'Full Contact Block';
        } ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <?php
    print $compact ? contact_us_compact_section_func(false) : contact_us_section_func();
    ?>
  <?php endif; ?>
</div>