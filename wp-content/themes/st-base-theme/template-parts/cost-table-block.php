<?php
  $rows = get_field('rows');
  $number_of_hour_columns = get_field('number_of_hour_columns');
  $hours_1_title = get_field('hours_1_title');

  if ($number_of_hour_columns === '2') {
    $hours_2_title = get_field('hours_2_title');
  }

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>
<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-cost-table-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Hours Table Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title)) . " (<strong>" . sizeof($rows) . "</strong> rows)"?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="cost-table-wrapper">
      <table>
        <thead>
          <tr>
            <th class="title empty"></th>
            <th class="description empty"></th>
            <?php if ($number_of_hour_columns === '1'): ?>
              <th class="hours double">
                <?=$hours_1_title?>
              </th>
            <?php else: ?>
              <th class="hours">
                <?=$hours_1_title?>
              </th>
              <th class="hours">
                <?=$hours_2_title?>
              </th>
            <?php endif; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row): ?>
            <tr>
              <td class="title"><?=$row['title']?></td>
              <td class="description"><?=$row['description']?></td>
              <td class="hours <?=$number_of_hour_columns === 1 ? 'double' : ''?>">
                <span class="mobile-label">
                  <?=$hours_1_title?>
                </span>
                <span>
                  <?=$row['hours_1_text']?>
                </span>
              </td>
              <?php if ($number_of_hour_columns === '2'): ?>
                <td class="hours">
                  <span class="mobile-label">
                    <?=$hours_2_title?>
                  </span>
                  <span>
                    <?=$row['hours_2_text']?>
                  </span>
                </td>
              <?php endif; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php endif; ?>
</div>