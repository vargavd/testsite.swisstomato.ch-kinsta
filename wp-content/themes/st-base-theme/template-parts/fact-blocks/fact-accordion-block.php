<?php
$add_text_before = get_field('add_text_before');
$text_before = get_field('text_before');
$accordions = get_field('accordions');
$number_of_accordions = sizeof($accordions);
$add_text_after = get_field('add_text_after');
$text_after = get_field('text_after');
$background_color = get_field('background_color');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-fact-accordion-block stbase-block <?= $is_preview && ('preview') ?>">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Accordion Block</h2>
        <h3><?= $number_of_accordions . ' accordion item' ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <div class="fact-content">
        <div class="fact-wrapper" style="background-color: <?=$background_color?>;">
            <?php if ($add_text_before) { ?>
            <div class="text-before"><?=$text_before?></div>
            <?php } 
            if ( have_rows('accordions') ) { ?>
                <div class="accordions">
                    <?php while( have_rows('accordions') ): the_row(); 
                        $accordion_title = get_sub_field('accordion_title');
                        $accordion_content = get_sub_field('accordion_content');
                        $add_extra_content = get_sub_field('add_extra_content');
                        $extra_content = get_sub_field('extra_content');
                        ?>
                        <div class="accordion-item">
                            <div class="accordion-header">
                                <h5><?=$accordion_title?></h5>
                                <span class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"/></svg>
                                </span>
                            </div>
                            <div class="accordion-content">
                                <?=$accordion_content?>
                                <?php if ($add_extra_content) { 
                                  if( have_rows('extra_content') ): ?>
                                    <?php while( have_rows('extra_content') ): the_row(); ?>
                                        <?php if( get_row_layout() == 'extra_wysiyg' ): ?>
                                            <?php the_sub_field('extra_textual_content'); ?>
                                        <?php elseif( get_row_layout() == 'icon_boxes' ): 
                                            $icon_box = get_sub_field('icon_box');
                                            ?>
                                            <?php if( have_rows('icon_box') ): ?>
                                              <div class="icon-box-wrapper">
                                                <?php while( have_rows('icon_box') ): the_row(); 
                                                    $icon = get_sub_field('icon');
                                                    $icon_title = get_sub_field('icon_title');
                                                    $icon_description = get_sub_field('icon_description');
                                                    ?>
                                                    <div class="icon-box">
                                                      <div class="icon"><?php echo wp_get_attachment_image( $icon['ID'], 'full' ); ?></div>
                                                      <h4 class="icon-title"><?=$icon_title?></h4>
                                                      <p class="icon-description"><?=$icon_description?></p>
                                                    </div>
                                                <?php endwhile; ?>
                                              </div>
                                          <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                <?php endif;
                                } ?>
                                <script>
                                    jQuery(document).ready(function() {
                                      jQuery('.accordion-content table').each(function() {
                                          var table = jQuery(this);
                                          table.wrap('<div class="table-wrapper" style="overflow-x:auto;"></div>');
                                      });
                                  });
                                </script>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php } 
            if ($add_text_after) { ?>
            <div class="text-after"><?=$text_after?></div>
            <?php } ?>
        </div>
    </div>  
  <?php endif; ?>
</div>