<?php
  $title = get_field('title');
  $description = get_field('description');
  $tools = get_field('tools');
  $extra_description = get_field('extra_description');
  $buttons = get_field('buttons');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-additional-parameters-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Additional Parameters Block</h2>
        <h3><?=(empty($title) ? 'No title' : $title) ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
      <h2 class="title"><?=$title?></h2>
      <div class="description"><?=$description?></div>
      <?php if( have_rows('tools') ): ?>
        <div class="tools">
          <?php while( have_rows('tools') ): the_row(); 
              $tool_title = get_sub_field('tool_title');
              $tool_image = get_sub_field('tool_image');
              $tool_description = get_sub_field('tool_description');
              ?>
              <div class="tool">
                <div class="tool-title"><?=$tool_title?></div>
                <img src="<?php echo esc_url($tool_image['url']); ?>" alt="<?php echo esc_attr($tool_image['alt']); ?>" class="tool-image" />
                <p class="tool-description"><?=$tool_description?></p>
              </div>
          <?php endwhile; ?>
        </div>
      <?php endif; ?>
      <div class="extra-description"><?=$extra_description?></div>
      <?php if( have_rows('buttons') ): ?>
        <div class="buttons">
          <?php while( have_rows('buttons') ): the_row(); 
              $button = get_sub_field('button');
              ?>
              <a href="<?=$button['url'];?>" class="st-button-link">
                <i class="far fa-arrow-right"></i><?=$button['title']?>          
              </a>
          <?php endwhile; ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>