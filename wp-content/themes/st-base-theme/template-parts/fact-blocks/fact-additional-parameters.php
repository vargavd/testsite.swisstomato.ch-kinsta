<?php
  $title = get_field('title');
  $description = get_field('description');
  $icons = get_field('icons');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-additional-parameters-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Additional Parameters Block</h2>
        <h3><?=(empty($title) ? 'No title' : $title) . " (" . sizeof($icons) . " icons)"?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
      APB
    </div>
  <?php endif; ?>
</div>