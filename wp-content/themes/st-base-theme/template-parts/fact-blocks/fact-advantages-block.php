<?php
$title = get_field('title');
$advantages = get_field('advantages');
$collapsible_advantages = get_field('collapsible_advantages');
$heading_type = get_field('heading_type');

if (empty($heading_type)) {
  $heading_type = 'h4';
}

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';

$list_classes = array('fact-advantages');

if ($collapsible_advantages) {
  array_push($list_classes, 'collapsible');
}

if (sizeof($advantages) === 2) {
  array_push($list_classes, 'item-2');
}

if (empty($title)) {
  array_push($list_classes, 'no-title');
}
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-fact-advantages-block fact-narrow-content-block stbase-block <?= $is_preview && ('preview') ?>">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Advantages Block</h2>
        <h3><?= (empty($title) ? 'No title' : strip_tags($title)) ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <div class="<?= implode(' ', $list_classes) ?>">
      <?php if (!empty($title)) : ?>
        <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      <?php endif; ?>

      <?php foreach ($advantages as $advantage) : ?>
        <div class="advantage">
          <div class="advantage-header">
            <?php if (array_key_exists('title', $advantage) && !empty($advantage['title'])) : ?>
              <h5><?= $advantage['title'] ?></h5>
            <?php endif; ?>

            <?php
            if (array_key_exists('logo', $advantage) && !empty($advantage['logo'])) {
              print get_img($advantage['logo'], $advantage['title']);
            }
            ?>
          </div>

          <div class="advantage-text">
            <?php if ($collapsible_advantages) : ?>
              <div class="show-more-text-item">
                <div class="text show-more-text" data-rows-in-collapsed="11">
                  <?= $advantage['text'] ?>
                </div>

                <a class="show-more-link">
                  <i class="far fa-chevron-down"></i>
                  <span class="more"><?= __('Show more', 'stbase') ?></span>
                  <span class="less"><?= __('Show less', 'stbase') ?></span>
                </a>
              </div>
            <?php else : ?>
              <?= $advantage['text'] ?>
            <?php endif; ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>