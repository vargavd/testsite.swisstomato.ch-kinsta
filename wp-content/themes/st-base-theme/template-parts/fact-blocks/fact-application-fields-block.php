<?php
  $title = get_field('title');
  $fields = get_field('fields');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fact-application-fields-block fact-narrow-content-block stbase-block <?=$is_preview && ('preview')?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Application Fields Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title)) . ' (' . sizeof($fields) . ' fields)'?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <h3><?=$title?></h3>
    
    <div class="fields">
      <?php foreach ($fields as $field): ?>
        <?php
          $background_image = $field['background_image'];
          $caption = $field['caption'];
        ?>
        <div class="field">
          <?=get_img($background_image, $caption)?>
          <div class="field__content">
            <?=$caption?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
