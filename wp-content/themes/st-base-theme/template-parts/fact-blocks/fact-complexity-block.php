<?php
  $items = get_field('items');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fact-complexity-block fact-narrow-content-block stbase-block <?=$is_preview && ('preview')?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Complexity Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title)) . ' (' . sizeof($items) . ' items)'?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="complexity-items">
      <?php 
        foreach ($items as $item): 
          $title = $item['title'];
          $text = $item['text'];

          $percentage = intval($item['percentage'])/100;
          $height = $percentage*190; // 190px is the full height of the bar wrapper
      ?>
        <div class="item">
          <div class="progress-bar-wrapper">
            <div class="progress-bar" style="height: <?=$height?>px"></div>
          </div>

          <div class="item-content">
            <header><?=$title?></header>
            <?=$text?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
