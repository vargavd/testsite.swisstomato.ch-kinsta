<?php
$number = get_field('number');
$background_image = get_field('background_image');
$add_overlay = get_field('add_overlay');
$title = get_field('title');
$heading_type = get_field('heading_type');
if (empty($heading_type)) {
  $heading_type = 'h3';
}
$custom_background_color = get_field('custom_background_color');
$background_color = get_field('background_color');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-fact-header-block stbase-block <?= $is_preview && ('preview') ?> top-block-margin">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Header Block</h2>
        <h3><?= (empty($title) ? 'No title' : strip_tags($title)) ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <header class="zoom-in-on-scroll">
      <div class="background">
        <?= get_img($background_image, esc_html($title)) ?>
        <?php if ($add_overlay) { ?>
          <div class="bg-overlay"></div>
        <?php } ?>
      </div>
      <div class="fact-header-content <?= empty($number) ? 'no-number' : '' ?>">
        <div class="fact-number"><?= $number ?></div>
        <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      </div>
    </header>
    <?php if ($custom_background_color) { ?>
      <style>
        <?= $id_attribute ? "#$id_attribute" : "" ?>.stbase-fact-header-block header::after {
          background: <?= $background_color ?>;
        }
      </style>
    <?php } 
  endif; ?>
</div>
