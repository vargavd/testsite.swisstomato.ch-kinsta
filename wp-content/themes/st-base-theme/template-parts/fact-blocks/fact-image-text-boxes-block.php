<?php
  $title = get_field('title');
  $boxes = get_field('boxes');

  $odd_number_of_boxes = (sizeof($boxes) % 2) === 1;

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fact-image-text-boxes-block fact-narrow-content-block stbase-block <?=$is_preview && ('preview')?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Image Text Boxes Block</h2>
        <h3>
          <?=(empty($title) ? 'No title' : strip_tags($title))?>
          <?=' (' . ($fields ? sizeof($fields) : '0') . ' fields)'?>
        </h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>    
    <div class="boxes <?=$odd_number_of_boxes ? 'odd' : ''?>">
      <?php foreach ($boxes as $box): ?>
        <?php
          $image = $box['image'];
          $title = $box['title'];
          $text = $box['text'];
        ?>
        <div class="box">
          <?=get_img($image, $title)?>
          <div class="box__content">
            <div class="box__title">
              <?=$title?>
            </div>
            <?=$text?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
