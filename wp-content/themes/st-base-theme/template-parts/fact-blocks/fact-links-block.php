<?php
$title = get_field('title');
$heading_type = get_field('heading_type');
$description = get_field('description');
$links = get_field('links');
$number_of_facts = is_array($links) ? sizeof($links) : 0;
$enable_custom_links = get_field('enable_custom_links');
$custom_links = get_field('custom_links');
$enable_additional_parameters = get_field('enable_additional_parameters');
$additional_parameters = get_field('additional_parameters');

if (empty($heading_type)) {
  $heading_type = 'h2';
}

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-fact-links-block stbase-block <?= $is_preview && ('preview') ?> block-margin">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Links Block</h2>
        <h3><?= (empty($title) ? 'No title' : strip_tags($title)) . " (<strong>$number_of_facts</strong> normal links" ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <div class="container">
      <?php if (!empty($title)) : ?>
        <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      <?php endif; ?>

      <?php if (!empty($description)) : ?>
        <p class="description"><?= $description ?></p>
      <?php endif; ?>
    </div>

    <!-- FACT LINKS -->
    <?php if ($number_of_facts): ?>
    <div class="fact-links <?php echo $enable_additional_parameters ? 'has-additional' : ''; ?>">
      <?php for ($i = 0; $i < $number_of_facts; $i++) : ?>
        <div class="fact-link" data-scroll-to="<?= $links[$i]['link']['url'] ?>">
          <i class="far fa-arrow-right"></i>

          <span class="fact-number" data-text-slide-from-up="<?= $i + 1 ?>">
            <span class="text-slide-down"><?= $i + 1 ?></span>
          </span>

          <p>
            <?php
              // replace space before question mark and exclamation mark with non-breaking space
              $title = $links[$i]['link']['title'];
              $title_nbp = str_replace(" ?", "&nbsp;?", $title);
              $title_nbp = str_replace(" !", "&nbsp;!", $title_nbp);

              echo $title_nbp;
            ?>
          </p>
        </div>
      <?php endfor; 
      //custom links
      if ($enable_custom_links) { 
        for ($i = 0; $i < sizeof($custom_links); $i++) : ?>
          <div class="fact-link custom-link" data-scroll-to="<?= $custom_links[$i]['custom_link']['url'] ?>">
            <i class="far fa-arrow-right"></i>

            <div class="num-wrapper">
              <span class="fact-number" data-text-slide-from-up="+<?= $i + 1 ?>">
                <span class="text-slide-down">+<?= $i + 1 ?></span>
              </span>

              <?php if (!empty($custom_links[$i]['label'])) : ?>
                <div class="label"><?= $custom_links[$i]['label'] ?></div>
              <?php endif; ?>
            </div>

            <p>
              <?= $custom_links[$i]['custom_link']['title'] ?>
            </p>
          </div>
        <?php endfor; 
      } ?>
    </div>
    <?php endif; ?>
    
    <?php if ($enable_additional_parameters) { ?>
      <div class="additional-parameters" data-scroll-to="<?= $additional_parameters['additional_parameters_link'] ?>">
        <?php if( $additional_parameters ): ?>
          <div class="content">
            <i class="far fa-arrow-right"></i>

            <p class="additional-parameters-title"data-text-slide-from-up="<?= $additional_parameters['additional_parameters_title'] ?>">
              <span class="text-slide-down"><?= $additional_parameters['additional_parameters_title'] ?></span>
            </p>
            <p class="additional-parameters-description"><?= $additional_parameters['additional_parameters_description'] ?></p>
          </div>
        <?php endif; ?>
      </div>
    <?php }
  endif; ?>
</div>
