<?php
  $steps = get_field('steps');
  $collapsible_steps = get_field('collapsible_steps');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fact-process-timing-block fact-narrow-content-block stbase-block <?=$is_preview && ('preview')?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Process Timing Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title)) . ' (' . sizeof($steps) . ' steps)'?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="process-timing-step-list process-timing-steps">
      <?php foreach ($steps as $step): ?>
        <div class="process-timing-step">
          <div class="step-red-bc">
              <div class="step-number"> 
                  <?=$step['number']?>
              </div>
              <div class="step-title">
                  <?=$step['title']?>
              </div>
              <div class="step-timing">
                  <?=$step['timing']?>
              </div>

              <?php if ($collapsible_steps): ?>
                <div class="show-more-text-item">            
                  <div class="show-more-text">
                    <?=$step['text']?>
                  </div>
                  
                  <a class="show-more-link">
                    <i class="far fa-chevron-down"></i>
                    <span class="more"><?=__('Show more', 'stbase')?></span>
                    <span class="less"><?=__('Show less', 'stbase')?></span>
                  </a>
                </div>
              <?php else: ?>
                <p>
                  <?=$step['text']?>
                </p>
              <?php endif; ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
