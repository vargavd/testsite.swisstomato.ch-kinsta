<?php
  $team_picture = get_field('team_picture');
  $team = get_field('team');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fact-team-block fact-narrow-content-block stbase-block <?=$is_preview && ('preview')?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Team Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title)) . ' (' . sizeof($team) . ' team members)'?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <?php if ($team_picture) print get_img($team_picture, 'Team Picture', 'team-picture'); ?>

    <div class="team">
      <?php foreach ($team as $team_member): ?>
        <div class="team-member">
        <header class="title">
          <?=$team_member['title']?>
        </header>

        <div class="show-more-text-item">            
          <div class="show-more-text" data-rows-in-collapsed="0">
            <?=$team_member['text']?>
          </div>
          
          <a class="show-more-link">
            <i class="far fa-chevron-down"></i>
            <span class="more"><?=__('Show more', 'stbase')?></span>
            <span class="less"><?=__('Show less', 'stbase')?></span>
          </a>
        </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
