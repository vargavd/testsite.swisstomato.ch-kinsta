<?php
$title = get_field('title');
$text = get_field('text');
$collapsible_text = get_field('collapsible_text');
$visible_rows_in_collapsed = get_field('visible_rows_in_collapsed');
$heading_type = get_field('heading_type');

if (empty($heading_type)) {
  $heading_type = 'h4';
}

if (empty($visible_rows_in_collapsed)) {
  $visible_rows_in_collapsed = 3;
}

$set_custom_background_color = get_field('set_custom_background_color');
$custom_background_color = get_field('custom_background_color');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-fact-text-block fact-narrow-content-block stbase-block <?= $is_preview && ('preview') ?> <?= $custom_background_color ? "has-custom-bg" : "" ?>">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Text Block</h2>
        <h3><?= (empty($title) ? 'No title' : strip_tags($title)) ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <?php if ($set_custom_background_color) : ?>
      <div class="custom-bg" style="background-color: <?= $custom_background_color ?>"></div>
    <?php endif; ?>
    <div class="fact-content">
      <?php if (!empty($title)) : ?>
        <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      <?php endif; ?>

      <?php if ($collapsible_text) : ?>
        <div class="show-more-text-item">
          <div class="text show-more-text" data-rows-in-collapsed="<?= $visible_rows_in_collapsed ?>">
            <?= $text ?>
          </div>

          <a class="show-more-link">
            <i class="far fa-chevron-down"></i>
            <span class="more"><?= __('Show more', 'stbase') ?></span>
            <span class="less"><?= __('Show less', 'stbase') ?></span>
          </a>
        </div>
      <?php else : ?>
        <div class="text">
          <?= $text ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>