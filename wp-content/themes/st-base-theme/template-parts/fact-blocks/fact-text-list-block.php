<?php
  $is_it_collapsible = get_field('is_it_collapsible');
  $type = get_field('type');
  $texts = get_field('texts');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fact-text-list-block fact-narrow-content-block stbase-block <?=$is_preview && ('preview')?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Facts Text List Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title))?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="fact-content">  
      <?php if ($is_it_collapsible): ?>
        <div class="show-more-text-item">
          <div class="text show-more-text" data-rows-in-collapsed="0">
            <div class="text-list <?=strtolower($type)?>">
              <?php for ($i = 0; $i < sizeof($texts); $i++): ?>
                <div class="text-item">
                  <?php if ($type === 'Numbers'): ?>
                    <div class="number"><?=$i+1?></div>
                  <?php else: ?>
                    <?=get_img($texts[$i]['image'], esc_html(strip_tags($texts[$i]['text'])))?>
                    <!-- <img src="<?=$texts[$i]['image']?>" alt="text <?=$i?>" /> -->
                  <?php endif; ?>
                  <?=$texts[$i]['text']?>
                </div>
              <?php endfor; ?>
            </div>
          </div>
  
          <a class="show-more-link">
            <i class="far fa-chevron-down"></i>
            <span class="more"><?=__('Show more', 'stbase')?></span>
            <span class="less"><?=__('Show less', 'stbase')?></span>
          </a>
        </div>
      <?php else: ?>
        <div class="text">
          <div class="text-list <?=strtolower($type)?>">
            <?php for ($i = 0; $i < sizeof($texts); $i++): ?>
              <div class="text-item">
                <?php if ($type === 'Numbers'): ?>
                  <div class="number"><?=$i?></div>
                <?php else: ?>
                  <?=get_img($texts[$i]['image'], "text-$i")?>
                  <!-- <img src="<?=$texts[$i]['image']?>" alt="text <?=$i?>" /> -->
                <?php endif; ?>
                <?=$texts[$i]['text']?>
              </div>
            <?php endfor; ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>
