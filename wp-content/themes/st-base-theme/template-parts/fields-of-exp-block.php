<?php
  $title = get_field('title');
  $subtitle = get_field('subtitle');
  $fields = get_field('fields');
  $number_of_fields = sizeof($fields);

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-fields-of-exp-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Fields Of Experience Block</h2>
        <h3><?=empty($title) ? 'No title' : "$title ($number_of_fields fields)"?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
      <?php if (!empty($title)): ?>
        <h2><?=$title?></h2>
      <?php endif; ?>

      <?php if (!empty($subtitle)): ?>
        <p class="subtitle">
          <?=$subtitle?>
        </p>
      <?php endif; ?>
      
      <div class="field-of-exp-list <?=$no_animation ? 'no-scroll' : ''?>">
        <?php foreach ($fields as $index => $field): ?>
          <div class="field">
            <div class="img-wrapper">
              <?=get_img($field['image'], $field['title'])?>
            </div>
            <p>
              <?=$field['title']?>
            </p>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endif; ?>
</div>