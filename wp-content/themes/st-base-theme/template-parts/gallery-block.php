<?php
  $gallery = get_field('gallery');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>
<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-gallery-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Gallery Block</h2>
        <h3><?='No title' . " (<strong>" . sizeof($gallery) . "</strong> items)"?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="gallery-items">
      <?php 
        foreach ($gallery as $item): 
          $image = $item['image'];
          $subtitle = $item['subtitle'];
          $image_alt = strip_tags($subtitle);
      ?>
        <div class="item">
          <?=get_img($image, $image_alt)?>
          <?=$subtitle?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>