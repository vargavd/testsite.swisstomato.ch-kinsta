let hsTable = [];

jQuery('h1, h2, h3, h4, h5').each(function () { 
  let $elem = jQuery(this);

  let tag = '';

  if ($elem.is('h1')) tag = 'h1';
  if ($elem.is('h2')) tag = 'h2';
  if ($elem.is('h3')) tag = 'h3';
  if ($elem.is('h4')) tag = 'h4';
  if ($elem.is('h5')) tag = 'h5';


  hsTable.push({ tag, text: jQuery(this).text().trim() })
});

hsTable.forEach(item => {
  console.log(item.tag, item.text);
});