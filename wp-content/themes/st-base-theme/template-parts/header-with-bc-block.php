<?php
  $background_image = get_field('background_image');
  $subtitle = get_field('subtitle');
  $title = get_field('title');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-header-with-bc-block stbase-block <?=$is_preview && ('preview')?> block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Header With Background Block</h2>
        <h3><?=(empty($title) ? 'No title' : strip_tags($title))?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <header>
      <?=get_img($background_image, $title)?>
      <div class="header__content">
        <div class="header__subtitle"><?=$subtitle?></div>
        <h3><?=$title?></h3>
      </div>
    </header>
  <?php endif; ?>
</div>
