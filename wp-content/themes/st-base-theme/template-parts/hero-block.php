<?php
  $hero_type = get_field('hero_type');
  $title = get_field('title');
  $text = get_field('text');
  $bc_image = get_field('background_image');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-hero-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Hero Block</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: 
    if ($hero_type == "original") { 
      print_stbase_hero_section($bc_image['url'], $title, $text); 
    } elseif ($hero_type == "divided") { ?>
      <div class="full-width-split-screen">
        <div class="content">
          <h1 class="title"><?=$title?></h1>
          <p class="text"><?=$text?></p>
        </div>
        <img class="hero-media" src="<?php echo esc_url($bc_image['url']); ?>" alt="<?php echo esc_attr($bc_image['alt']); ?>" />
      </div>
    <?php }
  endif; ?>
</div>