<?php
  $text = get_field('text');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-highlighted-words-text-block stbase-block <?=$is_preview && ('preview')?> top-block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Big Red Text</h2>
        <h3><?=empty($text) ? 'No text' : strip_tags($text)?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
      <?=$text?>
    </div>
  <?php endif; ?>
</div>