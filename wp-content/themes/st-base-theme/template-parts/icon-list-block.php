<?php

  /*
    * On the following pages (testsite):
    * - https://testsite.swisstomato.ch/creer-une-application/developpement-application-web-geneve/
    * - https://testsite.swisstomato.ch/creer-une-application/
    * - https://testsite.swisstomato.ch/developpement-app-web-startup-pme-geneve/
    * - https://testsite.swisstomato.ch/swiss-discovery-processus-de-planification/
    * - https://testsite.swisstomato.ch/creation-de-site-internet-a-geneve/
    * - https://testsite.swisstomato.ch/agence-web-geneve/services-de-developpement-wordpress/
    * - https://testsite.swisstomato.ch/agence-web-geneve/agence-de-services-du-developpement-drupal/
  */

  $type = get_field('type');
  if (empty($type)) {
    $type = 'centered';
  }

  $title = get_field('title');
  $description = get_field('description');
  $icons = get_field('icons');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>


<?php if ($is_preview): ?>

  <div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-icon-list-block stbase-block type-<?=$type?> block-margin">
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Icon List Block</h2>
        <h3><?=(empty($title) ? 'No title' : $title) . " (" . sizeof($icons) . " icons)"?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  </div>

<?php else: ?>

    <?php
      // START RENDERING TO STRING
      ob_start();
    ?>

    <div class="container">
      <?php if (!empty($title)) : ?>
        <h2 class="title"><?=$title?></h2>
      <?php endif; ?>

      <?php if (!empty($description)) : ?>
        <div class="description"><?=$description?></div>
      <?php endif; ?>

      <div class="icon-list">
        <?php foreach ($icons as $icon): ?>
          <div class="icon">
            <div class="icon-img">
              <?=get_img($icon['image'], esc_html(strip_tags($icon['text'])))?>
            </div>
            <?php if ($type == "left") : ?><div class="icon-content"><?php endif; ?>
              <h3><?=$icon['title']?></h3>
              <p><?=$icon['text']?></p>
            <?php if ($type == "left") : ?></div><?php endif; ?>
          </div>
        <?php endforeach; ?>
      </div>
    </div>

    <?php
      // END RENDERING TO STRING
      $output = ob_get_contents();
      ob_get_clean();
    ?>

    <script>
      window.iconListBlockContent = <?=json_encode($output)?>;
    </script>

    <div 
      <?=$id_attribute ? "id='$id_attribute'" : ""?>
      data-var="iconListBlockContent"
      class="stbase-icon-list-block stbase-block type-<?=$type?> block-margin"
    >
    </div>

<?php endif;?>



