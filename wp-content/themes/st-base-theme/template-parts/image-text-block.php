<?php
	$column_order = get_field('column_order');
	$title = get_field('title');
	$heading_type = get_field('heading_type');
	$text = get_field('text');
	$image = get_field('image');

	if (empty($heading_type)) { $heading_type = 'h2'; }

	$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-image-text-block stbase-block block-margin">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Image Text</h2>
        <h3><?= empty($title) ? 'No title' : $title ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <div class="container <?= $column_order ?>">
      <?php if ($column_order === 'image-text') : ?>
        <div class="column image-column zoom-in-on-scroll">
          <?= get_img($image, $title) ?>
        </div>
      <?php endif; ?>

      <div class="column text-column">
        <<?= $heading_type ?>><?= $title ?></<?= $heading_type ?>>
        <?= $text ?>
      </div>

      <?php if ($column_order !== 'image-text') : ?>
        <div class="column image-column zoom-out-on-scroll">
          <?= get_img($image, $title) ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
</div>