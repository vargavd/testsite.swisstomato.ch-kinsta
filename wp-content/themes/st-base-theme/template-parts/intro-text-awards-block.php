<?php
  $title = get_field('title');
  $text = get_field('text');
  $award_img = get_field('award_image');
  $button_link = get_field('button_link');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-intro-text-awards-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Intro Text Awards</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container center-1117">
      <div id="intro-title-and-text">
        <h1><?=$title?></h1>
        <div id="intro-text"><?=$text?></div>

        <?php if (!empty($button_link)): ?>
          <a href="<?=$button_link['url']?>" class="st-button-link">
            <i class="far fa-arrow-right"></i>
            <?=$button_link['title']?>
          </a>
        <?php endif; ?>
      </div>
      <?=get_img($award_img, 'full', 'Intro Awards', false)?>
    </div>
  <?php endif; ?>
</div>
