<?php
  $title = get_field('title');
  $text = get_field('text');
  $button_link = get_field('button_link');
  $image = get_field('image');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-intro-text-image-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Intro Text Image</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
      <div class="intro-block__text-column">
        <div class="text-column-content">
          <?=get_img($image, 'Intro Tablet Image', 'tablet-image', false)?>

          <h1><?=$title?></h1>
          <?=$text?>
          <a href="<?=$button_link['url']?>" class="st-button-link">
            <i class="far fa-arrow-right"></i>
            <?=$button_link['title']?>
          </a>
        </div>
      </div>
      <div class="intro-block__image-column">
        <?php
          if (!empty($image)) {
            print get_img($image, 'Intro Image', '', false);
          }
        ?>
      </div>
    </div>
  <?php endif; ?>
</div>