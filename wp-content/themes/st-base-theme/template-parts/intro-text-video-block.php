<?php
  $title = get_field('title');
  $text = get_field('text');
  $button_link = get_field('button_link');
  $video = get_field('video');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-intro-text-video-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Intro Text Video</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
      <div class="intro-block__text-column">
        <div class="text-column-content">
          <h1><?=$title?></h1>
          <?=$text?>
          <a href="<?=$button_link['url']?>" class="st-button-link">
            <i class="far fa-arrow-right"></i>
            <?=$button_link['title']?>
          </a>
  
          <a class="intro-block_next-block next-block-link">
            <i class="far fa-arrow-down"></i>
          </a>
        </div>
      </div>
      <div class="intro-block__video-column">
        <video class="intro-block__video" autoplay loop playsinline muted src="<?=$video['url']?>">
          Sorry, your browser doesn't support embedded videos.
        </video>
      </div>
    </div>
  <?php endif; ?>
</div>