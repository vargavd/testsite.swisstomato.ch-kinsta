<?php
  $id_attribute = get_field('id_attribute');
  $title = get_field('title');
  $intro = get_field('intro');
  $type = get_field('type');
  $category = get_field('category');
  $button = get_field('button');
  $button_link_to_category = get_field('button_link_to_category');

  $references = get_field('references');
  $number_of_references = is_array($references) ? sizeof($references) : 0;

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-references-masonry-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>References Masonry Block</h2>
        <h3>
          <?php 
            if ($type === 'like-archive') {
              print "Archive like (with intro and filters)";
            } elseif ($type === 'like-archive') {
              print (empty($title) ? 'No title' : $title) . " - <small>(Category: " . $category->name . ")</small>";
            } else {
              print (empty($title) ? 'No title' : $title) . " ($number_of_references references)";
            }
          ?>
        </h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <?php 
      $options = array();

      if ($type === 'one-category') {
        $options['one-category'] = array(
          'title' => $title,
          'category' => $category
        );
      } elseif ($type === 'like-archive') {
        $options['like-archive'] = array(
          'display_buttons' => true,
          'title'           => $title,
          'intro'           => $intro,
          'button_text'     => $button
        );
      } else {
        $options['specific-references'] = array(
          'title' => $title,
          'references' => $references,
          'button_text'     => $button,
          'button_link_to_category' => $button_link_to_category
        );
      }

      masonry_references_list_section_func($options);
    ?>
  <?php endif; ?>
</div>