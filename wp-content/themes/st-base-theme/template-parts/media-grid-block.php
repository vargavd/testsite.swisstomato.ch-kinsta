<?php
  $items = get_field('items');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-media-grid-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Media Grid</h2>
        <h3><?=sizeof($items) . ' items'?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="grid container-1250">
      <?php foreach ($items as $item): ?>
        <?php
          $title        = $item['title'];
          $inner_link   = $item['inner_link'];
          $image        = $item['image'];
        ?>
        <div class="item" data-scroll-to='<?=$inner_link?>'>
          <?=get_img($image, strip_tags($title)) ?>
          <div class="item__content">
            <div class="item__title">
              <?=$title?>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>