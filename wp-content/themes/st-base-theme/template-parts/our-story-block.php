<?php
  /*
    * On the following pages (testsite):
    * - https://testsite.swisstomato.ch/
    * - https://testsite.swisstomato.ch/creer-une-application/
    * - https://testsite.swisstomato.ch/creer-une-application/developpement-application-flutter-geneve/
    * - https://testsite.swisstomato.ch/creer-une-application/developpement-application-web-geneve/
    * - https://testsite.swisstomato.ch/developpement-app-web-startup-pme-geneve/
    * - https://testsite.swisstomato.ch/agence-web-geneve/services-de-developpement-wordpress/
    * - 
  */

  $title = get_field('title');
  $text = get_field('text');
  $img = get_field('image');
  $founder_1_title = get_field('founder_1_title'); 
  $founder_1_text = get_field('founder_1_text'); 
  $founder_2_title = get_field('founder_2_title'); 
  $founder_2_text = get_field('founder_2_text');

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<?php if ($is_preview): ?>
  <!-- PREVIEW -->  
  <div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-our-story-block stbase-block">
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Our Story</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  </div>

<?php else: ?>
  <!-- RENDER BLOCK -->

  <?php
    // START RENDERING TO STRING
    ob_start();
  ?>

  <div class="container">
    <div class="our-story-content">
      <h2><?=$title?></h2>

      <?=$text?>
    </div>

    <div class="our-story-image">
      <?=get_img($img, esc_html($title), '', true, false)?>

      <div class="founder-text-box">
        <header>
          <?=$founder_1_title?>
        </header>
        <p>
          <?=$founder_1_text?>
        </p>
      </div>
      <div class="founder-text-box">
        <header>
          <?=$founder_2_title?>
        </header>
        <p>
          <?=$founder_2_text?>
        </p>
      </div>
    </div>
  </div>

  <?php
    // END RENDERING TO STRING
    $output = ob_get_contents();
    ob_get_clean();
  ?>

  <script>
    window.ourStoryBlockContent = <?=json_encode($output)?>;
  </script>


  <div 
    <?=$id_attribute ? "id='$id_attribute'" : ""?>
    data-var="ourStoryBlockContent"
    class="stbase-our-story-block stbase-block"
  ></div>


<?php endif; ?>



</div>