<?php
  $title = get_field('title');
  $steps = get_field('steps');
  
  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-process-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Process Block</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container">
        <h2 class="title"><?= $title ?></h2>
        <?php if ( have_rows('steps') ) { 
            $counter = 1; ?>
            <div class="steps-wrapper">
                <?php while( have_rows('steps') ): the_row(); 
                    $step_title = get_sub_field('step_title');
                    $description = get_sub_field('description');
                    $add_explanation = get_sub_field('add_explanation');
                    $explanation = get_sub_field('explanation');
                    ?>
                    <div class="step">
                        <div class="number">
                            <span><?=$counter?></span>
                        </div>
                        <div class="content">
                            <h3 class="step-title"><?=$step_title?></h3>
                            <div class="step-description"><?=$description?></div>
                            <?php if ($add_explanation) { ?>
                                <div class="step-explanation"><?=$explanation?></div>
                            <?php } ?>
                        </div>
                    </div>
                <?php $counter ++; 
                endwhile; ?>
            </div>
        <?php } ?>
    </div>
  <?php endif; ?>
</div>