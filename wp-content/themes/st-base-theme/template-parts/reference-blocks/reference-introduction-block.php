<?php
$introduction_title = get_field('introduction_title');
$introduction_text = get_field('introduction_text');
$introduction_image = get_field('introduction_image');
$background_color = get_field('background_color');
$ID = get_the_ID();
$main_color = get_field('main_color', $ID);
$custom_color = get_field('custom_color');
$global_text_color_scheme = get_field('global_text_color_scheme', $ID);

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-reference-introduction-block  stbase-block color-<?= $global_text_color_scheme ?><?= $is_preview && ('preview') ?>">
    <?php if ($is_preview) : ?>
        <div class="stbase-block-preview">
            <div class="stbase-hero-title">
                <h2>Reference <em>(CS)</em> Introduction Block</h2>
                <h3><?= (empty($introduction_title) ? 'No title' : strip_tags($introduction_title)) ?></h3>
            </div>

            <span class="dashicons dashicons-ellipsis"></span>
        </div>
    <?php else : ?>
        <div class="introduction">
            <div class="content">
                <div class="content-inner" <?php if ($background_color == 'maincolor') { ?> style="background-color: <?= $main_color ?>;" <?php } elseif ($background_color == 'custom') { ?> style="background-color: <?= $custom_color ?>;" <?php } ?>>
                    <div class="container-860">
                        <h2 class="title"><?= $introduction_title ?></h2>
                        <div class="introduction-text"><?= $introduction_text ?></div>
                    </div>
                </div>
                <div class="introduction-image container-1080">
                    <img src=" <?= $introduction_image ?>" alt="<?= $introduction_title ?>" />
                </div>
            </div>
        </div>
</div>
<?php endif; ?>
</div>