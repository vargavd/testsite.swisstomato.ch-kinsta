<?php
$additional_text_block_before = get_field('additional_text_block_before');
$additional_text_title = get_field('additional_text_title');
$additional_text_text = get_field('additional_text_text');
$background_color = get_field('background_color');
$ID = get_the_ID();
$main_color = get_field('main_color', $ID);
$custom_color = get_field('custom_color');
$global_text_color_scheme = get_field('global_text_color_scheme', $ID);
$media_type = get_field('media_type');
$image = get_field('image');
$image_in_ipad = get_field('image_in_ipad');
$video_type = get_field('video_type');
$embedded_video = get_field('embedded_video');
$self_hosted_video = get_field('self_hosted_video');
$result_title = get_field('result_title');
$result_text = get_field('result_text');
$bottom_download_buttons = get_field('bottom_download_buttons');
$app_store_link = get_field('app_store_link');
$google_play_link = get_field('google_play_link');
$project_link_button = get_field('project_link_button');
$project_button = get_field('project_button');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-reference-result-block  stbase-block color-<?= $global_text_color_scheme ?><?= $is_preview && ('preview') ?> <?php if ($additional_text_block_before == "yes") { ?> additional<?php } ?>">
    <?php if ($is_preview) : ?>
        <div class="stbase-block-preview">
            <div class="stbase-hero-title">
                <h2>Reference <em>(CS)</em> Result Block</h2>
                <h3><?= (empty($result_title) ? 'No title' : strip_tags($result_title)) ?></h3>
            </div>

            <span class="dashicons dashicons-ellipsis"></span>
        </div>
        <?php else :
        if ($additional_text_block_before == "yes") { ?>
            <div class="additional-content content-inner container-640">
                <?php if ($additional_text_title) { ?>
                    <h2 class="title"><?= $additional_text_title ?></h2>
                <?php } ?>
                <div class="additional-text"><?= $additional_text_text ?></div>
            </div>
        <?php } ?>
        <div class="content media-type-<?= $media_type ?>">
            <div class="content-background" <?php if ($background_color == 'maincolor') { ?> style="background-color: <?= $main_color ?>;" <?php } elseif ($background_color == 'custom') { ?> style="background-color: <?= $custom_color ?>;" <?php } ?>></div>
            <?php if ($media_type == "image") { ?>
                <div class="media image container-1080">
                    <img src=" <?= $image ?>" alt="<?= $title ?>" />
                </div>
            <?php } elseif ($media_type == "ipad") { ?>
                <div class="media image-in-ipad container-1080">
                    <img src=" <?= $image_in_ipad ?>" class="ipad-inner" alt="<?= $title ?>" />
                    <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/iPad-frame.png" class="ipad-frame" alt="iPad frame" />
                </div>
            <?php } elseif ($media_type == "video") { ?>
                <div class="media video container-1080">
                    <?php if ($video_type == "embedded") { ?>
                        <div class='embed-container'><?= $embedded_video ?></div>
                    <?php } elseif ($video_type == "selfhosted") { ?>
                        <video width="100%" controls>
                            <source src="<?= $self_hosted_video ?>" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="result content-inner container-640">
                <h2 class="title"><?= $result_title ?></h2>
                <div class="result-text"><?= $result_text ?></div>
                <?php if ($bottom_download_buttons == "yes") { ?>
                    <div class="download-buttons">
                        <?php if ($app_store_link) { ?>
                            <a href="<?= $app_store_link ?>" target="_blank" class="button app-store">
                                <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/Appstore-badge-EN.svg" alt="App Store" />
                            </a>
                        <?php } ?>
                        <?php if ($google_play_link) { ?>
                            <a href="<?= $google_play_link ?>" target="_blank" class="button google-play">
                                <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/GooglePlay-badge-EN.svg" alt="Google Play" />
                            </a>
                        <?php } ?>
                    </div>
                <?php } if ($project_link_button == "yes") { ?>
                    <div class="project-button">
                        <a href="<?= $project_button['project_button_link'] ?>" target="_blank" class="button project">
                            <i class="fas fa-external-link-alt"></i>
                            <span><?= $project_button['project_button_text'] ?></span>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>
</div>