<?php
$title = get_field('title');
$text = get_field('text');
$image_left = get_field('image_left');
$image_right = get_field('image_right');
$background_color = get_field('background_color');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-reference-side-images-block  stbase-block <?= $is_preview && ('preview') ?>">
    <?php if ($is_preview) : ?>
        <div class="stbase-block-preview">
            <div class="stbase-hero-title">
                <h2>Reference <em>(CS)</em> Text + Side Images Block</h2>
                <h3><?= (empty($title) ? 'No title' : strip_tags($title)) ?></h3>
            </div>

            <span class="dashicons dashicons-ellipsis"></span>
        </div>
    <?php else : ?>
        <div class="background" style="background-color: <?= $background_color ?>"></div>
        <div class="content container-1080">
            <div class="image-left">
                <img src=" <?= $image_left ?>" alt="<?= $title ?>-left" />
            </div>
            <div class="content-inner container-640">
                <h2 class="title"><?= $title ?></h2>
                <div class="introduction-text"><?= $text ?></div>
            </div>
            <div class="image-right">
                <img src=" <?= $image_right ?>" alt="<?= $title ?>-right" />
            </div>
        </div>
    <?php endif; ?>
</div>