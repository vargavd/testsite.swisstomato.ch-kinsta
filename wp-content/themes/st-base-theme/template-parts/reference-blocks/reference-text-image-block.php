<?php
$title = get_field('title');
$text = get_field('text');
$image = get_field('image');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-reference-text-image-block  stbase-block <?= $is_preview && ('preview') ?>">
    <?php if ($is_preview) : ?>
        <div class="stbase-block-preview">
            <div class="stbase-hero-title">
                <h2>Reference <em>(CS)</em> Text + Image Block</h2>
                <h3><?= (empty($title) ? 'No title' : strip_tags($title)) ?></h3>
            </div>

            <span class="dashicons dashicons-ellipsis"></span>
        </div>
    <?php else : ?>
        <div class="content-inner container-640">
            <h2 class="title"><?= $title ?></h2>
            <div class="introduction-text"><?= $text ?></div>
        </div>
        <div class="image container-1080">
            <img src=" <?= $image ?>" alt="<?= $title ?>" />
        </div>
    <?php endif; ?>
</div>