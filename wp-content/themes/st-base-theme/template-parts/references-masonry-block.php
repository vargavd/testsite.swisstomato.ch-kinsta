<?php
  $id_attribute = get_field('id_attribute');
  $title = get_field('title');
  $show_intro_and_filters = get_field('show_intro_and_filters');
  $category = get_field('category');
?>

<div id="<?=$id_attribute?>" class="stbase-references-masonry-block stbase-block">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>References Masonry Block</h2>
        <h3><?=empty($title) ? 'No title' : $title?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <?php masonry_reference_list_section_func($title, $show_intro_and_filters, false, $category); ?>
  <?php endif; ?>
</div>