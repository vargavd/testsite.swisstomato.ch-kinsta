<?php
$services = get_field('services');

$id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-services-block stbase-block">
  <?php if ($is_preview) : ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Services</h2>
        <h3><?= sizeof($services) . ' services' ?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else : ?>
    <div id="services" class="<?= 'services-' . sizeof($services)?>">
      <?php foreach ($services as $service) : ?>
        <?php
        $background_image = $service['background_image'];
        $title = $service['title'];
        $subtitle = array_key_exists('subtitle', $service) ? $service['subtitle'] : '';
        $description = array_key_exists('description', $service) ? $service['description'] : '';
        $link_type = $service['link_type'];

        $link_attr = ($link_type === 'Inner Link') ? ('data-scroll-to="' . $service['inner_link'] . '"') : '';
        $normal_link = array_key_exists('normal_link', $service) ? $service['normal_link'] : '';
        ?>
        <div class="service" <?= $link_attr ?>>
          <div class="service-image">
            <?= get_img($background_image, esc_html(strip_tags($title))) ?>
          </div>
          <div class="sevice-titles-box">
            <h2 class="service-title">
              <?= $title ?>
            </h2>
            <?php if (!empty($subtitle)) : ?>
              <div class="service-subtitle">
                <?= $subtitle ?>
              </div>
            <?php endif; ?>
          </div>
          <?php if (!empty($description)) : ?>
            <div class="service-columns">
              <div class="column"></div>
              <div class="column"></div>
              <div class="column"></div>
            </div>
            <div class="service-cover">
              <?= $description ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($normal_link)) : ?>
            <a href="<?= $normal_link ?>" class="service-link"></a>
          <?php endif; ?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>