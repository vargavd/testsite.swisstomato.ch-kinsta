<?php
  /*
    On the following pages:
    - https://testsite.swisstomato.ch/
    - https://testsite.swisstomato.ch/creer-une-application/
    - https://testsite.swisstomato.ch/creer-une-application/agence-de-developpement-ios/
    - https://testsite.swisstomato.ch/creer-une-application/agence-de-developpement-dapplications-android/
    - https://testsite.swisstomato.ch/creer-une-application/developpement-application-flutter-geneve/
    - https://testsite.swisstomato.ch/creer-une-application/developpement-application-web-geneve/
    - https://testsite.swisstomato.ch/developpement-app-web-ong-nonprofit-geneve/
    - https://testsite.swisstomato.ch/agence-web-geneve/services-de-developpement-wordpress/
    - https://testsite.swisstomato.ch/agence-web-geneve/agence-de-services-du-developpement-drupal/
    - https://testsite.swisstomato.ch/developpement-app-web-startup-pme-geneve/
    - https://testsite.swisstomato.ch/applications-de-realite-augmentee-et-de-realite-virtuelle/
  */
  $title = get_field('title');
  $heading_type = get_field('heading_type');
  $testimonials = get_field('select_testimonials');

  if (empty($heading_type)) {
      $heading_type = 'h2';
  }

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<?php if ($is_preview) : ?>
  <div <?= $id_attribute ? "id='$id_attribute'" : "" ?> class="stbase-testimonials-block stbase-block">
    <div class="stbase-block-preview">
        <div class="stbase-hero-title">
            <h2>Testimonials</h2>
            <h3><?= empty($title) ? 'No title' : $title ?></h3>
            <div class="clientslist">Displayed Clients:
                <?php foreach ($testimonials as $testimonial) :
                    $name = get_the_title($testimonial->ID);
                    $company = get_field('company', $testimonial->ID); ?>
                    <span class="testimonial-name"><?= $name ?> <span>(<?= $company ?>)</span></span>
                <?php endforeach; ?>
            </div>
        </div>

        <span class="dashicons dashicons-ellipsis"></span>
    </div>
  </div>
<?php else : ?>

  <?php
    // START RENDERING TO STRING
    ob_start();
  ?>

  <div class="container">
      <<?= $heading_type ?> class="title"><?= $title ?></<?= $heading_type ?>>
      <div class="swiper">
          <?php if ($testimonials) : ?>
              <div class="swiper-wrapper">
                  <?php foreach ($testimonials as $testimonial) :
                      $name = get_the_title($testimonial->ID);
                      $testimonial_content = get_field('testimonial_content', $testimonial->ID);
                      $testimonial_image = get_the_post_thumbnail($testimonial->ID, 'thumbnail');
                      $position = get_field('position', $testimonial->ID);
                      $company = get_field('company', $testimonial->ID);
                      $company_logo = get_field('company_logo', $testimonial->ID);
                      $number_of_stars = get_field('number_of_stars', $testimonial->ID); ?>
                      <div class="swiper-slide" data-swiper-autoplay="6000">
                          <div class="testimonial">
                              <div class="stars">
                                  <?php for ($i = 0; $i < $number_of_stars; $i++) { ?>
                                      <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/icon-star.png" alt="★">
                                  <?php } ?>
                              </div>
                              <div class="testimonial-content"><?= $testimonial_content ?></div>
                              <div class="testimonial-bottom">
                                  <div class="testimonial-member">
                                      <div class="testimonial-image">
                                          <?php if ($testimonial_image) { ?>
                                              <?= $testimonial_image ?>
                                          <?php } else { ?>
                                              <img src="<?= get_stylesheet_directory_uri() ?>/assets/imgs/testimonial-placeholder.png" alt="Placeholder: <?= $name ?>">
                                          <?php } ?>
                                      </div>
                                      <div class="testimonial-details">
                                          <div class="testimonial-name"><?= $name ?></div>
                                          <div class="testimonial-position"><?= $position ?></div>
                                          <div class="testimonial-company"><?= $company ?></div>
                                      </div>
                                  </div>
                                  <div class="testimonial-company-logo"><img src="<?= $company_logo ?>" alt="<?= $company ?>"></div>
                              </div>
                          </div>
                      </div>
                  <?php endforeach; ?>
              </div>
              <div class=" swiper-pagination"></div>
          <?php endif; ?>
      </div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
  </div> <!-- /#testimonial-block -->

  <?php
    // END RENDERING TO STRING
    $output = ob_get_contents();
    ob_get_clean();
  ?>

  <script>
    window.testimonialsBlockContent = <?=json_encode($output)?>;
  </script>

  <div 
    <?= $id_attribute ? "id='$id_attribute'" : "" ?>
    data-testimonials-var="testimonialsBlockContent"
    class="stbase-testimonials-block stbase-block"
  >
  </div>
<?php endif; ?>