<?php
  $text = get_field('text');
  $button_link = get_field('button_link');
  $gray_background = get_field('gray_background');
  $text_align = get_field('text_align');

  $block_class = '';
  if ($gray_background && !$is_preview) {
    $block_class = 'block-with-background';
  }

  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-text-and-cta-block stbase-block block-margin <?=$block_class?>">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Text And CTA</h2>
        <h3><?=empty($text) ? 'No text' : strip_tags($text)?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div class="container <?=strtolower($text_align)?>">
      <div>
        <?=$text?>
      </div>
      <?php
        if (!empty($button_link)) {
          print render_acf_link($button_link, 'st-button-link', 'fas fa-angle-right');
        }
      ?>
    </div>
  <?php endif; ?>
</div>