<?php
  $title = get_field('title');
  $title_background = get_field('title_background');
  $reasons = get_field('reasons');
  
  $id_attribute = array_key_exists('anchor', $block) ? $block['anchor'] : '';
?>

<div <?=$id_attribute ? "id='$id_attribute'" : ""?> class="stbase-why-us-block stbase-block block-margin">
  <?php if ($is_preview): ?>
    <div class="stbase-block-preview">
      <div class="stbase-hero-title">
        <h2>Why Us</h2>
        <h3><?=empty($title) ? 'No text' : (strip_tags($title) . ' (' . sizeof($reasons) . ' reasons)')?></h3>
      </div>

      <span class="dashicons dashicons-ellipsis"></span>
    </div>
  <?php else: ?>
    <div id="why-us-title-wrapper">
      <?=get_img($title_background, $title)?>
      <h2 class="first">
        <?=$title?>
      </h2>
    </div>
    <div id="why-us-reasons">
      <?php foreach ($reasons as $reason): ?>
        <div class="why-us-reason">
          <?=get_img($reason['image'], $reason['title'])?>
          <h3><?=$reason['title']?></h3>
          <div class="reason-text">
            <p><?=$reason['text']?></p>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>