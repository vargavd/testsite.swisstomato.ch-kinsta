<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();
?>

	<div id="swisst2-404-page" class="swisst2-page" style="background-image: url(<?=get_field('404_page_background', 'option')?>);">
		
        <div class="container">
            <div id="page-404-content">
                <?=get_field('404_page_text', 'option')?>
            </div>
			<?= custom_image_size(get_field('404_page_smaller_image', 'option'), 'full', 'Tomato Splash' )  ?>
        </div>

    </div>

<?php
get_footer();
