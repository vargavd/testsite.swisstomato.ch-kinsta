<?php

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

// plugins
require_once "CMB2-plugins/CMB2-Post-Search-field-master/cmb2_post_search_field.php";

// site wide options
require "custom-settings-page.php";

// custom content types
require "custom-post-types.php";

// custom fields for pages
require "pages/home-page-fields.php";
