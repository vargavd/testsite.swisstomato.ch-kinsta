<?php

function swisst2_create_custom_post_types() {
    #region References
    register_taxonomy( 'reference_category', null, array(
        'labels' => array(
            'name'                       => 'Reference Categories',
            'singular_name'              => 'Reference Category',
            'search_items'               => 'Search Reference Categories',
            'all_items'                  => 'All Reference Categories',
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => 'Edit Reference Category',
            'update_item'                => 'Update Reference Category',
            'add_new_item'               => 'Add New Reference Category',
            'new_item_name'              => 'New Reference Category Name',
            'separate_items_with_commas' => 'Separate reference category with commas',
            'add_or_remove_items'        => 'Add or remove reference category',
            'choose_from_most_used'      => 'Choose from the most used reference categories',
            'not_found'                  => 'No reference category found.',
            'menu_name'                  => 'Reference Categories',
        ),
        'hierarchical'          => true,
		'show_ui'               => true,
		'show_admin_column'     => false,
		//'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
        'rewrite'               => array( 'slug' => 'reference_category' ),
        'show_in_rest'          => true
    ));

    register_taxonomy( 'reference_tag', null, array(
        'labels' => array(
            'name'                       => 'Reference Tags',
            'singular_name'              => 'Reference Tag',
            'search_items'               => 'Search Reference Tags',
            'all_items'                  => 'All Reference Tags',
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => 'Edit Reference Tag',
            'update_item'                => 'Update Reference Tag',
            'add_new_item'               => 'Add New Reference Tag',
            'new_item_name'              => 'New Reference Tag Name',
            'separate_items_with_commas' => 'Separate reference tag with commas',
            'add_or_remove_items'        => 'Add or remove reference tag',
            'choose_from_most_used'      => 'Choose from the most used reference Tags',
            'not_found'                  => 'No reference tag found.',
            'menu_name'                  => 'Reference Tags',
        ),
        'hierarchical'          => true,
		'show_ui'               => true,
		'show_admin_column'     => false,
		//'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
        'rewrite'               => array( 'slug' => 'reference_tag' ),
        'show_in_rest'          => true
    ));

    register_post_type( 'reference',
        array(
            'labels' => array(
                'name'               => 'References',
                'singular_name'      => 'Reference',
                'menu_name'          => 'References',
                'name_admin_bar'     => 'Reference', 
                'add_new'            => 'Add New', 'Reference',
                'add_new_item'       => 'Add New Reference',
                'new_item'           => 'New Reference',
                'edit_item'          => 'Edit Reference',
                'view_item'          => 'View Reference',
                'all_items'          => 'All References',
                'search_items'       => 'Search References',
                'parent_item_colon'  => 'Parent References',
                'not_found'          => 'No References found.',
                'not_found_in_trash' => 'No References found in Trash.',
            ),
            'public'         => true,
            'show_in_rest'  => true,
            'has_archive'    => true,
            'rewrite'        => array(
                'slug'       => 'references',
                'with_front' => true
            ),
            'query_var'      => 'casestudy',
            'taxonomies'     => array('reference_category', 'reference_tag'),
            'supports'       => array('title', 'editor', 'thumbnail'),
        )
    );
    #endregion
}
add_action( 'init', 'swisst2_create_custom_post_types' );