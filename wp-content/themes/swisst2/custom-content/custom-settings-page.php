<?php

/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function swisst2_register_main_options_metabox() {

	/**
	 * Registers main options page menu item and form.
	 */
	$custom_settings_page = new_cmb2_box( array(
		'id'           => 'swisst2_custom_settings_page',
		'title'        => 'Swisstomato General Settings',
		'object_types' => array( 'options-page' ),
		'option_key'      => 'swisst2_main_options', // The option key and admin menu page slug.
		// 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'cmb2' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'cmb2' ), // The text for the options-page save button. Defaults to 'Save'.
		// 'disable_settings_errors' => true, // On settings pages (not options-general.php sub-pages), allows disabling.
		// 'message_cb'      => 'swisst2_options_page_message_callback',
    ) );



    #region Contact
    $custom_settings_page->add_field( array(
        'name' => 'CONTACT SECTION',
        'type' => 'title',
        'id'   => 'swisst2_general_contact_section_admin_title'
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'Contact Title',
		'id'         => 'swisst2_contact_title',
        'type'       => 'text',
        'default'    => 'CONTACT US'
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'Contact Text',
		'id'         => 'swisst2_contact_text',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'textarea_rows' => 2
        ),
    ));

    $custom_settings_page->add_field( array(
		'name'    => 'Contact Image',
        'id'      => 'swisst2_contact_section_image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));
    
    $custom_settings_page->add_field( array(
		'name'       => 'Geneva Text',
		'id'         => 'swisst2_contact_geneva_text',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'textarea_rows' => 2
        ),
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'Zurich Text',
		'id'         => 'swisst2_contact_zurich_text',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'textarea_rows' => 2
        ),
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'Facebook Link',
		'id'         => 'swisst2_contact_facebook_link',
        'type'       => 'text',
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'Twitter Link',
		'id'         => 'swisst2_contact_twitter_link',
        'type'       => 'text',
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'LinkedIn Link',
		'id'         => 'swisst2_contact_linkedin_link',
        'type'       => 'text',
    ));
    #endregion


    #region Footer
    $custom_settings_page->add_field( array(
        'name' => 'FOOTER SECTION',
        'type' => 'title',
        'id'   => 'swisst2_general_footer_section_admin_title'
    ));

    $custom_settings_page->add_field( array(
		'name'    => 'Footer Logo',
        'id'      => 'swisst2_footer_logo',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));

    $custom_settings_page->add_field( array(
		'name'       => 'Privacy Policy Text',
		'id'         => 'swisst2_footer_privacy_policy_text',
        'type'       => 'text',
        'default'    => 'Privacy Policy'
    ));

    // $custom_settings_page->add_field( array(
	// 	'name'       => 'Privacy Policy Link',
	// 	'id'         => 'swisst2_footer_privacy_policy_link',
    //     'type'       => 'text',
    // ));

    $custom_settings_page->add_field( array(
		'name'       => 'Footer Text',
		'id'         => 'swisst2_footer_text',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'textarea_rows' => 3
        ),
    ));
    #endregion
}
add_action( 'cmb2_admin_init', 'swisst2_register_main_options_metabox' );


function swisst2_get_option( $key = '', $default = false ) {

	// if ( function_exists( 'cmb2_get_option' ) ) {
    //     // Use cmb2_get_option as it passes through some key filters.
	// 	return cmb2_get_option( 'swisst2_main_options', $key, $default );
	// }
	// Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option( 'swisst2_main_options', $default );
    
	$val = $default;
	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}
	return $val;
}

