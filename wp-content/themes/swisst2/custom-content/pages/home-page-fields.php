<?php

add_action( 'cmb2_admin_init', 'home_page_custom_fields_metabox' );
function home_page_custom_fields_metabox() {

	$custom_metabox = new_cmb2_box( array(
		'id'            => 'home_page_custom_metabox',
		'title'         => 'Home Page Custom Fields',
        'object_types'  => array( 'page' ), // Post type
        'show_on'      => array( 'id' => array( 9 ) )
    ) );

    #region INTRODUCTION panel
    $custom_metabox->add_field( array(
        'name' => 'INTRODUCTION SECTION',
        'type' => 'title',
        'id'   => 'home_page_introduction_admin_title'
    ) );

    // $custom_metabox->add_field( array(
	// 	'name'       => 'Title',
	// 	'id'         => 'home_page_introduction_section_title',
    //     'type'       => 'text',
    //     'default'    => 'Mobile App Development & Website Design'
    // ));

    // $custom_metabox->add_field( array(
	// 	'name'       => 'Text below title',
	// 	'id'         => 'home_page_introduction_text_below_title',
    //     'type'       => 'textarea_small',
    //     'default'    => 'We are a digital and web agency in Geneva & Zurich, specializing in app development + webdesign + pioneering in AR and VR apps.'
    // ));

    $custom_metabox->add_field( array(
		'name'    => 'Awards Image',
        'id'      => 'home_page_introduction_awards',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'medium',
    ));

    $services_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_introduction_services',
		'type'        => 'group',
		'description' => 'Services',
		'options'     => array(
			'group_title'   => 'Service {#}',
			'add_button'    => 'Add Service',
			'remove_button' => 'Remove Service',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
        'name'    => 'Image',
        'id'      => 'image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'medium',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'Title',
		'id'         => 'title',
		'type'       => 'text',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'Text',
		'id'         => 'text',
		'type'       => 'textarea_small',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'Button Text',
		'id'         => 'button_text',
		'type'       => 'text',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'Button Link',
		'id'         => 'button_link',
		'type'       => 'text',
    ));
    #endregion

    #region AWARDS panel
	$custom_metabox->add_field( array(
        'name' => 'AWARDS SECTION',
        'type' => 'title',
        'id'   => 'home_page_awards_admin_title'
    ));

    $custom_metabox->add_field( array(
        'name'    => 'Background Image',
        'id'      => 'home_page_awards_background_image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'large',
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Text',
		'id'         => 'home_page_awards_section_text',
        'type'       => 'text',
        'default'    => 'We are an award-winning web agency, bringing cutting-edge technical solutions wrapped in conscious designs that serve, not just amaze'
    ));

    $services_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_awards',
		'type'        => 'group',
		'description' => 'Awards',
		'options'     => array(
			'group_title'   => 'Award {#}',
			'add_button'    => 'Add Award',
			'remove_button' => 'Remove Award',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
        'name'    => 'Top Image',
        'id'      => 'top_image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'Award Title',
		'id'         => 'award_title',
		'type'       => 'text',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'App Title',
		'id'         => 'app_title',
		'type'       => 'text',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
		'name'       => 'Text',
		'id'         => 'text',
		'type'       => 'text',
    ));
    $custom_metabox->add_group_field( $services_repeatable_field_id, array(
        'name'    => 'Bottom Image',
        'id'      => 'bottom_image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));
    #endregion

    #region REFERENCES panel
	$custom_metabox->add_field( array(
        'name' => 'REFERENCES SECTION',
        'type' => 'title',
        'id'   => 'home_page_references_admin_title'
    ) );

    $custom_metabox->add_field( array(
		'name'       => 'Title',
		'id'         => 'home_page_references_section_title',
        'type'       => 'text',
        'default'    => 'References'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Mobile Dev Title',
		'id'         => 'home_page_references_mad_title',
        'type'       => 'text',
        'default'    => 'Mobile App Development'
    ));

    $mobile_apps_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_references_mobile_apps',
		'type'        => 'group',
		'description' => 'Mobile Apps',
		'options'     => array(
			'group_title'   => 'Mobile App {#}',
			'add_button'    => 'Add Mobile App',
			'remove_button' => 'Remove Mobile App',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    // $topics = get_terms(array(
    //     'taxonomy' => 'case_study_topic',
    //     'hide_empty' => false,
    //     'orderby' => 'id',
    // ));
    $mobile_app_array = array('' => '-');
    // foreach ($topics as $topic) {
    //     $mobile_app_array[$topic->slug] = $topic->name;
    // }
    $custom_metabox->add_group_field( $mobile_apps_repeatable_field_id, array(
		'name'             => 'Select an App',
        'id'               => 'app',
        'type'             => 'select',
        'show_option_none' => false,
        'options'        => $mobile_app_array, // TODO: ide kell majd megcsinálni a mobil appos arrayt
    ));

    $custom_metabox->add_field( array(
		'name'       => 'App Development Button Text',
		'id'         => 'home_page_references_mad_button_title',
        'type'       => 'text',
        'default'    => 'App Development references'
    ));

    // $custom_metabox->add_field( array(
	// 	'name'       => 'App Development Button Link',
	// 	'id'         => 'home_page_references_mad_button_link',
    //     'type'       => 'text',
    //     'default'    => 'App Development references'
    // ));

    $custom_metabox->add_field( array(
		'name'       => 'Webdev Title',
		'id'         => 'home_page_references_web_title',
        'type'       => 'text',
        'default'    => 'Website Development'
    ));

    $websites_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_references_websites',
		'type'        => 'group',
		'description' => 'Websites',
		'options'     => array(
			'group_title'   => 'Website {#}',
			'add_button'    => 'Add Website',
			'remove_button' => 'Remove Website',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    // $topics = get_terms(array(
    //     'taxonomy' => 'case_study_topic',
    //     'hide_empty' => false,
    //     'orderby' => 'id',
    // ));
    $website_array = array('' => '-');
    // foreach ($topics as $topic) {
    //     $website_array[$topic->slug] = $topic->name;
    // }
    $custom_metabox->add_group_field( $websites_repeatable_field_id, array(
		'name'             => 'Select an App',
        'id'               => 'app',
        'type'             => 'select',
        'show_option_none' => false,
        'options'        => $website_array, // TODO: ide kell majd megcsinálni a mobil appos arrayt
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Websites Button Text',
		'id'         => 'home_page_references_web_button_title',
        'type'       => 'text',
        'default'    => 'Webdesign references'
    ));

    // $custom_metabox->add_field( array(
	// 	'name'       => 'Websites Button Link',
	// 	'id'         => 'home_page_references_web_button_link',
    //     'type'       => 'text',
    //     'default'    => 'Webdesign references'
    // ));

    $custom_metabox->add_field( array(
		'name'       => 'ARVR Title',
		'id'         => 'home_page_references_arvr_title',
        'type'       => 'text',
        'default'    => 'Augmented & Virtual Reality'
    ));

    $arvr_apps_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_references_arvr_apps',
		'type'        => 'group',
		'description' => 'ARVR Apps',
		'options'     => array(
			'group_title'   => 'App {#}',
			'add_button'    => 'Add App',
			'remove_button' => 'Remove App',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    // $topics = get_terms(array(
    //     'taxonomy' => 'case_study_topic',
    //     'hide_empty' => false,
    //     'orderby' => 'id',
    // ));
    $app_array = array('' => '-');
    // foreach ($topics as $topic) {
    //     $app_array[$topic->slug] = $topic->name;
    // }
    $custom_metabox->add_group_field( $arvr_apps_repeatable_field_id, array(
		'name'             => 'Select an App',
        'id'               => 'app',
        'type'             => 'select',
        'show_option_none' => false,
        'options'        => $app_array, // TODO: ide kell majd megcsinálni a mobil appos arrayt
    ));

    $custom_metabox->add_field( array(
		'name'       => 'ARVR Button Text',
		'id'         => 'home_page_references_arvr_button_title',
        'type'       => 'text',
        'default'    => 'AR & VR references'
    ));

    // $custom_metabox->add_field( array(
	// 	'name'       => 'ARVR Button Link',
	// 	'id'         => 'home_page_references_arvr_button_link',
    //     'type'       => 'text',
    // ));

    $custom_metabox->add_field( array(
		'name'       => 'All References Button Text',
		'id'         => 'home_page_references_all_button_title',
        'type'       => 'text',
        'default'    => 'All references'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'All References Button Link',
		'id'         => 'home_page_references_all_button_link',
        'type'       => 'text',
    ));

    #endregion

    #region CLIENTS panel
	$custom_metabox->add_field( array(
        'name' => 'CLIENTS SECTION',
        'type' => 'title',
        'id'   => 'home_page_clients_admin_title'
    ) );

    $custom_metabox->add_field( array(
		'name'       => 'Title',
		'id'         => 'home_page_clients_section_title',
        'type'       => 'text',
        'default'    => 'Clients'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Text',
		'id'         => 'home_page_clients_section_text',
        'type'       => 'textarea_small',
        'default'    => 'We are proud to be trusted by major Clients across various industries'
    ));

    $clients_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_clients',
		'type'        => 'group',
		'description' => 'Clients',
		'options'     => array(
			'group_title'   => 'Client {#}',
			'add_button'    => 'Add Client',
			'remove_button' => 'Remove Client',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    $custom_metabox->add_group_field( $clients_repeatable_field_id, array(
        'name'    => 'Image',
        'id'      => 'image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));
    $custom_metabox->add_group_field( $clients_repeatable_field_id, array(
		'name'       => 'Client name (hover title)',
		'id'         => 'title',
		'type'       => 'text',
    ));
    #endregion

    #region ABOUT US panel
	$custom_metabox->add_field( array(
        'name' => 'ABOUT US SECTION',
        'type' => 'title',
        'id'   => 'home_page_about_us_admin_title'
    ) );

    $custom_metabox->add_field( array(
		'name'       => 'Title',
		'id'         => 'home_page_about_us_section_title',
        'type'       => 'text',
        'default'    => 'About us'
    ));

    $aboutus_boxes_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_aboutus_boxes',
		'type'        => 'group',
		'description' => 'Boxes',
		'options'     => array(
			'group_title'   => 'Box {#}',
			'add_button'    => 'Add Box',
			'remove_button' => 'Remove Box',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    $custom_metabox->add_group_field( $aboutus_boxes_repeatable_field_id, array(
        'name'    => 'Image',
        'id'      => 'image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));
    $custom_metabox->add_group_field( $aboutus_boxes_repeatable_field_id, array(
		'name'    => 'Title',
		'id'      => 'title',
        'type'    => 'text'
    ));
    $custom_metabox->add_group_field( $aboutus_boxes_repeatable_field_id, array(
		'name'    => 'Text',
		'id'      => 'text',
        'type'    => 'wysiwyg',
	    'options' => array(
            'rows' => 10
        ),
    ));
    #endregion

    #region WHY US panel
	$custom_metabox->add_field( array(
        'name' => 'WHY US SECTION',
        'type' => 'title',
        'id'   => 'home_page_why_us_admin_title'
    ) );

    $custom_metabox->add_field( array(
		'name'       => 'Title',
		'id'         => 'home_page_why_us_section_title',
        'type'       => 'text',
        'default'    => 'Why us?'
    ));

    $custom_metabox->add_field( array(
        'name'    => 'Title Background',
        'id'      => 'home_page_why_us_title_background',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'medium',
    ));

    $aboutus_boxes_repeatable_field_id = $custom_metabox->add_field( array(
		'id'          => 'home_page_why_us_reasons',
		'type'        => 'group',
		'description' => 'Boxes',
		'options'     => array(
			'group_title'   => 'Box {#}',
			'add_button'    => 'Add Box',
			'remove_button' => 'Remove Box',
            'sortable'      => true,
            'closed'        => true,
		),
    ));
    $custom_metabox->add_group_field( $aboutus_boxes_repeatable_field_id, array(
        'name'    => 'Image',
        'id'      => 'image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'small',
    ));
    $custom_metabox->add_group_field( $aboutus_boxes_repeatable_field_id, array(
		'name'    => 'Title',
		'id'      => 'title',
        'type'    => 'text'
    ));
    $custom_metabox->add_group_field( $aboutus_boxes_repeatable_field_id, array(
		'name'    => 'Text',
		'id'      => 'text',
        'type'    => 'wysiwyg',
	    'options' => array(
            'rows' => 5
        ),
    ));
    #endregion

    #region OUR STORY panel
	$custom_metabox->add_field( array(
        'name' => 'OUR STORY SECTION',
        'type' => 'title',
        'id'   => 'home_page_our_story_admin_title'
    ) );

    $custom_metabox->add_field( array(
		'name'       => 'Title',
		'id'         => 'home_page_our_story_section_title',
        'type'       => 'text',
        'default'    => 'Our story'
    ));
    
    $custom_metabox->add_field( array(
		'name'       => 'Text',
		'id'         => 'home_page_our_story_section_text',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'rows' => 5
        ),
    ));

    $custom_metabox->add_field( array(
        'name'    => 'Our Story Image',
        'id'      => 'home_page_our_story_image',
        'type'    => 'file',
        'options' => array(
            'url' => false, 
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select Image'
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => 'image', 
        ),
        'preview_size' => 'medium',
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Founder Name 1',
		'id'         => 'home_page_our_story_founder_name1',
        'type'       => 'text',
        'default'    => 'Daniel Racsko'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Founder Title 1',
		'id'         => 'home_page_our_story_founder_title1',
        'type'       => 'text',
        'default'    => 'Co-founder'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Founder Text 1',
		'id'         => 'home_page_our_story_founder_text1',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'rows' => 5
        ),
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Founder Name 2',
		'id'         => 'home_page_our_story_founder_name2',
        'type'       => 'text',
        'default'    => 'Norbert Racsko'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Founder Title 2',
		'id'         => 'home_page_our_story_founder_title2',
        'type'       => 'text',
        'default'    => 'Co-founder'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Founder Text 2',
		'id'         => 'home_page_our_story_founder_text2',
        'type'       => 'wysiwyg',
	    'options'    => array(
            'rows' => 5
        ),
    ));
    #endregion

    #region POSTS panel
	$custom_metabox->add_field( array(
        'name' => 'POSTS SECTION',
        'type' => 'title',
        'id'   => 'home_page_posts_admin_title'
    ) );

    $custom_metabox->add_field( array(
		'name'       => 'Title',
		'id'         => 'home_page_posts_section_title',
        'type'       => 'text',
        'default'    => 'Posts'
    ));

    $custom_metabox->add_field( array(
		'name'       => 'Button Text',
		'id'         => 'home_page_posts_button_text',
        'type'       => 'text',
    ));

    // $custom_metabox->add_field( array(
	// 	'name'       => 'Button Link',
	// 	'id'         => 'home_page_posts_button_link',
    //     'type'       => 'text',
    // ));
    #endregion

}
