<?php
/**
 * Template Name: App Cost Post Template
 * Template Post Type: post
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();
?>

	<div id="app-cost-post" class="swisst2-page">

        <div id="hero-section" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)" data-responsive-bc-infos="<?=get_field('hero_image_tablet', $ID)?>|800||<?=get_field('hero_image_mobile', $ID)?>|445">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>

        <div id="first-section">
            <div class="container">
                <div id="app-cost-body"><?php echo apply_filters('the_content', $post->post_content); ?></div>
    
                <div id="inner-links">
                    <?php
                        $links = get_field('inner_links', $ID);
    
                        foreach ($links as $link):
                    ?>
                        <div class="inner-link">
                            <div class="number">
                                <?=$link['number_text']?>
                            </div>
                            <a href="<?=$link['link']?>" class="title">
                                <?=$link['title']?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="app-cost-banner" style="background-image: url(<?=get_field('banner_background', $ID)?>);" data-responsive-bc-infos="<?=get_field('banner_background_mobile', $ID)?>|520">
                <div class="banner-logo-wrapper">
                    <div class="banner-logo-bc"><img src="<?=get_field('banner_swisstomato_logo', $ID)?>" alt="Banner Swiss Tomato Logo" class="banner-st-logo"></div>
                </div>
    
                <div class="banner-content">
                    <p><?=get_field('banner_text', $ID)?></p>
    
                    <a href="#contact-us" class="button-red">
                        <?=get_field('banner_button_text', $ID)?>
                    </a>
                </div>
            </div>
        </div>

        <div id="how-much-section" class="container">
            <h2><?=get_field('how_much_section_title', $ID)?></h2>

            <div class="first-text" id="how-much-first-text">
                <?=get_field('how_much_section_first_text', $ID)?>
            </div>

            <div id="how-much-main-text">
                <?=get_field('how_much_section_text', $ID)?>
            </div>

            <div id="how-much-big-list" class="big-list">
                <?php
                    $howMuchList = get_field('how_much_section_big_list', $ID);

                    foreach ($howMuchList as $howMuchItem): 
                ?>
                    <div class="big-list-item <?=$howMuchItem['highlighted'] ? 'highlighted' : ''?>">
                        <div class="big-list-item-text">
                            <?=$howMuchItem['text']?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        
        <div id="how-long-section" class="container">
            <h2><?=get_field('how_long_section_title', $ID)?></h2>

            <div class="first-text">
                <?=get_field('how_long_section_text', $ID)?>
            </div>

            <div class="big-list">
                <?php
                    $howLongList = get_field('how_long_section_big_list_', $ID);

                    foreach ($howLongList as $howLongItem):
                ?>
                    <div class="big-list-item <?=$howLongItem['highlighted'] ? 'highlighted' : ''?>">
                        <div class="big-list-media">
                            <div class="progress-bar-wrapper">
                                <div class="progress-bar"></div>
                            </div>
                        </div>

                        <div class="big-list-item-text">
                            <?=$howLongItem['text']?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <?php
            process_and_timing_func($ID);
        ?>

        <div class="container">
            <div class="app-cost-banner" style="background-image: url(<?=get_field('banner_background', $ID)?>);" data-responsive-bc-infos="<?=get_field('banner_background_mobile', $ID)?>|520">
                <div class="banner-logo-wrapper">
                    <div class="banner-logo-bc"><img src="<?=get_field('banner_swisstomato_logo', $ID)?>" alt="Banner Swiss Tomato Logo" class="banner-st-logo"></div>
                </div>
    
                <div class="banner-content">
                    <p><?=get_field('banner_text', $ID)?></p>
    
                    <a href="#contact-us" class="button-red">
                        <?=get_field('banner_button_text', $ID)?>
                    </a>
                </div>
            </div>
        </div>
                

        <?php
            reference_list_standard($ID);
        ?>

        <div id="swisstomato-section" class="container">
            <h2><?=get_field('swiss_tomato_section_title', $ID)?></h2>

            <div id="swisstomato-section-text">
                <?=get_field('swiss_tomato_section_text', $ID)?>
            </div>
        </div>

        <div id="app-cost-awards-wrapper">
            <h2><?=get_field('award_section_title', 'option')?></h2>
            
            <?php
                include "global-templates/awards-big.php";
            ?>
        </div>

        <div class="after-text container">
            <?=get_field('after_awards_text', $ID)?>
        </div>

        <?php
            include "global-templates/clients.php";
        ?>

        <div class="after-text container">
            <?=get_field('after_clients_text', $ID)?>
        </div>

        <?php
            field_of_experience_section($ID);
        ?>

        <div class="after-text container">
            <?=get_field('after_fields_text', $ID)?>
        </div>

    </div>

<?php
get_footer();
