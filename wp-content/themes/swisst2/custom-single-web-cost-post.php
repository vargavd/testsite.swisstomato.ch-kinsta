<?php
/**
 * Template Name: Web Cost Post Template
 * Template Post Type: post
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();
?>

<article id="web-cost-post" class="swisst2-page">

  <?php
    while ( have_posts() ) : 
      the_post();

      $img_infos = wp_get_attachment_image_src(get_post_thumbnail_id($post), 'full');
      $img_height = $img_infos[2];
      $img_url = $img_infos[0];

      $thumbnail_class = '';
      if ($img_infos[1] > 1160) {
        $thumbnail_class = "full-width-img";
      }

      if ($img_height > 564) {
        $img_height = 564;
      }
  ?>

      <div class="hero">
        <h1><?=get_the_title()?></h1>
  
        <div class="post-date"><?=get_the_date("F d, o")?></div>

        <img src="<?=$img_url?>" alt="<?=get_the_title()?>" id="post-thumbnail">
      </div>

      <div class="narrow">
        <?php
          the_content();
        ?>

        <?=get_field('summary_text', $ID)?>

        <?=get_field('main_cost_text', $ID)?>
      </div>

      <div class="cost-table-block">
        <div class="cost-table-wrapper">
          <table>
            <?php
              $main_cost_table = get_field('main_cost_table', $ID);
            ?>

            <thead>
              <tr>
                <th class="title empty"></th>
                <th class="description empty"></th>
                <th class="hours"><?=$main_cost_table['low_end_hours_text']?></th>
                <th class="hours"><?=$main_cost_table['high_end_hours_text']?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($main_cost_table['main_cost_rows'] as $row): ?>
                <tr>
                  <td class="title"><?=$row['title']?></td>
                  <td class="description"><?=$row['description']?></td>
                  <td class="hours">
                    <span class="mobile-label">
                      <?=$main_cost_table['low_end_hours_text']?>
                    </span>
                    <span><?=$row['low_end_hours']?></span>
                  </td>
                  <td class="hours">
                    <span class="mobile-label">
                      <?=$main_cost_table['high_end_hours_text']?>
                    </span>
                    <span><?=$row['high_end_hours']?></span>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>

      <!-- <div class="cost-examples-block">
        <?php 
          foreach (get_field('examples', $ID) as $example): 
            $image = $example['image'];
            $subtitle = $example['subtitle'];
            $image_alt = strip_tags($subtitle);
        ?>
          <div class="example">
            <?=get_img($image, $image_alt)?>
            <?=$subtitle?>
          </div>
        <?php endforeach; ?>
      </div> -->

      <div class="narrow">
        <?=get_field('additional_cost_text', $ID)?>
      </div>
      
      <div class="cost-table-block">
        <div class="cost-table-wrapper">
          <table>
            <?php
              $main_cost_table = get_field('additional_cost_table', $ID);
            ?>
  
            <thead>
              <tr>
                <th class="title empty"></th>
                <th class="description empty"></th>
                <th class="hours"><?=$main_cost_table['low_end_hours_text']?></th>
                <th class="hours"><?=$main_cost_table['high_end_hours_text']?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($main_cost_table['main_cost_rows'] as $row): ?>
                <tr>
                  <td class="title"><?=$row['title']?></td>
                  <td class="description"><?=$row['description']?></td>
                  <td class="hours">
                    <span class="mobile-label">
                      <?=$main_cost_table['low_end_hours_text']?>
                    </span>
                    <span><?=$row['low_end_hours']?></span>
                  </td>
                  <td class="hours">
                    <span class="mobile-label">
                      <?=$main_cost_table['high_end_hours_text']?>
                    </span>
                    <span><?=$row['high_end_hours']?></span>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="narrow">
        <?=get_field('additional_3rd_party_cost_text', $ID)?>
      </div>

      <div class="cost-table-block">
        <div class="cost-table-wrapper">
          <table>
            <?php
              $main_cost_table = get_field('additional_3rd_party_cost_table', $ID);
            ?>
  
            <thead>
              <tr>
                <th class="title empty"></th>
                <th class="description empty"></th>
                <th class="hours double"><?=$main_cost_table['low_end_hours_text']?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($main_cost_table['main_cost_rows'] as $row): ?>
                <tr>
                  <td class="title"><?=$row['title']?></td>
                  <td class="description"><?=$row['description']?></td>
                  <td class="hours double">
                    <span class="mobile-label">
                      <?=$main_cost_table['low_end_hours_text']?>
                    </span>
                    <span>
                      <?=$row['low_end_hours']?> - <?=$row['high_end_hours']?>
                    </span>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="narrow">
        <?=get_field('last_text', $ID)?>
      </div>

      <div class="website-development-link-block">
        <p><?=get_field('website_development_title', $ID)?></p>

        <?php
          $website_development_link = get_field('website_development_link', $ID);
        ?>

        <a href="<?=$website_development_link['url']?>" class="button-red more-references">
          <i class="fas fa-angle-right"></i>
          <?=$website_development_link['title']?>
        </a>
      </div>
  <?php endwhile; ?>

</article><!-- /#web-cost-post -->

<?php
get_footer();
