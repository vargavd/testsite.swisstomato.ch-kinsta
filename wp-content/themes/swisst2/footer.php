<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Swisstomato2
 */

$current_lang = get_bloginfo('language');

?>

	</div><!-- #content -->

    <div id="video-popup-wrapper">
        <div id="video-popup">
        </div>
    </div>

    <div id="contact-us-wrapper">
        <?php if ($current_lang === 'de-DE'): ?>
            <div id="contact-us" class="container mod-on-de">
                <div id="contact-column-1">
                    <h2>
                        <?=get_field('contact_section_title', 'option')?>
                    </h2>
                    <div id="contact-us-text">
                        <?=get_field('contact_section_text', 'option')?>
                    </div>
                    <?php 
                        if (get_bloginfo('language') == 'en-US') {
                            echo do_shortcode('[contact-form-7 id="5" title="Contact form"]');
                        } elseif (get_bloginfo('language') == 'de-DE') {
                            echo do_shortcode('[contact-form-7 id="6341" title="Contact form DE"]');
                        } else {
                            echo do_shortcode('[contact-form-7 id="6340" title="Contact form FR"]');
                        }
                    ?>
                </div>
                <div id="contact-column-2" style="background-image: url(<?=get_img_url_webp_if_possible(get_field('contact_section_background', 'option'))?>);">
    				<?= custom_image_size(get_field('contact_section_image', 'option'), 'full', 'Contact Us Image' ) ?>
                    <div id="contact-informations">
                        <!-- <div id="city-list">
                            <a data-target="#contact-geneva">Geneva</a>
                            <a data-target="#contact-zurich">Zurich</a>
                            <a data-target="#contact-london">London</a>
                            <a data-target="#contact-budapest">Budapest</a>
                        </div> -->
                        <?php
                            if (get_bloginfo('language') == 'de-DE') { ?>
                                <div class="contact-info-text" id="contact-zurich">
                                    <h3><?=__('Zurich', 'swisst2')?></h3>
                                    <?=get_field('contact_section_zurich_text', 'option')?>
                                </div>
                        <?php }else{ ?>
                                <div class="contact-info-text" id="contact-geneva">
                                    <h3><?=__('Geneva', 'swisst2')?></h3>
                                    <?=get_field('contact_section_geneva_text', 'option')?>
                                </div>
                                <div class="contact-info-text" id="contact-zurich">
                                    <h3><?=__('Zurich', 'swisst2')?></h3>
                                    <?=get_field('contact_section_zurich_text', 'option')?>
                                </div>
                        <?php
                            }
                        ?>
                        <!-- <div class="contact-info-text" id="contact-london">
                            <?=get_field('contact_section_london_text', 'option')?>
                        </div>
                        <div class="contact-info-text" id="contact-budapest">
                            <?=get_field('contact_section_budapest_text', 'option')?>
                        </div> -->
                    </div>
                    <a target="_blank" class="contact-social-icon" href="<?=get_field('facebook_link', 'option')?>">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <!-- <a target="_blank" class="contact-social-icon" href="<?=get_field('twitter_link', 'option')?>">
                        <i class="fab fa-twitter"></i>
                    </a> -->
                    <a target="_blank" class="contact-social-icon" href="<?=get_field('linkedin_link', 'option')?>">
                        <i class="fab fa-linkedin"></i>
                    </a>
                </div>
            </div>
        <?php else: ?>
    
            <div id="contact-us" class="container">
                <div id="contact-column-1">
                    <h2>
                        <?=get_field('contact_section_title', 'option')?>
                    </h2>
                    <div id="contact-us-text">
                        <?=get_field('contact_section_text', 'option')?>
                    </div>
                    <?php 
                        if (get_bloginfo('language') == 'en-US') {
                            echo do_shortcode('[contact-form-7 id="5" title="Contact form"]');
                        } elseif (get_bloginfo('language') == 'de-DE') {
                            echo do_shortcode('[contact-form-7 id="6341" title="Contact form DE"]');
                        } else {
                            echo do_shortcode('[contact-form-7 id="6340" title="Contact form FR"]');
                        }
                    ?>
                </div>
                <div id="contact-column-2">
    				<?= custom_image_size(get_field('contact_section_image', 'option'), 'full', 'Contact Us Image'); ?>
                    <div id="contact-informations">
                        <!-- <div id="city-list">
                            <a data-target="#contact-geneva">Geneva</a>
                            <a data-target="#contact-zurich">Zurich</a>
                            <a data-target="#contact-london">London</a>
                            <a data-target="#contact-budapest">Budapest</a>
                        </div> -->
                        <?php
                            if (get_bloginfo('language') == 'de-DE') { ?>
                                <div class="contact-info-text" id="contact-zurich">
                                    <h3><?=__('Zurich', 'swisst2')?></h3>
                                    <?=get_field('contact_section_zurich_text', 'option')?>
                                </div>
                        <?php }else{ ?>
                                <div class="contact-info-text" id="contact-geneva">
                                    <h3><?=__('Geneva', 'swisst2')?></h3>
                                    <?=get_field('contact_section_geneva_text', 'option')?>
                                </div>
                                <div class="contact-info-text" id="contact-zurich">
                                    <h3><?=__('Zurich', 'swisst2')?></h3>
                                    <?=get_field('contact_section_zurich_text', 'option')?>
                                </div>
    							<?php if ( strpos(get_page_template(), 'page-webar') && get_bloginfo('language') == 'en-US' ) : ?>
    							<div class="contact-info-text" id="contact-london">
    								<h3><?=__('London', 'swisst2')?></h3>
                                    <?=get_field('contact_section_london_text', 'option')?>
    							</div>
    							<?php endif; ?>
                        <?php
                            }
                        ?>
                        <!-- <div class="contact-info-text" id="contact-london">
                            <?=get_field('contact_section_london_text', 'option')?>
                        </div>
                        <div class="contact-info-text" id="contact-budapest">
                            <?=get_field('contact_section_budapest_text', 'option')?>
                        </div> -->
                    </div>
                    <a target="_blank" class="contact-social-icon" rel="noopener" href="<?=get_field('facebook_link', 'option')?>">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <!-- <a target="_blank" class="contact-social-icon" href="<?=get_field('twitter_link', 'option')?>">
                        <i class="fab fa-twitter"></i>
                    </a> -->
                    <a target="_blank" class="contact-social-icon" rel="noopener" href="<?=get_field('linkedin_link', 'option')?>">
                        <i class="fab fa-linkedin"></i>
                    </a>
                </div>
            </div>
    
        <?php endif; ?>
    </div>

	<div id="footer-wrapper">
	    <footer id="colophon" class="site-footer container <?=get_bloginfo('language')?>">
            <div id="footer-logo-wrapper">
    		   <?= custom_image_size(get_field('footer_logo', 'option'), 'full', 'Footer Logo'); ?>
            </div>
            <div id="footer-pivacy-policy-wrapper">
                <a href="<?=get_privacy_policy_url()?>" id="footer-privacy-policy-link">
                    <?=get_field('footer_privacy_policy_text', 'option')?>
                </a>
            </div>
            <div id="footer-text">
                <?=get_field('footer_text', 'option')?>
            </div>
            <div id="footer-social-links">
                <a target="_blank" rel="noopener" href="<?=get_field('facebook_link', 'option')?>">
                    <i class="fab fa-facebook"></i>
                </a>
                <!-- <a target="_blank" href="<?=get_field('twitter_link', 'option')?>">
                    <i class="fab fa-twitter"></i>
                </a> -->
                <a target="_blank" rel="noopener" href="<?=get_field('linkedin_link', 'option')?>">
                    <i class="fab fa-linkedin"></i>
                </a>
            </div>
    	</footer><!-- footer#colophon -->
	</div>
	<?php 

	// Cookie
	$cookie_text = trim(get_field('cookie_text', 'option')) ? trim(get_field('cookie_text', 'option')) : __('We are using cookies to give you the best experience on our website.', 'swisst2');
	$cookie_link = get_field('cookie_url', 'option') ? get_field('cookie_url', 'option') : '';
	$cookie_btn  = trim(get_field('cookie_button', 'option')) ? trim(get_field('cookie_button', 'option')) : __('Accept', 'swisst2');

	$cookie_url 	= '';
	$cookie_title 	= '';
	$cookie_target  = '';

	if ( $cookie_link ) {
		$cookie_url 	= $cookie_link['url'] ? $cookie_link['url'] : '';
		$cookie_title 	= trim($cookie_link['title']) ? trim($cookie_link['title']) : __('Privacy Policy', 'swisst2');
		$cookie_target  = $cookie_link['target'] ? $cookie_link['target'] : '_self';
	} ?>

	<div id="cookie" class="cookie">
		<p class="cookie__text"><?= $cookie_text ?></p>
		<?php if( $cookie_url ) : ?>
			<a href="<?= $cookie_url ?>" target="<?= $cookie_target ?>" class="cookie__url"><?= $cookie_title ?></a>
		<?php endif; ?>
		<button class="cookie__btn"><?= $cookie_btn ?></button>
	</div>

</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-50926355-8"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-50926355-8', { 'anonymize_ip': true });

jQuery(document).ready(function ($) {

    $('a[href="#contact-us"]').click(function () {
        gtag('event', 'Click', {
            'event_category' : 'Contact',
            'event_label' : 'Click on contact button'
        });
    });

	$('#contact-us .wpcf7').on('wpcf7mailsent', function () {
		gtag('event', 'Click', {
			'event_category' : 'Send',
			'event_label' : 'Click on send button'
		});
	});

	$('#contact-us a[href*="tel:"], #colophon a[href*="tel:"]').click(function () {
		gtag('event', 'Click', {
			'event_category' : 'Call',
			'event_label' : 'Click on phone number'
		});
	});

	$('#contact-us a[href*="mailto:"], #colophon a[href*="mailto:"]').click(function () {
		gtag('event', 'Click', {
			'event_category' : 'Email',
			'event_label' : 'Click on email address'
		});
    });

});
</script>
</body>
</html>
