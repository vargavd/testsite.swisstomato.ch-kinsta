<?php
/**
 * Swisstomato2 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Swisstomato2
 */

include "inc/enqueue.php";
include "inc/helper.php";
include "inc/setup.php";
include "inc/reference-reorder-enpoint.php";
include "inc/reference-list-columns.php";

include "custom-content/custom-post-types.php";

include "global-templates/reference-list-func.php";
include "global-templates/field-of-experience-func.php";
include "global-templates/process-timing-func.php";
include "global-templates/check-list-func.php";
include "global-templates/dm-internal-links-func.php";
include "global-templates/swisst-banner-func.php";
include "global-templates/why-swisstomato-section.php";
include "global-templates/big-bc-more-text-section.php";
include "global-templates/icon-text-list-func.php";
include "global-templates/services-func.php";
include "global-templates/intro-text-awards.php";
include "global-templates/why-us-our-story-func.php";
include "global-templates/big-red-text-func.php";
include "global-templates/our-story-func.php";
include "global-templates/awards-2022feb-func.php";
include "global-templates/reference-list-masonry-func.php";


// MODIFYING HREFLANGS
add_filter('wpml_hreflangs_html', function ($html) {
  return str_replace('swisstomato.ch"', 'swisstomato.ch/"', $html);
}, 100, 2);



// ADDING PAGE TEMPLATE TO THE PAGES LIST
add_filter( 'manage_pages_columns', function ($defaults)
{
   $defaults['page-layout'] = __('Template');
   return $defaults;
});
add_action( 'manage_pages_custom_column', function ($column_name, $id)
{
   if ( $column_name === 'page-layout' ) {
       $set_template = get_post_meta( get_the_ID(), '_wp_page_template', true );
       if ( $set_template == 'default' ) {
           echo 'Default';
       }
       $templates = get_page_templates();
       ksort( $templates );
       foreach ( array_keys( $templates ) as $template ) :
           if ( $set_template == $templates[$template] ) echo $template;
       endforeach;
   }
}, 5, 2 );



// DISABLE JS WebP REWRITING (EWWW plugin) FOR CERTAIN IMAGES
add_filter('ewww_image_optimizer_skip_webp_rewrite', function ($bypass, $url) {
    $skip_img_url_parts = [
        'imgs/home/parallax',
        'Appdev-SIHH@2x.png',
        'Appdev-GM@2x.png',
        'Appdev-Liberte@2x.png',
        'Appdev-Plantwise@2x.png',
        'Graphic-purple.png',
        'Lines-circle.png',
        'Dots.png',

    ];
    
    $lang = get_bloginfo('language');

    if ($lang === 'fr-FR' || $lang === 'de-DE') {
        array_push($skip_img_url_parts, 'No-app-needed-img-1.png');
        array_push($skip_img_url_parts, 'Coffee-machine-img.png');
    }

    foreach ($skip_img_url_parts as $part) {
        if (strpos($url, $part) !== false) {
            return true;
        }
    }


    $skip_current_url_parts = [
        'laptop-ar',
        'keyboard-ar',
        'beer-ar',
        'beer-ar-without-logo',
        'kwc-gastro',
        'idexx-catalyst-one',
        'philips-toothbrush',
        'rebo-smart-bottle',
        'hipp-hopp-zebra',
        'en/rebo',
        'en/delonghi3'
    ];
    $current_url = get_current_url();
    foreach ($skip_current_url_parts as $part) {
        if (strpos($current_url, $part) !== false) {
            return true;
        }
    }

    return $bypass;
}, 10, 2);


// FILTER CONTACT FORM 7 SUBMISSION FOR GOOGLE AND YANDEX
add_action("wpcf7_before_send_mail", function ($cf7) {
    $wpcf = WPCF7_ContactForm::get_current();

    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
    }

    if (!array_key_exists('your-company', $posted_data) || !array_key_exists('your-email', $posted_data)) {
        return $wpcf;
    }

    $company = trim(strtolower($posted_data['your-company']));
    $email = trim(strtolower($posted_data['your-email']));

    if ($company === "google" || 
        strpos($email, 'yandex.ru') !== false)
    {
        add_filter('wpcf7_skip_mail', function () { return true; });
    }



    return $wpcf;
});



// disable automatic redirects in Yoast SEO (to prevent some disasterous event)
add_filter('wpseo_premium_post_redirect_slug_change', '__return_true' );
add_filter('wpseo_premium_term_redirect_slug_change', '__return_true' );

// dont store ip addresses in contact form 7
add_filter( 'wpcf7_remote_ip_addr', 'wpcf7_anonymize_ip_addr' );


// removing rest api header link due to a w3c error (https://wordpress.stackexchange.com/questions/211817/how-to-remove-rest-api-link-in-http-headers)
remove_action( 'template_redirect', 'rest_output_link_header', 11);



// references order by reference_order on archive
add_action( 'pre_get_posts', function ( $query ) {
	if ( is_admin() || ! $query->is_main_query() || !isset($query->query['post_type']) || $query->query['post_type'] !== 'reference' || !is_post_type_archive('reference')) {
		return;
	}

  // ORDERING
  $query->set('orderby', 'meta_value_num');
  $query->set('meta_key', 'no_category_order');
  $query->set('order', 'ASC');

  // ONLY REFERENCES WHERE MASONRY IMAGE IS FILLED
  $query->set('meta_query', array(
    'key' => 'masonry_thumbnail_image',
    'compare' => 'NOT IN',
    'value' => ''
  ));
}, 1 );

// referemces order by reference order in rest queries
add_filter( 'rest_reference_query', function ($query_vars) {
  // ORDERING
  $query_vars["orderby"] = "meta_value_num";
  $query_vars["order"] = "ASC";
  $query_vars["meta_key"] = "no_category_order";

  if ($_GET['reference_category'] === '12' || $_GET['reference_category'] === '75' || $_GET['reference_category'] === '76') {
    $query_vars["meta_key"] = "app_category_order";
  }

  if ($_GET['reference_category'] === '14' || $_GET['reference_category'] === '79' || $_GET['reference_category'] === '80') {
    $query_vars["meta_key"] = "arvr_category_order";
  }

  if ($_GET['reference_category'] === '13' || $_GET['reference_category'] === '77' || $_GET['reference_category'] === '78') {
    $query_vars["meta_key"] = "web_category_order";
  }

  // ONLY REFERENCES WHERE MASONRY IMAGE IS FILLED
  $query_vars['meta_query'] = array(
    'key' => 'masonry_thumbnail_image',
    'compare' => 'NOT IN',
    'value' => ''
  );

  return $query_vars;
}, 10, 2);


// adding custom redirects
function swisstomato_template_redirect()
{
    $current_url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    #region [colored] Web page 301 redirect (2021.07)

    $web_pages_redirections = array();
    
    // Web EN
    array_push($web_pages_redirections, array('https://swisstomato.ch/en/website-development-geneva-switzerland','https://swisstomato.ch/en/webdesign-geneva-zurich'));

    // Web FR
    array_push($web_pages_redirections, array('https://swisstomato.ch/creation-site-internet-suisse-lausanne-geneve','https://swisstomato.ch/agence-web-geneve'));

    // Web DE
    array_push($web_pages_redirections, array('https://swisstomato.ch/de/webdesign-genf-schweiz','https://swisstomato.ch/de/webdesign-zurich'));

    foreach ($web_pages_redirections as $redirection) {
        if ($redirection[0] === rtrim($current_url, '/')) {
            wp_redirect($redirection[1], 301);
            exit;
        }
    }

    #endregion

    #region [colored] App page 301 redirect (2021.07)

    $app_pages_redirections = array();
    
    // App EN
    array_push($app_pages_redirections, array('https://swisstomato.ch/en/app-development-geneva-switzerland', 'https://swisstomato.ch/en/app-development-geneva-zurich/'));

    // App FR
    array_push($app_pages_redirections, array('https://swisstomato.ch/developpement-application-geneve-suisse', 'https://swisstomato.ch/creer-une-application/'));

    // App DE
    array_push($app_pages_redirections, array('https://swisstomato.ch/de/app-development-zurich-switzerland', 'https://swisstomato.ch/de/app-entwickeln-zurich/'));

    foreach ($app_pages_redirections as $redirection) {
        if ($redirection[0] === rtrim($current_url, '/')) {
            wp_redirect($redirection[1], 301);
            exit;
        }
    }

    #endregion

    if (strpos($current_url, 'references-reorder') !== false && !is_user_logged_in()) {
      wp_redirect(get_home_url());
      exit;
    }

    if (strpos($current_url, 'portfolio-category/agence-web-geneve') !== false) {
        wp_redirect('https://swisstomato.ch');
        exit;
    }

    if (strpos($current_url, 'en/our-portfolio') !== false) {
        wp_redirect('https://swisstomato.ch/en/references');
        exit;
    }

    if (strpos($current_url, '/portfolio-fr') !== false) {
        wp_redirect('https://swisstomato.ch/references/');
        exit;
    }

    if (strpos($current_url, '/portfolio-de') !== false) {
        wp_redirect('https://swisstomato.ch/de/referenzen/');
        exit;
    }

    if (strpos($current_url, 'fr/developpement-dapplications-mobiles/utiliser-google-adwords-quand-on-est-une-pme-en-suisse/') !== false) {
        wp_redirect('https://swisstomato.ch/developpement-application-mobiles-suisse-lausanne-geneve/google-adwords-search-marketing/');
        exit;
    }

    if (strpos($current_url, 'de/app-entwicklung/swisstomato-chdeapp-entwicklungapp-entwicklung-augmented-reality-und-virtual-reality/') !== false) {
        wp_redirect('https://swisstomato.ch/de/app-entwicklung/app-entwicklung-augmented-reality-und-virtual-reality/');
        exit;
    }

    if (strpos($current_url, 'de/app-entwicklung/swisstomato-chdeapp-entwicklungapp-entwicklung-augmented-reality-und-virtual-reality/') !== false) {
        wp_redirect('https://swisstomato.ch/de/app-entwicklung/app-entwicklung-augmented-reality-und-virtual-reality/');
        exit;
    }
    
    if (strpos($current_url, 'de/mobile-app-entwicklung/google-adwords-anzeigen-fuer-kleine-und-mittelstaendische-unternehmen-in-der-schweiz-nutzen/') !== false) {
        wp_redirect('https://swisstomato.ch/de/app-entwicklung/google-adwords-suchmaschinenmarketing/');
        exit;
    }

    if (strpos($current_url, 'de/mobile-app-entwicklung/suchmaschinenoptimierung-fuer-kleine-und-mittlere-unternehmen/') !== false) {
        wp_redirect('https://swisstomato.ch/de/app-entwicklung/suchmaschinenoptimierung-fur-kleine-und-mittlere-unternehmen/');
        exit;
    }

    if (strpos($current_url, 'uploads/2017/02/blog_bestswiss') !== false) {
        wp_redirect('https://swisstomato.ch/wp-content/uploads/2018/12/Swiss-tomato-logo.png');
        exit;
    }

    if (strpos($current_url, 'magic-cube-ar-app-android') !== false) {
        wp_redirect('https://play.google.com/store/apps/details?id=com.swisstomato.magiccubear');
        exit;
    }

    if (strpos($current_url, 'magic-cube-ar-app-ios') !== false) {
        wp_redirect('https://apps.apple.com/us/app/ar-magic-cube/id1469397965?l=hu&ls=1');
        exit;
    }

    if (strpos($current_url, 'developpement-application-mobiles-suisse-lausanne-geneve') !== false) {
        // original url: https://swisstomato.ch/developpement-application-mobiles-suisse-lausanne-geneve/
        $current_url = str_replace('developpement-application-mobiles-suisse-lausanne-geneve', 'developpement-application-geneve-suisse', $current_url);
        wp_redirect($current_url);
        exit;
    }

    // if (strpos($current_url, 'de/app-entwicklung') !== false) {
    //     // original url: https://swisstomato.ch/de/app-entwicklung/
    //     $current_url = str_replace('de/app-entwicklung', 'de/app-development-zurich-switzerland', $current_url);
    //     wp_redirect($current_url);
    //     exit;
    // }

    if (is_category() || is_tag() || is_date() || is_tax()) {
        wp_redirect(home_url());
        exit();
    }
}
add_action( 'template_redirect', 'swisstomato_template_redirect' );

// unlimited references
add_action( 'pre_get_posts', function ( $query ) {
    if (is_admin() || $query->is_singular() || !$query->is_main_query()) {
        return;
    }

    if (is_post_type_archive( 'reference' )) {
        $query->set( 'posts_per_page', '-1' );
        return;
    }
});


// Signs to mobile menu
function swisst2_plus_signs_to_menu_items($items, $args) {
    if ($args->theme_location == 'primary-menu') {
        $items = str_replace('</a', '<i class="fas fa-plus"></i><i class="fas fa-minus"></i></a', $items);
    }
    return $items;
}
add_filter('wp_nav_menu_items', 'swisst2_plus_signs_to_menu_items', 10, 2);



// add fields to rest api result
function swisst2_restapi_add_fields_to_reference( $data, $post, $context ) {
    // // ADD featured image url to restapi
    // $featured_image_id = $data->data['featured_media']; // get featured image id
    // $featured_image_url = wp_get_attachment_image_src( $featured_image_id, 'original' ); // get url of the original size

    // if( $featured_image_url ) {
    //     $data->data['featured_image_url'] = $featured_image_url[0];
    // } else {
    //     $data->data['featured_image_url'] = get_stylesheet_directory_uri() . '/imgs/swisstomato.png';
    // }


    // ADD category name (filter_text)
    $category_ids = $data->data['reference_category'];
    if (is_array($category_ids) && sizeof($category_ids) > 0) {
      $data->data['category_name'] = get_field('filter_text', "reference_category_" .$category_ids[0]);
    }

    
    // // ADD date string to restapi
    // $data->data['date_string'] = get_the_date("F d, o", $data->data['id']);

    return $data;
}
add_filter( 'rest_prepare_reference', 'swisst2_restapi_add_fields_to_reference', 10, 3 );


// add fields to references rest api result
function swisst2_restapi_add_fields( $data, $reference, $context ) {
    $data->data['thumbnail_image_url'] = get_field('thumbnail_image', $reference->ID);

    return $data;
}
add_filter( 'rest_prepare_reference', 'swisst2_restapi_add_fields', 10, 3 );




if ( ! function_exists( 'swisst2_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function swisst2_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Swisstomato2, use a find and replace
		 * to change 'swisst2' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'swisst2', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary-menu' => esc_html__( 'Primary', 'swisst2' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'swisst2_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'swisst2_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function swisst2_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'swisst2_content_width', 640 );
}
add_action( 'after_setup_theme', 'swisst2_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function swisst2_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'swisst2' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'swisst2' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'swisst2_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function swisst2_scripts() {

	// Is admin
	$wp_admin = is_admin();

    // jQuery
    if ( $wp_admin ) {
		return;
    } else {

		wp_enqueue_style( 'swisst2-style', get_stylesheet_directory_uri() . '/style.min.css');

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Deregister basic jQuery
		wp_deregister_script( 'jquery' );
        wp_deregister_script( 'jquery-core' );
		wp_deregister_script( 'jquery-migrate' );
		
		// Register new jQuery
        wp_register_script( 'jquery-core', get_template_directory_uri() . '/js/jquery.min.js', [], null, false );
        wp_register_script( 'jquery', false, ['jquery-core'], null, false );
		wp_enqueue_script( 'jquery' );
		
		// Deregister Extra CSS
		wp_deregister_style( 'wpml-menu-item-0' );
		// wp_deregister_style( 'wp-block-library' );
		// wp_deregister_style( 'wc-block-style' );
		wp_deregister_style( 'moove_gdpr_lity' );
		wp_deregister_style( 'moove_gdpr_frontend' );

		// Remove scripts
		wp_deregister_script( 'wp-embed' );

    // GSAP and scroll magic
    swisst2_register_script_with_filetime('swisstomato2-tweenmax-script', '/js/GSAP/TweenMax.min.js', ['jquery']);
    swisst2_register_script_with_filetime('swisstomato2-scrollmagic-script', '/js/ScrollMagic/ScrollMagic.min.js', ['jquery']);
    swisst2_register_script_with_filetime('swisstomato2-scrollmagic-gsap-script', '/js/ScrollMagic/animation.gsap.min.js', ['jquery']);

    // own scroll triggerer
    swisst2_enqueue_script_with_filetime('swisstomato2-scroll-triggerer', '/js/animation/scroll-trigger.js', array('jquery'));
		
		// lodash
		swisst2_enqueue_script_with_filetime('swisstomato2-lodash-script', '/js/lodash.min.js', ['jquery']);

    // parollel parallax library https://tgomilar.github.io/paroller.js/#usage
    swisst2_register_script_with_filetime('swisstomato2-paroller-script', '/js/parollel/jquery.paroller.min.js', array('jquery'));

    // utils
    swisst2_enqueue_script_with_filetime('st-scroll-to-script', '/js/utils/scroll-to/scroll-to.js', array('jquery'));


    // sections
    swisst2_register_script_with_filetime('swisstomato2-reference-masonry-list', '/js/sections/reference-list-masonry-section.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-services-section', '/js/sections/services-section.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-awards-2022feb-section', '/js/sections/awards-2022feb.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-clients-section', '/js/sections/clients.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-icon-text-list-section', '/js/sections/icon-text.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-big-red-text-section', '/js/sections/big-red-text.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-our-story-section', '/js/sections/our-story.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-why-us-section', '/js/sections/our-story.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-field-of-experience-section', '/js/sections/field-of-experience.js', array('jquery'));
    swisst2_register_script_with_filetime('swisstomato2-process-timing-section', '/js/sections/process-timing.js', array('jquery'));


		if ( strpos(get_page_template(), 'page-home') !== false && !wp_is_mobile() ) {
			// GSAP (animation)
			wp_enqueue_script('swisstomato2-tweenmax-script');
			wp_enqueue_script('swisstomato2-scrollmagic-script');
			wp_enqueue_script('swisstomato2-scrollmagic-gsap-script');
		}

		//$gsap_scripts = strpos(get_page_template(), 'page-home') !== false ? "" : '';

		// basic swisstomato2 style and script
		swisst2_enqueue_style_with_filetime('swisstomato2-style', '/css/swisstomato2.css', NULL);
		swisst2_enqueue_script_with_filetime('swisstomato2-script', '/js/swisstomato2.js', ['jquery']);
		wp_localize_script("swisstomato2-script", 'swisstInfos', [
			'baseUrl' 			 => get_home_url(),
			'privacyPolicyUrl'   => get_permalink(3),
			'language' 			 => get_locale(),
			'no_references_text' => __('There are no exact matches for your search criteria but check out these project.', 'swisst2'),
      'minWidthForAnimations' => 768
		]);


		// Lightbox
		if ( is_singular(['reference']) ) {
			swisst2_register_style_with_filetime('swisstomato2-lightbox-styles', '/js/Lightbox/css/lightbox.min.css', NULL);
			swisst2_register_script_with_filetime('swisstomato2-lightbox-script', '/js/Lightbox/js/lightbox.min.js', ['jquery']);
		}
		
		/* PAGES - css files for specific pages */
		if (strpos(get_page_template(), 'page-home') !== false) {
			swisst2_enqueue_script_with_filetime('bxslider-script', '/js/bxslider.min.js', NULL);
			swisst2_enqueue_script_with_filetime('home', '/js/pages/home.js', ['jquery']);
			swisst2_enqueue_style_with_filetime('home', '/css/pages/home.css', NULL);
		}

		if (strpos(get_page_template(), 'page-app-dev') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
      wp_enqueue_script('swisstomato2-services-section');
      wp_enqueue_script('swisstomato2-awards-2022feb-section');
      wp_enqueue_script('swisstomato2-clients-section');
      wp_enqueue_script('swisstomato2-big-red-text-section');
      wp_enqueue_script('swisstomato2-our-story-section');
      // wp_enqueue_script('swisstomato2-why-us-section');
      wp_enqueue_script('swisstomato2-field-of-experience-section');
      wp_enqueue_script('swisstomato2-process-timing-section');

      swisst2_enqueue_script_with_filetime('app-dev', '/js/pages/app-dev.js', array('jquery', 'swisstomato2-paroller-script'));
			swisst2_enqueue_style_with_filetime('app-dev', '/css/pages/app-dev.css', NULL);
		}

		if (strpos(get_page_template(), 'page-ios-app-dev') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			swisst2_enqueue_style_with_filetime('ios-app-dev', '/css/pages/ios-app-dev.css', NULL);
		}

		if (strpos(get_page_template(), 'page-androind-app-dev') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			swisst2_enqueue_style_with_filetime('android-app-dev', '/css/pages/android-app-dev.css', NULL);
		}

		if (strpos(get_page_template(), 'page-unity') !== false) {
			swisst2_enqueue_style_with_filetime('unity', '/css/pages/unity.css', NULL);
		}

		if (strpos(get_page_template(), 'page-webar') !== false) {
			swisst2_enqueue_style_with_filetime('webar', '/css/pages/webar.css', NULL);
			wp_enqueue_script( 'swisstomato2-gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.1/gsap.min.js', array('jquery'), '3.4.1', true);
			wp_enqueue_script( 'swisstomato2-scroll-trigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.1/ScrollTrigger.min.js', array('jquery', 'swisstomato2-gsap'), '3.4.1', true);
			wp_enqueue_script( 'swisstomato2-webar', get_template_directory_uri() . '/js/pages/webar.js', array('jquery','swisstomato2-scrollmagic-script', 'swisstomato2-scrollmagic-gsap-script'), '1.0.0', true);
		}

		if (strpos(get_page_template(), 'page-smartwatch') !== false) {
			swisst2_enqueue_style_with_filetime('smartwatch', '/css/pages/smartwatch.css', NULL);
		}

		if (strpos(get_page_template(), 'page-web-dev') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
      wp_enqueue_script('swisstomato2-services-section');
      wp_enqueue_script('swisstomato2-awards-2022feb-section');
      wp_enqueue_script('swisstomato2-clients-section');
      wp_enqueue_script('swisstomato2-icon-text-list-section');
      wp_enqueue_script('swisstomato2-big-red-text-section');
      wp_enqueue_script('swisstomato2-our-story-section');
      wp_enqueue_script('swisstomato2-why-us-section');
      wp_enqueue_script('swisstomato2-field-of-experience-section');
      wp_enqueue_script('swisstomato2-process-timing-section');
      swisst2_enqueue_script_with_filetime('web-dev', '/js/pages/web-dev.js', array('jquery', 'swisstomato2-scroll-triggerer'));
			swisst2_enqueue_style_with_filetime('web-dev', '/css/pages/web-dev.css', NULL);
		}

		if (strpos(get_page_template(), 'page-webshop-small-midsized') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			swisst2_enqueue_style_with_filetime('webshop-small-midsized', '/css/pages/web-shop-small-mids.css', NULL);
		}

		if (strpos(get_page_template(), 'page-web-dev-small-companies') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			swisst2_enqueue_style_with_filetime('web-dev-small-companies', '/css/pages/web-dev-smallc.css', NULL);
		}

		if (strpos(get_page_template(), 'page-drupal') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			swisst2_enqueue_style_with_filetime('drupal', '/css/pages/drupal.css', NULL);
		}

		if (strpos(get_page_template(), 'page-wordpress') !== false) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			swisst2_enqueue_style_with_filetime('wordpress', '/css/pages/wordpress.css', NULL);
		}

		if (is_404()) {
			swisst2_enqueue_style_with_filetime('404', '/css/pages/404.css', NULL);
		}

		if (strpos(get_page_template(), 'page-arvr') !== false) {
			swisst2_enqueue_style_with_filetime('arvr', '/css/pages/arvr.css', NULL);
		}

		if (strpos(get_page_template(), 'page-google-adwords') !== false) {
			swisst2_enqueue_style_with_filetime('google-adwords', '/css/pages/google-adwords.css', NULL);
		}

		if (strpos(get_page_template(), 'page-swiss-app') !== false) {
			swisst2_enqueue_style_with_filetime('swiss-app', '/css/pages/swiss-app.css', NULL);
		}

		if (strpos(get_page_template(), 'page-social-media') !== false) {
			swisst2_enqueue_style_with_filetime('social-media', '/css/pages/social-media.css', NULL);
		}

		if (strpos(get_page_template(), 'page-seo-email-marketing') !== false) {
			swisst2_enqueue_style_with_filetime('seo-email-marketing', '/css/pages/seo-email-marketing.css', NULL);
		}

		if (strpos(get_page_template(), 'page-whitepaper') !== false) {
			swisst2_enqueue_style_with_filetime('whitepaper', '/css/pages/whitepaper.css', NULL);
		}

		if (strpos(get_page_template(), 'page-job') !== false) {
			swisst2_enqueue_style_with_filetime('job', '/css/pages/job.css', NULL);
		}

		if (strpos(get_page_template(), 'page-magic-cube') !== false) {
			swisst2_enqueue_style_with_filetime('magic-cube', '/css/pages/magic-cube.css', NULL);
		}

		if (strpos(get_page_template(), 'page-search-engine') !== false) {
			swisst2_enqueue_style_with_filetime('search-engine', '/css/pages/search-engine.css', NULL);
		}

		if (strpos(get_page_template(), 'page-whitepaper-20200403') !== false) {
			swisst2_enqueue_script_with_filetime('swisst2-whitepaper-20200403', '/js/pages/whitepaper-20200403.js', NULL);
		} else if (strpos(get_page_template(), 'page-whitepaper-20200708') !== false) {
			swisst2_enqueue_script_with_filetime('swisst2-whitepaper-20200708', '/js/pages/whitepaper-20200708.js', NULL);
		} else if (strpos(get_page_template(), 'page-whitepaper') !== false) {
			swisst2_enqueue_script_with_filetime('swisst2-whitepaper', '/js/pages/whitepaper.js', NULL);
		}
		
		if (strpos(get_page_template(), 'page-whitepaper_copy') !== false) {
			swisst2_enqueue_script_with_filetime('swisst2-whitepaper_copy', '/js/pages/whitepaper_copy.js', NULL);
		}

		if (strpos(get_page_template(), 'page-digital-marketing-strategy') !== false) {
			swisst2_enqueue_style_with_filetime('digital-marketing-strategies', '/css/pages/digital-marketing-strategies.css', NULL);
		} else if (strpos(get_page_template(), 'page-digital-marketing') !== false) {
			swisst2_enqueue_style_with_filetime('digital-marketing', '/css/pages/digital-marketing.css', NULL);
			swisst2_enqueue_script_with_filetime('swisst2-digital-marketing', '/js/pages/digital-marketing.js', array('swisstomato2-tweenmax-script', 'swisstomato2-scrollmagic-script', 'swisstomato2-scrollmagic-gsap-script'));
		}

        if (strpos(get_page_template(), 'page-frontend-technics') !== false) {
			wp_enqueue_style('jquery-ui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', NULL);
            swisst2_enqueue_style_with_filetime('swisst2-frontend-technics', '/css/pages/frontend-technics.css', NULL);
            swisst2_enqueue_script_with_filetime('swisst2-frontend-technics', '/js/pages/frontend-technics.js', array('jquery', 'jquery-ui-tabs'));
		}

		if (is_home()) {
			swisst2_enqueue_script_with_filetime('swisst2-posts-page', '/js/pages/posts.js', array('jquery'));
			swisst2_enqueue_style_with_filetime('posts', '/css/pages/posts.css', NULL);
		}

		if (is_post_type_archive('reference')) {
      wp_enqueue_script('swisstomato2-reference-masonry-list');
			// swisst2_enqueue_script_with_filetime('swisst2-references-page', '/js/pages/references.js', array('jquery', 'swisstomato2-lodash-script'));
			swisst2_enqueue_style_with_filetime('references', '/css/pages/references.css', NULL);
		}

		if (is_singular('reference')) {
			wp_enqueue_style('swisstomato2-lightbox-styles');
			swisst2_enqueue_script_with_filetime('swisst2-reference-page', '/js/pages/reference.js', array('jquery', 'swisstomato2-lodash-script', 'swisstomato2-lightbox-script'));
			swisst2_enqueue_style_with_filetime('reference', '/css/pages/reference.css', NULL);
		}

		if (is_singular('post')) {
			swisst2_enqueue_style_with_filetime('post', '/css/pages/post.css', NULL);

      if (strpos(get_page_template(), 'custom-single-app-cost-post') !== false) {
        swisst2_enqueue_style_with_filetime('app-cost-custom-post', '/css/posts/app-cost.css', NULL);
      }

      if (strpos(get_page_template(), 'custom-single-web-cost-post') !== false) {
        swisst2_enqueue_style_with_filetime('web-cost-custom-post', '/css/posts/web-cost.css', NULL);
      }
		}

    if (strpos(get_page_template(), 'page-references-reorder') !== false) {
      wp_enqueue_style('jquery-ui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', NULL);
			swisst2_enqueue_style_with_filetime('swisst2-references-reorder', '/css/pages/references-reorder.css', NULL);
      swisst2_enqueue_script_with_filetime('swisst2-references-reorder', '/js/pages/references-reorder.js', array('jquery', 'jquery-ui-sortable'));
      wp_localize_script('swisst2-references-reorder', 'wpApiSettings', array(
        'nonce' => wp_create_nonce('wp_rest')
    ) );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'swisst2_scripts', 9999 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



// Change Error Print
add_filter('login_errors', 'wrong_login');

function wrong_login() {
	return 'Wrong data!';
}


// Disable Srcset
add_filter( 'wp_calculate_image_srcset', '__return_false' );


// Modify Edit Post Link Structure
add_filter( 'edit_post_link', 'edit_link_blank', 10, 3);

function edit_link_blank( $link, $post_id, $text ) {
	
	if( false === strpos( $link, 'target=' ) )
		$link = str_replace( '<a ', '<a target="_blank" ', $link );

	return $link;
}


// Modify Edit Term Link Structure
add_filter( 'edit_term_link', 'edit_term_link_blank', 10, 2);

function edit_term_link_blank( $link, $term_id ) {
	
	if( false === strpos( $link, 'target=' ) )
		$link = str_replace( '<a ', '<a target="_blank" class="post-edit-link" ', $link );

	return $link;
}


// Hide CSS / JS Version Number
// add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
// add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

// function remove_cssjs_ver( $src ) {
// 	if( strpos( $src, '?ver=' ) )
// 		$src = remove_query_arg( 'ver', $src );
// 	return $src;
// }


// Disable self pingbacks
add_action( 'pre_ping', 'disable_self_pingbacks' );

function disable_self_pingbacks( &$links ) {
	foreach ( $links as $l => $link )
	if ( 0 === strpos( $link, get_option( 'home' ) ) )
	unset($links[$l]);
}


// Remove Xmlrpc, Pingback support
add_filter( 'xmlrpc_methods', 'remove_xmlrpc_pingback_ping' );

function remove_xmlrpc_pingback_ping( $methods ) {
	unset( $methods['pingback.ping'] );
	return $methods;
}


// Remove Head Actions
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'noindex' );
remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wp_oembed_add_host_js');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );


// Remove WP Styles
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

global $sitepress;
remove_action( 'wp_head', array( $sitepress, 'meta_generator_tag' ) );