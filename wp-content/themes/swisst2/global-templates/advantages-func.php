<?php

function advantages_func($advantages, $type="type-1") {
?>
    <div id="advantages-section" class="container p-215 <?=$type?>">
        <div id="advantage-list">
            <?php
                foreach ($advantages as $advantage):
            ?>
                <div class="advantage">
                    <?php if ($type=="type-1"): ?>
                        <i class="fas fa-check"></i>

                        <hr>
                    <?php elseif ($type=="type-2"): ?>
                        <i class="far fa-check-circle"></i>
                    <?php endif; ?>

                    <div class="advantage-text">
                        <?=$advantage['text']?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php
}
