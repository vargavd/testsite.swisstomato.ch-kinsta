<?php function awards_2022feb_func($title, $text, $awards, $bgImage) { ?>

  <div class="awards-2022feb-section">
    <div class="st-image-wrapper">
      <?=get_img($bgImage, $icon_link['title'])?>
      <!-- <img src="<?=get_img_url_webp_if_possible($bgImage)?>" alt="<?=$icon_link['title']?>" loading="lazy"> -->
    </div>
    <div class="container">
      <h2><?=$title?></h2>

      <p><?=$text?></p>

      <div class="awards">
        <?php
          // pr1($awards);
        ?>
        <?php foreach ($awards as $index => $award): ?>
          <div class="award">
            <?=get_img($award['image'], 'award')?>
            <!-- <img src="<?=$award['image']?>" alt="award" /> -->

            <?php if (!empty($award['list'])): ?>
              <ul class="award__list list-no">
                <?php
                  $award_info_list = explode('<br />', $award['list']);
                  foreach ($award_info_list as $award_info):
                ?>
                  <li 
                    style="background-color: <?=$award['list_items_background_color']?>; color: <?=$award['list_items_text_color']?>"
                  >
                    <?=$award_info?>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

            <?php if (!empty($award['text'])): ?>
              <p class="award__text">
                <?=$award['text']?>  
              </p>
            <?php endif; ?>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>

<?php }
