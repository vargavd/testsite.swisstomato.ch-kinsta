<div id="awards-section" class="container-1250 big">
    <div id="awards-section-text">
        <?=get_field('awards_section_text', 'option')?>
    </div>
        <div id="awards" class="top-row">
            <?php foreach (get_field('awards_top_row', 'option') as $award): ?>
                <div class="award">
                    <div class="award-first-image-wrapper">
                        <?php
                            $first_image = $award['first_image'];
                            if (is_numeric($first_image)) {
                                $first_image = wp_get_attachment_url($first_image);
                            }
                        ?>
						<?= custom_image_size($first_image, 'full', 'Award First Image'); ?>
                    </div>
                    <div class="award-second-column">
                        <div class="award-second-image-wrapper">
                            <?php
                                $second_image = $award['second_image'];

                                if (is_numeric($second_image)) {
                                    $second_image = wp_get_attachment_url($second_image);
                                }
                            ?>
							<?= custom_image_size($second_image, 'full', 'Award Second Image'); ?>
                        </div>
                        <h3 class="award-title-1">
                            <?=$award['award_title_1']?>
                        </h3>
                        <div class="award-title-2">
                            <?=$award['award_title_2']?>
                        </div>
                        <p class="award-text">
                            <?=$award['text']?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div id="awards" class="bottom-row">
            <?php foreach (get_field('awards_bottom_row', 'option') as $award): ?>
                <div class="award">
                    <div class="award-first-image-wrapper">
                        <?php
                            $first_image = $award['first_image'];
                            if (is_numeric($first_image)) {
                                $first_image = wp_get_attachment_url($first_image);
                            }
                        ?>
						<?= custom_image_size($first_image, 'full', 'Award First Image'); ?>
                    </div>
                    <h3 class="award-title-1">
                        <?=$award['award_title_1']?>
                    </h3>
                    <div class="award-title-2">
                        <?=$award['award_title_2']?>
                    </div>
                    <div class="award-second-image-wrapper">
                        <?php
                            $second_image = $award['second_image'];

                            if (is_numeric($second_image)) {
                                $second_image = wp_get_attachment_url($second_image);
                            }
                        ?>
                        <?= custom_image_size($second_image, 'full', 'Award Second Image'); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
</div>