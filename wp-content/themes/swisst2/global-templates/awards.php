<div id="awards-section" class="container">
    <h2>
        <?=get_field('award_section_title', 'option')?>
    </h2>
    <div id="awards-section-text">
        <?=get_field('awards_section_text', 'option')?>
    </div>
    <div id="awards">
        <?php foreach (get_field('awards', 'option') as $award): ?>
            <div class="award">
                <div class="award-top-image-wrapper">
					<?= custom_image_size($award['top_image'], 'full', 'Award Top Image', 'award-top-image') ?>
                </div>
                <h3 class="award-title">
                    <?=$award['award_title']?>
                </h3>
                <div class="award-app-title">
                    <?=$award['app_title']?>
                </div>
                <p class="award-text">
                    <?=$award['text']?>
                </p>
                <div class="award-bottom-image-wrapper">
					<?= custom_image_size($award['bottom_image'], 'full', 'Award Bottom Image', 'award-bottom-image') ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>