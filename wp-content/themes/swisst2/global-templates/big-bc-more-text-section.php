<?php function big_bc_more_text_section($values, $more_text_id = '', $more_text_link = false) { ?>
    <div class="swisst2-big-bc-mt-section">
        <?php
            $big_bc = $values['background'];
        ?>
        <?=custom_image_size($values['background'], "full", $big_bc['alt'], "big-bc-img")?>
        <div class="big-bc-content">
            <div class="container">
                <h2><?=$values['title']?></h2>
                <?=$values['text']?>
                <?php if ($more_text_id): ?>
                    <a class="swisst2-read-more" target-id="#<?=$more_text_id?>">
                        <i class="fas fa-angle-down"></i>
                        <?=$values['read_more_link_text']?>
                    </a>
                <?php elseif ($more_text_link): ?>
                    <a class="swisst2-read-more" href="<?=$values['read_more_link']?>">
                        <?=$values['read_more_link_text']?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if ($more_text_id): ?>
        <div class="swisst2-big-bc-mt-wrapper container">
            <div id="<?=$more_text_id?>" class="big-bc-section-more-text swisst2-more-text">
                <?=$values['more_text']?>
            </div>
        </div>
    <?php endif; ?>
<?php } ?>