<?php

function check_list_func($checkList, $type="type-1") {
?>
    <div class="check-list-section container <?=$type?>">
        <div class="check-item-list">
            <?php
                $i = 0;
                foreach ($checkList as $checkItem):
            ?>
                <div class="check-item">
                    <?php if ($type=="type-1"): ?>
                        <i class="fas fa-check"></i>

                        <hr>
                    <?php elseif ($type=="type-2"): ?>
                        <i class="far fa-check-circle"></i>
                    <?php elseif ($type=="type-3"): ?>
                        <span>
                            <?=$checkItem['number']?>
                        </span>
                    <?php endif; ?>

                    <div class="check-item-title">
                        <?=$checkItem['title']?>
                    </div>

                    <?php if (array_key_exists('text', $checkItem) && !empty($checkItem['text'])): ?>
                        <div class="check-item-text">
                            <?=$checkItem['text']?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php
}
