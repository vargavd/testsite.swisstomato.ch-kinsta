<?php
  if (strpos(get_page_template(), 'page-app-dev.php') === false && strpos(get_page_template(), 'page-web-dev.php') === false) {
    $no_animation = true;
  }
?>
<div id="clients-section" class="container">
    <h2><?=get_field('clients_section_title', 'option')?></h2>
    <div id="clients-text">
        <?=get_field('clients_section_text', 'option')?>
    </div>
    <div class="client-list <?=$no_animation ? 'no-scroll' : ''?>">
        <?php foreach (get_field('clients', 'option') as $client): ?>
          <a data-href="<?=$client['reference_link']?>" target="_blank" class="client" title="<?=$client['name_hover']?>">
  				  <?= custom_image_size($client['image'], 'full', $client['name_hover']); ?>
          </a>
        <?php endforeach; ?>
    </div>

    <?php
        $clients_reviews_image = get_field('clients_reviews_image', 'option');
        $clients_reviews_link_text = get_field('clients_reviews_link_text', 'option');
        $clients_reviews_link_url = get_field('clients_reviews_link_url', 'option');
    ?>

    <?php if (!empty($clients_reviews_image) && !empty($clients_reviews_link_text) && !empty($clients_reviews_link_url)): ?>
        <div id="clients-reviews-wrapper">
            <?php if (!empty($clients_reviews_image)): ?>
                <?=custom_image_size($clients_reviews_image['url'], "full", $clients_reviews_image['alt'])?>
            <?php endif; ?>

            <?php if (!empty($clients_reviews_link_text) && !empty($clients_reviews_link_url)): ?>
                <a href="<?=$clients_reviews_link_url?>" target="_blank">
                    <?=$clients_reviews_link_text?>
                </a>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div> <!-- /#clients-section -->
