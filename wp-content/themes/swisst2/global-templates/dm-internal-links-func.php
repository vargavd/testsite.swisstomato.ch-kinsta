<?php

function digital_marketing_internal_links($ID) {
    $boxes = get_field('dm_internal_links_section_boxes', $ID);

    if (!$boxes) {
        return;
    }
?>
    <div id="dm-internal-links-section" class="container">
        <div id="dm-internal-links-text">
            <?=get_field('dm_internal_link_section_text', $ID)?>
        </div>

        <div id="dm-internal-links-boxes">
            <?php foreach ($boxes as $box): ?>
                <a href="<?=$box['link']?>" class="dm-internal-link-box" style="background-image: url(<?=$box['background_image']?>);">
                    <div class="dark-layer">
						<?= custom_image_size($box['logo'], 'full', $box['title'], 'logo') ?>
                        <div class="title">
                            <?=$box['title']?>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
    
    <?php
}
