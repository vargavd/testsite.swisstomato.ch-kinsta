<?php

function field_of_experience_section($ID) {
    $title = get_field('experience_fields_title', $ID);
    $subtitle = get_field('experience_fields_subtitle', $ID);
    $fields = get_field('experience_fields', $ID);

    if (strpos(get_page_template(), 'page-app-dev.php') === false && strpos(get_page_template(), 'page-web-dev.php') === false) {
      $no_animation = true;
    }
?>
    <div id="fields-of-exp-section" class="container">
        <?php if (!empty($title)): ?>
            <h2><?=$title?></h2>
        <?php endif; ?>

        <?php if (!empty($subtitle)): ?>
            <p class="subtitle">
                <?=$subtitle?>
            </p>
        <?php endif; ?>
        
        <div class="field-of-exp-list <?=$no_animation ? 'no-scroll' : ''?>">
            <?php foreach ($fields as $index => $field): ?>
                <div class="field">
                    <div class="img-wrapper">
						          <?= custom_image_size($field['image'], 'full', $field['title']); ?>
                    </div>
                    <p>
                        <?=$field['title']?>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php
}

