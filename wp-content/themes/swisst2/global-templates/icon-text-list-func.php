<?php

function icon_text_list_func() {
    $ID = get_the_ID();

    $iconTextList = get_field('icon_text_list_items', $ID);

    if (!is_array($iconTextList) || empty($iconTextList)) {
        return;
    }
?>
    <div class="icon-text-list-section">
        <div class="container">
            <div class="icon-text-list">
                <?php
                    $i = 0;
                    foreach ($iconTextList as $iconTextItem):
                        $icon = $iconTextItem['icon'];
                        $title = $iconTextItem['title'];
                ?>
                    <div class="icon-text-item">
                        <div class="icon-text-item-icon">
                            <?=custom_image_size($icon['url'], 'full', $title);?>
                        </div>    

                        <p class="icon-text-item-title">
                            <?=$title?>
                        </p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php
}
