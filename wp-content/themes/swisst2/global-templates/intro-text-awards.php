<?php

function intro_text_awards_func($title, $text, $img_url, $img_alt, $class = '') {
?>
    <div id="intro-text-awards" class="container center-1117 <?=$class?>">
        <div id="intro-title-and-text">
            <h1><?=$title?></h1>
            <div id="intro-text"><?=$text?></div>
        </div>
        <?= custom_image_size($img_url, 'full', $img_alt, '', 'none' ) ?>
    </div>
<?php
}


function intro_text_awards_func_2022($title, $text, $img_url, $img_alt) {
  ?>
      <div id="intro-text-awards-2022" class="container center-1117">
          <div id="intro-title-and-text">
              <h1><?=$title?></h1>
              <div id="intro-text"><?=$text?></div>
          </div>
          <?= custom_image_size($img_url, 'full', $img_alt, '', 'none' ) ?>
      </div>
  <?php
  }

