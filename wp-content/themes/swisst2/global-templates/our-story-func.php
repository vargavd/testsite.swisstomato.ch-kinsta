<?php function our_story_func($title, $text, $img, $founder_1_title, $founder_1_text, $founder_2_title, $founder_2_text) { 
?>

  <div class="our-story-section">
    <div class="big-red-strip-wrapper">
      <div class="big-red-strip"></div>
    </div>

    <div class="container">
      <div class="our-story-content">
        <h2><?=$title?></h2>

        <?=$text?>
      </div>

      <div class="our-story-image" data-background-image="<?=get_img_url_webp_if_possible($img)?>">
        <!-- <img src="<?=$img?>" alt="<?=$title?>" /> -->
        <?=get_img($img, $title)?>

        <div class="founder-text-box">
          <header>
            <?=$founder_1_title?>
          </header>
          <p>
            <?=$founder_1_text?>
          </p>
        </div>
        <div class="founder-text-box">
          <header>
            <?=$founder_2_title?>
          </header>
          <p>
            <?=$founder_2_text?>
          </p>
        </div>
      </div>
    </div>
  </div>

<?php }
