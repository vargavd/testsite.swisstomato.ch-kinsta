<?php

function process_and_timing_func($ID) {
  if (strpos(get_page_template(), 'page-app-dev.php') === false && strpos(get_page_template(), 'page-web-dev.php') === false) {
    $no_animation = true;
  }
?>
    <div id="process-timing-section" class="process-timing-section container p-68">
        <h2><?=get_field('process_and_timing_title', $ID)?></h2>
        
        <div id="process-timing-text">
            <?=get_field('process_and_timing_text', $ID)?>
        </div>

         <div id="process-timing-step-list" class="process-timing-steps <?=$no_animation ? 'no-scroll' : ''?>">
            <?php 
                $steps = get_field('process_and_timing_steps', $ID);

                for ($i = 0; $i < sizeof($steps); $i++): 
            ?>
            <div class="process-timing-step">
                <div class="step-red-bc">
                    <div class="step-number"> 
                        <?=$i+1?>.
                    </div>
                    <div class="step-title">
                        <?=$steps[$i]['title']?>
                    </div>
                    <p>
                        <?=$steps[$i]['description']?>
                    </p>

                    <div class="step-timing-on-mobile">
                        <?=$steps[$i]['timing']?>
                    </div>
                </div>
            </div>
            <?php endfor; ?>
         </div>
         <div id="process-timing-step-list-weeks">
            <?php foreach ($steps as $step): ?>
                <div class="process-timing-step-week">
                    <?=$step['timing']?>
                </div>
            <?php endforeach; ?>
         </div>
    </div>
<?php
}