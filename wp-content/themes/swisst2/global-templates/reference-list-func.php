<?php
/**
 * Example for last parameter:
 * $filters = array(
 *  'category_slug' => '',
 *  'tag_slug' => '',
 *  'ids' => array(3, 45, 10)
 * )
 */
function reference_list($title, $button_text, $category_slug = '', $tag_slug = '', $ids = array()) {
  $number_of_refs = sizeof($ids);
?>
  <div class="references-section container">
    <?php if (!empty($title)): ?>
      <h2><?=$title?></h2>
    <?php endif; ?>

    <?php
      $args = array(
        'post_type'     => 'reference',
        'posts_per_page'  => $number_of_refs > 3 ? $number_of_refs : 3,
      );

      if (sizeof($ids) > 0) {
        $args['post__in'] = $ids;
        $args['orderby'] = 'post__in';
      } else {
        if (!empty($category_slug)) {
          $args['tax_query'] = array(
            array (
              'taxonomy' => 'reference_category',
              'field'    => 'slug',
              'terms'    => $category_slug,
            )
          );

          if (!empty($tag_slug)) {
            array_push($args['tax_query'], array(
              'taxonomy' => 'reference_tag',
              'field'    => 'slug',
              'terms'    => $tag_slug,
            ));
          }
        } else if (!empty($tag_slug)) {
          $args['tax_query'] = array(
            array (
              'taxonomy' => 'reference_tag',
              'field'    => 'slug',
              'terms'    => $tag_slug,
            )
          );
        }
      }

      $posts = get_posts($args);

      $lowNumClass = (sizeof($posts) < 3) ? 'low-num-item' : '';
    ?>

    <div class="post-list <?=$lowNumClass?>">
        <?php
          foreach ($posts as $post):
            $tags = wp_get_post_terms($post->ID, 'reference_tag');
            $categories = wp_get_post_terms($post->ID, 'reference_category');
            $post_link = get_permalink($post->ID);
        ?>
          <div class="post">
            <a href="<?=$post_link?>" class="post-image">
              <?= custom_image_size( get_field('thumbnail_image', $post->ID), 'reference-thumbnail', $post->post_title ); ?>
            </a>
            <a href="<?=$post_link?>" class="post-title">
              <?=$post->post_title?>
            </a>
            <div class="post-categories">
              <?php foreach ($categories as $category): ?>
                <a target="_blank" href="<?=get_post_type_archive_link('reference') . '#platform=' . $category->slug?>" data-slug="<?=$category->slug?>">
                  #<?=$category->name?>
                </a>
              <?php endforeach; ?>

              <?php foreach ($tags as $tag): ?>
                <a target="_blank" href="<?=get_post_type_archive_link('reference') . '#area=' . $tag->slug?>">
                  #<?=$tag->name?>
                </a>
              <?php endforeach; ?>
            </div>
          </div>
        <?php endforeach; ?>
    </div> <!-- /.post-list -->

    
    <?php if (!empty($button_text)): ?>
      <div class="text-center">
        <a href="<?=swisst2_get_references_link($category_slug, $tag_slug)?>" class="button-red more-references">
          <i class="fas fa-angle-right"></i>
          <?=$button_text?>
        </a>
      </div>
    <?php endif; ?>

  </div> <!--/.reference-section -->
<?php
}

function reference_list_standard($ID) {
  $category = get_field('references_section_button_category', $ID);
  $tag = get_field('references_section_button_tag', $ID);

  $cat_slug = !empty($category) ? $cat_slug = $category->slug : '';
  $tag_slug = !empty($tag) ? $tag->slug : '';
  
  reference_list(get_field('references_section_title', $ID),
    get_field('references_section_button_title', $ID), 
    $cat_slug,
    $tag_slug,
    get_field('references_section_references', $ID)
  );
}
