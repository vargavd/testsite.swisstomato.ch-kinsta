<?php

function reference_list($category_slug, $title, $button_text, $button_link) {
    $posts = get_posts(array(
        'post_type'     => 'reference',
        'posts_per_page'  => 3,
        'tax_query' => array(
            array(
                'taxonomy' => 'reference_category',
                'field'    => 'slug',
                'terms'    => $$category_slug,
            ),
        ),
    ));

    print '<div class="reference-block">';

    if (!empty($title)) {
        print "<h3>$title</h3>";
    }

    print '<div class="post-list">';

    foreach ($posts as $post):
        $tags = wp_get_post_terms($post->ID, 'reference_tag');
        $post_link = get_permalink($post->ID);
?>
        <div class="post">
            <a href="<?=$post_link?>" class="post-image" style="background-image: url(<?=get_field('thumbnail_image', $post->ID)?>)"></a>
            <a href="<?=$post_link?>" class="post-title">
                <?=$post->post_title?>
            </a>
            <div class="post-categories">
                <?php foreach ($tags as $tag): ?>
                    <a target="_blank" href="<?=get_term_link($tag->term_id)?>">
                        #<?=$tag->name?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>    
<?php

    print '</div>'; // /.post-list

    if (!empty($button_link) && !empty($button_text)): 
?>
        <div class="text-center">
            <a href="<?=$button_link?>" class="button-red more-references">
                <i class="fas fa-angle-right"></i>
                <?=$button_text?>
            </a>
        </div>
<?php
    endif;

    print '</div>'; // /.reference-block
}

