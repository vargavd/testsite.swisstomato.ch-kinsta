<?php

function services_func($services, $additional_class = '') {
    $ID = get_the_ID();

    if (!is_array($services) || empty($services)) {
        return;
    }

    $odd_even_class = ((sizeof($services) % 2) === 0) ? 'even' : 'odd';
?>
    <div id="services-section" class="center-1250 <?=$odd_even_class?> <?=$additional_class?>">
        <div id="services">
            <?php 
                foreach ($services as $service): ?>
                <div class="service">
                    <div class="service-image">
                        <?= custom_image_size($service['background_image'], 'full', $service['title'] ) ?>
                    </div>
                    <div class="service-title">
                        <?=str_replace('Development', '<br />Development', $service['title'])?>
                    </div>
                    <?php if (array_key_exists('description', $service) && $service['description']): ?>
                        <div class="service-cover">
                            <div class="service-text">
                                <?=$service['description']?>
                            </div>
                            <div class="text-center">
                                <a class="button-white" href="<?=$service['button_link']?>">
                                    <i class="fas fa-angle-right"></i>
                                    <div>
                                        <?=$service['button_text']?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php
}


function services_func_2022($services, $additional_class = '') {
?>
  <div id="services-section-2022">
    <?php foreach ($services as $index => $service): ?>
      <div class="service">
        <div class="service__image">
          <?=get_img($service['background_image']['url'], $service['title'], '', is_mobile())?>
          <!-- <img src="<?=$service['background_image']['url']?>" alt="<?=$service['title']?>" /> -->
        </div>
        <div class="service__content">
          <h2 class="service__title">
            <?=str_replace('Development', '<br />Development', $service['title'])?>
          </h2>
          <?php if (!empty($service['text'])): ?>
            <p class="service__text"><?=$service['text']?></p>
          <?php endif; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
<?php
}

