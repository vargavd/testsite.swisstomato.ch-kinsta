<?php

function swisst2_banner_func($text, $button_title, $background, $mobile_background) {
    $background = $background;
    $mobile_background = $mobile_background;
?>
	<div class="swisst-banner container-no-padding">
		<?php if ( !wp_is_mobile() ) : ?>
			<?= custom_image_size($background, 'full', 'Banner Image') ?>
		<?php else : ?>
			<?= custom_image_size($mobile_background, 'full', 'Mobile Banner Image') ?>
		<?php endif; ?>
        <div class="swisst-banner-content">
            <p><?=$text?></p>
            <a href="#contact-us" class="button-red">
                <?=$button_title?>
            </a>
        </div>
    </div>
<?php
}