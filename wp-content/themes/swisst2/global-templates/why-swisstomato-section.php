<?php function why_swisstomato_section($values) { ?>
    <div id="why-section">
        <h2><?=$values['title']?></h2>
        <div id="why-section-pink-bc">
            <div class="container">
                <div class="why-text-column">
                    <p><?=$values['left_text']?></p>

                    <p class="handwritten-text">
                        <?=$values['left_handwritten_text']?>
                    </p>
                </div>
                <div class="why-img-column">
                    <?=custom_image_size($values['right_image'], 'full', $values['right_image_alt'])?>
                </div>
            </div>
        </div>
        <div id="why-section-gray-bc">
            <div class="container">
                <div class="why-img-column">
                    <?=custom_image_size($values['left_image'], 'full', $values['left_image_alt'])?>
                </div>
                <div class="why-text-column">
                    <p><?=$values['right_text']?></p>
                    <p class="handwritten-text">
                        <?=$values['right_handwritten_text']?>
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>