<?php

function why_us_our_story_section_func() {
?>
    <div id="why-us-section">
        <div id="why-us-title-wrapper">
            <?= custom_image_size(get_field('why_us_section_title_background', $ID), 'full', get_field('why_us_section_title', $ID) ) ?>
            <h2 class="first"><?=get_field('why_us_section_title', $ID)?></h2>
        </div>
        <div id="why-us-reasons">
            <?php foreach (get_field('why_us_section_reasons', $ID) as $reason): ?>
                <div class="why-us-reason">
                    <?= custom_image_size($reason['image'], 'full', $reason['title'] ) ?>
                    <h3><?=$reason['title']?></h3>
                    <div class="reason-text">
                        <?=$reason['text']?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div> <!-- /#why-us-section-->

    <!-- <div id="our-story-section">
        <div id="our-story-column">
            <h2><?=get_field('our_story_section_title', $ID)?></h2>                    
            <div id="our-story-text">
                <?=get_field('our_story_section_text', $ID)?>
            </div>
        </div>
        <div id="founders-column">
            <div id="founders-image-wrapper">
                <?= custom_image_size(get_field('our_story_section_image', $ID), 'full', 'Founders' ) ?>
            </div>
            <div id="founders-list">
                <div class="founder">
                    <h3>
                        <?=get_field('our_story_section_founder_1_name', $ID)?>
                    </h3>
                    <div class="founder-title">
                        <?=get_field('our_story_section_founder_1_title', $ID)?>
                    </div>
                    <div class="founder-text">
                        <?=get_field('our_story_section_founder_1_text', $ID)?>
                    </div>
                </div>
                <div class="founder">
                    <h3>
                        <?=get_field('our_story_section_founder_2_name', $ID)?>
                    </h3>
                    <div class="founder-title">
                        <?=get_field('our_story_section_founder_2_title', $ID)?>
                    </div>
                    <div class="founder-text">
                        <?=get_field('our_story_section_founder_2_text', $ID)?>
                    </div>
                </div>
            </div>
        </div>
    </div>  -->
    <!-- /#our-story-section -->  
<?php
}
