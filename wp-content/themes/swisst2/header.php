<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Swisstomato2
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<!-- Fonts Preloading -->
	<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
	<link rel="stylesheet preload prefetch" href="https://fonts.googleapis.com/css?family=Rubik:wght@0,300;0,400;0,500;0,700&display=swap" as="style" type="text/css" crossorigin="anonymous">


	<?php include "inc/preload-in-header.php"; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(get_bloginfo('language')); ?>>
	<div id="page" class="site">
		<?php if (is_singular()) : ?>
			<!-- Load Facebook SDK for JavaScript -->
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s);
					js.id = id;
					js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
		<?php endif; ?>


		<a class="screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'swisst2'); ?></a>

		<header id="masthead" class="site-header">
			<div class="container center-1117">
				<div class="site-branding">
					<?php the_custom_logo(); ?>
				</div><!-- .site-branding -->

				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<span class="bar1"></span>
					<span class="bar2"></span>
					<span class="bar3"></span>
				</button>

				<nav id="site-navigation" class="main-navigation">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'primary-menu',
						'menu_id'        => 'primary-menu',
					));
					?>
				</nav><!-- #site-navigation -->
			</div>
			<div id="desktop-sub-menu-background"></div>
		</header><!-- #masthead -->

		<div id="contact-popup-trigger">
			<i class="far fa-envelope"></i>
      <span><?=get_field('contact_floating_button', 'option')?></span>
		</div>

		<div id="content" class="site-content">