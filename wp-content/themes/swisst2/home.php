<?php

get_header();

?>

<div id="swisst2-blogposts-page" class="container">

        <div id="intro-section" class="container">
            <h1><?=get_the_title( get_option('page_for_posts', true) ) ?></h1>
        </div>

        <?php if ( have_posts() ) : ?>

            <div id="blog-post-list">

                <div class="post post-template">
                    <a href="" class="post-image">
                    </a>
                    <div class="post-content">
                        <div class="post-categories">
                            <a href="">
                                    
                            </a>
                        </div>
                        <h2 class="post-title">
                            <a href="">
                                
                            </a>
                        </h2>
                        <div class="post-date"></div>
                        <div class="post-text"></div>
                    </div>
                </div>

                <?php 
                    $first_post = true;
                    while ( have_posts() ) : the_post(); 
                        get_template_part( 'loop-templates/content', 'blogpost' );
                        $first_post = false;
                    endwhile; 
                ?>

            </div><!-- /#blog-post-list -->

            <div id="loading-more-post-wrapper" class="text-center">
		        <img id="loading-posts-gif" src="<?=get_stylesheet_directory_uri()?>/imgs/spinner-blog-post.gif" alt="Loading Spinner">

                <button id="load-more-posts-button" data-next-page="2" class="button-red">
                    <i class="fa fa-angle-right"></i>
                    <?=__('More', 'swisst2')?>
                </button>
            </div>

        <?php else: echo "There are no posts yet.";  endif; ?>		

</div><!-- /#swisst-blogposts-page -->

<?php get_footer(); ?>
