<?php

/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Swisstomato2
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses swisst2_header_style()
 */
function swisst2_custom_header_setup()
{
	add_theme_support('custom-header', apply_filters('swisst2_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
		'wp-head-callback'       => 'swisst2_header_style',
	)));
}
add_action('after_setup_theme', 'swisst2_custom_header_setup');

if (!function_exists('swisst2_header_style')) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see swisst2_custom_header_setup().
	 */
	function swisst2_header_style()
	{
		$header_text_color = get_header_textcolor();

		/*
		 * If no custom options for text are set, let's bail.
		 * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: add_theme_support( 'custom-header' ).
		 */
		if (get_theme_support('custom-header', 'default-text-color') === $header_text_color) {
			return;
		}

		// If we get this far, we have custom styles. Let's do this.
?>
		<style>
			<?php
			// Has the text been hidden?
			if (!display_header_text()) :
			?>.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px, 1px, 1px, 1px);
			}

			<?php
			// If the user has set a custom color for the text use that.
			else :
			?>.site-title a,
			.site-description {
				color: #<?php echo esc_attr($header_text_color); ?>;
			}

			<?php endif; ?>
		</style>
<?php
	}
endif;


// WPML is active?
if (function_exists('icl_object_id')) {

	// Define WPML Constants - Remove all of them
	define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
	define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
	define('ICL_DONT_LOAD_LANGUAGES_JS', true);


	// Get language native name | Usage: get_language_name($code)
	function get_language_name($code = '')
	{
		global $sitepress;
		$details = $sitepress->get_language_details($code);
		$language_name = $details['native_name'];
		return $language_name;
	}


	// Custom Menu Language Switcher
	add_filter('wp_nav_menu_items', 'custom_nav_menu_items', 10, 2);
	function custom_nav_menu_items($items, $args)
	{

		// uncomment this to find your theme's menu location
		//echo "args:<pre>"; print_r($args); echo "</pre>";

		// get languages
		$languages = apply_filters('wpml_active_languages', NULL, 'skip_missing=0');

		// add $args->theme_location == 'primary-menu' in the conditional if we want to specify the menu location.

		if ($languages && $args->theme_location == 'primary-menu') {

			if (!empty($languages)) {
				$language = '<ul class="sub-menu">';
				foreach ($languages as $l) {
					if ($l['active']) {
						$active_item = '<li class="menu-item wpml-ls-item wpml-ls-current-language wpml-ls-menu-item wpml-ls-first-item menu-item-type-wpml_ls_menu_item menu-item-object-wpml_ls_menu_item menu-item-has-children">
						<a>' . $l['language_code'] . '<i class="fas fa-plus"></i><i class="fas fa-minus"></i></a>';
					}
					if (!$l['active']) {
						// language only with flag
						$language .= '<li class="menu-item"><a href="' . $l['url'] . '">' . $l['language_code'] . '</a></li>';
					}
				}
				$language .= '</ul>';
			}
			$items = $items . $active_item . $language . '</li>';
		}
		return $items;
	}

	// Translating [] of IDs | Usage: lang_object_ids([1,3,6], 'category')
	function lang_object_ids($object_id, $type)
	{
		if (is_array($object_id)) {
			$translated_object_ids = [];
			foreach ($object_id as $id) {
				$translated_object_ids[] = apply_filters('wpml_object_id', $id, $type, true, ICL_LANGUAGE_CODE);
			}
			return $translated_object_ids;
		} else {
			return apply_filters('wpml_object_id', $object_id, $type, true, ICL_LANGUAGE_CODE);
		}
	}
}
