<?php

function swisst2_enqueue_style_with_filetime($name, $filePath, $deps) {
    wp_enqueue_style($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath));
}
function swisst2_register_style_with_filetime($name, $filePath, $deps) {
    wp_register_style($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath));
}
function swisst2_enqueue_script_with_filetime($name, $filePath, $deps, $in_footer = true) {
    wp_enqueue_script($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath), $in_footer);
}
function swisst2_register_script_with_filetime($name, $filePath, $deps, $in_footer = true) {
    wp_register_script($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath), $in_footer);
}

add_action( 'admin_enqueue_scripts', 'swisst2_admin_enqueue_styles_and_scripts' );
function swisst2_admin_enqueue_styles_and_scripts() {
    swisst2_enqueue_style_with_filetime('swisstomato2-admin-styles', '/css/swisstomato2-admin.css', NULL);
}