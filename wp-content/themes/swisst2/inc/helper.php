<?php

// DEBUG
function vd1($var)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	var_dump($var);
	echo "\n</pre>\n";
}
function vd2($var1, $var2)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	var_dump($var1);
	var_dump($var2);
	echo "\n</pre>\n";
}
function vd3($var1, $var2, $var3)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	var_dump($var1);
	var_dump($var2);
	var_dump($var3);
	echo "\n</pre>\n";
}
function pr1($var)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	print_r($var);
	echo "\n</pre>\n";
}
function pr2($var1, $var2)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	print_r($var1);
	echo "\n";
	print_r($var2);
	echo "\n</pre>\n";
}
function pr3($var1, $var2, $var3)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	print_r($var1);
	echo "\n";
	print_r($var2);
	echo "\n";
	print_r($var3);
	echo "\n</pre>\n";
}
function pr4($var1, $var2, $var3, $var4)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	print_r($var1);
	echo "\n";
	print_r($var2);
	echo "\n";
	print_r($var3);
	echo "\n";
	print_r($var4);
	echo "\n</pre>\n";
}

function get_current_url()
{
	return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function swisst2_scrape_image($text)
{
	$pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
	preg_match($pattern, $text, $link);
	$link = $link[1];
	$link = urldecode($link);
	return $link;
}

function swisst2_get_wysiwyg_output($content)
{
	global $wp_embed;

	$content = $wp_embed->autoembed($content);
	$content = $wp_embed->run_shortcode($content);
	$content = wpautop($content);
	$content = do_shortcode($content);

	return $content;
}
function get_image_tag_with_bj_lazy_load($src, $attrs, $srcset = "")
{
	$img_html = "<img src='$src'";

	if (sizeof($attrs) > 0) {
		foreach ($attrs as $key => $value) {
			$img_html .= " $key='$value'";
		}
	}

	$img_html .= ">";
	$img_html = apply_filters('bj_lazy_load_html', $img_html);
	echo $img_html;
}

function custom_image_size_for_acf_img_array($img_array, $class = "")
{
	if (is_array($img_array)) {
		$img_url = $img_array['url'];
	} else  if (is_integer($img_array)) {
		$img_url = wp_get_attachment_image_url($img_array, 'full');
	}

	return custom_image_size($img_url, "full", $img_array['alt'], $class);
}

function get_img_url_webp_if_possible($url)
{
	// WebP for Only non Safari browsers
	$webp = '.webp';
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	if (stripos($user_agent, 'Chrome') !== false) {
	} elseif (stripos($user_agent, 'Safari') !== false) {
		$webp = '';
	}

	return $url . $webp;
}

function get_img($img, $alt, $class = '', $lazy = true) {
  if (is_integer($img)) {
		$img_id = $img;
	} else if (is_array($img)) {
		$img_id = $img['ID'];
	} else {
		$img_id = attachment_url_to_postid($img);
	}

  $img_info = wp_get_attachment_image_src($img_id, 'full');

	$url 	= $img_info[0];
	$width 	= $img_info[1];
	$height 	= $img_info[2];

  
  // if (stripos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== false && strpos($url, '.svg') === false) {
	// 	$url = "$url.webp";
	// }

  $source_url = (stripos($url, '.svg') !== false) ? $url : "$url.webp";

  $lazy = $lazy ? 'loading="lazy"' : '';

  return "<picture>
    <source srcset='$source_url' type=image/webp>
    <img $lazy src='$url' srcset='$srcset' width='$width'  height='$height' alt='$alt' class='$class'>
  </picture>";
}


function custom_image_size($img, $size, $title, $class = '', $lazy = 'lazy')
{
	// reference-thumbnail 350 x 341
	// post-thumbnail 350 x 240
	// post-home-thumbnail 502 x 295

	if (is_integer($img)) {
		$img_id = $img;
	} else if (is_array($img)) {
		$img_id = $img['ID'];
	} else {
		$img_id = attachment_url_to_postid($img);
	}

	$img_url 	= wp_get_attachment_image_src($img_id, $size)[0];

	if (!$img_url) {
		$size 		= 'full';
		$img_url 	= wp_get_attachment_image_src($img_id, $size)[0];
	}
	$img_width 	= wp_get_attachment_image_src($img_id, $size)[1];
	$img_height = wp_get_attachment_image_src($img_id, $size)[2];

	if ($lazy !== 'none') {
		$lazy = 'loading="' . $lazy . '"';
	} else {
		$lazy = '';
	}

	// Srcset support
	$srcset = wp_get_attachment_image_srcset($img_id);

	$picture = '<picture>
		<source srcset="' . $img_url . '.webp" type="image/webp">
		<img ' . $lazy . ' src="' . $img_url . '" srcset="' . $srcset . '" width="' . $img_width . '"  height="' . $img_height . '" alt="' .  $title .  '" class="' . $class . '">
	</picture>';

	return $picture;
}

function swisst2_get_references_link($platform = '', $area = '')
{
	$url = get_post_type_archive_link('reference');

	if (!empty($platform)) {
		$url .= '#platform=' . $platform;

		if (!empty($area)) {
			$url .= '&area=' . $area;
		}
	} else {
		if (!empty($area)) {
			$url .= '#area=' . $area;
		}
	}

	return $url;
}

function swisst2_primary_term_first($terms, $post_id, $taxonomy)
{
	if (sizeof($terms) < 2 || !class_exists('WPSEO_Primary_Term')) {
		return $terms;
	}

	$wpseo_primary_term = new WPSEO_Primary_Term($taxonomy, $post_id);
	$prm_term_id = $wpseo_primary_term->get_primary_term();

	if (!$prm_term_id) {
		return $terms;
	}

	for ($i = 0; $i < sizeof($terms); $i++) {
		if ($terms[$i]->term_id === $prm_term_id) {
			break;
		}
	}

	$prm_term = $terms[$i];
	array_splice($terms, $i, 1);
	array_unshift($terms, $prm_term);

	return $terms;
}

function is_mobile()
{
	return (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
		'|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
		'|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT']);
}
