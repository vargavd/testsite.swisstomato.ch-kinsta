
<?php 
  // WEB DEV - preload awards image for f***ing LCP
  if (strpos(get_page_template(), 'page-web-dev') !== false): 
    $page_id = 'en' !== ICL_LANGUAGE_CODE ? apply_filters('wpml_object_id', 290, 'page', true, ICL_LANGUAGE_CODE) : 290;

    $awards_img = get_field('title_awards_image', $page_id);
    $awards_img_url = (is_array($awards_img) ? $awards_img['url'] : '');

    print "<link rel='preload' as='image' href='$awards_img_url.webp'>";
  endif;
?>


