<?php

/* ADD FIELDS TO POSTS ADMIN LIST */
add_filter('manage_reference_posts_columns', function ($columns) {
  $date_column_header = $columns['date'];
  unset($columns['date']);

  $columns['masonry_title'] = 'Masonry Title';
  $columns['masonry_text'] = 'Masonry Text';
  $columns['masonry_thumbnail'] = 'Masonry Thumbnail';
  $columns['date'] = $date_column_header;

  return $columns;
});


/* ADD FIELD VALUES TO BOTH POST TYPE */
add_action('manage_posts_custom_column', function ($column, $post_id) {
  switch ( $column ) {
    case 'masonry_title':
      echo get_post_meta ( $post_id, 'masonry_reference_title', true );
      break;
    case 'masonry_text':
      echo get_post_meta ( $post_id, 'masonry_reference_text', true );
      break;
    case 'masonry_thumbnail':
      $img = get_field('masonry_thumbnail_image', $post_id);
      if (is_array($img)) {
        print "<img src='" . $img['url'] . "' alt='" . $img['title'] . "' style='max-width: 60px;'/>";
      }
      break;
  }
}, 10, 2 );
