<?php

/* ADD ACF OPTIONS PAGE */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
        'page_title' => 'Swisstomato General Settings'
    ));
}

/* ADDING REFERENCE THUMBNAIL SIZE */
add_action('after_setup_theme', 'swisst2_after_setup_theme' );
function swisst2_after_setup_theme() {
    if (!has_image_size('reference-thumbnail')) {
        add_image_size('reference-thumbnail', 350, 341, true);
    }

    if (!has_image_size('post-thumbnail')) {
        add_image_size('post-thumbnail', 350, 240, true);
    }

    if (!has_image_size('post-home-thumbnail')) {
        add_image_size('post-home-thumbnail', 502, 295, true);
    }
}