var swisstScrollTriggerer = (function () {
    var $window = $(window), sections = [], viewportHeight = $(window).height(), browserScrolled = function () {
        var browserTopScroll = $window.scrollTop(), browserBottom = browserTopScroll + viewportHeight;
        sections.forEach(function (section) {
            if (section.triggered) {
                return;
            }
            if (browserBottom > section.triggerPointTop) {
                section.triggered = true;
                if (typeof section.callbackOrClassName === 'string') {
                    section.$section.addClass(section.callbackOrClassName);
                }
                else {
                    section.callbackOrClassName();
                }
            }
        });
    };
    $window.on('scroll', browserScrolled);
    return function ($section, callbackOrClassName, offset, $target, startOfSection, mobile) {
        var _a;
        if (offset === void 0) { offset = 0; }
        if ($target === void 0) { $target = null; }
        if (startOfSection === void 0) { startOfSection = false; }
        if (mobile === void 0) { mobile = false; }
        var elementHeight = $section.height(), elementTop = $section.position().top, triggerPointTop = elementTop + offset, minWidthForAnim = (_a = swisstInfos.minWidthForAnimations) !== null && _a !== void 0 ? _a : 768, animationId = (new Date()).getTime(), removeThisAnimation = function () {
            sections.splice(sections.indexOf(sections.find(function (s) { return s.id === animationId; })), 1);
        };
        if (!mobile && window.innerWidth < minWidthForAnim) {
            if ($target) {
                $target.addClass('no-scroll');
            }
            else {
                $section.addClass('no-scroll');
            }
            return removeThisAnimation;
        }
        if (!startOfSection) {
            triggerPointTop += 0.66 * elementHeight;
        }
        sections.push({
            id: animationId,
            $section: $target ? $target : $section,
            triggerPointTop: triggerPointTop,
            callbackOrClassName: callbackOrClassName,
            triggered: false
        });
        browserScrolled();
        return removeThisAnimation;
    };
}());
var swisstSplitTextAnimation = function (element, delay) {
    if (delay === void 0) { delay = 0.02; }
    var textInside = element.innerText, words = textInside.split(" "), wordsLength = words.length, newElements = [];
    element.style.opacity = 1;
    element.innerHTML = "";
    var animatedCharCounter = 0;
    for (var i = 0; i < wordsLength; i++) {
        animatedCharCounter++;
        var newElement = document.createElement("span"), wordText = words[i], characters = [];
        var whiteSpace = void 0;
        for (var j = 0; j < words[i].length; j++) {
            animatedCharCounter++;
            characters[j] = document.createElement("span");
            characters[j].innerHTML = words[i][j];
            characters[j].classList.add("split__char");
            characters[j].setAttribute("aria-hidden", "true");
            characters[j].style.animationDelay = delay * animatedCharCounter + "s";
            newElement.appendChild(characters[j]);
        }
        newElement.classList.add("split__word");
        newElement.setAttribute("aria-label", wordText);
        newElements.push(newElement);
        if (i < wordsLength - 1) {
            whiteSpace = document.createElement("span");
            whiteSpace.innerHTML = " ";
            whiteSpace.classList.add("split__whitespace");
            newElements.push(whiteSpace);
        }
    }
    newElements.forEach(function (arrElement) {
        element.appendChild(arrElement);
    });
};
//# sourceMappingURL=scroll-trigger.js.map