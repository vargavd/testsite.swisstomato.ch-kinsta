/// <reference path="../_common.d.ts" />

const swisstScrollTriggerer = (function () {
  let
    // DOM
    $window = $(window),

    // misc
    sections = [],
    viewportHeight = $(window).height(),

    // events
    browserScrolled = () => {
      let 
        browserTopScroll = $window.scrollTop(),
        browserBottom = browserTopScroll + viewportHeight;

      sections.forEach(section => {
        if (section.triggered) {
          return;
        }

        if (browserBottom > section.triggerPointTop) {
          section.triggered = true;

          if (typeof section.callbackOrClassName === 'string') {
            section.$section.addClass(section.callbackOrClassName);
          } else {
            section.callbackOrClassName();
          }
        }
      });
    };

  $window.on('scroll', browserScrolled);

  return function (
    $section: JQuery<HTMLElement>, 
    callbackOrClassName: any, 
    offset: number = 0, 
    $target: JQuery<HTMLElement> | null = null, 
    startOfSection: boolean = false,
    mobile: boolean = false
  ) {
    let
      // misc
      elementHeight = $section.height(),
      elementTop = $section.position().top,
      triggerPointTop = elementTop + offset,
      minWidthForAnim = swisstInfos.minWidthForAnimations ?? 768,
      animationId = (new Date()).getTime(),
      
      removeThisAnimation = () => {
        sections.splice(sections.indexOf(sections.find(s => s.id === animationId)), 1);
      };

    if (!mobile && window.innerWidth < minWidthForAnim) {
      if ($target) {
        $target.addClass('no-scroll');
      } else {
        $section.addClass('no-scroll');
      }
      
      return removeThisAnimation;
    }

    if (!startOfSection) {
      triggerPointTop += 0.66*elementHeight;
    }

    sections.push({
      id: animationId,
      $section: $target ? $target : $section,
      triggerPointTop, 
      callbackOrClassName, 
      triggered: false 
    });

    browserScrolled();

    // return a functions which removes that section from animation
    return removeThisAnimation;
  };
}());


const swisstSplitTextAnimation = (element, delay = 0.02) => {
  const 
    textInside = element.innerText,
    words = textInside.split(" "),
    wordsLength = words.length,
    newElements = [];

  element.style.opacity = 1;

  element.innerHTML = ""; // delete inital text
  let animatedCharCounter = 0;
  for (let i = 0; i < wordsLength; i++) {
    animatedCharCounter++;

    const newElement = document.createElement("span"),
          wordText = words[i],
          characters = [];
    let whiteSpace;

    for (let j = 0; j < words[i].length; j++) {
      animatedCharCounter++;
      characters[j] = document.createElement("span");
      characters[j].innerHTML = words[i][j];
      characters[j].classList.add("split__char");

      // Make it invisible for screen-readers.
      characters[j].setAttribute("aria-hidden", "true");

      // set animation delays
      characters[j].style.animationDelay = delay * animatedCharCounter + "s";

      newElement.appendChild(characters[j]);
    }

    newElement.classList.add("split__word");

    // Word for screenreaders
    newElement.setAttribute("aria-label", wordText);
    newElements.push(newElement);

    if (i < wordsLength - 1) {
      whiteSpace = document.createElement("span");
      whiteSpace.innerHTML = " ";
      whiteSpace.classList.add("split__whitespace");
      newElements.push(whiteSpace);
    }
  }

  newElements.forEach(function (arrElement) {
    element.appendChild(arrElement);
  });
}
