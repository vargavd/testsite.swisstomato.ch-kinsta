'use strict';

jQuery(document).ready(function ($) {
  var
    // DOM
    $window = $(window),
    $body = $('body'),
    $appHoursSection = $('#app-hours-complexity-section'),
    $bgBcSections = $('.swisst2-big-bc-mt-section'),
    $letsWorkSection = $('#lets-work-section'),
    $agencySection = $('.agency-section'),
    $imageTextRow1 = $('#image-text-row-1'),
    $imageTextRow2 = $('#image-text-row-2'),

    // misc
    is_FR = $body.hasClass('fr-FR'),
    is_DE = $body.hasClass('de-DE'),

    // INIT
    init = function () {
      var
        initProcessTimingSteps = function () {
            var
              // DOM
              $steps = $('#process-timing-steps .step'),
              $readMoreLinks = $steps.find('a.step-read-more'),

              // helper funcs
              initStepTextHeights = function (i, elem) {
                  var
                    // DOM
                    $step = $(this),
                    $stepText = $step.find('.step-text'),
                    $stepTitle = $step.find('h3'),

                    // misc
                    defaultHeights, defaultHeight, mobileDefaultHeight,
                    textLineHeight = window.getComputedStyle($step.find('p:first-child')[0]).getPropertyValue('line-height'),
                    titleLineHeight = window.getComputedStyle($stepTitle[0]).getPropertyValue('line-height'),
                    titleHeight = $stepTitle.height();

                  textLineHeight = +textLineHeight.replaceAll('px', '');
                  defaultHeight = textLineHeight*5;
                  
                  if (window.innerWidth > 1060) {
                    titleLineHeight = +titleLineHeight.replaceAll('px', '');

                    defaultHeight = defaultHeight - (titleHeight - titleLineHeight);
                  }

                  $stepText.attr('data-default-height', defaultHeight);

                  $stepText.height(defaultHeight);
              },
              clickOnReadMore = function () {
                  var
                    // DOM
                    $readMoreLink = $(this),
                    $step = $readMoreLink.closest('.step'),
                    $stepText = $step.find('.step-text'),
                    $stepParagraphs = $stepText.find('p'),

                    // misc
                    fullHeight = 0,
                    defaultHeight = $stepText.attr('data-default-height'),
                    paragraphTopMarginText = window.getComputedStyle($step.find('p:last-child')[0]).getPropertyValue('margin-top'),
                    paragraphTopMargin = +paragraphTopMarginText.replaceAll('px', '');

                  // calculate full height
                  $stepParagraphs.each(function (index) {
                    let $stepParagraph = $(this);

                    fullHeight += $stepParagraph.height();

                    if (index != 0) {
                      fullHeight += +paragraphTopMargin;
                    }
                  });

                  if ($readMoreLink.hasClass('opened')) {
                      $readMoreLink.removeClass('opened');
                      $stepText.animate({
                          height: defaultHeight
                      });
                  } else {
                      $readMoreLink.addClass('opened');
                      $stepText.animate({
                          height: fullHeight
                      });
                  }
              };

            // on mobile firefox, the opening will not work properly (height bug)
            if (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Firefox') > 0) {
              $readMoreLinks.hide();

              $steps.find('.step-text p').css({
                lineHeight: '23px',
                fontSize: '14px'
              });

              $steps.css('padding-bottom', '32px');

              return;
            }
              
            $steps.each(initStepTextHeights);
            $readMoreLinks.click(clickOnReadMore);
        },
        initAnimations = function () {
          // IMAGE TEXT ROW SECTIONS
          swisstScrollTriggerer($imageTextRow1, 'scrolled-to');
          swisstScrollTriggerer($imageTextRow2, 'scrolled-to');

          // APP HOURS SECTION
          $appHoursSection.find('.ahc-card').each((_, element) => {
            swisstScrollTriggerer($(element), 'scrolled-to', 100);
          });

          // BIG BACKGROUND SECTIONS
          $bgBcSections.each((_, element) => {
            swisstScrollTriggerer($(element), 'scrolled-to', -200);
          });

          // LETS WORK SECTION
          swisstScrollTriggerer($letsWorkSection.find('.container'), 'scrolled-to', -100);
        };

      
      initProcessTimingSteps();
      initAnimations();
    };

  init();
});
