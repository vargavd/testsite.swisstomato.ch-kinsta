'use strict';

jQuery(document).ready(function ($) {
    var
        // DOM
        $window = $(window),
        $parallaxHeroImage = $('#parallax-image'),
        $body = $('body'),

        // misc
        is_FR = $body.hasClass('fr-FR'),
        is_DE = $body.hasClass('de-DE'),

        // INIT
        init = function () {
            var
                initParallax = function () {
                    if (window.innerWidth > 470) {
                        $parallaxHeroImage.paroller({
                            factor: -0.1,            // multiplier for scrolling speed and offset
                            type: 'background',     // background, foreground
                            direction: 'vertical', // vertical, horizontal
                        });

                        $window.scroll();
                    }
                },
                initProcessTimingSteps = function () {
                    var
                        // DOM
                        $steps = $('#process-timing-steps .step'),
                        $readMoreLinks = $steps.find('a.step-read-more'),

                        // helper funcs
                        initStepTextHeights = function (i, elem) {
                            var
                                // DOM
                                $step = $(this),
                                $stepText = $step.find('.step-text'),
                                $readMoreLink = $step.find('a.step-read-more'),

                                // misc
                                fullHeight = $stepText.height(),
                                defaultHeights, defaultHeight;

                            defaultHeights = (is_FR) ? [152, 135, 108, 135] :
                                (is_DE) ? [135, 135, 108, 135] :
                                    [179, 162, 162, 162];

                            defaultHeight = (window.innerWidth > 1060) ? defaultHeights[$step.index()] : 135;

                            if ($readMoreLink.length > 0) {
                                $stepText.attr('data-full-height', fullHeight);
                                $stepText.attr('data-default-height', defaultHeight);

                                $stepText.height(defaultHeight);
                            }
                        },
                        clickOnReadMore = function () {
                            var
                                // DOM
                                $readMoreLink = $(this),
                                $step = $readMoreLink.closest('.step'),
                                $stepText = $step.find('.step-text'),

                                // misc
                                fullHeight = $stepText.attr('data-full-height'),
                                defaultHeight = $stepText.attr('data-default-height');

                            if ($readMoreLink.hasClass('opened')) {
                                $readMoreLink.removeClass('opened');
                                $stepText.animate({
                                    height: defaultHeight
                                });
                            } else {
                                $readMoreLink.addClass('opened');
                                $stepText.animate({
                                    height: fullHeight
                                });
                            }
                        };

                    $steps.each(initStepTextHeights);
                    $readMoreLinks.click(clickOnReadMore);
                };

            initParallax();
            initProcessTimingSteps();
        };

    init();
});
