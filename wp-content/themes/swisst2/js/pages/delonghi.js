jQuery(document).ready(function ($) {
    var
        // DOM
        $viewButton = $('#product-content a.view-button'),
        $qrPopupBackground = $('#qr-popup-background'),
        $closeQRPopupBackground = $('#qr-popup button'),

        // events
        openCloseQRPopup = function () {
            $qrPopupBackground.toggle();
        };

    $viewButton.click(openCloseQRPopup);
    $closeQRPopupBackground.click(openCloseQRPopup);

    // $viewButton.click();
    // $popupViewInSpaceButton.click();
});
