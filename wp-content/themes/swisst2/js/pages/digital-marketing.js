"use strict";

jQuery(document).ready(function($) {
    var initParallax = function() {
        var scrollMagicController = new ScrollMagic.Controller();

        // Timeline 1: paradicsom esés, svájci bicska
        var timeline1 = new TimelineMax().add([
            TweenMax.to(".tomato-1", 2, {
                top: -200,
                ease: Power0.easeNone
            }),
            TweenMax.to(".tomato-2", 1, {
                top: 600,
                ease: Power0.easeNone
            })
        ]);

        new ScrollMagic.Scene({
            triggerElement: "#mission-section",
            duration: 2000,
            offset: -300
        })
            .setTween(timeline1)
            .addTo(scrollMagicController);
    };

    initParallax();
});
