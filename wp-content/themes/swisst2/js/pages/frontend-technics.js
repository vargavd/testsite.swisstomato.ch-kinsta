'use strict';

jQuery(document).ready(function ($) {
    var
        // DOM
        $tabs = $('#tabs'),

        // INIT
        init = function () {
            $tabs.tabs();
        };

    init();
});
