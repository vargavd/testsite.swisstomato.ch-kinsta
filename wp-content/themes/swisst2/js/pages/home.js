'use strict';

jQuery(function () {
    var
        // DOM
        $postList = $('#post-list'),
        $bxPagerItems,

        // misc
        bxSlider,

        // events
        startPagerAnim = function (index) {
            $bxPagerItems.removeClass('animation');
            $bxPagerItems.eq(index).addClass('animation');
        },
        startSliding = function () {
            bxSlider.startAuto();
            startPagerAnim(0);
        },
        changeServiceBackgroundImages = function () {
            var
                // DOM
                $serviceImages = $('.service-image');

            $serviceImages.each(function () {
                var
                    // DOM
                    $serviceImage = $(this),

                    // misc
                    mobileBcAttr = $serviceImage.attr('data-mobile-bc');

                if ($serviceImage.width() > 313 && mobileBcAttr) {
                    console.log($serviceImage.width(), mobileBcAttr);
                    $serviceImage.css('background-image', 'url(' + mobileBcAttr + ')');
                }
            });
        },

        INIT = function () {
            var
                initParallax = function () {
                    var
                        bgTomato1, bgTomato2, bgTomato3, bgTomato4, fgTomato1, fgTomato2, fgTomato3, fgTomato4, bgJuice,
                        knife, line, splash1, splash2, splash3, splash4, splash5, splash6, splash7, splash8, splashC, wave, fill,

                        // events
                        rotateKnifeOnScroll = function () {
                            var windowTop = $(window).scrollTop();

                            if (windowTop < 400 || windowTop > 1700) {
                                return;
                            }

                            var theta = ((1300 - $(window).scrollTop()) / 100) * Math.PI * 1.5;

                            $('.blade').css({ transform: 'scale(.5) rotate(' + -theta + 'rad)' });
                        };

                    var scrollMagicController = new ScrollMagic.Controller();


                    // anim parameters (fontos: nem mindegyik param van használva, azért gyűjtöttem így össze hogy látni lehessen honnan hova megy)
                    if (window.innerWidth > 1250) {
                        bgTomato1 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato2 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato3 = { duration: 3, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato4 = { duration: 3.2, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        fgTomato1 = { duration: 1.4, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato2 = { duration: 1.8, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato3 = { duration: 2.6, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato4 = { duration: 2.9, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        bgJuice = { duration: 2, left1: '0', top1: '2000px', left2: '0', top2: '360px', ease: Power0.easeNone };
                        knife = { duration: 3.2, left1: '-70%', top1: '2000px', left2: '100%', top2: '2000px', ease: Linear.easeNone };
                        line = { duration: 1, left1: '-50%', top1: '400px', left2: '50%', top2: '400px', ease: Linear.easeNone, height1: 0, height2: '800px' };
                        splash1 = { duration: 6, left1: '51.5%', top1: '1040px', left2: '12.5%', top2: '820px', ease: Power2.easeOut, delay: 0.7 };
                        splash2 = { duration: 6, left1: '49.5%', top1: '1040px', left2: '27.5%', top2: '740px', ease: Power2.easeOut, delay: 0.7 };
                        splash3 = { duration: 6, left1: '49.5%', top1: '1060px', left2: '56.5%', top2: '520px', ease: Power2.easeOut, delay: 0.7 };
                        splash4 = { duration: 6, left1: '48.5%', top1: '1060px', left2: '62.5%', top2: '770px', ease: Power2.easeOut, delay: 0.7 };
                        splash5 = { duration: 4, left1: '49.5%', top1: '1040px', left2: '69.5%', top2: '880px', ease: Power2.easeOut, delay: 0.7 };
                        splash6 = { duration: 3.5, left1: '40%', top1: '1040px', left2: '49.5%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash7 = { duration: 3.5, left1: '47%', top1: '1040px', left2: '49.5%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash8 = { duration: 3.5, left1: '50%', top1: '1040px', left2: '44.5%', top2: '940px', ease: Power2.easeOut, delay: 0.7 };
                        splashC = { duration: 3, left1: '46.3%', top1: '1060px', left2: '46.3%', top2: '1000px', ease: Power2.easeOut, delay: 0.7 };
                        wave = { duration: 2, left1: '0', top1: '1046px', left2: '-300px', top2: '750px', ease: Power1.easeOut, delay: 2.3, rotation: 15 };
                        fill = { duration: 1, height1: '320px', height2: '600px', ease: Power1.easeOut, delay: 3.1 };
                    } else if (window.innerWidth > 1100) {
                        bgTomato1 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato2 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato3 = { duration: 3, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato4 = { duration: 3.2, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        fgTomato1 = { duration: 1.4, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato2 = { duration: 1.8, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato3 = { duration: 2.6, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato4 = { duration: 2.9, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        bgJuice = { duration: 2, left1: '0', top1: '2000px', left2: '0', top2: '360px', ease: Power0.easeNone };
                        knife = { duration: 3.2, left1: '-70%', top1: '2000px', left2: '100%', top2: '2000px', ease: Linear.easeNone };
                        line = { duration: 1, left1: '-50%', top1: '400px', left2: '50%', top2: '400px', ease: Linear.easeNone, height1: 0, height2: '800px' };
                        splash1 = { duration: 6, left1: '52%', top1: '1040px', left2: '13%', top2: '820px', ease: Power2.easeOut, delay: 0.7 };
                        splash2 = { duration: 6, left1: '50%', top1: '1040px', left2: '28%', top2: '740px', ease: Power2.easeOut, delay: 0.7 };
                        splash3 = { duration: 6, left1: '50%', top1: '1060px', left2: '57%', top2: '520px', ease: Power2.easeOut, delay: 0.7 };
                        splash4 = { duration: 6, left1: '49%', top1: '1060px', left2: '63%', top2: '770px', ease: Power2.easeOut, delay: 0.7 };
                        splash5 = { duration: 4, left1: '50%', top1: '1040px', left2: '70%', top2: '880px', ease: Power2.easeOut, delay: 0.7 };
                        splash6 = { duration: 3.5, left1: '40.8%', top1: '1040px', left2: '49.8%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash7 = { duration: 3.5, left1: '46.8%', top1: '1040px', left2: '49.8%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash8 = { duration: 3.5, left1: '50%', top1: '1040px', left2: '44.5%', top2: '940px', ease: Power2.easeOut, delay: 0.7 };
                        splashC = { duration: 3, left1: '46%', top1: '1060px', left2: '46%', top2: '1000px', ease: Power2.easeOut, delay: 0.7 };
                        wave = { duration: 2, left1: '0', top1: '1046px', left2: '-300px', top2: '750px', ease: Power1.easeOut, delay: 2.3, rotation: 15 };
                        fill = { duration: 1, height1: '320px', height2: '600px', ease: Power1.easeOut, delay: 3.1 };
                    } else if (window.innerWidth > 1000) {
                        bgTomato1 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato2 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato3 = { duration: 3, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato4 = { duration: 3.2, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        fgTomato1 = { duration: 1.4, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato2 = { duration: 1.8, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato3 = { duration: 2.6, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato4 = { duration: 2.9, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        bgJuice = { duration: 2, left1: '0', top1: '2000px', left2: '0', top2: '360px', ease: Power0.easeNone };
                        knife = { duration: 3.2, left1: '-70%', top1: '2000px', left2: '100%', top2: '2000px', ease: Linear.easeNone };
                        line = { duration: 1, left1: '-50%', top1: '391px', left2: '50%', top2: '391px', ease: Linear.easeNone, height1: 0, height2: '800px' };
                        splash1 = { duration: 6, left1: '52%', top1: '1040px', left2: '13%', top2: '820px', ease: Power2.easeOut, delay: 0.7 };
                        splash2 = { duration: 6, left1: '50%', top1: '1040px', left2: '28%', top2: '740px', ease: Power2.easeOut, delay: 0.7 };
                        splash3 = { duration: 6, left1: '50%', top1: '1060px', left2: '57%', top2: '520px', ease: Power2.easeOut, delay: 0.7 };
                        splash4 = { duration: 6, left1: '49%', top1: '1060px', left2: '63%', top2: '770px', ease: Power2.easeOut, delay: 0.7 };
                        splash5 = { duration: 4, left1: '50%', top1: '1040px', left2: '70%', top2: '880px', ease: Power2.easeOut, delay: 0.7 };
                        splash6 = { duration: 3.5, left1: '39%', top1: '1040px', left2: '49.5%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash7 = { duration: 3.5, left1: '47%', top1: '1040px', left2: '49.5%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash8 = { duration: 3.5, left1: '48%', top1: '1040px', left2: '44.5%', top2: '940px', ease: Power2.easeOut, delay: 0.7 };
                        splashC = { duration: 3, left1: '46%', top1: '1060px', left2: '46%', top2: '1000px', ease: Power2.easeOut, delay: 0.7 };
                        wave = { duration: 2, left1: '0', top1: '1046px', left2: '-300px', top2: '750px', ease: Power1.easeOut, delay: 2.3, rotation: 15 };
                        fill = { duration: 1, height1: '320px', height2: '600px', ease: Power1.easeOut, delay: 3.1 };
                    } else {
                        bgTomato1 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato2 = { duration: 3.5, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato3 = { duration: 3, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        bgTomato4 = { duration: 3.2, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power0.easeNone };
                        fgTomato1 = { duration: 1.4, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato2 = { duration: 1.8, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato3 = { duration: 2.6, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        fgTomato4 = { duration: 2.9, left1: '-8%', top1: '-1000px', left2: '-8%', top2: '30%', ease: Power1.easeIn };
                        bgJuice = { duration: 2, left1: '0', top1: '2000px', left2: '0', top2: '360px', ease: Power0.easeNone };
                        knife = { duration: 3.2, left1: '-70%', top1: '2000px', left2: '100%', top2: '2000px', ease: Linear.easeNone };
                        line = { duration: 1, left1: '-50%', top1: '390px', left2: '50%', top2: '390px', ease: Linear.easeNone, height1: 0, height2: '800px' };
                        splash1 = { duration: 6, left1: '51.5%', top1: '1040px', left2: '12.5%', top2: '820px', ease: Power2.easeOut, delay: 0.7 };
                        splash2 = { duration: 6, left1: '49.5%', top1: '1040px', left2: '27.5%', top2: '740px', ease: Power2.easeOut, delay: 0.7 };
                        splash3 = { duration: 6, left1: '49.5%', top1: '1060px', left2: '56.5%', top2: '520px', ease: Power2.easeOut, delay: 0.7 };
                        splash4 = { duration: 6, left1: '48%', top1: '1060px', left2: '62.5%', top2: '770px', ease: Power2.easeOut, delay: 0.7 };
                        splash5 = { duration: 4, left1: '49%', top1: '1040px', left2: '69.5%', top2: '880px', ease: Power2.easeOut, delay: 0.7 };
                        splash6 = { duration: 3.5, left1: '39%', top1: '1040px', left2: '49%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash7 = { duration: 3.5, left1: '46.5%', top1: '1040px', left2: '49%', top2: '930px', ease: Power2.easeOut, delay: 0.7 };
                        splash8 = { duration: 3.5, left1: '50%', top1: '1040px', left2: '43.5%', top2: '950px', ease: Power2.easeOut, delay: 0.7 };
                        splashC = { duration: 3, left1: '45.2%', top1: '1060px', left2: '45.2%', top2: '1000px', ease: Power2.easeOut, delay: 0.7 };
                        wave = { duration: 2, left1: '0', top1: '1046px', left2: '-300px', top2: '750px', ease: Power1.easeOut, delay: 2.3, rotation: 15 };
                        fill = { duration: 1, height1: '320px', height2: '600px', ease: Power1.easeOut, delay: 3.1 };
                    }


                    // Timeline 1: paradicsom esés, svájci bicska
                    var timeline1 = new TimelineMax().add([
                        TweenMax.to('.fg-tomato-1', fgTomato1.duration, { top: fgTomato1.top2, ease: fgTomato1.ease }),
                        TweenMax.to('.fg-tomato-2', fgTomato2.duration, { top: fgTomato2.top2, ease: fgTomato2.ease }),
                        TweenMax.to('.fg-tomato-3', fgTomato3.duration, { top: fgTomato3.top2, ease: fgTomato3.ease }),
                        TweenMax.to('.fg-tomato-4', fgTomato4.duration, { top: fgTomato4.top2, ease: fgTomato4.ease }),
                        TweenMax.to('.bg-tomato-1', bgTomato1.duration, { top: bgTomato1.top2 }),
                        TweenMax.to('.bg-tomato-2', bgTomato2.duration, { top: bgTomato2.top2 }),
                        TweenMax.to('.bg-tomato-3', bgTomato3.duration, { top: bgTomato3.top2 }),
                        TweenMax.to('.bg-tomato-4', bgTomato4.duration, { top: bgTomato4.top2 }),
                        TweenMax.to('.bg', bgJuice.duration, { top: bgJuice.top2 }),
                        TweenMax.to('.knife', knife.duration, { left: knife.left2, ease: knife.ease }),
                    ]);

                    new ScrollMagic.Scene({
                        triggerElement: '#parallax-section',
                        duration: 1000,
                        offset: -300,
                    }).setTween(timeline1).addTo(scrollMagicController);


                    // Timeline 2:
                    var timeline2 = new TimelineMax()
                        .add([
                            TweenMax.fromTo('#line', line.duration, { top: line.top1 }, { top: line.top2, height: line.height2, ease: line.ease }),
                            TweenMax.to('.one', splash1.duration, { top: splash1.top2, left: splash1.left2, ease: splash1.ease, delay: splash1.delay }),
                            TweenMax.to('.two', splash2.duration, { top: splash2.top2, left: splash2.left2, ease: splash2.ease, delay: splash2.delay }),
                            TweenMax.to('.three', splash3.duration, { top: splash3.top2, left: splash3.left2, ease: splash3.ease, delay: splash3.delay }),
                            TweenMax.to('.four', splash4.duration, { top: splash4.top2, left: splash4.left2, ease: splash4.ease, delay: splash4.delay }),
                            TweenMax.to('.five', splash5.duration, { top: splash5.top2, left: splash5.left2, ease: splash5.ease, delay: splash5.delay }),
                            TweenMax.fromTo('.six', splash6.duration, { top: splash6.top1, left: splash6.left1 }, { top: splash6.top2, left: splash6.left2, ease: splash6.ease, delay: splash6.delay }),
                            TweenMax.fromTo('.seven', splash7.duration, { top: splash7.top1, left: splash7.left1 }, { top: splash7.top2, left: splash7.left2, ease: splash7.ease, delay: splash7.delay }),
                            TweenMax.fromTo('.eight', splash8.duration, { top: splash8.top1, left: splash8.left1 }, { top: splash8.top2, left: splash8.left2, ease: splash8.ease, delay: splash8.delay }),
                            TweenMax.fromTo('.center', splashC.duration, { top: splashC.top1, left: splashC.left1 }, { top: splashC.top2, left: splashC.left2, ease: splashC.ease, delay: splashC.delay }),
                            TweenMax.to('#bottom-wave', wave.duration, { left: wave.left2, rotation: wave.rotation, top: wave.top2, ease: wave.ease, delay: wave.delay }),
                            TweenMax.to('#fill', fill.duration, { height: fill.height2, ease: fill.ease, delay: fill.delay }),
                            TweenMax.to('#awards-section-text', 1, { opacity: 1, ease: Power1.easeOut, delay: 1.4 }),
                            TweenMax.to('#awards .award', 0, { opacity: 1, top: 0, delay: 2 }),
                        ]);

                    new ScrollMagic.Scene({
                        triggerElement: '.knife',
                        duration: 2000,
                        offset: 50,
                    })
                        .setTween(timeline2)
                        .addTo(scrollMagicController);

                    var content = new TimelineMax()
                        .add([TweenMax.to('#down-section', 2, { opacity: 1, ease: Linear.easeNone })]);

                    new ScrollMagic.Scene({
                        triggerElement: '.knife',
                        duration: 200,
                        offset: 700,
                    })
                        .setTween(content)
                        .addTo(scrollMagicController);

                    $(window).scroll(rotateKnifeOnScroll);

                },
                initAwardAnimsOnSmallerScreens = function () {
                    var
                        // DOM
                        $awardsTopRow = $('#awards.top-row'),
                        $awardsBottomRow = $('#awards.bottom-row');

                    if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
                        let observer = new IntersectionObserver((entries) => {
                            $(entries[0].target).toggleClass('triggered', entries[0].isIntersecting);
                        }, {
                            rootMargin: "0px 0px 50px 0px"
                        });

                        observer.observe($awardsTopRow[0]);
                        observer.observe($awardsBottomRow[0]);
                    } else {
                        $awardsBottomRow.addClass('triggered');
                        $awardsTopRow.addClass('triggered');
                    }
                };

            if ((window.matchMedia('(max-width: 911px)').matches === true) || typeof TweenMax === "undefined" || $('#parallax-section').length === 0) {
                initAwardAnimsOnSmallerScreens();
            } else {
                initParallax();
            }

            changeServiceBackgroundImages();

            if (window.innerWidth > 600) {
                // SLIDER
                bxSlider = $postList.bxSlider({
                    slideMargin: 10,
                    controls: false,
                    auto: false,
                    pause: 5000,
                    touchEnabled: false,
                    autoHover: false,
                    onSlideBefore: function ($slideElem, oldIndex, newIndex) {
                        startPagerAnim(newIndex);
                    },
                });
                $bxPagerItems = $('.bx-pager-item');
                setTimeout(startSliding, 400);
            }
        };

    INIT();
});
