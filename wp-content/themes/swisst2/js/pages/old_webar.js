jQuery(document).ready(function ($) {
    var
        // helper
        windowWidth = $(window).width(),

        // DOM
        $nextBigEcommerceSection = $('#next-big-ecommerce-section'),
        $nextBigEcommerceVideo = $nextBigEcommerceSection.find('video'),
        $nextBigEcommerceDarkLayer = $nextBigEcommerceSection.find('.dark-layer'),
        $heroSection = $('#hero-section'),
        $purpleSectionPlayIcons = $('.purple-section-item-img-video i.fa-play'),

        // helper funcs
        adjustSizes = function () {
            if (windowWidth > 1360) {
                $nextBigEcommerceSection.height($nextBigEcommerceVideo.height()).css('visibility', 'visible');
                $nextBigEcommerceDarkLayer.height($nextBigEcommerceVideo.height());
            }

            if (windowWidth < 681) {
                console.log(windowWidth);
                $heroSection.css('background-image', 'url(' + $heroSection.attr('data-mobile-bc') + ')');
            }
        },
        animationsInit = function () {
            var controller = new ScrollMagic.Controller();


            /*** NO APP SECTION ***/
            var noAppScene = new ScrollMagic.Scene({
                triggerElement: "#no-app-section",
                duration: 2000,
                offset: -200
            })
                .setTween("#no-app-section-image", 0.5, { top: '-200px' }) // trigger a TweenMax.to tween
                .addTo(controller);

            console.log(jQuery('#no-app-section-image').length);


            /*** VIEW3D SECTION ***/
            var view3dTimeline = new TimelineMax().add([
                TweenMax.to('#view3d-images img:first-of-type', 1, { top: '-300px' }),
                TweenMax.to('#view3d-images img:last-of-type', 1, { top: '-100px' }),
            ]);

            var view3dScene = new ScrollMagic.Scene({
                triggerElement: "#view3d-section",
                duration: 2500,
                offset: 0
            })
                .setTween(view3dTimeline)
                .addTo(controller);


            /*** PURPLE SECTION ***/
            var purpleSectionTimeline = new TimelineMax().add([
                TweenMax.to('.purple-section-media-item:nth-of-type(2n)', 2, { top: '-270px' }),
                TweenMax.to('.purple-section-media-item:nth-of-type(2n+1)', 2, { top: '-500px' }),
            ]);

            var purpleSectionScene = new ScrollMagic.Scene({
                triggerElement: "#purple-section",
                duration: 2000,
                offset: -300
            })
                .setTween(purpleSectionTimeline)
                .addTo(controller);


            var purpleSection2Timeline = new TimelineMax().add([
                TweenMax.to('#purple-background-curve', 2, { left: '-150px' }),
                TweenMax.to('#purple-background-dots-1', 1, { top: '0px' }),
                TweenMax.to('#purple-background-dots-2', 2, { top: '450px' }),
                TweenMax.to('#purple-background-lines-1', 2, { bottom: '150px' }),
                TweenMax.to('#purple-background-lines-2', 2, { bottom: '0px' }),
            ]);

            var purpleSection2Scene = new ScrollMagic.Scene({
                triggerElement: "#purple-section",
                duration: 2000,
                offset: -300
            })
                .setTween(purpleSection2Timeline)
                .addTo(controller);

        },
        startPurpleSectionVideo = function () {
            var
                // DOM
                $playIcon = $(this),
                $imgVideoWrapper = $playIcon.closest('.purple-section-item-img-video'),
                $img = $imgVideoWrapper.find('img'),
                $video = $imgVideoWrapper.find('video');

            $playIcon.hide();
            $img.hide();
            $video.show();
        };

    $(window).on('resize', adjustSizes);

    adjustSizes();

    if (windowWidth > 1360) {
        setTimeout(function () {
            $nextBigEcommerceSection.height($nextBigEcommerceVideo.height()).css('visibility', 'visible');
            $nextBigEcommerceDarkLayer.height($nextBigEcommerceVideo.height());
        }, 300);
    } else {
        $nextBigEcommerceSection.css('visibility', 'visible');
    }


    if (windowWidth > 900) {
        animationsInit();
    }

    $purpleSectionPlayIcons.click(startPurpleSectionVideo);
});
