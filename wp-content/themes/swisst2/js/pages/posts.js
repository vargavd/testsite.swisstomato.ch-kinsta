'use strict';

jQuery(document).ready(function ($) {
    var
        // DOM
        $loadingGif = $('#loading-posts-gif'),
        $loadMoreButton = $('#load-more-posts-button'),
        $blogPostList = $('#blog-post-list'),
        $postTemplate = $('.post-template').detach(),
        $post,

        // misc
        restPostsUrl = swisstInfos.baseUrl + '/wp-json/wp/v2/posts?lang=' + swisstInfos.language.split('_')[0] + '&acf_format=standard',
        perPage = 6,
        alreadyShown = 7,
        totalPost = 0,

        // events
        loadMorePosts = function () {
            var
                url = restPostsUrl;

            $loadMoreButton.hide();
            $loadingGif.show();

            url += '&offset=' + alreadyShown + '&per_page=' + perPage;

            $.get(url, {}, function (posts, status, request) {
                posts.forEach(function (post) {
                    $post = $postTemplate.clone();
                    $post.removeClass('post-template');

                    $post.find('.post-image')
                        .css({ 'background-image': 'url(' + post.acf.post_thumbnail + ')' })
                        .attr('href', post.link);
                    $post.find('.post-title a')
                        .text(decodeURI(post.title.rendered.replace('&#8211;', '–').replace('&#8217;', '\'').replace(/&rsquo;/g, "'")))
                        .attr('href', post.link);
                    $post.find('.post-date').text(post.data_string);
                    $post.find('.post-text').html(post.excerpt.rendered);

                    $blogPostList.append($post);
                });

                alreadyShown += perPage;
                $loadingGif.hide();

                if (!totalPost) {
                    totalPost = request.getResponseHeader('x-wp-total');
                }

                if (alreadyShown < totalPost) {
                    $loadMoreButton.show();
                }
            });
        };

    $loadMoreButton.click(loadMorePosts);
});
