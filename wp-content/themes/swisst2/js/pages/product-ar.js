jQuery(document).ready(function ($) {
    var
        // DOM
        $viewButton = $('#product-content a.view-button'),
        $qrPopupBackground = $('#qr-popup-background'),
        $closeQRPopupBackground = $('#qr-popup button'),

        // events
        openCloseQRPopup = function () {
            $qrPopupBackground.toggle();
        },

        // misc
        osString = platform.os.toString().toLowerCase();

    $viewButton.click(openCloseQRPopup);
    $closeQRPopupBackground.click(openCloseQRPopup);

    // $viewButton.click();
    // $popupViewInSpaceButton.click();

    if (osString.indexOf('android') !== -1) {
        window.location.href = "intent://arvr.google.com/scene-viewer/1.0?file=<?=$glb_file?>&mode=ar_preferred#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://swisstomato.ch/en/delonghi;end;";
    }
});
