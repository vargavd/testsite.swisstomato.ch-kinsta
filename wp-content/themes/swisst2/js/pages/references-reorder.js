'use strict';

jQuery(document).ready(function ($) {
  var
    // DOM
    $referencesList = $('.references'),
    $getReferences = () => $referencesList.find('.reference'),
    $loadingPopup = $('.loading-popup'),
    $savingButton = $('button.saving-button'),

    // misc
    orderField = $referencesList.attr('data-order-field'),
    references = [],

    // helper func
    updateReferenceAjax = function (referenceId, position, successCB) {
      console.log(`Updating: ${referenceId}, ${position} ...`);

      // https://www.advancedcustomfields.com/resources/wp-rest-api-integration/#authentication
      $.ajax(`${swisstInfos.baseUrl}/wp-json/wp/v2/reference/${referenceId}`, {
        method: 'POST',
        data: {
          "acf": {
            orderField: position
          }
        },
        beforeSend: xhr => xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce),
      }).done(successCB).fail(() => console.log(`Update failed: ${referenceId}, ${fields}, ${position}`));
    },

    // events
    sortingFinished = function () {
      $getReferences().each(function (index) {
        var
          // DOM
          $reference = $(this),
          $referencePosition = $reference.find('.reference-position');

        $referencePosition.text(index + 1);

        references.push({
          id: $reference.attr('data-id'),
          position: index + 1
        });
      });
    },
    saveReferenceOrder = function () {
      $loadingPopup.css('display', 'flex');

      $.ajax(`${swisstInfos.baseUrl}/wp-json/references/v1/reorder`, {
        method: 'POST',
        data: {
          "order_field": orderField,
          "reference_infos": references
        },
      }).done(() => {
        $loadingPopup.hide();
        console.log('saving references done');
      }).fail(data => {
        console.error('Saving failed: ', data);
      });
    };

  $referencesList.sortable();
  $referencesList.on('sortupdate', sortingFinished);

  $savingButton.click(saveReferenceOrder);
});