'use strict';

jQuery(document).ready(function ($) {
    var
        // DOM
        $document = $(this),
        $htmlBody = $('html, body'),
        $platformFilters = $('#platform-filters .filter-box'),
        $areaFilters = $('#areas-filter-area .filter-box'),
        $loadingGif = $('#loading-gif'),
        $loadingGifMobileWrapper = $('#loading-gif-mobile-wrapper'),
        $referenceList = $('.references-section .post-list'),
        $referenceTemplate = $('#post-template').detach().removeAttr('id'),

        // filters DOM
        $filterSection = $('#filter-section'),
        $categoryFilters = $filterSection.find('.filter-boxes'),
        $spinner = $filterSection.find('.fa-spinner'),

        // helper variables for URL making
        platformUrlName = 'platform',
        areaUrlName = 'area',
        platformRestName = 'reference_category',
        areaRestName = 'reference_tag',
        baseRestUrl = swisstInfos.baseUrl + '/wp-json/wp/v2/reference', // https://swisstomato.ch/nextgen/wp-json/wp/v2/reference?reference_category=14&reference_tag=17&_embed

        // misc
        ajaxResults = [], // <- ide fogja berakni a referencesDownloaded esemény a case studykat és az ajaxStop innen fogja kivenni a leguccsó kérés elemeit
        loadReferencesOnHashChange = true,
        loadingAllReferencesForEmptyResult = false,
        thereWasNoReferenceLoadingYet = true,

        // helper functions
        getSelectedPlatformFilter = function () {
            return $platformFilters.filter('.selected').first();
        },
        getSelectedAreaFilters = function () {
            return $areaFilters.filter('.selected');
        },
        constructBrowserUrl = function () {
            var
                // DOM
                $selectedPlatformFilter = getSelectedPlatformFilter(),
                $selectedAreaFilters = getSelectedAreaFilters(),

                // misc
                areaSlugs = [],
                url = location.href.split('#')[0];

            if ($selectedPlatformFilter.length > 0) {
                url += '#' + platformUrlName + '=' + $selectedPlatformFilter.attr('data-slug');
            }

            if ($selectedAreaFilters.length > 0) {
                $selectedAreaFilters.each(function () {
                    areaSlugs.push($(this).attr('data-slug'));
                });

                if (url.indexOf('#') !== -1) {
                    url += '&' + areaUrlName + '=' + areaSlugs.join(',');
                } else {
                    url += '#' + areaUrlName + '=' + areaSlugs.join(',');
                }
            }

            if (url.indexOf('#') === -1) {
                url += '#';
            }

            return url;
        },
        constructRestUrl = function () {
            var
                // DOM
                $selectedPlatformFilter = getSelectedPlatformFilter(),
                $selectedAreaFilters = getSelectedAreaFilters(),

                // misc
                areaIDs = [],
                restParamParts = [],
                url = baseRestUrl;

            if ($selectedPlatformFilter.length > 0 && loadingAllReferencesForEmptyResult !== true) {
                restParamParts.push(platformRestName + '=' + $selectedPlatformFilter.attr('data-id'));
            }

            if ($selectedAreaFilters.length > 0 && loadingAllReferencesForEmptyResult !== true) {
                $selectedAreaFilters.each(function () {
                    areaIDs.push($(this).attr('data-id'));
                });

                restParamParts.push(areaRestName + '=' + areaIDs.join(','));
            }

            restParamParts.push('_embed');
            restParamParts.push('per_page=100');

            url += '?' + restParamParts.join('&');

            return url;
        },
        showLoadingGif = function () {
            if (window.innerWidth < 500) {
                $loadingGifMobileWrapper.css('display', 'block');
            } else {
                $loadingGif.css('display', 'block');
            }
        },
        hideLoadingGif = function () {
            $loadingGifMobileWrapper.hide();
            $loadingGif.hide();
        },

        // events
        clickPlatform = function () {
            var
                // DOM
                $platformFilter = $(this);

            if ($platformFilter.hasClass('selected')) {
                $platformFilter.removeClass('selected');
            } else {
                $platformFilters.removeClass('selected');

                $platformFilter.addClass('selected');
            }

            loadReferences();
        },
        clickArea = function () {
            var
                // DOM
                $area = $(this),
                $images = $area.find('.area-icon picture');

            $area.toggleClass('selected');

            if ($area.hasClass('selected')) {
                $images.first().css('display', 'none');

                $images.last().css('opacity', '0.5').show().animate({
                    opacity: 1,
                }, 200);
            }

            loadReferences();
        },
        clickOnTagLink = function (e) {
            var
                // DOM
                $link = $(this),

                // misc
                slug = $link.attr('data-slug');

            e.preventDefault();

            $platformFilters.removeClass('selected');
            $areaFilters.removeClass('selected');

            if ($link.attr('href').indexOf('area=') !== -1) {
                $areaFilters.filter('[data-slug="' + slug + '"]').click();
            }

            if ($link.attr('href').indexOf('platform=') !== -1) {
                $platformFilters.filter('[data-slug="' + slug + '"]').click();
            }

            $htmlBody.animate({
                scrollTop: 300,
            }, 300, 'swing');
        },
        getMediaFromReference = function (reference) {
            if (!reference.thumbnail_image_url) {
                return '/wp-content/themes/swisst2/imgs/swisstomato.png';
            }

            return reference.thumbnail_image_url;
        },
        onHashChange = function () {
            var
                // misc
                fragment, fragmentParts, platformSlug, areaSlugs;

            if (location.href.indexOf('#') === -1 || !loadReferencesOnHashChange || location.href.indexOf('#contact-us') !== -1) {
                loadReferencesOnHashChange = true;
                return;
            }

            fragment = location.href.split('#')[1];

            if (fragment === '') {
                return;
            }

            fragmentParts = fragment.split('&');

            _.forEach(fragmentParts, function (fragmentPart) {
                if (fragmentPart.indexOf(platformUrlName + '=') !== -1) {
                    platformSlug = fragmentPart.replace(platformUrlName + '=', '');
                }
                if (fragmentPart.indexOf(areaUrlName + '=') !== -1) {
                    areaSlugs = fragmentPart.replace(areaUrlName + '=', '').split(',');

                    if (areaSlugs[0] === '') {
                        areaSlugs.splice(0, 1);
                    }
                }
            });

            // reset filters
            $platformFilters.removeClass('selected');
            $areaFilters.removeClass('selected');

            // add selected classes to the filters
            $platformFilters.filter('[data-slug=' + platformSlug + ']').addClass('selected');
            _.forEach(areaSlugs, function (areaSlug) {
                $areaFilters.filter('[data-slug=' + areaSlug + ']').addClass('selected');
            });

            loadReferences();
        },
        areaFilterMouseEnter = function () {
            var
                // DOM
                $filterBox = $(this),
                $images = $filterBox.find('.area-icon picture');

            if ($filterBox.hasClass('selected')) {
                $images.first().css('display', 'none');

                $images.last().css('opacity', '0.5').show().animate({
                    opacity: 1,
                }, 200);
                return;
            }

            $images.first().css('display', 'none');

            $images.last().css('opacity', '0.5').show().animate({
                opacity: 1,
            }, 200);
        },
        areaFilterMouseLeave = function () {
            var
                // DOM
                $filterBox = $(this),
                $images = $filterBox.find('.area-icon picture');

            if ($filterBox.hasClass('selected')) {
                return;
            }

            $images.last().css('display', 'none');

            $images.first().css('opacity', '0.5').show().animate({
                opacity: 1,
            }, 200);
        },

        // main functions
        setUpTagClicks = function () {
            $('.post-categories a').unbind('click').click(clickOnTagLink);
        },
        createReferenceElem = function (reference) {
            var
                // DOM
                $reference = $referenceTemplate.clone();


            /* REFERENCE LINK URL */
            $reference.find('a').attr('href', reference.link);


            /* REFERENCE IMAGE */
            $reference.find('.post-image').css('background-image', 'url(' + getMediaFromReference(reference) + ')');


            /* REFERENCE TOPICS */
            _.forEach(reference.reference_category, function (platformId) {
                var
                    // DOM
                    $platformFilter = $platformFilters.filter('[data-id=' + platformId + ']'),
                    $platformLink = $('<a></a>'),

                    // misc
                    platformName = $platformFilter.attr('data-name'),
                    platformSlug = $platformFilter.attr('data-slug'),
                    platformUrl;

                platformUrl = location.href.replace(location.hash, '');
                platformUrl = platformUrl.replace('#', '');
                platformUrl += '#' + platformUrlName + '=' + platformSlug;

                $platformLink
                    .attr('title', platformName)
                    .attr('data-slug', platformSlug)
                    .attr('href', platformUrl)
                    .text('#' + platformName);

                $reference.find('.post-categories').append($platformLink);
            });


            /* REFERENCE TOPICS */
            _.forEach(reference.reference_tag, function (areaId) {
                var
                    // DOM
                    $areaFilter = $areaFilters.filter('[data-id=' + areaId + ']'),
                    $areaLink = $('<a></a>'),

                    // misc
                    areaName = $areaFilter.attr('data-name'),
                    areaSlug = $areaFilter.attr('data-slug'),
                    areaUrl;

                areaUrl = location.href.replace(location.hash, '');
                areaUrl = areaUrl.replace('#', '');
                areaUrl += '#' + areaUrlName + '=' + areaSlug;

                $areaLink
                    .attr('title', areaName)
                    .attr('data-slug', areaSlug)
                    .attr('href', areaUrl)
                    .text('#' + areaName);

                $reference.find('.post-categories').append($areaLink);
            });


            /* REFERENCE TITLE */
            $reference.find('.post-title').html(reference.title.rendered);


            // put into the DOM
            $referenceList.append($reference);
        },
        loadReferences = function () {
            var
                // misc
                restUrl = constructRestUrl(),
                requestStartTime = Date.now(),

                // events
                referencesDownloaded = function (references) {
                    ajaxResults.push({
                        references: references,
                        requestStartTime: requestStartTime,
                    });
                };

            thereWasNoReferenceLoadingYet = false;

            $referenceList.empty().hide();
            showLoadingGif();

            if (loadingAllReferencesForEmptyResult !== true) {
                loadReferencesOnHashChange = false;
                location.href = constructBrowserUrl();
            }

            $.get(restUrl, {}, referencesDownloaded);
        },
        allAjaxDone = function () {
            var
                // misc
                references;

            if (thereWasNoReferenceLoadingYet) {
                return;
            }

            references = _.maxBy(ajaxResults, 'requestStartTime').references;

            if (references.length === 0) {
                loadingAllReferencesForEmptyResult = true;
                loadReferences(true);
                return;
            }

            if (loadingAllReferencesForEmptyResult) {
                loadingAllReferencesForEmptyResult = false;
                $referenceList.append('<div class="text-center" style="width: 100%; margin-bottom: 50px;">' + swisstInfos.no_references_text + '</div>');
            }

            _.forEach(references, createReferenceElem);

            $referenceList.css('display', 'flex');
            hideLoadingGif();

            ajaxResults = [];

            $referenceList.removeClass('low-num-item');
            if (references.length < 3) {
                $referenceList.addClass('low-num-item');
            }

            setUpTagClicks();
        },
        init = function () {
            if (swisstInfos.language.toLowerCase() === 'fr-fr') {
                baseRestUrl = baseRestUrl.replace('wp-json', 'fr/wp-json');
            } else if (swisstInfos.language.toLowerCase() === 'de-de') {
                baseRestUrl = baseRestUrl.replace('wp-json', 'de/wp-json');
            }

            // load references if there is a hash
            onHashChange();

            // display the post list
            $referenceList.css('visibility', 'visible');

            // setting the click events
            $platformFilters.click(clickPlatform);
            $areaFilters.click(clickArea);
            setUpTagClicks();

            // on hash change
            window.addEventListener('hashchange', onHashChange);

            // this function will create the reference list from the last ajax call
            $document.ajaxStop(allAjaxDone);

            if (window.innerWidth > 500) {
                $areaFilters.hover(areaFilterMouseEnter, areaFilterMouseLeave);
            }
        };

    init();

});