'use strict';

jQuery(document).ready(function ($) {
    var
        // DOM
        $readMoreLinks = $('a.read-more'),

        // events
        clickOnReadMore = function () {
            var
                // DOM
                $link = $(this),
                $text = $link.next('.more-text');

            $link.toggleClass('opened');
            $text.toggle(400);
        };

    $readMoreLinks.click(clickOnReadMore);
});
