'use strict';

jQuery(function () {
  var
    // section DOM
    $ourStorySection = $('.our-story-section'),
    $bigRedTextSection = $('#big-red-text-section'),
    $whyUsSection = $('#why-us-section'),
    $agencySection = $('#agency-section'),
    $websiteCreationSection = $('#website-creation-section'),
    $bgBcSections = $('.swisst2-big-bc-mt-section'),
    $whyImportantSection = $('#why-important-section'),
    $letsShapeSection = $('#lets-shape-section'),

    // INIT
    initAnimations = function () {
      // animations
      swisstScrollTriggerer($agencySection, 'scrolled-to');
      //$agencySection.addClass('no-scroll');
      swisstScrollTriggerer($websiteCreationSection, 'scrolled-to');
      //$websiteCreationSection.addClass('no-scroll');
      swisstScrollTriggerer($whyImportantSection.find('#why-important-cards'), 'scrolled-to');
      //$whyImportantSection.find('#why-important-cards').addClass('no-scroll');

      $bgBcSections.each((_, element) => {
        swisstScrollTriggerer($(element), 'scrolled-to', -100);
        //$(element).addClass('no-scroll');
      });

      swisstScrollTriggerer($letsShapeSection.find('.container'), 'scrolled-to');
      //$letsShapeSection.find('.container').addClass('no-scroll');

    };

    initAnimations();
});
