gsap.registerPlugin(ScrollTrigger, CSSPlugin)

function animateHero() {
	gsap.to('.js-hero-header', {
		scrollTrigger: {
			trigger: '.js-hero',
			start: 'top',
			scrub: 0,
		},
		y: -200,
	})

	gsap.set('.js-hero-media', {
		opacity: 0,
		backgroundPosition: 'left bottom 0, left 30rem top -25rem',
	})

	gsap.to('.js-hero-media', {
		scrollTrigger: {
			trigger: '.js-hero',
			endTrigger: '.js-you-choose',
			start: 'top',
			scrub: 0,
		},
		y: -250,
	})

	gsap.to('.js-hero-media', 1, {
		delay: 1,
		opacity: 1,
		ease: 'power4.inOut',
	})

	gsap.to('.js-hero-media', 1, {
		delay: 1.5,
		backgroundPosition: 'left bottom 30rem, left 30rem top 0',
	})
}

function animateExperience() {
	gsap.set('.js-experience-media', {
		y: 100,
	})

	gsap.to('.js-experience-media', {
		scrollTrigger: {
			trigger: '.js-experience-media',
			scrub: 0,
		},
		y: -100,
	})
}

function animateYouChoose() {
	gsap.set('.js-you-choose-item:nth-of-type(1)', {
		y: 280,
	})

	gsap.to('.js-you-choose-item:nth-of-type(1)', {
		scrollTrigger: {
			trigger: '.js-you-choose-item:nth-of-type(1)',
			scrub: 0,
		},
		y: -400,
	})

	gsap.set('.js-you-choose-item:nth-of-type(2)', {
		y: 180,
	})

	gsap.to('.js-you-choose-item:nth-of-type(2)', {
		scrollTrigger: {
			trigger: '.js-you-choose-item:nth-of-type(2)',
			scrub: 0,
		},
		y: -130,
	})
}

function animateCompatibleClip() {
	gsap.set('.js-clip-compatible', {
		backgroundPosition: `left 3rem bottom -40rem, right 3rem top 20rem, left -70vw center`,
		clipPath: 'polygon(0 0vh, 100% 0, 100% 100%, 0 100%)',
	})

	gsap.to('.js-clip-compatible', 1, {
		scrollTrigger: {
			trigger: '.js-clip-compatible',
			start: 'top bottom',
			scrub: .2,
		},
		backgroundPosition: `left 3rem bottom 40rem, right 3rem top 3rem, left 0vw center`,
		ease: 'power2.out',
	})

	gsap.to('.js-clip-compatible', 1, {
		scrollTrigger: {
			trigger: '.js-compatible',
			start: 'top bottom-=120px',
			end: 'center bottom',
			scrub: .2,
		},
		clipPath: 'polygon(0 25vh, 100% 0, 100% 100%, 0 100%)',
		ease: 'power1.out',
	})
}

function animateCompatible() {
	animateCompatibleClip()

	gsap.set('.js-compatible-item:nth-of-type(1)', {
		y: 250,
	})

	gsap.to('.js-compatible-item:nth-of-type(1)', {
		scrollTrigger: {
			trigger: '.js-compatible-item:nth-of-type(1)',
			scrub: 0,
		},
		y: -300,
	})

	gsap.set('.js-compatible-item:nth-of-type(2)', {
		y: 50,
	})

	gsap.to('.js-compatible-item:nth-of-type(2)', {
		scrollTrigger: {
			trigger: '.js-compatible-item:nth-of-type(2)',
			scrub: 0,
		},
		y: -130,
	})

	gsap.to('.js-compatible-header', {
		scrollTrigger: {
			trigger: '.js-compatible-header',
			scrub: 0,
		},
		y: -50,
	})
}

function animateWebBased() {
	gsap.set('.js-web-based-media', {
		y: 280,
	})

	gsap.to('.js-web-based-media', {
		scrollTrigger: {
			trigger: '.js-web-based-media',
			endTrigger: '.js-you-choose',
			scrub: 0,
		},
		y: -400,
	})
}

function animateCopy() {
	const copy = document.querySelectorAll('.js-copy')

	copy.forEach((e, i, arr) => {
		let j = i

		if (j > 0 && j % 2 !== 0) {
			j = j - 1
		}

		gsap.from(arr[i], {
			scrollTrigger: {
				trigger: arr[j],
			},
			opacity: 0,
			duration: 1,
			y: 80,
			ease: 'power4.inOut',
		})
	})
}

function animateCommon() {
	animateHero()
	animateExperience()
	animateYouChoose()
	animateCompatible()
	animateWebBased()
	animateCopy()
}

function animateHeroGraphicLg() {
	const screenWidth = document.documentElement.clientWidth
	const heroSnakeBgWidth = 1146
	const heroPostHeader = document.querySelector('.js-hero-header')
	const heroPostHeaderRight = heroPostHeader.getBoundingClientRect().right
	const gap = 48;
	let heroSnakeBgToRight = screenWidth - heroSnakeBgWidth - heroPostHeaderRight - gap
	heroSnakeBgToRight = heroSnakeBgToRight < 0 ? heroSnakeBgToRight : 0

	gsap.set('.js-hero-graphic', {
		backgroundPosition: `right ${-screenWidth}px top 15rem`,
		opacity: 0,
		transform: 'rotateX(-75deg)',
	})

	gsap.to('.js-hero-graphic', 1.4, {
		backgroundPosition: `right ${heroSnakeBgToRight}px top 15rem`,
		opacity: 1,
		ease: 'power4.inOut',
	})

	gsap.to('.js-hero-graphic', 1.4, {
		transform: 'rotateX(0)',
		delay: .3,
		ease: 'power4.inOut',
	})
}

function animateHeroGraphicMd() {
	gsap.set('.js-hero-graphic', {
		backgroundPosition: `right -100vw bottom 14rem`,
		opacity: 0,
		transform: 'rotateX(-75deg)',
	})

	gsap.to('.js-hero-graphic', 1.4, {
		backgroundPosition: `right 0vw bottom 14rem`,
		opacity: 1,
		ease: 'power4.inOut',
	})

	gsap.to('.js-hero-graphic', 1.4, {
		transform: 'rotateX(0)',
		delay: .3,
		ease: 'power4.inOut',
	})
}

function animationsMediaLg() {
	animateCommon()
	animateHeroGraphicLg()
}

function animationsMediaMd() {
	animateCommon()
	animateHeroGraphicMd()
}

ScrollTrigger.matchMedia({
	'(min-width: 896px) and (max-width: 1359px)': animationsMediaMd,
	'(min-width: 1360px)': animationsMediaLg,
})