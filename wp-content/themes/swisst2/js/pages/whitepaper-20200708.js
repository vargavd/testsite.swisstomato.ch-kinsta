'use strict';

jQuery(document).ready(function ($) {
    var
        // DOM
        $form = $('#section-form form'),
        $heroButton = $('#hero-section .button-red'),
        $htmlBody = $('html,body'),

        // helper funcs
        scrollToForm = function () {
            $htmlBody.animate({
                scrollTop: $form.offset().top - 200
            }, 700, 'swing');
        },

        // init
        initWhitepaper = function () {
            var
                initForm = function () {
                    var
                        // DOM
                        $form = $('#section-form form'),
                        $inputs = $form.find('input[type=text], input[type=tel], textarea, input[type=email]'),
                        $checkbox = $form.find('input[type=checkbox]'),
                        $conditionLabel = $form.find('#wp-conditions-checkbox .wpcf7-list-item-label'),

                        // misc
                        labelContent = $conditionLabel.html(),
                        newLinkContentEN = '<a class="privacy-policy-link" href="' + swisstInfos.privacyPolicyUrl + '" target="_blank">terms & conditions</a>',

                        // helper
                        focusOnContactInput = function () {
                            var
                                // DOM
                                $input = $(this),
                                $fieldGroup = $input.closest('.field-group');

                            $fieldGroup.toggleClass('focus');

                            if ($fieldGroup.hasClass('filled') && $input.val() === '') {
                                $fieldGroup.removeClass('filled');
                            } else {
                                $fieldGroup.addClass('filled');
                            }
                        };

                    // set up label animation
                    $inputs.focus(focusOnContactInput);
                    $inputs.focusout(focusOnContactInput);

                    // insert privacy policy link
                    labelContent = labelContent.replace('terms &amp; conditions', newLinkContentEN);
                    $conditionLabel.html(labelContent);

                    // setting up the checkbox
                    $checkbox.iCheck({
                        checkboxClass: 'icheckbox icheckbox_minimal-red',
                    });
                    $conditionLabel.click(function () {
                        $checkbox.iCheck('toggle');
                    });


                    // form redirection
                    document.addEventListener('wpcf7mailsent', function (event) {
                        location = 'https://swisstomato.ch/en/ebook-digital-thank-you/';
                    }, false);

                };

            initForm();
        },

        initWhitepaperThankYou = function () {
            var
                // DOM
                $clickHereLink = $('#whitepaper-thank-you-content a'),

                // misc
                url = $clickHereLink.attr('href');

            window.location.href = 'https://swisstomato.ch/whitepaper.php?url=' + url;
        };

    if ($('#whitepaper-page').length !== 0) {
        initWhitepaper();
    }

    if ($('#whitepaper-thank-you-page').length !== 0) {
        initWhitepaperThankYou();
    }

    $heroButton.click(scrollToForm);
});
