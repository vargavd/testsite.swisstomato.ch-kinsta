jQuery(function ($) {
  let
    // DOM
    $bigRedTextSection = $('#big-red-text-section'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($bigRedTextSection, 'seen-first', -50);
      swisstScrollTriggerer($bigRedTextSection, 'seen-second', 100);
    };

  initAnimations();
});