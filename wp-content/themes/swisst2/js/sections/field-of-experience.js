jQuery(function ($) {
  let
    // DOM
    $fieldOfExperienceSection = $('#fields-of-exp-section'),
    $list = $fieldOfExperienceSection.find('.field-of-exp-list'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($list, 'scrolled-to');
      //$list.addClass('no-scroll');
    };

  initAnimations();
});