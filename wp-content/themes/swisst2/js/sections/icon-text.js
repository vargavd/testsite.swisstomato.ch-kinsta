jQuery(function ($) {
  let
    // DOM
    $iconTextSection = $('.icon-text-list-section'),
    $iconTextList = $iconTextSection.find('.icon-text-list'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($iconTextList, 'scrolled-to');
      //$iconTextList.addClass('no-scroll');
    };

  initAnimations();
});