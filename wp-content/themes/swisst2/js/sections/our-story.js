jQuery(function ($) {
  let
    // DOM
    $window = $(window),
    $ourStorySection = $('.our-story-section'),
    $container = $ourStorySection.find('.container'),
    ourStorySectionDOM = $ourStorySection[0],
    $imgWrapper = $ourStorySection.find('.our-story-image'),
    $bigRedStripWrapper = $ourStorySection.find('.big-red-strip-wrapper'),
    $bigRedStrip = $bigRedStripWrapper.find('.big-red-strip'),
    $ourStoryContent = $ourStorySection.find('.our-story-content'),

    // misc
    heightOfViewport = $window.height(),
    sectionTop = $ourStorySection.position().top,
    sectionBottom = sectionTop + $ourStorySection.height(),
    enteredToViewport = false,
    parallaxStarted = false,
    
    // events
    browserScrolled = function () {
      let browserTop = $window.scrollTop();
      let browserBottom = browserTop + heightOfViewport;
      let browserBottomMinusSectionTop = browserBottom - sectionTop;

      if (0 < browserBottomMinusSectionTop && browserBottomMinusSectionTop < 700) {
        let containerTopPadding = browserBottomMinusSectionTop*0.081;

        $bigRedStrip.height(160 - browserBottomMinusSectionTop*0.235);
        $bigRedStripWrapper.height(50 - browserBottomMinusSectionTop*0.76);
        $container.css('padding-top', containerTopPadding);
        $ourStoryContent.css('top', 300 - browserBottomMinusSectionTop*0.48);

        parallaxStarted = true;
      } else {
        if (browserBottomMinusSectionTop > 700) {
          $window.unbind('scroll', browserScrolled);

          if (!parallaxStarted) {
            $bigRedStrip.height(0);
            $bigRedStripWrapper.height(0);
            $container.css('padding-top', 55);
            $ourStoryContent.css('top', -25);
          }
        }
      }

      // console.log(Math.ceil(bottomPositionOfSection), Math.ceil(browserBottomPos));
    };

    // swisstScrollTriggerer($ourStorySection, 'scrolled-to');

    $ourStorySection.addClass('no-scroll');

    if (window.innerWidth < 756) {
      return;  
    }

    // $window.on('scroll', browserScrolled);
    // browserScrolled();
    // $ourStorySection.addClass('no-scroll');
});