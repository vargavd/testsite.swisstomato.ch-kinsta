jQuery(function ($) {
  let
    // DOM
    $processTimingSection = $('.process-timing-section'),
    $list = $processTimingSection.find('.process-timing-steps'),
    $h2 = $processTimingSection.find('h2'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($h2, 'scrolled-to', 450, $list);
      //$list.addClass('no-scroll');
    };

  initAnimations();
});