jQuery(function ($) {
    var $masonryReferenceListSection = $('.masonry-reference-list-section'), $referenceList = $masonryReferenceListSection.find('.masonry-reference-list'), $template = $masonryReferenceListSection.find('.template').first().detach().removeClass('template'), bottomPosOfLastReference = 0, removeAnimationsFromNonExistentReferences = [], initLayout = function () {
        var $references = $referenceList.find('.reference'), referenceListComputedStyle = window.getComputedStyle($referenceList[0]), referenceListGridAutoRowsValue = parseInt(referenceListComputedStyle.getPropertyValue('grid-auto-rows')), referenceListDisplayStyle = referenceListComputedStyle.getPropertyValue('display'), nextFreeRows = [3, 1], mobileCurrentTop = 0, indexOfLastReference = $references.length - 1, alignReferenceToMasonry = function (index, elem) {
            var $reference = $(elem), imgHeight = +$reference.attr('data-height'), rowSpanValue = Math.floor(imgHeight / referenceListGridAutoRowsValue), currentColumn = index % 2;
            if (indexOfLastReference === index) {
                console.log(currentColumn);
                currentColumn = nextFreeRows[0] < nextFreeRows[1] ? 0 : 1;
                console.log(currentColumn);
            }
            if (801 < window.innerWidth && window.innerWidth < 990) {
                rowSpanValue = Math.floor(2 * rowSpanValue / 3);
            }
            if (651 < window.innerWidth && window.innerWidth < 800) {
                rowSpanValue = Math.floor(rowSpanValue / 2);
            }
            $reference.css({
                gridRow: "".concat(nextFreeRows[currentColumn], " / span ").concat(rowSpanValue),
                gridColumnStart: "".concat(currentColumn + 1)
            });
            $reference.attr('data-top', nextFreeRows[currentColumn] * referenceListGridAutoRowsValue);
            nextFreeRows[currentColumn] += rowSpanValue;
            $reference.attr('data-index', index);
        }, alignReferenceToMobile = function (_, elem) {
            var $reference = $(elem), referenceHeight = $reference.height();
            $reference.attr('data-top', mobileCurrentTop - 50);
            mobileCurrentTop += referenceHeight;
        };
        $references.on('click', function (event) { return location.href = $(event.currentTarget).attr('data-url'); });
        if (referenceListDisplayStyle === 'grid') {
            $references.each(alignReferenceToMasonry);
        }
        else {
            $references.each(alignReferenceToMobile);
        }
        $referenceList.css('opacity', 1);
        bottomPosOfLastReference = Math.max.apply(Math, nextFreeRows) * referenceListGridAutoRowsValue;
    }, initFiltering = function () {
        var $references = $referenceList.find('.reference'), $filterSection = $('#filter-section'), $categoryFilters = $filterSection.find('.filter-box'), baseRestUrl = swisstInfos.baseUrl + '/wp-json/wp/v2/reference?_embed&per_page=100&acf_format=standard', currentCategory = location.hash.includes('platform=') ? location.hash.split('platform=')[1] : '', loadReferences = function (categorySlug) {
            var $categoryFilter = categorySlug ?
                $categoryFilters.filter("[data-slug=\"".concat(categorySlug, "\"]")) :
                $categoryFilters.filter(':not([data-slug])'), categoryId = $categoryFilter.attr('data-id'), constructedRestUrl = baseRestUrl;
            $categoryFilters.removeClass('selected');
            $categoryFilter.addClass('selected');
            $filterSection.addClass('loading');
            $masonryReferenceListSection.addClass('loading');
            if (typeof categoryId !== 'undefined') {
                constructedRestUrl += "&reference_category=".concat(categoryId);
                location.hash = "platform=".concat(categorySlug);
            }
            else {
                location.hash = '';
            }
            $.get(constructedRestUrl, {}, function (references) {
                $references.remove();
                references.forEach(function (reference) {
                    var $reference = $template.clone(), imageUrl = "".concat(location.origin, "/wp-content/themes/swisst2/imgs/gray-624x460.jpg"), imageWidth = 624, imageHeight = 460;
                    if (reference.acf.masonry_thumbnail_image) {
                        imageUrl = reference.acf.masonry_thumbnail_image.url;
                        imageWidth = reference.acf.masonry_thumbnail_image.width;
                        imageHeight = reference.acf.masonry_thumbnail_image.height;
                    }
                    if (reference.acf.masonry_black_layer_below_title) {
                        $reference.addClass('black-layer');
                    }
                    if (reference.acf.masonry_black_layer_below_title_mobile) {
                        $reference.addClass('mobile-black-layer');
                    }
                    $reference.attr({
                        'data-width': imageWidth,
                        'data-height': imageHeight,
                        'data-url': reference.link,
                        'data-id': reference.id
                    });
                    $reference.find('h3').text(reference.acf.masonry_reference_title);
                    $reference.find('p').text(reference.acf.masonry_reference_text);
                    $reference.find('img').attr({
                        src: imageUrl,
                        alt: reference.acf.masonry_reference_title
                    });
                    $reference.find('.reference__content__category').text(categorySlug ? $categoryFilter.text().trim() : reference.category_name);
                    $referenceList.append($reference);
                    $references = $referenceList.find('.reference');
                });
                $filterSection.removeClass('loading');
                $masonryReferenceListSection.removeClass('loading');
                initLayout();
                initAnimation();
            });
        }, clickOnFilter = function (event) {
            var $categoryFilter = $(event.currentTarget), categorySlug = $categoryFilter.attr('data-slug');
            if ($filterSection.hasClass('loading')) {
                return;
            }
            loadReferences(categorySlug);
        };
        if ($filterSection.length === 0) {
            return;
        }
        $categoryFilters.on('click', clickOnFilter);
        if (currentCategory) {
            loadReferences(currentCategory);
        }
    }, initAnimation = function () {
        var $references = $referenceList.find('.reference'), $moreReferencesButton = $masonryReferenceListSection.find('.more-references'), animateReference = function (_, referenceDOM) {
            var $reference = $(referenceDOM), top = +$reference.attr('data-top');
            removeAnimationsFromNonExistentReferences.push(swisstScrollTriggerer($referenceList, 'scrolled-to', top, $reference, true, true));
        };
        removeAnimationsFromNonExistentReferences.forEach(function (removeAnim) { return removeAnim(); });
        $references.each(animateReference);
        swisstScrollTriggerer($masonryReferenceListSection, 'scrolled-to', bottomPosOfLastReference + 200, $moreReferencesButton, true);
        $masonryReferenceListSection.addClass('no-scroll');
    };
    initLayout();
    initAnimation();
    initFiltering();
});
//# sourceMappingURL=reference-list-masonry-section.js.map