/// <reference path="../_common.d.ts" />

jQuery(function ($) {
  let
    // global DOM
    $masonryReferenceListSection = $('.masonry-reference-list-section') as JQuery<HTMLDivElement>,
    $referenceList = $masonryReferenceListSection.find('.masonry-reference-list') as JQuery<HTMLDivElement>,
    $template = $masonryReferenceListSection.find('.template').first().detach().removeClass('template') as JQuery<HTMLDivElement>,

    // global variables
    bottomPosOfLastReference = 0,

    // helper funcs
    removeAnimationsFromNonExistentReferences: {():void }[] = [],
    
    // init
    initLayout = function () {
      let
        // DOM
        $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>,

        // style variables
        referenceListComputedStyle = window.getComputedStyle($referenceList[0]),
        referenceListGridAutoRowsValue = parseInt(referenceListComputedStyle.getPropertyValue('grid-auto-rows')),
        referenceListDisplayStyle = referenceListComputedStyle.getPropertyValue('display'),

        // masonry helper variables
        nextFreeRows = [3, 1],
        mobileCurrentTop = 0,
        indexOfLastReference = $references.length -1,
        
        // helper funcs
        alignReferenceToMasonry = (index: number, elem: HTMLElement) => {
          let
            // DOM
            $reference = $(elem),

            // misc
            imgHeight = +$reference.attr('data-height'),
            rowSpanValue = Math.floor(imgHeight / referenceListGridAutoRowsValue),
            currentColumn = index%2;

          // last post should go to the smaller column
          if (indexOfLastReference === index) {
            console.log(currentColumn);
            currentColumn = nextFreeRows[0] < nextFreeRows[1] ? 0 : 1;
            console.log(currentColumn);
          }

          if (801 < window.innerWidth && window.innerWidth < 990) {
            rowSpanValue = Math.floor(2*rowSpanValue/3);
          }

          if (651 < window.innerWidth && window.innerWidth < 800) {
            rowSpanValue = Math.floor(rowSpanValue/2);
          }

          $reference.css({
            // grid-row: grid-row-start / grid-row-end
            gridRow: `${nextFreeRows[currentColumn]} / span ${rowSpanValue}`,
            gridColumnStart: `${currentColumn+1}`
          });

          // the top attr is needed for animation later
          $reference.attr('data-top', nextFreeRows[currentColumn]*referenceListGridAutoRowsValue);

          nextFreeRows[currentColumn] += rowSpanValue;

          $reference.attr('data-index', index);
        },
        alignReferenceToMobile = (_: number, elem: HTMLElement) => {
          let
            // DOM
            $reference = $(elem),
            
            referenceHeight = $reference.height();

          $reference.attr('data-top', mobileCurrentTop - 50);

          mobileCurrentTop += referenceHeight;
        };

      // click event
      $references.on('click', event => location.href = $(event.currentTarget).attr('data-url'));

      // align references into masonry
      if (referenceListDisplayStyle === 'grid') {
        $references.each(alignReferenceToMasonry);
      } else {
        $references.each(alignReferenceToMobile);
      }
      

      // display the list
      $referenceList.css('opacity', 1);

      // set bottom post - for animation
      bottomPosOfLastReference = Math.max(...nextFreeRows)*referenceListGridAutoRowsValue;
    },
    initFiltering = function () {
      let
        // references DOM
        $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>,

        // filter DOM
        $filterSection = $('#filter-section') as JQuery<HTMLDivElement>,
        $categoryFilters = $filterSection.find('.filter-box') as JQuery<HTMLDivElement>,

        // misc
        baseRestUrl = swisstInfos.baseUrl + '/wp-json/wp/v2/reference?_embed&per_page=100&acf_format=standard',
        currentCategory = location.hash.includes('platform=') ? location.hash.split('platform=')[1] : '',

        // events
        loadReferences = (categorySlug: string) => {
          let 
            // DOM
            $categoryFilter = categorySlug ? 
              $categoryFilters.filter(`[data-slug="${categorySlug}"]`) : 
              $categoryFilters.filter(':not([data-slug])'),
    
            // misc
            categoryId = $categoryFilter.attr('data-id'),
            constructedRestUrl = baseRestUrl;
    
    
          // state classes
          $categoryFilters.removeClass('selected');
          $categoryFilter.addClass('selected');
          $filterSection.addClass('loading');
          $masonryReferenceListSection.addClass('loading');

          // construct rest url
          if (typeof categoryId !== 'undefined') {
            constructedRestUrl += `&reference_category=${categoryId}`;
            location.hash = `platform=${categorySlug}`;
          } else {
            location.hash = '';
          }
    
          // ajax call
          $.get(constructedRestUrl, {}, references => {
            // remove references from DOM
            $references.remove();
            
            // add references to DOM
            references.forEach(reference => {
              let 
                // DOM
                $reference = $template.clone(),
    
                // misc
                imageUrl = `${location.origin}/wp-content/themes/swisst2/imgs/gray-624x460.jpg`,
                imageWidth = 624,
                imageHeight = 460;
    
              if (reference.acf.masonry_thumbnail_image) {
                imageUrl = reference.acf.masonry_thumbnail_image.url;
                imageWidth = reference.acf.masonry_thumbnail_image.width;
                imageHeight = reference.acf.masonry_thumbnail_image.height;
              }

              // class - black layer
              if (reference.acf.masonry_black_layer_below_title) {
                $reference.addClass('black-layer');
              }

              // class - mobile black layer
              if (reference.acf.masonry_black_layer_below_title_mobile) {
                $reference.addClass('mobile-black-layer');
              }
    
              // data attributes
              $reference.attr({
                'data-width': imageWidth, 
                'data-height': imageHeight,
                'data-url': reference.link,
                'data-id': reference.id
              });
    
              // title
              $reference.find('h3').text(reference.acf.masonry_reference_title);
    
              // text
              $reference.find('p').text(reference.acf.masonry_reference_text);
    
              // image
              $reference.find('img').attr({
                src: imageUrl,
                alt: reference.acf.masonry_reference_title
              });

              // category
              $reference.find('.reference__content__category').text(categorySlug ? $categoryFilter.text().trim() : reference.category_name);
    
              $referenceList.append($reference);
    
              $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>;
            });
    
            // remove loading status from DOM
            $filterSection.removeClass('loading');
            $masonryReferenceListSection.removeClass('loading');

            // reinit Masonry and animations
            initLayout();
            initAnimation();
          });
        },
        clickOnFilter = (event: JQuery.ClickEvent) => {
          let 
            // DOM
            $categoryFilter = $(event.currentTarget),
    
            // misc
            categorySlug = $categoryFilter.attr('data-slug');
    
          if ($filterSection.hasClass('loading')) {
            return;
          }

          loadReferences(categorySlug);
        };

      if ($filterSection.length === 0) {
        return;
      }

      $categoryFilters.on('click', clickOnFilter);

      if (currentCategory) {
        loadReferences(currentCategory);
      }
    },
    initAnimation = function () {
      let
        // DOM
        $references = $referenceList.find('.reference') as JQuery<HTMLDivElement>,
        $moreReferencesButton = $masonryReferenceListSection.find('.more-references') as JQuery<HTMLButtonElement>,

        // helper funcs
        animateReference = (_, referenceDOM) => {
          let
            // DOM
            $reference = $(referenceDOM),
    
            // misc
            top = +$reference.attr('data-top');
    
          removeAnimationsFromNonExistentReferences.push(swisstScrollTriggerer($referenceList, 'scrolled-to', top, $reference, true, true));
          // $reference.addClass('no-scroll');
        };

      // remove old animations
      removeAnimationsFromNonExistentReferences.forEach(removeAnim => removeAnim());

      // animation
      $references.each(animateReference);

      // animate the button
      swisstScrollTriggerer($masonryReferenceListSection, 'scrolled-to', bottomPosOfLastReference + 200, $moreReferencesButton, true);
      $masonryReferenceListSection.addClass('no-scroll');
    };

  

  initLayout();

  initAnimation();

  initFiltering();

  // setTimeout(() => $('.filter-box:nth-child(2)').trigger('click'));
});