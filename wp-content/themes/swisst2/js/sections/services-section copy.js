jQuery(function ($) {
  let
    // DOM
    $servicesSection2022 = $('#services-section-2022'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($servicesSection2022, 'scrolled-to');
    };

  initAnimations();
});