jQuery(function ($) {
  let
    // DOM
    $whyUsSection = $('#why-us-section'),
    
    // init funcs
    initAnimations = function () {
      swisstScrollTriggerer($whyUsSection, 'scrolled-to');
    };

  initAnimations();
});