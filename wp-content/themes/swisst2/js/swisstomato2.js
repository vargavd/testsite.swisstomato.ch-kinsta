'use strict';

jQuery(function () {

    $('body').addClass('loaded');

    // Moved here from js/navigation JS
    var container, button, menu, lsName = 'Cookie Bar', lsValue = localStorage.getItem(lsName);
	
	if ( lsValue === null ) {
		$('#cookie').addClass('active');
	}

	// if click cookie simply hide it
	$(document).on('click', '.cookie__btn', function () {
		localStorage.setItem(lsName, 1);
		$('#cookie').addClass('hide');
	});

    container = document.getElementById('site-navigation');
    if (!container) {
        return;
    }

    button = document.getElementsByClassName('menu-toggle')[0];
    if ('undefined' === typeof button) {
        return;
    }

    menu = container.getElementsByTagName('ul')[0];

    // Hide menu toggle button if menu is empty and return early.
    if ('undefined' === typeof menu) {
        button.style.display = 'none';
        return;
    }

    menu.setAttribute('aria-expanded', 'false');
    if (-1 === menu.className.indexOf('nav-menu')) {
        menu.className += ' nav-menu';
    }

    button.onclick = function () {
        if (-1 !== container.className.indexOf('toggled')) {
            container.className = container.className.replace(' toggled', '');
            button.className = button.className.replace(' toggled', '');

            $(container).hide(300, function () {
                button.setAttribute('aria-expanded', 'false');
                menu.setAttribute('aria-expanded', 'false');
            });
        } else {
            container.className += ' toggled';
            button.className += ' toggled';

            // $(container).find('ul.sub-menu').show();
            // $(container).find('li.menu-item-has-children').addClass('expanded');

            $(container).show(300, function () {
                button.setAttribute('aria-expanded', 'true');
                menu.setAttribute('aria-expanded', 'true');
            });
        }
    };


    // Moved here from Footer
    $('a[href="#contact-us"]').on('click', function () {
        $(".menu-toggle.toggled").click();
    });
    $(".wpml-ls-current-language a:first").removeAttr("href");


    var
        // general DOM
        $window = $(window),
        $header = $('header#masthead'),
        $footer = $('footer#colophon'),
        $htmlBody = $('html, body'),
        $body = $('body'),

        // header DOM
        $subMenus = $('ul#primary-menu ul'),
        $menuLisWithChildren = $('ul#primary-menu li.menu-item-has-children'),
        $subMenuBC = $('#desktop-sub-menu-background'),

        // contact DOM
        $contactPopupTrigger = $('#contact-popup-trigger'),
        $contactPopupCloseBtn = $('#contact-popup-close'),
        $contactUs = $('#contact-us'),
        $contactInputs = $contactUs.find('input[type=text], input[type=tel], textarea, input[type=email]'),
        $fieldGroups = $('#contact-us .field-group'),
        $toWebArTryIt = $('a[href="#try-it-now"]'),
        $webArTryIt = $("#try-it-now"),

        // misc
        isSafari = Boolean(navigator.userAgent.match(/safari/i)) && !navigator.userAgent.match(/chrome/i) && typeof document.body.style.webkitFilter !== 'undefined' && !window.chrome,

        mobileMaxWidth = 910,
        previousScrollTop,
        responsiveBCEntities = [],
        bcChangesNumber = 0,

        // helper
        focusOnContactInput = function () {
            var
                // DOM
                $input = $(this),
                $fieldGroup = $input.closest('.field-group');

            $fieldGroup.toggleClass('focus');

            if ($fieldGroup.hasClass('filled') && $input.val() === '') {
                $fieldGroup.removeClass('filled');
            } else {
                $fieldGroup.addClass('filled');
            }
        },
        browserScrolled = function () {
            var
                currentScrollTop = $window.scrollTop();

            if ($body.hasClass('error404')) {
                return;
            }


            if (currentScrollTop > 50) {
                $header.addClass('addshadow');
                /*
                if (currentScrollTop < previousScrollTop) {
                    $header.addClass('visible');
                } else {
                    $header.removeClass('visible');
                } */
            } else {
                $header.removeClass('addshadow');
            }
            /*
            else {
                $header.removeClass('fixed visible');
            }
            */
            previousScrollTop = currentScrollTop;
        },
        triggerContactPopup = function () {
            var scrwidth = screen.width;
            if (scrwidth >= 1024) {
                $(window).scrollTop($('#more-posts').offset().top);
            } else {
                setTimeout(function () {
                    $(window).scrollTop($('#contact-us').offset().top);
                }, 500);
            }
        },
        defineSubMenuHeight = function () {
            var
                // DOM
                $subMenu = $(this);

            $subMenu.addClass('leftauto');
            $subMenu.attr('data-height', $subMenu.outerHeight());
            $subMenu.removeClass('leftauto');
        },
        changeResponsiveBCs = function () {
            var
                // misc
                width = $(document).innerWidth();

            if ((bcChangesNumber % 10) !== 0) {
                bcChangesNumber++;
                return;
            }

            bcChangesNumber++;

            if (bcChangesNumber > 10) {
                bcChangesNumber = 1;
            }

            responsiveBCEntities.forEach((entity) => {
                var
                    correctBC = false;

                jQuery.each(entity.breakpoints, (index, bp) => {
                    if (width > bp.breakpointWidth) {
                        return true;
                    }

                    if (entity.$elem.attr('active-breakpoint') == bp.breakpointWidth) {
                        correctBC = true;
                        return false;
                    }

                    correctBC = true;

                    entity.$elem.attr('active-breakpoint', bp.breakpointWidth);
                    entity.$elem.css('background-image', 'url(' + bp.bcImage + ')');

                    return false;
                });

                if (!correctBC && entity.$elem.attr('active-breakpoint') != undefined) {
                    entity.$elem.removeAttr('active-breakpoint');
                    entity.$elem.css('background-image', 'url(' + entity.originalBC + ')');
                }
            });
        },

        // events
        clickOnMobileMenuParentItem = function (e) {
            var
                // DOM
                $a = $(this),
                $parentLi = $a.closest('li'),
                $subMenuUl = $parentLi.find('ul.sub-menu');

            if ($subMenuUl.length === 0) {
                return;
            }

            e.preventDefault();

            if (!$parentLi.hasClass('expanded')) {
                $subMenuUl.show(200, function () {
                    $parentLi.addClass('expanded');
                });
            } else {
                $subMenuUl.hide(200, function () {
                    $parentLi.removeClass('expanded');
                });
            }
        },
        clickOnMobileMenuSubLink = function () {
            var
                // DOM
                $a = $(this);

            $a.addClass('highlighted');
        },
        mouseEnterParentMenuLink = function () {
            var
                // DOM
                $menuLi = $(this),
                $subMenu = $menuLi.find('ul.sub-menu');

            if ($body.hasClass('error404')) {
                return;
            }

            $subMenuBC
                .height($subMenu.attr('data-height'))
                .show();
        },
        mouseLeaveParentMenuLink = function () {
            $subMenuBC.hide();
        },

        init = function () {
            var
                initContactSection = function () {
                    var
                        // DOM
                        $contactLinks = $('a[href="#contact-us"]');

                    $contactInputs.focus(focusOnContactInput);
                    $contactInputs.focusout(focusOnContactInput);

                    if (isSafari) {
                        $fieldGroups.addClass('.safari');
                    }

                    $contactLinks.on('click', function () {
                        $htmlBody.animate({
                            scrollTop: $contactUs.position().top,
                        }, 700, 'swing');
                    });

                    $contactPopupTrigger.on('click', function () {
                        $htmlBody.animate({
                            scrollTop: $htmlBody.height() - $footer.height() - $contactUs.height() - (window.innerWidth < 500 ? 200 : 140),
                        }, 700, 'swing');
                    });

                    $toWebArTryIt.on('click', function (e) {
                        e.preventDefault();

                        $htmlBody.animate({
                            scrollTop: $webArTryIt.position().top,
                        }, 700, 'swing');
                    });

                    $contactPopupCloseBtn.on('click', triggerContactPopup);
                },
                initAwardSection = function () {
                    var
                        // DOM
                        $awardsTopRow = $('#awards.top-row'),
                        $awardsBottomRow = $('#awards.bottom-row');

                    if ($('body').hasClass('home') || $awardsTopRow.length === 0) {
                        // the home award animation is handled by home.js
                        return;
                    }

                    if ((window.matchMedia('(max-width: 911px)').matches === false) && "IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
                        let observer = new IntersectionObserver((entries) => {
                            if (entries[0].isIntersecting) {
                                $(entries[0].target).addClass('triggered');
                            }
                        }, {
                            rootMargin: "0px 0px 50px 0px"
                        });

                        $awardsBottomRow.removeClass('triggered');
                        $awardsTopRow.removeClass('triggered');

                        observer.observe($awardsTopRow[0]);
                        observer.observe($awardsBottomRow[0]);
                    }
                },
                initMenu = function () {
                    if ($window.innerWidth() > mobileMaxWidth) {
                        $subMenuBC.css('top', $header.outerHeight() - $header.css('padding-bottom'));
                        $subMenus.each(defineSubMenuHeight);
                        $menuLisWithChildren.hover(mouseEnterParentMenuLink, mouseLeaveParentMenuLink);

                        return;
                    }

                    $('ul#primary-menu li.menu-item-has-children > a').on('click', clickOnMobileMenuParentItem);
                    $('ul#primary-menu li.menu-item-has-children ul.sub-menu a').on('click', clickOnMobileMenuSubLink);
                },
                initMisc = function () {
                    // making header fixed when scrolling
                    $window.on('scroll', browserScrolled);
                    browserScrolled();
                },
                initResponsiveBCs = function () {
                    var
                        // misc
                        width = $(document).innerWidth(),

                        // DOM
                        $responsiveBcElems = $('[data-responsive-bc-infos]');

                    $responsiveBcElems.each((index, elem) => {
                        var
                            // DOM
                            $elem = $(elem),

                            // misc
                            infosAttr = $elem.attr('data-responsive-bc-infos'),
                            differentBcStrings = infosAttr.split('||'),
                            breakpoints = [],
                            bcImage, breakpointWidth,
                            currentStyle = $elem.css('background-image');

                        differentBcStrings.forEach(bcString => {
                            breakpoints.push({
                                bcImage: bcString.split('|')[0],
                                breakpointWidth: bcString.split('|')[1]
                            });
                        });

                        responsiveBCEntities.push({
                            $elem: $elem,
                            originalBC: currentStyle.replace(/(url\(|\)|")/g, ''),
                            breakpoints: _.sortBy(breakpoints, 'breakpointWidth')
                        });
                    });

                    $window.on('resize', changeResponsiveBCs);

                    changeResponsiveBCs();
                },
                initReadMoreLinks = function () {
                    var
                        // DOM
                        $readMoreLinks = $('a.swisst2-read-more'),

                        // events
                        clickOnReadMore = function () {
                            var
                                // DOM
                                $link = $(this),
                                $text = $link.attr('target-id') ? $($link.attr('target-id')) : $link.next('.swisst2-more-text');

                            $link.toggleClass('opened');
                            $text.toggle(400);
                        };

                    $readMoreLinks.click(clickOnReadMore);
                };

            initContactSection();
            initAwardSection();
            initMenu();
            initMisc();
            initResponsiveBCs();
            initReadMoreLinks();

            previousScrollTop = $window.scrollTop();
        };

    init();

    // AOS.init();
    // const scrollAnimation = sal({
    //   threshold: 1
    // });

    // scrollAnimation.enable();
});


(function (window, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], function ($) {
            return factory(window, $);
        });
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(window, require('jquery'));
    } else {
        window.lity = factory(window, window.jQuery || window.Zepto);
    }
}(typeof window !== "undefined" ? window : this, function (window, $) {
    'use strict';

    var document = window.document;

    var _win = $(window);
    var _deferred = $.Deferred;
    var _html = $('html');
    var _instances = [];

    var _attrAriaHidden = 'aria-hidden';
    var _dataAriaHidden = 'lity-' + _attrAriaHidden;

    var _focusableElementsSelector = 'a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),iframe,object,embed,[contenteditable],[tabindex]:not([tabindex^="-"])';

    var _defaultOptions = {
        esc: true,
        handler: null,
        handlers: {
            image: imageHandler,
            inline: inlineHandler,
            youtube: youtubeHandler,
            vimeo: vimeoHandler,
            googlemaps: googlemapsHandler,
            facebookvideo: facebookvideoHandler,
            iframe: iframeHandler
        },
        template: '<div class="lity" role="dialog" aria-label="Dialog Window (Press escape to close)" tabindex="-1"><div class="lity-wrap" data-lity-close role="document"><div class="lity-loader" aria-hidden="true">Loading...</div><div class="lity-container"><div class="lity-content"></div><button class="lity-close" type="button" aria-label="Close (Press escape to close)" data-lity-close>&times;</button></div></div></div>'
    };

    var _imageRegexp = /(^data:image\/)|(\.(png|jpe?g|gif|svg|webp|bmp|ico|tiff?)(\?\S*)?$)/i;
    var _youtubeRegex = /(youtube(-nocookie)?\.com|youtu\.be)\/(watch\?v=|v\/|u\/|embed\/?)?([\w-]{11})(.*)?/i;
    var _vimeoRegex = /(vimeo(pro)?.com)\/(?:[^\d]+)?(\d+)\??(.*)?$/;
    var _googlemapsRegex = /((maps|www)\.)?google\.([^\/\?]+)\/?((maps\/?)?\?)(.*)/i;
    var _facebookvideoRegex = /(facebook\.com)\/([a-z0-9_-]*)\/videos\/([0-9]*)(.*)?$/i;

    var _transitionEndEvent = (function () {
        var el = document.createElement('div');

        var transEndEventNames = {
            WebkitTransition: 'webkitTransitionEnd',
            MozTransition: 'transitionend',
            OTransition: 'oTransitionEnd otransitionend',
            transition: 'transitionend'
        };

        for (var name in transEndEventNames) {
            if (el.style[name] !== undefined) {
                return transEndEventNames[name];
            }
        }

        return false;
    })();

    function transitionEnd(element) {
        var deferred = _deferred();

        if (!_transitionEndEvent || !element.length) {
            deferred.resolve();
        } else {
            element.one(_transitionEndEvent, deferred.resolve);
            setTimeout(deferred.resolve, 500);
        }

        return deferred.promise();
    }

    function settings(currSettings, key, value) {
        if (arguments.length === 1) {
            return $.extend({}, currSettings);
        }

        if (typeof key === 'string') {
            if (typeof value === 'undefined') {
                return typeof currSettings[key] === 'undefined' ?
                    null :
                    currSettings[key];
            }

            currSettings[key] = value;
        } else {
            $.extend(currSettings, key);
        }

        return this;
    }

    function parseQueryParams(params) {
        var pairs = decodeURI(params.split('#')[0]).split('&');
        var obj = {},
            p;

        for (var i = 0, n = pairs.length; i < n; i++) {
            if (!pairs[i]) {
                continue;
            }

            p = pairs[i].split('=');
            obj[p[0]] = p[1];
        }

        return obj;
    }

    function appendQueryParams(url, params) {
        return url + (url.indexOf('?') > -1 ? '&' : '?') + $.param(params);
    }

    function transferHash(originalUrl, newUrl) {
        var pos = originalUrl.indexOf('#');

        if (-1 === pos) {
            return newUrl;
        }

        if (pos > 0) {
            originalUrl = originalUrl.substr(pos);
        }

        return newUrl + originalUrl;
    }

    function error(msg) {
        return $('<span class="lity-error"></span>').append(msg);
    }

    function imageHandler(target, instance) {
        var desc = (instance.opener() && instance.opener().data('lity-desc')) || 'Image with no description';
        var img = $('<img src="' + target + '" alt="' + desc + '"/>');
        var deferred = _deferred();
        var failed = function () {
            deferred.reject(error('Failed loading image'));
        };

        img
            .on('load', function () {
                if (this.naturalWidth === 0) {
                    return failed();
                }

                deferred.resolve(img);
            })
            .on('error', failed);

        return deferred.promise();
    }

    imageHandler.test = function (target) {
        return _imageRegexp.test(target);
    };

    function inlineHandler(target, instance) {
        var el, placeholder, hasHideClass;

        try {
            el = $(target);
        } catch (e) {
            return false;
        }

        if (!el.length) {
            return false;
        }

        placeholder = $('<i style="display:none !important"></i>');
        hasHideClass = el.hasClass('lity-hide');

        instance
            .element()
            .one('lity:remove', function () {
                placeholder
                    .before(el)
                    .remove();

                if (hasHideClass && !el.closest('.lity-content').length) {
                    el.addClass('lity-hide');
                }
            });

        return el
            .removeClass('lity-hide')
            .after(placeholder);
    }

    function youtubeHandler(target) {
        var matches = _youtubeRegex.exec(target);

        if (!matches) {
            return false;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://www.youtube' + (matches[2] || '') + '.com/embed/' + matches[4],
                    $.extend({
                        autoplay: 1
                    },
                        parseQueryParams(matches[5] || '')
                    )
                )
            )
        );
    }

    function vimeoHandler(target) {
        var matches = _vimeoRegex.exec(target);

        if (!matches) {
            return false;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://player.vimeo.com/video/' + matches[3],
                    $.extend({
                        autoplay: 1
                    },
                        parseQueryParams(matches[4] || '')
                    )
                )
            )
        );
    }

    function facebookvideoHandler(target) {
        var matches = _facebookvideoRegex.exec(target);

        if (!matches) {
            return false;
        }

        if (0 !== target.indexOf('http')) {
            target = 'https:' + target;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://www.facebook.com/plugins/video.php?href=' + target,
                    $.extend({
                        autoplay: 1
                    },
                        parseQueryParams(matches[4] || '')
                    )
                )
            )
        );
    }

    function googlemapsHandler(target) {
        var matches = _googlemapsRegex.exec(target);

        if (!matches) {
            return false;
        }

        return iframeHandler(
            transferHash(
                target,
                appendQueryParams(
                    'https://www.google.' + matches[3] + '/maps?' + matches[6], {
                    output: matches[6].indexOf('layer=c') > 0 ? 'svembed' : 'embed'
                }
                )
            )
        );
    }

    function iframeHandler(target) {
        return '<div class="lity-iframe-container"><iframe frameborder="0" allowfullscreen allow="autoplay; fullscreen" src="' + target + '"/></div>';
    }

    function winHeight() {
        return document.documentElement.clientHeight ?
            document.documentElement.clientHeight :
            Math.round(_win.height());
    }

    function keydown(e) {
        var current = currentInstance();

        if (!current) {
            return;
        }

        // ESC key
        if (e.keyCode === 27 && !!current.options('esc')) {
            current.close();
        }

        // TAB key
        if (e.keyCode === 9) {
            handleTabKey(e, current);
        }
    }

    function handleTabKey(e, instance) {
        var focusableElements = instance.element().find(_focusableElementsSelector);
        var focusedIndex = focusableElements.index(document.activeElement);

        if (e.shiftKey && focusedIndex <= 0) {
            focusableElements.get(focusableElements.length - 1).focus();
            e.preventDefault();
        } else if (!e.shiftKey && focusedIndex === focusableElements.length - 1) {
            focusableElements.get(0).focus();
            e.preventDefault();
        }
    }

    function resize() {
        $.each(_instances, function (i, instance) {
            instance.resize();
        });
    }

    function registerInstance(instanceToRegister) {
        if (1 === _instances.unshift(instanceToRegister)) {
            _html.addClass('lity-active');

            _win
                .on({
                    resize: resize,
                    keydown: keydown
                });
        }

        $('body > *').not(instanceToRegister.element())
            .addClass('lity-hidden')
            .each(function () {
                var el = $(this);

                if (undefined !== el.data(_dataAriaHidden)) {
                    return;
                }

                el.data(_dataAriaHidden, el.attr(_attrAriaHidden) || null);
            })
            .attr(_attrAriaHidden, 'true');
    }

    function removeInstance(instanceToRemove) {
        var show;

        instanceToRemove
            .element()
            .attr(_attrAriaHidden, 'true');

        if (1 === _instances.length) {
            _html.removeClass('lity-active');

            _win
                .off({
                    resize: resize,
                    keydown: keydown
                });
        }

        _instances = $.grep(_instances, function (instance) {
            return instanceToRemove !== instance;
        });

        if (!!_instances.length) {
            show = _instances[0].element();
        } else {
            show = $('.lity-hidden');
        }

        show
            .removeClass('lity-hidden')
            .each(function () {
                var el = $(this),
                    oldAttr = el.data(_dataAriaHidden);

                if (!oldAttr) {
                    el.removeAttr(_attrAriaHidden);
                } else {
                    el.attr(_attrAriaHidden, oldAttr);
                }

                el.removeData(_dataAriaHidden);
            });
    }

    function currentInstance() {
        if (0 === _instances.length) {
            return null;
        }

        return _instances[0];
    }

    function factory(target, instance, handlers, preferredHandler) {
        var handler = 'inline',
            content;

        var currentHandlers = $.extend({}, handlers);

        if (preferredHandler && currentHandlers[preferredHandler]) {
            content = currentHandlers[preferredHandler](target, instance);
            handler = preferredHandler;
        } else {
            // Run inline and iframe handlers after all other handlers
            $.each(['inline', 'iframe'], function (i, name) {
                delete currentHandlers[name];

                currentHandlers[name] = handlers[name];
            });

            $.each(currentHandlers, function (name, currentHandler) {
                // Handler might be "removed" by setting callback to null
                if (!currentHandler) {
                    return true;
                }

                if (
                    currentHandler.test &&
                    !currentHandler.test(target, instance)
                ) {
                    return true;
                }

                content = currentHandler(target, instance);

                if (false !== content) {
                    handler = name;
                    return false;
                }
            });
        }

        return {
            handler: handler,
            content: content || ''
        };
    }

    function Lity(target, options, opener, activeElement) {
        var self = this;
        var result;
        var isReady = false;
        var isClosed = false;
        var element;
        var content;

        options = $.extend({},
            _defaultOptions,
            options
        );

        element = $(options.template);

        // -- API --

        self.element = function () {
            return element;
        };

        self.opener = function () {
            return opener;
        };

        self.options = $.proxy(settings, self, options);
        self.handlers = $.proxy(settings, self, options.handlers);

        self.resize = function () {
            if (!isReady || isClosed) {
                return;
            }

            content
                .css('max-height', winHeight() + 'px')
                .trigger('lity:resize', [self]);
        };

        self.close = function () {
            if (!isReady || isClosed) {
                return;
            }

            isClosed = true;

            removeInstance(self);

            var deferred = _deferred();

            // We return focus only if the current focus is inside this instance
            if (
                activeElement &&
                (
                    document.activeElement === element[0] ||
                    $.contains(element[0], document.activeElement)
                )
            ) {
                try {
                    activeElement.focus();
                } catch (e) {
                    // Ignore exceptions, eg. for SVG elements which can't be
                    // focused in IE11
                }
            }

            content.trigger('lity:close', [self]);

            element
                .removeClass('lity-opened')
                .addClass('lity-closed');

            transitionEnd(content.add(element))
                .always(function () {
                    content.trigger('lity:remove', [self]);
                    element.remove();
                    element = undefined;
                    deferred.resolve();
                });

            return deferred.promise();
        };

        // -- Initialization --

        result = factory(target, self, options.handlers, options.handler);

        element
            .attr(_attrAriaHidden, 'false')
            .addClass('lity-loading lity-opened lity-' + result.handler)
            .appendTo('body')
            .focus()
            .on('click', '[data-lity-close]', function (e) {
                if ($(e.target).is('[data-lity-close]')) {
                    self.close();
                }
            })
            .trigger('lity:open', [self]);

        registerInstance(self);

        $.when(result.content)
            .always(ready);

        function ready(result) {
            content = $(result)
                .css('max-height', winHeight() + 'px');

            element
                .find('.lity-loader')
                .each(function () {
                    var loader = $(this);

                    transitionEnd(loader)
                        .always(function () {
                            loader.remove();
                        });
                });

            element
                .removeClass('lity-loading')
                .find('.lity-content')
                .empty()
                .append(content);

            isReady = true;

            content
                .trigger('lity:ready', [self]);
        }
    }

    function lity(target, options, opener) {
        if (!target.preventDefault) {
            opener = $(opener);
        } else {
            target.preventDefault();
            opener = $(this);
            target = opener.data('lity-target') || opener.attr('href') || opener.attr('src');
        }

        var instance = new Lity(
            target,
            $.extend({},
                opener.data('lity-options') || opener.data('lity'),
                options
            ),
            opener,
            document.activeElement
        );

        if (!target.preventDefault) {
            return instance;
        }
    }

    lity.version = '@VERSION';
    lity.options = $.proxy(settings, lity, _defaultOptions);
    lity.handlers = $.proxy(settings, lity, _defaultOptions.handlers);
    lity.current = currentInstance;

    $(document).on('click.lity', '[data-lity]', lity);

    return lity;
}));