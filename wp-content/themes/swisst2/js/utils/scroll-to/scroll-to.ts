/// <reference path="../common.ts" />

jQuery(function ($) {
  let
    // DOM
    $htmlBody = $('html,body'),
    $mainHeader = $('#masthead')!,
    $scrollToLinks = $('[data-scroll-to]'),

    // events
    clickOnScrollToLink = function (this: any) {
      var
        // DOM
        $link = $(this) as JQuery<HTMLElement>,
        $target: JQuery<HTMLElement> | null,

        // misc
        hashLink = $link.attr('data-scroll-to')! as string;

      hashLink = hashLink.indexOf('#') === -1 ? `#${hashLink}` : hashLink;

      $target = $(hashLink);

      $htmlBody.animate({
        scrollTop: $target.position().top - $mainHeader.height()!,
      }, 700, 'swing');
    };

  $scrollToLinks.on('click', clickOnScrollToLink);
});