<?php
    $ID = get_the_ID();

    $categories = get_the_category();

    $wp_post_thumbnail = get_the_post_thumbnail_url($ID, 'full');

    global $first_post;
    if ($first_post) {
        $first_post_thumbnail = get_field('first_post_thumbnail', $ID);

        if (!empty($first_post_thumbnail)) {
            $postStyle = "background-image: url(" . $first_post_thumbnail . ");";
        } else {
            $postStyle = "background-image: url(" . $wp_post_thumbnail . ");";
        }
        
        $pimgStyle = "";
    } else {
        $post_thumbnail = get_field('post_thumbnail', $ID);

        if (!empty($post_thumbnail)) {
            $pimgStyle = "background-image: url(" . $post_thumbnail . ");";
        } else {
            $pimgStyle = "background-image: url(" . $wp_post_thumbnail . ");";
        }

        $postStyle = "";
    }
?>

<div class="post" style="<?=$postStyle?>">
    <a href="<?=get_permalink()?>" class="post-image" style="<?=$pimgStyle?>">
    </a>
    <div class="post-content">
        <div class="post-categories">
            <?php foreach ($categories as $category): ?>
                <a href="<?=get_category_link($category)?>">
                    <?=$category->name?>
                </a>
            <?php endforeach; ?>
        </div>
        <h2 class="post-title">
            <a href="<?=get_permalink()?>">
                <?php the_title(); ?>
            </a>
        </h2>
        <div class="post-date"><?=get_the_date("F d, o")?></div>
        <div class="post-text"><?=the_excerpt()?></div>
    </div>
</div>
