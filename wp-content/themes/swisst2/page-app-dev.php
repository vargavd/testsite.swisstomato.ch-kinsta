<?php

/**
 * Template Name: App Dev
 *
 * Template for the app dev page.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

<div id="app-dev-page" class="swisst2-page">

  <!-- <div class="container center-1117">
    <?php //pr1($_SERVER['HTTP_USER_AGENT']); ?>
  </div> -->

    <?php 
        $title = get_the_title();
        $content = apply_filters('the_content', $post->post_content);
        $awards_img = get_field('title_awards_image');

        if (is_array($awards_img)) {
            $awards_img_url = (is_array($awards_img) ? $awards_img['url'] : '');
        } else  if (is_integer(intval($awards_img)) && (intval($awards_img) > 1)) {
            $awards_img_url = wp_get_attachment_image_url($awards_img, 'full');
        } else {
            $awards_img_url = $awards_img;
        }

        
        $awards_img_alt = 'Introduction Awards Image';

        intro_text_awards_func_2022($title, $content, $awards_img_url, $awards_img_alt, 'version-2022'); 
    ?>

    <?php services_func_2022(get_field('app_development_services', $ID), 'version-2022'); ?>

    <?php big_red_text_func(get_field('big_red_text', $ID)); ?>

    <?php 
    our_story_func(
      get_field('our_story_2022feb_title', $ID), 
      get_field('our_story_2022feb_text', $ID), 
      get_field('our_story_founders_image', $ID),
      get_field('our_story_2022feb_founder_1_title', $ID),
      get_field('our_story_2022feb_founder_1_text', $ID),
      get_field('our_story_2022feb_founder_2_title', $ID),
      get_field('our_story_2022feb_founder_2_text', $ID)
    ); ?>

  <?php
    awards_2022feb_func(
      get_field('awards_2022feb_title', $ID), 
      get_field('awards_2022feb_text', $ID),
      get_field('awards_2022feb_awards', $ID),
      get_field('awards_2022feb_background_image', $ID)
    );
  ?>

  <?php include "global-templates/clients.php"; ?>

  <?php
    $category_slug = !empty(get_field('references_section_button_category', $ID)) ? get_field('references_section_button_category', $ID)->slug : '';
    reference_list_masonry_section(
      get_field('references_section_title', $ID), 
      get_field('references_section_references', $ID),
      $category_slug,
      get_field('references_section_button_title', $ID)
    );
  ?>

  <?php why_us_our_story_section_func(); ?>

  <div id="image-text-row-1" class="image-text-row image-text container zoom-in-on-scroll">
		<div class="column image-column">
			<?= custom_image_size_for_acf_img_array(get_field('agency_left_image', $ID)) ?>
		</div>
		<div class="column text-column">
			<h2><?= get_field('agency_title', $ID) ?></h2>
			<?= get_field('agency_text', $ID) ?>
		</div>
	</div>

	<div id="image-text-row-2" class="image-text-row text-image container zoom-in-on-scroll">
		<div class="column text-column">
			<h2><?= get_field('app_development_title', $ID) ?></h2>
			<?= get_field('app_development_text', $ID) ?>
		</div>
		<div class="column image-column">
			<?= custom_image_size_for_acf_img_array(get_field('app_development_image', $ID)) ?>
		</div>
	</div>

	<div id="web-dev-section" class="container text-center">
		<p>
			<?= get_field('web_development_text', $ID) ?>
		</p>
		<a href="<?= get_field('web_development_button_link') ?>" class="button-red">
			<i class="fas fa-angle-right"></i>
			<?= get_field('web_development_button_text') ?>
		</a>
	</div>

	<?php
	  field_of_experience_section($ID);
	?>

	<div id="appdev-process-timing-section" class="process-timing-section container">
		<?php
		  $process_timing_section = get_field('process_timing_section', $ID);
		?>
		<h2><?= $process_timing_section['title'] ?></h2>

		<div id="pt-section-text">
			<?= $process_timing_section['text'] ?>
		</div>

    <div id="appdev-process-timing-log" class="container center-1117">
      
    </div>

		<div id="process-timing-steps" class="process-timing-steps">
			<?php
        $lang = get_bloginfo('language');
        $read_more_text = 'Read more';
        $less_text = 'Less';

        if ($lang === 'fr-FR') {
          $read_more_text = 'En lire plus';
          $less_text = 'Moins';
        }

        if ($lang === 'de-DE') {
          $read_more_text = 'Weiterlesen';
          $less_text = 'Weniger';
        }
        
        for ($i = 0; $i < sizeof($process_timing_section['steps']); $i++) :
          $step = $process_timing_section['steps'][$i];
			?>
				<div class="step">
					<div class="step-number"><?= ($i + 1) ?>.</div>

					<h3><?= $step['title'] ?></h3>

					<div class="step-timing">
						<?= $step['timing'] ?>
					</div>

					<div class="step-text">
						<?= $step['text'] ?>
					</div>

					<a class="step-read-more" data-read-more-text="<?=$read_more_text?>" data-less-text="<?=$less_text?>">
            <span class="read-more"><?=$read_more_text?></span>
            <span class="less"><?=$less_text?></span>
						<i class="fas fa-angle-down"></i>
					</a>
				</div>
			<?php endfor; ?>
		</div>
	</div>

	<?php
	  big_bc_more_text_section(get_field('how_much_cost_section', $ID));
	?>

	<div id="app-hours-complexity-section">
		<?php
		  $ahc_section = get_field('app_hours_complexity_section', $ID);
		?>
		<h2><?= $ahc_section['title'] ?></h2>

		<div id="ahc-text">
			<?= $ahc_section['text'] ?>
		</div>

		<div id="ahc-cards">
			<?php
			for ($i = 0; $i < sizeof($ahc_section['cards']); $i++) :
			?>
				<div class="ahc-card">
					<div class="progress-bar-wrapper">
						<div class="progress-bar-bc">
							<div class="progress-bar"></div>
						</div>
					</div>
					<div class="ahc-card-text">
						<h3><?= $ahc_section['cards'][$i]['title'] ?></h3>
						<?= $ahc_section['cards'][$i]['text'] ?>
					</div>
				</div>
			<?php endfor; ?>
		</div>
	</div>

	<div id="lets-work-section">
		<?php
		$lets_work_section = get_field('lets_work_section', $ID);
		?>
		<div class="container">
			<h2><?= $lets_work_section['title'] ?></h2>

			<?= $lets_work_section['text'] ?>
		</div>
	</div>

</div>

<?php
get_footer();
