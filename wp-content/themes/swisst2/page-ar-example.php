<?php
/**
 * Template Name: AR Example
 *
 * Template for the AR Example Page template.
 *
 * @package Swisstomato2
 */

get_header();
?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main center-1117">

		<h1><?php the_title(); ?></h1>

        <script type=module src=https://unpkg.com/@google/model-viewer/dist/model-viewer.js></script>
        <model-viewer src="https://swisstomato.ch/wp-content/themes/swisst2/imgs/delonghi/Delonghi-machine.glb" ios-src="https://swisstomato.ch/wp-content/themes/swisst2/imgs/delonghi/Delonghi-machine.usdz" alt="Chair" quick-look-browsers="safari chrome" auto-rotate camera-controls ar magic-leap></model-viewer>

        <!-- <script
      data-token="demoaccount"
      src="https://view.seekxr.com/demoaccount/seek.min.js"></script>
    <a data-seek-key="Dinamica_Plus">
      <img alt="Dinamica Plus ECAM370.85.SB" style="width:100%" src="https://view.seekxr.com/demoaccount/Dinamica_Plus/thumbnail">
    </a> -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
