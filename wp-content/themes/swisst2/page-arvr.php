<?php
/**
 * Template Name: ARVR Page
 *
 * Template for the ARVR Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="arvr-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?= get_the_post_thumbnail_url($ID, 'full') ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div id="why-section" class="container">
            <h2><?=get_field('why_title', $ID)?></h2>
            <p><?=get_field('why_text', $ID)?></p>
        </div>

        <div id="ar-apps-section" class="container p-68" style="background-image: url(<?=get_field('ar_apps_background_image', $ID)?>);">
            <div id="ar-apps-content">
                <h2><?=get_field('ar_apps_title', $ID)?></h2>
				<?= custom_image_size(get_field('ar_apps_mobile_image', $ID), 'full', 'AR Apps Mobile', 'ar-apps-mobile-image') ?>
                <?=get_field('ar_apps_text', $ID)?>
            </div>
        </div>

        <div id="vr-apps-section-1" class="container">
            <div id="vr-apps-left-img-wrapper">
				<?= custom_image_size(get_field('vr_apps_img_left', $ID), 'full',get_field('vr_apps_title', $ID)) ?>
            </div>
            <div id="vr-apps-right-content">
                <h2><?=get_field('vr_apps_title', $ID)?></h2>
				<?= custom_image_size(get_field('vr_apps_img_left', $ID), 'full',get_field('vr_apps_title', $ID), 'vr-apps-mobile-image') ?>
                <?=get_field('vr_apps_text_1', $ID)?>
            </div>
        </div>

        <div id="vr-apps-section-2" class="container">
            <div id="vr-apps-left-content">
				<?= custom_image_size(get_field('vr_apps_img_right_copy', $ID), 'full',get_field('vr_apps_title', $ID)) ?>
                <?=get_field('vr_apps_text_2', $ID)?>
            </div>
            <div id="vr-apps-right-img-wrapper" style="background-image: url(<?=get_field('vr_apps_img_right_copy', $ID)?>);">
            </div>
        </div>

        <div id="fields-title-section" class="container">
            <h2>
                <?=get_field('fields_title', $ID)?>
            </h2>
        </div>

        <div class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('fields_left_image', $ID)?>);">
            </div>
            <div class="column text-column">
                <h2><?=get_field('fields_right_title', $ID)?></h2>
                <?=get_field('fields_right_text', $ID)?>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('fields_left_title', $ID)?></h2>
                <?=get_field('fields_left_text', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('fields_right_image', $ID)?>);">                
            </div>
        </div>

        <div id="fields-of-exp-section" class="container">
            <h2><?=get_field('ar_title', $ID)?></h2>
            
            <div class="field-of-exp-list no-scroll">
                <?php foreach (get_field('ar_solutions', $ID) as $field): ?>
                    <div class="field">
                        <div class="img-wrapper">
							<?= custom_image_size($field['image'], 'full',$field['text']) ?>
                        </div>
                        <p>
                            <?=$field['text']?>
                        </p>
                    </div>
                <?php endforeach; ?>
            </div>

            <h2><?=get_field('vr_title', $ID)?></h2>

            <div class="field-of-exp-list no-scroll">
                <?php foreach (get_field('vr_solutions', $ID) as $field): ?>
                    <div class="field">
                        <div class="img-wrapper">
							<?= custom_image_size($field['image'], 'full',$field['text']) ?>
                        </div>
                        <p>
                            <?=$field['text']?>
                        </p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div id="arvr-site-section" class="container text-center">
            <?=get_field('arvr_site_section_text', $ID)?>
            <a href="<?=get_field('arvr_site_link_url', $ID)?>" class="button-red more-references" target="_blank">
                <i class="fas fa-angle-right"></i>
                <?=get_field('arvr_site_link_text', $ID)?>
            </a>
        </div>

        <?php
            // reference_list_standard($ID);
        ?>

        <?php
            // process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
