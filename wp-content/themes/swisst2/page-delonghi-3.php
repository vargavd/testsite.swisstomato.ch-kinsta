<?php
/**
 * Template Name: DeLonghi 3 Full Page
 *
 * Template for the DeLonghi 3 Full Page template.
 *
 * @package Swisstomato2
 */

/* We don't need any menu, header or footer for this page, so it has a custom header and footer */

$delonghi_css_path = 'https://swisstomato.ch/wp-content/themes/swisst2/css/pages/delonghi3.css';
$delonghi_css_path .= '?ver=' . filemtime(get_stylesheet_directory() . '/css/pages/delonghi3.css');

$delonghi_js_path = 'https://swisstomato.ch/wp-content/themes/swisst2/js/pages/delonghi.js';
$delonghi_js_path .= '?ver=' . filemtime(get_stylesheet_directory() . '/js/pages/delonghi.js');

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title><?php the_title(); ?></title>

    <link rel='stylesheet' id='swisst-delonghi-css'  href='<?=$delonghi_css_path?>' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome'  href='https://swisstomato.ch/wp-content/themes/swisst2/css/fonts/font-awesome/css/all.min.css' type='text/css' media='all' />
</head>

<body>

<div class="hfeed site" id="page">


<!-- Actual page template from here -->
<?php
    $ID = get_the_ID();
?>
<div id="delonghi-page">
    <header>
        <img loading="lazy" src="<?=get_stylesheet_directory_uri()?>/imgs/delonghi/DeLonghi-logo.png" alt="logo" />

        <ul id="menu">
            <li>
                <a href="#">Coffee</a>
            </li>
            <li>
                <a href="#">Kitchen</a>
            </li>
            <li>
                <a href="#">Comfort</a>
            </li>
        </ul>
    </header>

    <div id="product">

        <div id="product-mode-viewer-wrapper">
            <h1><?php the_title(); ?></h1>

            <model-viewer src="https://swisstomato.ch/wp-content/themes/swisst2/imgs/delonghi/Delonghi-machine.glb" ios-src="https://swisstomato.ch/wp-content/themes/swisst2/imgs/delonghi/Delonghi-machine.usdz" alt="Delonghi" quick-look-browsers="safari chrome" auto-rotate camera-controls ar magic-leap> 
                <button slot="ar-button" style="background-color: #58B0E2; border-radius: 4px; border: none; position: absolute; left: 50%; margin-left: -100px; width: 200px; padding-top: 10px; padding-bottom: 10px; border-radius: 23px; font-weight: bold; color: white; font-size: 14px; outline: none !important; border: none !important;">View in your space</button> 
            </model-viewer>
        </div>

        <div id="product-content">
            <h1><?php the_title(); ?></h1>

            <?php echo apply_filters('the_content', $post->post_content); ?>

            <div id="price">£944.99</div>

            <div id="open-qr-popup-wrapper">
                <a class="view-button" id="open-qr-popup">
                    View in 3D
                </a>
            </div>
        </div>
    </div>
</div>

<div id="qr-popup-background">
    <div id="qr-popup">
        <div id="qr-code-wrapper">
            <div id="qr-code-text">
                <h2>How to View in Augmented Reality</h2>

                <p>Scan this QR code with your phone to view the object in your space.</p>
            </div>
    
            <img loading="lazy" src="<?=get_stylesheet_directory_uri()?>/imgs/delonghi/delonghi-ar-qr-code.png" alt="QR code" />
        </div>

        <button class="close-button"> Close </button>
    </div>
</div>


<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.js"></script>
<script src="https://swisstomato.ch/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script src="<?=$delonghi_js_path?>"></script>

<!-- Custom footer from here -->

</div><!-- #page we need this extra closing tag here -->
</body>

</html>


