<?php
/**
 * Template Name: DeLonghi AR Full Page
 *
 * Template for the DeLonghi AR Full Page template.
 *
 * @package Swisstomato2
 */

/* We don't need any menu, header or footer for this page, so it has a custom header and footer */

$delonghi_css_path = 'https://swisstomato.ch/wp-content/themes/swisst2/css/pages/delonghi-ar.css';
$delonghi_css_path .= '?ver=' . filemtime(get_stylesheet_directory() . '/css/pages/delonghi-ar.css');

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title><?php the_title(); ?></title>

    <link rel='stylesheet' id='swisst-delonghi-css'  href='<?=$delonghi_css_path?>' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome'  href='https://swisstomato.ch/wp-content/themes/swisst2/css/fonts/font-awesome/css/all.min.css' type='text/css' media='all' />
</head>

<body>

<div class="hfeed site" id="page">


<!-- Actual page template from here -->
<?php
    $ID = get_the_ID();
?>
<div id="delonghi-ar-page">
    <header>
        <img loading="lazy" src="<?=get_stylesheet_directory_uri()?>/imgs/delonghi/DeLonghi-logo.png" alt="logo" />

        <ul id="menu">
            <li>
                <a href="#">Coffee</a>
            </li>
            <li>
                <a href="#">Kitchen</a>
            </li>
            <li>
                <a href="#">Comfort</a>
            </li>
        </ul>
    </header>

    <div id="product">
        <div id="product-image-wrapper">
            <img loading="lazy" src="<?=get_stylesheet_directory_uri()?>/imgs/delonghi/Product-img.png" alt="Product Image" />
        </div>

        <div id="product-content">
            <h1><?php the_title(); ?></h1>

            <img loading="lazy" class="mobile" src="<?=get_stylesheet_directory_uri()?>/imgs/delonghi/Product-img.png" alt="logo" />

            <?php echo apply_filters('the_content', $post->post_content); ?>

            <div id="price">£944.99</div>

            <div id="view-in-ar-wrapper">
                <script data-token="demoaccount" src="https://view.seekxr.com/demoaccount/seek.min.js"></script>
                <a class="view-button" data-seek-key="Dinamica_Plus">
                    View in 3D
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Custom footer from here -->

</div><!-- #page we need this extra closing tag here -->
</body>

</html>


