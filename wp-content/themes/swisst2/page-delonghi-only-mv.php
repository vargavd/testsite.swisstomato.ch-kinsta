<?php
/**
 * Template Name: DeLonghi MV Full Page
 *
 * Template for the DeLonghi MV Full Page template.
 *
 * @package Swisstomato2
 */

/* We don't need any menu, header or footer for this page, so it has a custom header and footer */

$delonghi_css_path = 'https://swisstomato.ch/wp-content/themes/swisst2/css/pages/delonghi.css';
$delonghi_css_path .= '?ver=' . filemtime(get_stylesheet_directory() . '/css/pages/delonghi.css');

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title><?php the_title(); ?></title>

    <link rel='stylesheet' id='swisst-delonghi-css'  href='<?=$delonghi_css_path?>' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome'  href='https://swisstomato.ch/wp-content/themes/swisst2/css/fonts/font-awesome/css/all.min.css' type='text/css' media='all' />

    <!-- <style>
        .intent-link {
            display: none;
        }
    </style> -->
</head>

<?php
    $ID = get_the_ID();

    $glb_file = get_stylesheet_directory_uri() . "/imgs/delonghi/Delonghi-machine.glb";
    $usdz_file = get_stylesheet_directory_uri() . "/imgs/delonghi/Delonghi-machine.usdz";
?>

<body>

<div class="hfeed site" id="page">

<!-- Actual page template from here -->
<script type="module" src="<?=get_stylesheet_directory_uri()?>/js/model-viewer/model-viewer.js"></script>
<div id="delonghi-mv-page">
    <model-viewer id="model-viewer" src="<?=$glb_file?>" ios-src="<?=$usdz_file?>" alt="Swiss Tomato" auto-rotate camera-controls ar magic-leap>
        <button id="mv-ar-button" slot="ar-button" style="background-color: #58B0E2; border-radius: 4px; border: none; position: absolute; bottom: 0; left: 50%; margin-left: -100px; width: 200px; padding-top: 10px; padding-bottom: 10px; border-radius: 23px; font-weight: bold; color: white; font-size: 14px; outline: none !important; border: none !important;">View in your space</button>
    </model-viewer>
</div>

<script src="https://swisstomato.ch/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script src="https://swisstomato.ch/wp-content/themes/swisst2/js/platform-detection/platform.js"></script>
<script>
    osString = platform.os.toString().toLowerCase();
    browserString = platform.name.toLowerCase();

    var times = 0;

    if (osString.indexOf('android') !== -1) {
        
        // ON ANDROID
        window.location.href = "intent://arvr.google.com/scene-viewer/1.0?file=<?=$glb_file?>&mode=ar_preferred#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=https://swisstomato.ch/en/delonghi;end;";

    } else {

        // Desktop or iOS
        var modelViewer = document.getElementById('model-viewer');
        modelViewer.addEventListener('load', function () { 
            modelViewer.activateAR(); 
        });

        document.onvisibilitychange = function(e) { 
            times++;

            if (times > 1) {
                window.location.href = "https://swisstomato.ch/en/delonghi";
            }
        };

        // setTimeout(function () {
        //     jQuery('#delonghi-mv-page').css('visibility', 'visible');
        // }, 2000);
    }

    // else if (osString.indexOf('ios') !== -1 && browserString.indexOf('chrome') === -1) {
        
    //     // ON IOS (not chrome)
    //     window.location.href = "<?=$usdz_file?>";

    // }

    // setTimeout(function () {
    //     // var modelViewer = document.getElementById('model-viewer');
    //     // modelViewer.activateAR();
    //     var clickTarget = document.getElementById("model-viewer");
    //     var fakeMouseEvent = document.createEvent('MouseEvents');
    //     fakeMouseEvent.initMouseEvent("click", true, true, window,
    //         0, 0, 0, 20, 10, false, false, false, false, 0, null);

    //     clickTarget.dispatchEvent(fakeMouseEvent);
    // }, 2000);
</script>


<!-- Custom footer from here -->

</div><!-- #page we need this extra closing tag here -->
</body>

</html>


