<?php
/**
 * Template Name: Digital Marketing Redesign
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="digital-marketing-redesign-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
            <?php echo apply_filters('the_content', $post->post_content); ?>
			<?= custom_image_size(get_the_post_thumbnail_url($ID, 'full'), 'full', 'Intro Image') ?>
        </div>

        <div id="mission-section" class="container">
            <img loading="lazy" class="tomato" src="<?=get_stylesheet_directory_uri()?>/imgs/home/parallax/tomato.png" alt="Tomato">
            <img loading="lazy" class="tomato" src="<?=get_stylesheet_directory_uri()?>/imgs/home/parallax/tomato.png" alt="Tomato">
            <h2><?=get_field('mission_title', $ID)?></h2>
            <p><?=get_field('mission_text', $ID)?></p>
        </div>

        <div id="services-section" class="container ">
            <h2><?=get_field('services_title', $ID)?></h2>
            <p><?=get_field('services_text', $ID)?></p>

            <div id="services-list">
                <?php
                    $services = get_field('services', $ID);
                    foreach ($services as $service):
                ?>
                    <div class="service">
						<?= custom_image_size($service['icon'], 'full', $service['title']) ?>
                        <p><?=$service['title']?></p>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <div id="references-section" class="container">
            <h2><?=get_field('references_title', $ID)?></h2>
            <p id="references-text"><?=get_field('references_text', $ID)?></p>

            <div id="references-list">
                <?php
                    $references = get_field('references', $ID);
                    foreach ($references as $reference):
                ?>
                    <div class="reference">
                        <a href="<?=get_permalink($reference['reference'])?>" target="_blank">
						<?= custom_image_size($reference['image'], 'full', $reference['reference']->post_title) ?></a>
                        <h3>
                            <a href="<?=get_permalink($reference['reference'])?>"><?=$reference['reference']->post_title?></a>
                        </h3>
                        <p><?=$reference['text']?></p>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <div id="lightbulb-section" class="container" style="background-image: url(<?=get_field('lightbulb_section_background', $ID)?>);">
            <div id="lightbulb-text">
                <?=get_field('lightbulb_section_text', $ID)?>
            </div>
        </div>

        <div id="how-we-work-section" class="container">
            <h2><?=get_field('how_we_work_section_title', $ID)?></h2>

            <div class="how-we-work-list">
                <?php
                    $services = get_field('how_we_work_steps', $ID);
                    for ($i = 0; $i < sizeof($services) && $i < 4; $i++):
                ?>
                    <div class="how-we-work-item">
                        <div class="pink-bc">
                            <div class="number"> <?=$i+1?> </div>
                            <h3><?=$services[$i]['title']?></h3>
                            <?=$services[$i]['text']?>
                        </div>
                        <span class="ball"></span>
                    </div>
                <?php endfor; ?>
            </div>

            <div class="how-we-work-lines">
                <div class="top-line"></div>
                <div class="top-line bottom-line"></div>
                <div class="top-line bottom-line"></div>
                <div class="right-round-line"></div>
            </div>

            <div class="how-we-work-list reversed">
                <?php
                    $services = get_field('how_we_work_steps', $ID);
                    for ($i = 4; $i < sizeof($services) && $i < 8; $i++):
                ?>
                    <div class="how-we-work-item">
                        <div class="pink-bc">
                            <div class="number"> <?=$i+1?> </div>
                            <h3><?=$services[$i]['title']?></h3>
                            <?=$services[$i]['text']?>
                        </div>
                        <span class="ball"></span>
                    </div>
                <?php endfor; ?>
            </div>

            <a href="#contact-us" class="button-red">
                <i class="fas fa-angle-right"></i>
                <?=get_field('how_we_work_button_text', $ID)?>
            </a>
        </div>

        <div id="packages-section" class="container">
            <h2><?=get_field('packages_title', $ID)?></h2>
            <div id="packages-text"><?=get_field('packages_text', $ID)?></div>

            <div id="packages-list">
                <?php
                    $packages = get_field('packages', $ID);
                    $num_of_packages = sizeof($packages);

                    for ($i = 0; $i < sizeof($packages); $i++):
                        $package = $packages[$i];
                        $percentage = floor(100*(($i+1)/$num_of_packages));

                        $price_range_style = "width: $percentage%; background: linear-gradient(to right, #ffffff 0%,#F2000C 80px,#F2000C 100%);";
                ?>
                    <div class="package <?=($package['recommended'] ? 'recommended' : '')?>">
                        <?php if ($package['recommended']): ?>
                            <img src="<?=get_field('reference_badge_image', $ID)?>" class="recommended-strip" />
                        <?php endif; ?>

                        <h3><?=$package['title']?></h3>

                        <div class="package-price-bar">
                            <div class="price-range" style="<?=$price_range_style?>">
                                <?php for ($j = -1; $j < $i; $j++): ?>
                                    <div class="dollar-wrapper"><i class="fas fa-dollar-sign"></i></div>
                                <?php endfor; ?>
                            </div>
                        </div>

                        <ul class="points">
                            <?php foreach ($package['points'] as $point): ?>
                                <li class="point <?=$point['included'] ? 'included' : 'not-included'?>">
                                    <div><?=$point['title']?></div>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                        <a href="#contact-us" class="button-red">
                            <i class="fas fa-angle-right"></i>
                            <?=$package['button_text']?>
                        </a>
                    </div>

                <?php
                    endfor;
                ?>
            </div>
        </div>

	</div>

<?php
get_footer();
