<?php
/**
 * Template Name: Digital Marketing Strategy
 *
 * Template for the Digital Marketing Strategy Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="digital-marketing-strategy-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="full-width" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div class="container">
                    <div id="hero-content">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="below-hero-section" class="container">
            <?=get_field('heading_text', $ID)?>
        </div>

        <div class="container vertical-left-right-numbered-list">
            <div class="steps">
                <?php 
                    $steps = get_field('process_numbered_list', $ID);

                    foreach ($steps as $step):
                ?>
                    <div class="step">
                        <div class="logo-and-number">
							<?= custom_image_size($step['logo'], 'full', $step['title']) ?>
                            <div class="number"><?=$step['number']?></div>
                        </div>
                        <div class="text-and-title">
                            <h3><?=$step['title']?></h3>
                            <div class="text">
                                <?=$step['text']?>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

        <div class="pink-p-button-section container">
             <p>
                <?=get_field('get_a_quote_text', $ID)?>
            </p>
            <a href="#contact-us" class="button-red">
                <i class="fas fa-angle-right"></i>
                <?=get_field('get_a_quote_button_text', $ID)?>
            </a>           
        </div>
        
        <?php
            digital_marketing_internal_links($ID);
        ?>

	</div>

<?php
get_footer();
