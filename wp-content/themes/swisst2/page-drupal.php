<?php
/**
 * Template Name: Drupal Page
 *
 * Template for the Drupal Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="drupal-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div id="section-1" class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_1_title', $ID)?></h2>
				<?= custom_image_size(get_field('section_1_mobile_image', $ID), 'full', get_field('section_1_title', $ID), 'mobile') ?>
                <?=get_field('section_1_text', $ID)?>
            </div>
        </div>

        <div id="section-2" class="container image-text-row text-image">
            <div class="column text-column">
				<?= custom_image_size(get_field('section_2_mobile_image', $ID), 'full', 'Outstanding Performance', 'mobile') ?>
                <?=get_field('section_2_text', $ID)?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_2_image', $ID)?>);"></div>
        </div>

        <div id="section-3" class="container image-text-row image-text">
            <?php
                $section_3_img = get_field('section_3_image', $ID);

                if (is_integer($section_3_img) || intval($section_3_img) > 0) {
                    $section_3_img = wp_get_attachment_image_src(intval($section_3_img), $size)[0];
                }
            ?>
            <div class="column image-column bc-img-center" style="background-image: url(<?=$section_3_img?>);"></div>
            <div class="column text-column">
               <!--  <h2><?=get_field('section_3_title', $ID)?></h2> -->
			   <?= custom_image_size(get_field('section_3_mobile_image', $ID), 'full', 'Drupal Security', 'mobile') ?>
               <?=get_field('section_3_text', $ID)?>
            </div>
        </div>

        <div id="why-pick-us-title" class="container">
            <h2>
                <?=get_field('why_pick_us_title', $ID)?>
            </h2>
        </div>

        <?php
            $checkList = get_field('check_list', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <div id="section-4" class="container image-text-row text-image">
            <div class="column text-column">
                <h2><?=get_field('section_4_title', $ID)?></h2>
				<?= custom_image_size(get_field('section_4_image', $ID), 'full', get_field('section_4_title', $ID), 'mobile') ?>
                <?=get_field('section_4_text', $ID)?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_4_image', $ID)?>);"></div>
        </div>

        <div id="section-5" class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_5_image', $ID)?>);"></div>
            <div class="column text-column">
                <?=get_field('section_5_text', $ID)?>
            </div>
        </div>

        <?php
          $category_slug = !empty(get_field('references_section_button_category', $ID)) ? get_field('references_section_button_category', $ID)->slug : '';
          reference_list_masonry_section(
            get_field('references_section_title', $ID), 
            get_field('references_section_references', $ID),
            $category_slug,
            get_field('references_section_button_title', $ID)
          );
        ?>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
