<?php
/**
 * Template Name: CSS/JS animations, technichs
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>
    <script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

	<div class="swisst2-page container">

        <h1><?php the_title(); ?></h1>

        <div id="tabs">
            <ul style="list-style-type: none;">
                <li><a href="#general-css">General CSS</a></li>
                <li><a href="#form">Forms Elements</a></li>
                <li><a href="#complex">Complex UI</a></li>
            </ul>

            <div id="general-css">
                <div class="codepen-example">
                    <h2>Limit the text height and display ... if overflows</h2>
                        <p class="codepen" data-height="265" data-theme-id="dark" data-default-tab="css,result" data-user="daniel-varga" data-slug-hash="bGqmNpd" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="bGqmNpd">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/bGqmNpd">
                        bGqmNpd</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                        </p>
                </div>
        
                <div class="codepen-example">
                    <h2>Sticky element - only visible within its container</h2>
                    <p class="codepen" data-height="265" data-theme-id="dark" data-default-tab="html,result" data-user="daniel-varga" data-slug-hash="yLMRJOm" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="yLMRJOm">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/yLMRJOm">
                        yLMRJOm</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>

                <div class="codepen-example">
                    <h2>Animated text per characters (with Javascript, the text is splitted)</h2>
                    <p class="codepen" data-height="392" data-theme-id="dark" data-default-tab="js,result" data-user="daniel-varga" data-slug-hash="ZEewMXY" style="height: 392px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="SplitText Word + Char">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/ZEewMXY">
                        SplitText Word + Char</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>

                <div class="codepen-example">
                    <h2>Reveal on scroll animations</h2>
                    <p><em><a href="https://michalsnik.github.io/aos/" target="_blank">AOS library</a></em></p>
                    <p class="codepen" data-height="431" data-default-tab="result" data-slug-hash="gOWOwdY" data-user="daniel-varga" style="height: 431px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
                    <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/gOWOwdY">
                    Scroll reveal with AOS</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                    on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>
            </div>

            <div id="form">
                <div class="codepen-example">
                    <h2>Styled radio buttons and checkboxes</h2>
                    <p class="codepen" data-height="265" data-theme-id="dark" data-default-tab="css,result" data-user="daniel-varga" data-slug-hash="gOmBMQZ" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="gOmBMQZ">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/gOmBMQZ">
                        gOmBMQZ</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>
        
                <div class="codepen-example">
                    <h2>Button Hover Animations with text change without borders</h2>
                    <p><em> It does not work with borders in Chrome </em></p>
                    <p class="codepen" data-height="265" data-theme-id="dark" data-default-tab="css,result" data-user="daniel-varga" data-slug-hash="vYxbQOE" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Button Hover Animations">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/vYxbQOE">
                        Button Hover Animations</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>
        
                <div class="codepen-example">
                    <h2>Button Hover Only Background Change</h2>
                    <p class="codepen" data-height="265" data-theme-id="dark" data-default-tab="css,result" data-user="daniel-varga" data-slug-hash="BaWMvGM" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Button Hover Only Background Change">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/BaWMvGM">
                        Button Hover Only Background Change</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>
        
                <div class="codepen-example">
                    <h2>Other button hover effexts</h2>
                    <a href="https://ianlunn.github.io/Hover/" target="_blank">https://ianlunn.github.io/Hover/</a>
                </div>
            </div>

            <div id="complex">
                <div class="codepen-example">
                    <h2>Animated loading cover</h2>
                    <p>
                        <em>
                            The example is based on the menu from this <a style="color:#F2000C" href="https://www.dropbox.com/s/jhfbzj25v627h4g/Bucher%2BSuter-website-UI-interactions.mp4?dl=0" target="_blank">video</a>. Not an exact implementation. Only works with icons and svgs in Firefox.
                        </em>
                    </p>
                    <p class="codepen" data-height="265" data-theme-id="dark" data-default-tab="html,result" data-user="daniel-varga" data-slug-hash="XWMGqrw" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Loading cover with animation">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/XWMGqrw">
                        Loading cover with animation</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>

                <div class="codepen-example">
                    <h2>Animated full height hamburger menu with animated inputs, links</h2>
                    <p>
                        <em>
                            The example is based on the menu from this <a style="color:#F2000C" href="https://www.dropbox.com/s/jhfbzj25v627h4g/Bucher%2BSuter-website-UI-interactions.mp4?dl=0" target="_blank">video</a>. Not an exact implementation, just trying out some effects.
                        </em>
                    </p>
                    <p class="codepen" data-height="530" data-theme-id="dark" data-default-tab="html,result" data-user="daniel-varga" data-slug-hash="QWpYXPB" style="height: 530px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Full height menu with animations">
                        <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/QWpYXPB">
                        Full height menu with animations</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                        on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>

                <div class="codepen-example">
                    <h2>Cards with hover animations</h2>
                    <p>You have to click on rerun bottom right to get a working example.</p>
                    <p class="codepen" data-height="584" data-default-tab="result" data-slug-hash="xxdbqPX" data-user="daniel-varga" style="height: 500px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
                    <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/xxdbqPX">
                    Cards with hover animations</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                    on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>

                <div class="codepen-example">
                    <h2>Hero with lines and reveal animation</h2>
                    <p><em>Click rerun (bottom-right) to see the initial animation.</em></p>
                    <p class="codepen" data-height="520" data-default-tab="result" data-slug-hash="rNmapWL" data-user="daniel-varga" style="height: 520px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
                    <span>See the Pen <a href="https://codepen.io/daniel-varga/pen/rNmapWL">
                    Hero with line and reveal animations</a> by Daniel Varga (<a href="https://codepen.io/daniel-varga">@daniel-varga</a>)
                    on <a href="https://codepen.io">CodePen</a>.</span>
                    </p>
                </div>
            </div>
        </div>
		
	</div><!-- #job-page -->

<?php
//get_sidebar();
get_footer();
