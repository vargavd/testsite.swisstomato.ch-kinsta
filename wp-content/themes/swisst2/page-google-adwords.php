<?php
/**
 * Template Name: Google Adwords Page
 *
 * Template for the Google Adwords Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="google-adwords-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div id="below-hero-section" class="container">
            <?=get_field('heading_section_text', $ID)?>
        </div>

        <div class="container image-text-row image-text">
            <div class="column image-column" style="background-image: url(<?=get_field('advantages_section_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('advantages_section_title', $ID)?></h2>
                <?=get_field('advantages_section_text', $ID)?>
            </div>
        </div>

        <div id="get-in-touch-section" class="container pink-p-button-section">
            <p>
                <?=get_field('get_in_touch_section_text', $ID)?>
            </p>
            <a href="#contact-us" class="button-red">
                <i class="fas fa-angle-right"></i>
                <?=get_field('get_in_touch_button_text', $ID)?>
            </a>
        </div>

        <div class="container vertical-left-right-numbered-list">
            <h2><?=get_field('methodology_section_title', $ID)?></h2>
            <div class="steps">
                <?php 
                    $steps = get_field('methodology_steps', $ID);

                    foreach ($steps as $step):
                ?>
                    <div class="step">
                        <div class="logo-and-number">
							<?= custom_image_size($step['logo'], 'full', $step['title'], 'logo') ?>
                            <div class="number"><?=$step['number']?></div>
                        </div>
                        <div class="text-and-title">
                            <h3><?=$step['title']?></h3>
                            <div class="text">
                                <?=$step['text']?>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

        <div class="pink-p-button-section container">
             <p>
                <?=get_field('contact_section_text', $ID)?>
            </p>
            <a href="#contact-us" class="button-red">
                <i class="fas fa-angle-right"></i>
                <?=get_field('contact_section_button_text', $ID)?>
            </a>           
        </div>

        <?php
            digital_marketing_internal_links($ID);
        ?>

	</div>

<?php
get_footer();
