<?php
/**
 * Template Name: Home Page
 *
 * Template for the home page.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$theme_url = get_stylesheet_directory_uri();


error_reporting(0);
?>

	<div id="home-page" class="swisst2-page">
		
        <?php 
            $title = get_the_title();
            $content = apply_filters('the_content', $post->post_content);
            $awards_img = get_field('title_awards_image');
            $awards_img_url = (is_array($awards_img) ? $awards_img['url'] : '');
            $awards_img_alt = 'Introduction Awards Image';

            intro_text_awards_func($title, $content, $awards_img_url, $awards_img_alt); 
        ?>

        <?php services_func(get_field('services', $ID)); ?>

		<?php if ( !wp_is_mobile() ) : ?>

			<div id="parallax-section" class="center-1250">
				<!--PARALLAX SECTION-->
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato-blur.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato bg-tomato-1" src="<?=$theme_url.'/imgs/home/parallax/tomato-blur.png'?>" alt="Tomato background 1" width="378" height="226" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato-blur.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato bg-tomato-2" src="<?=$theme_url.'/imgs/home/parallax/tomato-blur.png'?>" alt="Tomato background 2" width="378" height="226" >
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato-blur.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato bg-tomato-3" src="<?=$theme_url.'/imgs/home/parallax/tomato-blur.png'?>" alt="Tomato background 3" width="378" height="226" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato-blur.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato bg-tomato-4" src="<?=$theme_url.'/imgs/home/parallax/tomato-blur.png'?>" alt="Tomato background 4" width="378" height="226" />
				</picture>

				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato fg-tomato-1" src="<?=$theme_url.'/imgs/home/parallax/tomato.png'?>" alt="Tomato foreground 1" width="306" height="154" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato fg-tomato-2" src="<?=$theme_url.'/imgs/home/parallax/tomato.png'?>" alt="Tomato foreground 2" width="306" height="154" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato fg-tomato-3" src="<?=$theme_url.'/imgs/home/parallax/tomato.png'?>" alt="Tomato foreground 3" width="306" height="154" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/tomato.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="tomato fg-tomato-4" src="<?=$theme_url.'/imgs/home/parallax/tomato.png'?>" alt="Tomato foreground 4" width="306" height="154" />
				</picture>

				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/juice-pink.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="bg" src="<?=$theme_url.'/imgs/home/parallax/juice-pink.png'?>" alt="Pink background 1" width="1400" height="1080"/>
				</picture>

				<div class="bg knife">
					<picture>
						<source srcset="<?= $theme_url.'/imgs/home/parallax/knife-1.png.webp' ?>" type="image/webp">
						<img loading="lazy" class="blade" src="<?=$theme_url.'/imgs/home/parallax/knife-1.png'?>" alt="Blade" width="242" height="152" />
					</picture>
					<picture>
						<source srcset="<?= $theme_url.'/imgs/home/parallax/knife-2.png.webp' ?>" type="image/webp">
						<img loading="lazy" class="handle" src="<?=$theme_url.'/imgs/home/parallax/knife-2.png' ?>" alt="Handle" width="344" height="132" />
					</picture>
				</div>

				<!-- <img loading="lazy" class="bg" src="<?=$theme_url.'/imgs/home/parallax/bg.png'?>" /> -->

				<div id="line"></div>

				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash1.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash one" src="<?=$theme_url.'/imgs/home/parallax/splash1.png'?>" alt="Splash 1" width="35" height="40" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash2.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash two" src="<?=$theme_url.'/imgs/home/parallax/splash2.png'?>" alt="Splash 2" width="42" height="36" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash3.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash three" src="<?=$theme_url.'/imgs/home/parallax/splash3.png'?>" alt="Splash 3" width="26" height="33" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash4.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash four" src="<?=$theme_url.'/imgs/home/parallax/splash4.png'?>" alt="Splash 4" width="39" height="36" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash5.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash five" src="<?=$theme_url.'/imgs/home/parallax/splash5.png'?>" alt="Splash 5" width="30" height="35" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash6.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash six" src="<?=$theme_url.'/imgs/home/parallax/splash6.png'?>" alt="Splash 6" width="116" height="106" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash7.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash seven" src="<?=$theme_url.'/imgs/home/parallax/splash7.png'?>" alt="Splash 7" width="52" height="93" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash8.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash eight" src="<?=$theme_url.'/imgs/home/parallax/splash8-reverse.png'?>" alt="Splash 8" width="113" height="89" />
				</picture>
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/splash-center.png.webp' ?>" type="image/webp">
					<img loading="lazy" class="splash center" src="<?=$theme_url.'/imgs/home/parallax/splash-center.png'?>" alt="Splash 9" width="100" height="70" />
				</picture>
				<!-- <img loading="lazy" class="splash nine" src="<?=$theme_url.'/imgs/home/parallax/splash9.png'?>" />
				<img loading="lazy" class="splash ten" src="<?=$theme_url.'/imgs/home/parallax/splash10.png'?>" /> -->
				<picture>
					<source srcset="<?= $theme_url.'/imgs/home/parallax/juice.png.webp' ?>" type="image/webp">
					<img loading="lazy" id="bottom-wave" src="<?=$theme_url.'/imgs/home/parallax/juice.png'?>" alt="Bottom wave" width="1250" height="693"/>
				</picture>
				<div id="fill"></div>

			</div>

		<?php endif; ?>

        <?php
            include "global-templates/awards-big.php";
        ?>

        <div id="references-sections">
            <h2 class="text-center"><?=get_field('references_title')?></h2>

            <div id="references-text">
                <?=get_field('references_text')?>
            </div>
    
            <?php
                $reference_sections = get_field('references', $ID);
                //pr1($reference_sections);

                foreach ($reference_sections as $section) {
                    reference_list($section['title'], $section['button_text'], $section['button_link_filter_category']->slug, '', $section['reference_posts']);
                }
            ?>

            <!-- <div class="container center-1117"><hr /></div>
    
            <div class="container text-center">
                <a class="button-red" href="<?=get_field('all_references_button_link', $ID)?>" id="all-references-link">
                    <i class="fas fa-angle-right"></i>
                    <?=get_field('all_references_button_text', $ID)?>
                </a>
            </div> -->
        </div>

        <?php
            include "global-templates/clients.php";
        ?>

        <?php
            // $why_section = get_field('why_swiss_tomato_section', $ID);

            // why_swisstomato_section(array(
            //     'title' => $why_section['title'],
            //     'left_text' => $why_section['first_left_text'],
            //     'left_handwritten_text' => $why_section['first_left_handwritten_text'],
            //     'right_image' => $why_section['first_right_image']['url'],
            //     'right_image_alt' => 'William Tell',
            //     'left_image' => $why_section['second_left_image']['url'],
            //     'left_image_alt' => 'Cake',
            //     'right_text' => $why_section['second_right_text'],
            //     'right_handwritten_text' => $why_section['second_right_handwritten_text']
            // ));
        ?>

        <?php why_us_our_story_section_func(); ?>

        <div id="posts-section" class="container">
            <h2><?=get_field('posts_title', $ID)?></h2>
            <div id="post-list">
                <?php
                    $posts = get_posts(array(
                        'posts_per_page'  => 3,
                        'suppress_filters' => 0
                    ));
    
                    foreach ($posts as $post):
                        setup_postdata($post);
                        $categories = wp_get_post_terms($post->ID, 'category');
                        $post_link = get_permalink($post->ID);
                ?>
                    <div class="post">
                        <?php
                            $post_image_url = get_field('home_slider_image', $post->ID);
                            if (empty($post_image_url)) {
                                $post_image_url = get_the_post_thumbnail_url($post->ID, 'full' );
                            }
                        ?>
                        <a href="<?=$post_link?>" class="post-image">
							<?= custom_image_size($post_image_url, 'post-home-thumbnail', $post->post_title ) ?>
						</a>
                        <div class="post-content">
                            <!-- <div class="post-categories">
                                <?php foreach ($categories as $category): ?>
                                    <a href="<?=get_term_link($category->term_id)?>">
                                        <?=$category->name?>
                                    </a>
                                <?php endforeach; ?>
                            </div> -->
                            <a href="<?=$post_link?>" class="post-title">
                                <?=$post->post_title?>
                            </a>
                            <div class="post-excerpt">
                                <?=get_the_excerpt($post)?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>
            <div class="text-center">
                <a href="<?=get_permalink(get_option( 'page_for_posts' ))?>" class="button-red" id="more-posts">
                    <i class="fas fa-angle-right"></i>
                    <?=get_field('posts_button_text', $ID)?>
                </a>
            </div>
        </div>

        <?php
            big_bc_more_text_section(get_field('mobile_app_development_big_section', $ID), '', true);

            big_bc_more_text_section(get_field('website_development_big_section', $ID), '', true);

            big_bc_more_text_section(get_field('arvr_big_section', $ID), '', true);
        ?>

	</div>

<?php
get_footer();
