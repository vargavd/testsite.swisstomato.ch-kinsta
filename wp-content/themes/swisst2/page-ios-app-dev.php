<?php
/**
 * Template Name: iOS App Dev
 *
 * Template for the android app dev page.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

wp_enqueue_script('app-dev');
?>

	<div id="ios-app-dev-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?=get_field('intro_background', $ID)?>)">
            <div id="hero-dark-layer">
                <div id="intro-awards-wrapper">
					<?= custom_image_size(get_field('intro_awards', $ID), 'full', 'Intro awards') ?>
                </div>
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <?php
            $checkList = get_field('check_list', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <div class="container image-text-row image-text">
            <div class="column image-column" style="background-image: url(<?=get_field('ios_advantages_image', $ID)?>);">
            </div>
            <div class="column text-column">
                <h2><?=get_field('ios_advantages_title', $ID)?></h2>
                <?=get_field('ios_advantages_text', $ID)?>
            </div>
        </div>

        <div class="container image-text-row text-image">
            <div class="column text-column">
                <h2><?=get_field('apple_users_title', $ID)?></h2>
                <?=get_field('apple_users_text', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('apple_users_image', $ID)?>);">
            </div>
        </div>

        <div id="help-you-section" class="container">
            <h2><?=get_field('swisstomato_help_title', $ID)?></h2>
            <div id="help-you-row">
                <div id="help-you-img-wrapper" class="bc-img-center" style="background-image: url(<?=get_field('swisstomato_help_image', $ID)?>);">    
                </div>
                <div id="help-you-text-column">
                    <?=get_field('swisstomato_help_text', $ID)?>
                </div>
            </div>
        </div>

        <?php
          $category_slug = !empty(get_field('references_section_button_category', $ID)) ? get_field('references_section_button_category', $ID)->slug : '';
          reference_list_masonry_section(
            get_field('references_section_title', $ID), 
            get_field('references_section_references', $ID),
            $category_slug,
            get_field('references_section_button_title', $ID)
          );
        ?>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
