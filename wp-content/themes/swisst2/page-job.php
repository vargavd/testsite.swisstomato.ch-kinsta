<?php
/**
 * Template Name: Job Page
 *
 * Template for the job page.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

	<div id="job-page" class="swisst2-page container">

        <h1><?php the_title(); ?></h1>

        <div class="post-date"><?=get_the_date("F d, o")?></div>
		<?= custom_image_size(get_the_post_thumbnail_url($post->ID, 'full' ), 'full', 'Job Header', 'job-header') ?>
        <?= apply_filters('the_content', $post->post_content); ?>        
		
	</div><!-- #job-page -->

<?php
//get_sidebar();
get_footer();
