<?php
/**
 * Template Name: Magic Cube
 *
 * Template for the Magic Cube Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

?>

	<div id="magic-cube-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer"><div id="hero-content"><?php echo apply_filters('the_content', $post->post_content); ?></div></div>
        </div>

        <div id="key-features-section" class="container">
            <h2><?=get_field('key_features_title', $ID)?></h2>
            <div id="key-features-list">
                <?php
                    $key_features = get_field('key_features', $ID);
                    foreach ($key_features as $key_feature): ?>
                    <div class="key-feature">
						<?= custom_image_size($key_feature['image'], 'full', $key_feature['title']) ?>
                        <h3><?=$key_feature['title']?></h3>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div id="download-marker-section" class="container">
            <div id="download-marker-section-content">
                <p><?=get_field('marker_text', $ID)?></p>
            </div>
            <a class="button-red" id="download-marker-link" href="<?=get_field('download_markers_button_link', $ID)?>">
                <i class="fas fa-angle-right"></i>
                <?=get_field('download_markers_button_text', $ID)?>
            </a>
        </div>

        <?php if (is_user_logged_in()): ?>
            <div id="video-section" class="text-center">
                <iframe width="640" height="400" src="https://www.youtube.com/embed/PA5uLoZZDxY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        <?php endif; ?>

        <div id="download-apps-section" class="container">
            <h2><?=get_field('download_apps_title', $ID)?></h2>
            <div id="download-apps-list">
                <?php
                    $app_links = get_field('download_apps_list', $ID);

                    foreach ($app_links as $app_link):
                ?>
                    <div class="download-app">
						<?= custom_image_size($app_link['platform_image'], 'full','Platform') ?>
						<?= custom_image_size($app_link['qr_code_image'], 'full', $app_link['button_text'], 'qr-code-image') ?>
                        <a class="button-red" href="<?=$app_link['link']?>">
                            <i class="fas fa-angle-right"></i>
                            <?=$app_link['button_text']?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        
	</div>

<?php
get_footer();
