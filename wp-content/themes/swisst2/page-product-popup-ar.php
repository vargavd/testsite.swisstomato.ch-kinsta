<?php
/**
 * Template Name: Product Popup AR Page
 *
 * Template for the Product Popup AR Page template.
 *
 * @package Swisstomato2
 */

/* We don't need any menu, header or footer for this page, so it has a custom header and footer */

$delonghi_css_path = 'https://swisstomato.ch/wp-content/themes/swisst2/css/pages/product-popup-ar.css';
$delonghi_css_path .= '?ver=' . filemtime(get_stylesheet_directory() . '/css/pages/product-popup-ar.css');

$is_scaled = get_field('is_model_viewer_scaled', $ID);

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title><?php the_title(); ?></title>

    <link rel='stylesheet' id='swisst-delonghi-css'  href='<?=$delonghi_css_path?>' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome'  href='https://swisstomato.ch/wp-content/themes/swisst2/css/fonts/font-awesome/css/all.min.css' type='text/css' media='all' />
</head>

<body>

<div class="hfeed site" id="page">


<!-- Actual page template from here -->
<?php
    $ID = get_the_ID();

    $android_file = get_field('android_file', $ID);
    $ios_file = get_field('ios_file', $ID);

    $only_mobile = get_field('only_mobile', $ID);
?>
<div id="product-popup-ar-page" class="<?=$only_mobile ? 'only-mobile' : ''?>">
    <?php if ($only_mobile): ?>
        <model-viewer id="model-viewer" src="<?=$android_file?>" ios-src="<?=$ios_file?>" alt="<?=the_title()?>" quick-look-browsers="safari chrome" auto-rotate camera-controls ar magic-leap> 
            <button slot="ar-button">View in your space</button> 
        </model-viewer>
    <?php else: ?>
        <header>
            <img loading="lazy" src="<?=get_field('header_logo', $ID)?>" alt="logo" />

            <button class="mobile-menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                
            </button>

            <ul id="menu">
                <span class="menu-close"></span>
                <?php foreach (get_field('menu', $ID) as $menu_item): ?>
                    <li>
                        <a href="#"><?=$menu_item['text']?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </header>

        <div id="product">

            <div id="product-image-column">
                <h1><?php the_title(); ?></h1>

                <div id="product-image-wrapper"><img src="<?=get_field('left_image', $ID)?>" alt="Product Image" /></div>
            </div>

            <div id="model-popup">
                <div class="container">
                    <img loading="lazy" src="<?=get_stylesheet_directory_uri()?>/imgs/product-ar/exit.svg" alt="Close Popup" id="close-model-popup"/>
                    
                    <div id="model-popup-inner">
                        <div id="model-viewer-wrapper">
                            <model-viewer id="model-viewer" src="<?=$android_file?>" ios-src="<?=$ios_file?>" alt="<?=the_title()?>" quick-look-browsers="safari chrome" auto-rotate camera-controls ar magic-leap> 
                                <?php if (!$is_scaled): ?>
                                    <button slot="ar-button" style="">View in your space</button> 
                                <?php endif; ?>
                            </model-viewer>
                        </div>
                
                        <button class="view-button">
                            View in your space
                        </button>
                    </div>

                    <div id="powered-by-message">
                        Powered by <strong>Swiss Tomato</strong>
                    </div>
                </div>
            </div>

            <div id="product-content">
                <h1><?php the_title(); ?></h1>

                <?php echo apply_filters('the_content', $post->post_content); ?>

                <div id="open-model-popup-wrapper">
                    <a class="view-button" id="open-qr-popup">
                        <?=get_field('button_text', $ID)?>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<div id="qr-popup-background">
    <div id="qr-popup">
        <div id="qr-code-wrapper">
            <div id="qr-code-text">
                <h2><?=get_field('qr_popup_title', $ID)?></h2>

                <p><?=get_field('qr_popup_text', $ID)?></p>
            </div>
    
            <img loading="lazy" src="<?=get_field('qr_popup_image', $ID)?>" alt="QR code" />
        </div>

        <button class="close-button"> <?=get_field('qr_popup_close_text', $ID)?> </button>
    </div>
</div>

<script type="module" src="<?=get_stylesheet_directory_uri()?>/js/model-viewer/model-viewer.js"></script>
<script src="https://swisstomato.ch/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script src="https://swisstomato.ch/wp-content/themes/swisst2/js/platform-detection/platform.js"></script>
<script>
    jQuery(document).ready(function ($) {
        var
            // DOM
            $productARPage = $('#product-popup-ar-page'),
            $viewButton = $('#product-content a.view-button'),
            $qrPopupBackground = $('#qr-popup-background'),
            $closeQRPopupBackground = $('#qr-popup button'),
            $menu = $('ul#menu'),
            $mobileMenuToggle = $('.mobile-menu-toggle'),
            $menuClose = $('.menu-close'),
            $modelPopup = $('#model-popup'),
            $modelPopupClose = $('#model-popup #close-model-popup'),
            $viewInSpaceButton = $modelPopup.find('.view-button'),
            $modelViewer = $('#model-viewer'),

            // events
            openCloseQRPopup = function () {
                if (isModelViewerScaled) {
                    if (osString.indexOf('android') !== -1) {
                        window.location.href = "intent://arvr.google.com/scene-viewer/1.0?file=<?=$android_file?>&mode=ar_preferred#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=" + hrefWithoutHash + ";end;";
                    }
                } else {
                    $qrPopupBackground.toggle();
                }
            },
            openCloseMobileMenu = function () {
                $menu.toggleClass('open');
            },
            openClosePopup = function () {
                $modelPopup.toggle();
            },

            // helper
            calculateModelViewerScaling = function () {
                var
                    // DOM
                    $modelViewerWrapper = $('#model-viewer-wrapper'),
                    $modelViewerInner = $('#model-popup-inner'),

                    // helper
                    wrapperWidth = $modelViewerWrapper.width(),
                    wrapperHeight = $modelViewerWrapper.height(),
                    modelViewerWidth = Number("<?=get_field('model_viewer_width', $ID)?>"),
                    modelViewerHeight = Number("<?=get_field('model_viewer_height', $ID)?>"),
                    marginLeftModelViewer = Number("<?=get_field('model_viewer_margin_left', $ID)?>"),
                    marginTopModelViewer = Number("<?=get_field('model_viewer_margin_top', $ID)?>");

                if (!marginLeftModelViewer) {
                    marginLeftModelViewer = ((modelViewerWidth-wrapperWidth)/2)*(-1);
                }

                if (!marginTopModelViewer) {
                    marginTopModelViewer = ((modelViewerHeight-wrapperHeight)/2)*(-1);
                }

                console.log(wrapperWidth, wrapperHeight, modelViewerWidth, modelViewerHeight, marginLeftModelViewer, marginTopModelViewer);

                $modelViewer.css({
                    'width': modelViewerWidth,
                    'height': modelViewerHeight,
                    'marginLeft': marginLeftModelViewer,
                    'marginTop': marginTopModelViewer,
                    'maxWidth': 'none'
                });

                $modelViewerWrapper.css('overflow', 'hidden');

                $modelViewerInner.addClass('scaled');
            },

            // misc
            osString = platform.os.toString().toLowerCase(),
            times = 0,
            windowWidth = window.innerWidth,
            startARAuto = location.hash.indexOf('startarauto') !== -1 || "<?=$only_mobile?>" === "1",
            openPopupAuto = location.hash.indexOf('openpopupauto') !== -1,
            hrefWithoutHash = location.href.replace(location.hash, ''),
            isModelViewerScaled = "<?=$is_scaled?>" === "1";

        $viewInSpaceButton.click(openCloseQRPopup);
        $closeQRPopupBackground.click(openCloseQRPopup);

        $viewButton.click(openClosePopup);
        $modelPopupClose.click(openClosePopup);

        if (location.href.indexOf('kwc') !== -1) {
            $productARPage.addClass('kwc');
        }


        //window.location.href = "intent://arvr.google.com/scene-viewer/1.0?file=<?=$android_file?>&mode=ar_preferred#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=" + hrefWithoutHash + ";end;";

        // $viewButton.click();
        // $popupViewInSpaceButton.click();

        if (startARAuto) {
            if (osString.indexOf('android') !== -1) {
                //alert("<?=$android_file?>");
                window.location.href = "intent://arvr.google.com/scene-viewer/1.0?file=<?=$android_file?>&mode=ar_preferred#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=" + hrefWithoutHash + ";end;";
            } else {
                // iOS
                var modelViewer = document.getElementById('model-viewer');
                modelViewer.addEventListener('load', function () { 
                    modelViewer.activateAR(); 
                });

                document.onvisibilitychange = function(e) { 
                    times++;

                    if (times > 1) {
                        window.location.href = hrefWithoutHash;
                    }
                };
            }
        } else {
            $productARPage.css('visibility', 'visible');

            if (osString.indexOf('ios') !== -1) {
                $modelViewer.addClass('ios');
            }

            if (openPopupAuto && windowWidth > 450) {
                openClosePopup();
            }

            console.log(isModelViewerScaled);

            if (isModelViewerScaled) {
                calculateModelViewerScaling();
            }
        }
    });
</script>

<!-- Custom footer from here -->

</div><!-- #page we need this extra closing tag here -->
</body>

</html>


