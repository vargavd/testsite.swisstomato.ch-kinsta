<?php
/**
 * Template Name: Product Popup Scaling AR Page
 *
 * Template for the Product Popup Scaling AR Page template.
 *
 * @package Swisstomato2
 */

/* We don't need any menu, header or footer for this page, so it has a custom header and footer */

$delonghi_css_path = 'https://swisstomato.ch/wp-content/themes/swisst2/css/pages/product-popup-scaling-ar.css';
$delonghi_css_path .= '?ver=' . filemtime(get_stylesheet_directory() . '/css/pages/product-popup-scaling-ar.css');

$is_scaled = get_field('is_model_viewer_scaled', $ID);

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title><?php the_title(); ?></title>

    <link rel='stylesheet' id='swisst-delonghi-css'  href='<?=$delonghi_css_path?>' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome'  href='https://swisstomato.ch/wp-content/themes/swisst2/css/fonts/font-awesome/css/all.min.css' type='text/css' media='all' />
</head>

<body>

<div class="hfeed site" id="page">


<!-- Actual page template from here -->
<?php
    $ID = get_the_ID();

    $android_file = get_field('android_file', $ID);
    $ios_file = get_field('ios_file', $ID);

    $only_mobile = get_field('only_mobile', $ID);
?>
<div id="product-popup-scaling-ar-page" class="<?=$only_mobile ? 'only-mobile' : ''?>">
    <?php if ($only_mobile): ?>
        <model-viewer id="model-viewer" src="<?=$android_file?>" ios-src="<?=$ios_file?>" alt="<?=the_title()?>" quick-look-browsers="safari chrome" auto-rotate camera-controls ar magic-leap> 
            <button slot="ar-button">View in your space</button> 
        </model-viewer>
    <?php else: ?>
        <header>
			<?= custom_image_size(get_field('header_logo', $ID), 'full', 'logo') ?>

            <button class="mobile-menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                
            </button>

            <ul id="menu">
                <span class="menu-close"></span>
                <?php foreach (get_field('menu', $ID) as $menu_item): ?>
                    <li>
                        <a href="#"><?=$menu_item['text']?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </header>

        <div id="product">

            <div id="product-image-column">
                <h1><?php the_title(); ?></h1>

                <div id="product-image-wrapper"><img loading="lazy" src="<?=get_field('left_image', $ID)?>" alt="Product Image" /></div>
            </div>

            <div id="model-popup">
                <div class="container">
                    <img loading="lazy" src="<?=get_stylesheet_directory_uri()?>/imgs/product-ar/exit.svg" alt="Close Popup" id="close-model-popup"/>
                    
                    <div id="model-popup-inner">
                        <div id="model-viewer-wrapper">
                            <model-viewer id="model-viewer" src="<?=$android_file?>" ios-src="<?=$ios_file?>" alt="<?=the_title()?>" quick-look-browsers="safari chrome" auto-rotate camera-controls ar magic-leap> 
                                <?php if (!$is_scaled): ?>
                                    <button slot="ar-button" style="">View in your space</button> 
                                <?php endif; ?>
                            </model-viewer>
                        </div>
                
                        <button class="view-button">
                            View in your space
                        </button>

                        <img loading="lazy" id="mobile-ar-loading-gif" src="https://swisstomato.ch/wp-content/themes/swisst2/imgs/loading.gif" />
                    </div>

                    <div id="powered-by-message">
                        Powered by <a href="https://swisstomato.ch/en/" target="_blank">Swiss Tomato</a>
                    </div>
                </div>
            </div>

            <div id="product-content">
                <h1><?php the_title(); ?></h1>

                <?php echo apply_filters('the_content', $post->post_content); ?>

                <div id="open-model-popup-wrapper">
                    <a class="view-button" id="first-popup-open-button">
                        <?=get_field('button_text', $ID)?>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<div id="qr-popup-background">
    <div id="qr-popup">
        <div id="qr-code-wrapper">
            <div id="qr-code-text">
                <h2><?=get_field('qr_popup_title', $ID)?></h2>

                <p><?=get_field('qr_popup_text', $ID)?></p>
            </div>
    
            <img loading="lazy" src="<?=get_field('qr_popup_image', $ID)?>" alt="QR code" />
        </div>

        <button class="close-button"> <?=get_field('qr_popup_close_text', $ID)?> </button>
    </div>
</div>

<script src="<?=get_stylesheet_directory_uri()?>/js/model-viewer/node_modules/@webcomponents/webcomponentsjs/webcomponents-loader.js"></script>
<script src="<?=get_stylesheet_directory_uri()?>/js/model-viewer/node_modules/intersection-observer/intersection-observer.js"></script>
<script src="<?=get_stylesheet_directory_uri()?>/js/model-viewer/node_modules/resize-observer-polyfill/dist/ResizeObserver.js"></script>

<script type="module" src="<?=get_stylesheet_directory_uri()?>/js/model-viewer/model-viewer.js"></script>
<script src="https://swisstomato.ch/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
<script src="https://swisstomato.ch/wp-content/themes/swisst2/js/platform-detection/platform.js"></script>
<script>
    jQuery(document).ready(function ($) {
        var
            // page DOM
            $productARPage = $('#product-popup-scaling-ar-page'),
            $firstPopupOpenButton = $('#first-popup-open-button'),
            
            // QR Popup DOM
            $qrPopupBackground = $('#qr-popup-background'),
            $closeQRPopupBackground = $('#qr-popup button'),

            // model popup DOM
            $modelPopup = $('#model-popup'),
            $modelPopupClose = $('#model-popup #close-model-popup'),
            $buttonInModelPopup = $modelPopup.find('.view-button'),
            $loadingARGif = $('#mobile-ar-loading-gif'),
            $modelViewer = $('#model-viewer'),

            // misc
            osString = platform.os.toString().toLowerCase(),
            times = 0,
            windowWidth = window.innerWidth,
            startARAuto = location.hash.indexOf('startarauto') !== -1 || "<?=$only_mobile?>" === "1",
            openModelPopupAuto = location.hash.indexOf('openpopupauto') !== -1,
            hrefWithoutHash = location.href.replace(location.hash, ''),
            isModelViewerScaled = "<?=$is_scaled?>" === "1"

            // helper funcs
            launchARProgramatically = function () {
                if (osString.indexOf('android') !== -1) {
                    window.location.href = "intent://arvr.google.com/scene-viewer/1.0?file=<?=$android_file?>&mode=ar_preferred#Intent;scheme=https;package=com.google.android.googlequicksearchbox;action=android.intent.action.VIEW;S.browser_fallback_url=" + hrefWithoutHash + ";end;";
                } else {
                    // iOS
                    var modelViewer = document.getElementById('model-viewer');

                    if (startARAuto) {
                        modelViewer.addEventListener('load', function () { 
                            modelViewer.activateAR();
                        });
                    } else {
                        modelViewer.activateAR();
                    }
                }
            },
            isItMobileInLandscapeMode = function () {
                return (osString.indexOf('ios') !== -1 || osString.indexOf('android') !== -1) && (window.innerHeight < window.innerWidth);
            },
            calculateModelViewerScaling = function () {
                var
                    // DOM
                    $modelViewerWrapper = $('#model-viewer-wrapper'),
                    $modelViewerInner = $('#model-popup-inner'),

                    // helper
                    wrapperWidth = $modelViewerWrapper.width(),
                    wrapperHeight = $modelViewerWrapper.height(),
                    modelViewerWidth = Number("<?=get_field('model_viewer_width', $ID)?>"),
                    modelViewerHeight = Number("<?=get_field('model_viewer_height', $ID)?>"),
                    marginLeftModelViewer = Number("<?=get_field('model_viewer_margin_left', $ID)?>"),
                    marginTopModelViewer = Number("<?=get_field('model_viewer_margin_top', $ID)?>");

                if (!isModelViewerScaled) {
                    return;
                }

                if (windowWidth > 450) {
                    modelViewerWidth = Number("<?=get_field('model_viewer_width', $ID)?>");
                    modelViewerHeight = Number("<?=get_field('model_viewer_height', $ID)?>");
                    marginLeftModelViewer = Number("<?=get_field('model_viewer_margin_left', $ID)?>");
                    marginTopModelViewer = Number("<?=get_field('model_viewer_margin_top', $ID)?>");
                } else {
                    modelViewerWidth = Number("<?=get_field('model_viewer_width_mobile', $ID)?>");
                    modelViewerHeight = Number("<?=get_field('model_viewer_height_mobile', $ID)?>");
                    marginLeftModelViewer = Number("<?=get_field('model_viewer_margin_left_mobile', $ID)?>");
                    marginTopModelViewer = Number("<?=get_field('model_viewer_margin_top_mobile', $ID)?>");
                }

                console.log(marginLeftModelViewer, marginTopModelViewer);

                if (!marginLeftModelViewer) {
                    marginLeftModelViewer = ((modelViewerWidth-wrapperWidth)/2)*(-1);
                }

                if (!marginTopModelViewer) {
                    marginTopModelViewer = ((modelViewerHeight-wrapperHeight)/2)*(-1);
                }

                $modelViewer.css({
                    'width': modelViewerWidth,
                    'height': modelViewerHeight,
                    'marginLeft': marginLeftModelViewer,
                    'marginTop': marginTopModelViewer,
                    'maxWidth': 'none'
                });

                $modelViewerWrapper.css('overflow', 'hidden');

                $modelViewerInner.addClass('scaled');
            },

            // events
            clickOnButtonInModelPopup = function () {
                if (osString.indexOf('android') !== -1 || osString.indexOf('ios') !== -1) {
                    $buttonInModelPopup.hide();
                    $loadingARGif.show();
                    launchARProgramatically();
                } else {
                    $qrPopupBackground.toggle();
                }
            },
            openCloseModelPopup = function () {
                var
                    windowScrollPos = $(window).scrollTop();

                $modelPopup.toggle();

                calculateModelViewerScaling();

                if (isItMobileInLandscapeMode) {
                    $modelPopup.css('top', windowScrollPos);
                } else {
                    $modelPopup.css('top', 0);
                }
            },
            onVisibilityChange = function () {
                times++;

                $buttonInModelPopup.show();
                $loadingARGif.hide();

                if (times > 1 && osString.indexOf('ios') !== -1) {
                    window.location.href = hrefWithoutHash;
                }
            },
            
            // init events
            init = function () {
                var
                    // helper funcs
                    initEvents = function () {
                        $buttonInModelPopup.click(clickOnButtonInModelPopup);
                        $closeQRPopupBackground.click(clickOnButtonInModelPopup);

                        $modelPopupClose.click(openCloseModelPopup);
                        $firstPopupOpenButton.click(openCloseModelPopup);

                        document.onvisibilitychange = onVisibilityChange;
                    };

                initEvents();

                if (startARAuto) {
                    launchARProgramatically();
                    return;
                }

                $productARPage.css('visibility', 'visible');

                if (openModelPopupAuto && windowWidth > 450) {
                    openCloseModelPopup();
                }

                if (windowWidth < 451) {
                    calculateModelViewerScaling();
                }

                // this doesnt need anymore but keep it just in case we return to the native button solution
                if (osString.indexOf('ios') !== -1) {
                    $modelViewer.addClass('ios');
                }
            };

        init();

        if (hrefWithoutHash.indexOf('rebo-smart-bottle-scaling') !== -1) {
            //openCloseModelPopup();
        }

    });
</script>

<!-- Custom footer from here -->

</div><!-- #page we need this extra closing tag here -->
</body>

</html>


