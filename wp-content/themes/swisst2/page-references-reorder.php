<?php
/**
 * Template Name: References Reorder Page
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$url = "http://$_SERVER[HTTP_HOST]" . strtok($_SERVER["REQUEST_URI"], '?');
$current_category_slug = $_GET["category"] ?: 'no-category';

$current_order_field = 'no_category_order';

if (strpos($current_category_slug, 'web') !== false) {
  $current_order_field = 'web_category_order';
}

if (strpos($current_category_slug, 'app') !== false) {
  $current_order_field = 'app_category_order';
}

if (strpos($current_category_slug, 'virtual') !== false) {
  $current_order_field = 'arvr_category_order';
}

$lang_slug = get_bloginfo('language');
?>

	<div class="swisst2-page container references-reorder">

    <h1><?php the_title(); ?></h1>

    <?php
      $reference_categories = get_terms(array(
        'taxonomy' => 'reference_category',
        'hide_empty' => false,
        'orderby' => 'id',
      ));
    ?>

    <ul class="reference-categories">
      <li class="<?=($current_category_slug === 'no-category') ? 'selected' : ''?>">
        <a href="<?=$url?>">
          No Category
        </a>
      </li>
      <?php foreach ($reference_categories as $category): ?>
        <li class="<?=($current_category_slug === $category->slug) ? 'selected' : ''?>">
          <a href="<?=$url . '?category=' . $category->slug?>">
            <?=$category->name?>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>

    <div class="text-center">
      <button class="saving-button">
        <i class="fas fa-cloud-upload-alt"></i>
        Save reference order (field: <?=$current_order_field?>) (language: <?=$lang_slug?>)
      </button>
    </div>

    <div class="references" data-order-field="<?=$current_order_field?>">
      <?php
        // pr2($current_order_field, $current_category_slug);
        
        $get_posts_args = array(
          'posts_per_page' => -1,
          'post_type' => 'reference',
          'suppress_filters' => 0,
          'meta_key'  => $current_order_field,
          'orderby'   => 'meta_value_num',
          'order'     => 'ASC'
        );

        if ($current_category_slug !== 'no-category') {
          $get_posts_args['tax_query'] = array(
            array(
              'taxonomy' => 'reference_category',
              'field' => 'slug',
              'terms' => $current_category_slug,
            )
          );
        }

        $references = get_posts($get_posts_args);

        foreach ($references as $reference):
          $thumbnail = get_field('thumbnail_image', $reference->ID);
      ?>
        <div class="reference" data-id="<?=$reference->ID?>">
          <div class="reference-position">
            <?=get_field($current_order_field, $reference->ID)?>
          </div>

          <div class="img-wrapper">
            <img src="<?=$thumbnail?>">
          </div>

          <h2>
            <?=$reference->post_title?>
          </h2>
        </div>
      <?php endforeach; ?>
    </div>

    <div class="text-center">
      <button class="saving-button">
        <i class="fas fa-cloud-upload-alt"></i>
        Save reference order (field: <?=$current_order_field?>) (language: <?=$lang_slug?>)
      </button>
    </div>

    <div class="loading-popup">
      <i class="fas fa-circle-notch"></i>
      <p>
        Saving reference order
      </p>
    </div>
		
	</div><!-- #references-reorder-page -->

<?php
//get_sidebar();
get_footer();
