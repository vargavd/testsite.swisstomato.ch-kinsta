<?php
/**
 * Template Name: SEO - Email Marketing Page Template
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

    <div id="seo-email-marketing-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div id="below-hero-section" class="container">
            <?=get_field('below_header_text', $ID)?>
        </div>

        <div class="container image-text-row image-text">
            <div class="column image-column" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
                <?=get_field('section_1_text', $ID)?>
            </div>
        </div>

        <div class="pink-p-button-section container">
             <?=get_field('get_a_quote_text', $ID)?>
            <a href="#contact-us" class="button-red">
                <i class="fas fa-angle-right"></i>
                <?=get_field('get_a_quote_button_text', $ID)?>
            </a>
        </div>

        <?php
            digital_marketing_internal_links($ID);
        ?>

	</div>

<?php
get_footer();
