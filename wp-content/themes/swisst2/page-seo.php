<?php
/**
 * Template Name: Digital Marketing Page
 *
 * Template for the Digital Marketing Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="digital-marketing-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-content">
                <?php echo apply_filters('the_content', $post->post_content); ?>
            </div>
        </div>

        <div id="section-1" class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_1_title', $ID)?></h2>
                <?=get_field('section_1_text', $ID)?>
            </div>
        </div>

        <?php
            $checkList = get_field('check_list', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <div id="read-more-1-section" class="container read-more-section">
            <p>
                <?=get_field('read_more_1_text', $ID)?>
            </p>
            <a class="button-red" href="<?=get_field('read_more_1_button_link', $ID)?>">
                <i class="fas fa-angle-right"></i>
                <?=get_field('read_more_1_button_text', $ID)?>
            </a>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('ga_search_marketing_title', $ID)?></h2>
                <?=get_field('ga_search_marketing_text', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('ga_search_markegint_image', $ID)?>);">
            
            </div>
        </div>

        <div id="read-more-2-section" class="container read-more-section">
            <p>
                <?=get_field('read_more_2_text', $ID)?>
            </p>
            <a class="button-red" href="<?=get_field('read_more_2_button_link', $ID)?>">
                <i class="fas fa-angle-right"></i>
                <?=get_field('read_more_2_button_text', $ID)?>
            </a>
        </div>

        <div class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('videos_infographics_newsletter_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('videos_infographics_newsletter_title', $ID)?></h2>
                <?=get_field('videos_infographics_newsletter_text', $ID)?>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('seo_title', $ID)?></h2>
                <?=get_field('seo_text', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('seo_image', $ID)?>);">
            
            </div>
        </div>

        <div id="read-more-3-section" class="container read-more-section">
            <p>
                <?=get_field('read_more_3_text', $ID)?>
            </p>
            <a class="button-red" href="<?=get_field('read_more_3_button_link', $ID)?>">
                <i class="fas fa-angle-right"></i>
                <?=get_field('read_more_3_button_text', $ID)?>
            </a>
        </div>

	</div>

<?php
get_footer();
