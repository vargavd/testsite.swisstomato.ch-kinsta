<?php
/**
 * Template Name: Smartwatch Page
 *
 * Template for the Smartwatch Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="smartwatch-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer"><div id="hero-content"><?php echo apply_filters('the_content', $post->post_content); ?></div></div>
        </div>

        <?php
            $checkList = get_field('check_list', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <div class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('smartwatch_image', $ID)?>)">
            </div>
            <div class="column text-column">
                <h2><?=get_field('smartwatch_title', $ID)?></h2>
                <?=get_field('smartwatch_text', $ID)?>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2>
                    <?=get_field('apps_title', $ID)?>
                </h2>
                <?=get_field('apps_text_1', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('apps_image_1', $ID)?>);">
            </div>
        </div>

        <div class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('apps_image_2', $ID)?>);">
            </div>
            <div class="column text-column">
                <?=get_field('apps_text_2', $ID)?>
            </div>
        </div>

        <div id="smartwatch-app-dev-section">
            <h2><?=get_field('smartwatch_app_dev_title', $ID)?></h2>
            <div class="container p-68">
                <div id="smartwatch-app-dev-img-wrapper" style="background-image: url(<?=get_field('smartwatch_app_dev_image', $ID)?>);"></div>
                <div id="smartwatch-app-dev-content">
					<?= custom_image_size(get_field('smartwatch_app_dev_image', $ID), 'full', get_field('smartwatch_app_dev_title', $ID)) ?>
                    <?=get_field('smartwatch_app_dev_text', $ID)?>
                </div>
            </div>
        </div>

        <?php
            field_of_experience_section($ID);
        ?>

        <?php
            include "global-templates/clients.php";
        ?>

        <?php
            //reference_list_standard($ID);
        ?>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
