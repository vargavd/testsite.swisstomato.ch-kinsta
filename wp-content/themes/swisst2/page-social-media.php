<?php
/**
 * Template Name: Social Media Page
 *
 * Template for the Social Media Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="social-media-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="full-width" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div class="container">
                    <div id="hero-content">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="below-hero-section" class="container">
            <?=get_field('get_a_quote_text', $ID)?>
            <a href="#contact-us" class="button-red">
                <i class="fas fa-angle-right"></i>
                <?=get_field('get_a_quote_button_text', $ID)?>
            </a>           
        </div>

        <div class="container image-text-row image-text">
            <div class="column image-column" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_1_title', $ID)?></h2>
                <?=get_field('section_1_text', $ID)?>
                <a class="swisst2-read-more arrow-down">
                    <i class="fas fa-angle-down"></i>
                    <?=get_field('section_1_link_text', $ID)?>
                </a>
                <div class="swisst2-more-text">
                    <?=get_field('section_1_more_text', $ID)?>
                </div>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('section_2_title', $ID)?></h2>
                <?=get_field('section_2_text', $ID)?>
                <a class="swisst2-read-more arrow-down">
                    <i class="fas fa-angle-down"></i>
                    <?=get_field('section_2_link_text', $ID)?>
                </a>
                <div class="swisst2-more-text">
                    <?=get_field('section_2_more_text', $ID)?>
                </div>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('section_2_image', $ID)?>);">
            
            </div>
        </div>

        <div class="container image-text-row image-text">
            <div class="column image-column" style="background-image: url(<?=get_field('section_3_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_3_title', $ID)?></h2>
                <?=get_field('section_3_text', $ID)?>
                <a class="swisst2-read-more arrow-down">
                    <i class="fas fa-angle-down"></i>
                    <?=get_field('section_3_link_text', $ID)?>
                </a>
                <div class="swisst2-more-text">
                    <?=get_field('section_3_more_text', $ID)?>
                </div>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('section_4_title', $ID)?></h2>
                <?=get_field('section_4_text', $ID)?>
                <a class="swisst2-read-more arrow-down">
                    <i class="fas fa-angle-down"></i>
                    <?=get_field('section_4_link_text', $ID)?>
                </a>
                <div class="swisst2-more-text">
                    <?=get_field('section_4_more_text', $ID)?>
                </div>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('section_4_image', $ID)?>);">
            
            </div>
        </div>

        <?php
            digital_marketing_internal_links($ID);
        ?>

	</div>

<?php
get_footer();
