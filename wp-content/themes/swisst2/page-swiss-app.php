<?php
/**
 * Template Name: Swiss App Page
 *
 * Template for the Swiss App Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="swiss-app-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div id="section-1" class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
               <!--  <h2><?=get_field('section_1_title', $ID)?></h2> -->
                <?=get_field('section_1_text', $ID)?>
            </div>
        </div>

        <?php
            $checkList = get_field('check_list', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <?php
            include "global-templates/clients.php";
        ?>

        <?php
            reference_list_standard($ID);
        ?>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
