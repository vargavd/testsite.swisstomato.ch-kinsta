<?php
/**
 * Template Name: Unity Page
 *
 * Template for the Unity Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="unity-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_1_title', $ID)?></h2>
                <?=get_field('section_1_text', $ID)?>
            </div>
        </div>

        <div class="container image-text-row text-image">
            <div class="column text-column">
                <?=get_field('section_2_text', $ID)?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_2_image', $ID)?>);"></div>
        </div>

        <div id="section-3" class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_3_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_3_title', $ID)?></h2>
                <?=get_field('section_3_text', $ID)?>
            </div>
        </div>

        <div id="swisstomato-section" class="container">
            <h2><?=get_field('swisstomato_section_title', $ID)?></h2>
            <div class="image-text-row text-image">
                <div class="column text-column">
                    <?=get_field('swisstomato_section_text', $ID)?>
                </div>
                <div class="column image-column" style="background-image: url(<?=get_field('swisstomato_section_image', $ID)?>);"></div>
            </div>
        </div>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
