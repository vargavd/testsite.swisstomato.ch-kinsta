<?php
/**
 * Template Name: Web Dev Small Companies
 *
 * Template for the web dev small companies page.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

wp_enqueue_script('web-dev');
?>

	<div id="web-dev-smallc-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer"><div id="hero-content"><?php echo apply_filters('the_content', $post->post_content); ?></div></div>
        </div>

        <?php
            $checkList = get_field('check_list_1', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <div id="website-developer-section" class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('website_developer_image', $ID)?>);">
            </div>
            <div class="column text-column">
                <h2><?=get_field('website_developer_title', $ID)?></h2>
                <?=get_field('website_developer_text', $ID)?>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('website_creation_title', $ID)?></h2>
                <?=get_field('website_creation_text', $ID)?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('website_creation_image', $ID)?>);">                
            </div>
        </div>

        <?php
            $checkList = get_field('check_list_2', $ID);
            check_list_func($checkList, 'type-2');
        ?>

        <?php
            field_of_experience_section($ID);
        ?>

        <div id="clients-section" class="container p-68">
            <h2><?=get_field('clients_title', $ID)?></h2>
            <div id="clients-text">
                <?=get_field('clients_text', $ID)?>
            </div>
            <h3><?=get_field('smb_clients_title', $ID)?></h3>
            <div class="client-list">
                <?php foreach (get_field('smb_clients', $ID) as $client): ?>
                    <a href="<?=$client['reference_link']?>" target="_blank" class="client" title="<?=$client['name_hover']?>">
						<?= custom_image_size($client['image'], 'full', $client['name_hover']) ?>
                    </a>
                <?php endforeach; ?>
            </div>
            <h3><?=get_field('blue_chip_clients_title', $ID)?></h3>
            <div class="client-list">
                <?php foreach (get_field('blue-chip_clients', $ID) as $client): ?>
                    <a href="<?=$client['reference_link']?>" target="_blank" class="client" title="<?=$client['name_hover']?>">
						<?= custom_image_size($client['image'], 'full', $client['name_hover']) ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div> <!-- /#clients-section -->

        <?php
          $category_slug = !empty(get_field('references_section_button_category', $ID)) ? get_field('references_section_button_category', $ID)->slug : '';
          reference_list_masonry_section(
            get_field('references_section_title', $ID), 
            get_field('references_section_references', $ID),
            $category_slug,
            get_field('references_section_button_title', $ID)
          );
        ?>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
