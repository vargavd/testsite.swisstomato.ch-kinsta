<?php

/**
 * Template Name: Web Dev
 *
 * Template for the web dev page.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

?>

<div id="web-dev-page" class="swisst2-page">

  <?php 
    $title = get_the_title();
    $content = apply_filters('the_content', $post->post_content);
    $awards_img = get_field('title_awards_image');
    $awards_img_url = (is_array($awards_img) ? $awards_img['url'] : '');
    $awards_img_alt = 'Introduction Awards Image';

    intro_text_awards_func_2022($title, $content, $awards_img_url, $awards_img_alt, 'version-2022'); 
  ?>

  <?php services_func_2022(get_field('web_dev_services', $ID), 'version-2022'); ?>

  <?php big_red_text_func(get_field('big_red_text', $ID)); ?>

  <?php 
    our_story_func(
      get_field('our_story_2022feb_title', $ID), 
      get_field('our_story_2022feb_text', $ID), 
      get_field('our_story_founders_image', $ID),
      get_field('our_story_2022feb_founder_1_title', $ID),
      get_field('our_story_2022feb_founder_1_text', $ID),
      get_field('our_story_2022feb_founder_2_title', $ID),
      get_field('our_story_2022feb_founder_2_text', $ID)
    ); ?>

  <?php
    awards_2022feb_func(
      get_field('awards_2022feb_title', $ID), 
      get_field('awards_2022feb_text', $ID),
      get_field('awards_2022feb_awards', $ID),
      get_field('awards_2022feb_background_image', $ID)
    );
  ?>

  <?php
	  include "global-templates/clients.php";
	?>

  <?php
    $category = get_term(77, 'reference_category');
    
    reference_list_masonry_section(
      get_field('references_section_title', $ID), 
      get_field('references_section_references', $ID),
      $category->slug,
      get_field('references_section_button_title', $ID)
    );
  ?>

	<?php
        icon_text_list_func();
	?>

  <?php why_us_our_story_section_func(); ?>

  <div id="agency-section" class="image-text-row image-text container zoom-in-on-scroll">
		<div class="column image-column">
			<?php
			$agency_image = get_field('agency_image', $ID);
			?>

			<?= custom_image_size($agency_image['url'], "full", $agency_image['alt']) ?>
		</div>
		<div class="column text-column">
			<h2><?= get_field('agency_title', $ID) ?></h2>
			<?= get_field('agency_text', $ID) ?>
		</div>
	</div>

	<div id="website-creation-section" class="image-text-row text-image container zoom-in-on-scroll">
		<div class="column text-column">
			<h2><?= get_field('website_creation_title', $ID) ?></h2>
			<?= get_field('website_creation_text', $ID) ?>
		</div>
		<div class="column image-column">
			<?php
			$website_creation_image = get_field('website_creation_image', $ID);
			?>

			<?= custom_image_size($website_creation_image['url'], "full", $website_creation_image['alt']) ?>
		</div>
	</div>

	<div id="web-dev-section" class="container text-center">
		<p>
			<?= get_field('website_development_text', $ID) ?>
		</p>
		<a href="<?= get_field('website_development_button_link') ?>" class="button-red">
			<i class="fas fa-angle-right"></i>
			<?= get_field('website_development_button_text') ?>
		</a>
	</div>

	<?php
	field_of_experience_section($ID);
	?>

	<?php
	// $why_section = get_field('why_swiss_tomato_section', $ID);

	// why_swisstomato_section(array(
	// 	'title' => $why_section['title'],
	// 	'left_text' => $why_section['first_left_text'],
	// 	'left_handwritten_text' => $why_section['first_left_handwritten_text'],
	// 	'right_image' => $why_section['first_right_image'],
	// 	'right_image_alt' => 'William Tell',
	// 	'left_image' => $why_section['second_left_image'],
	// 	'left_image_alt' => 'Cake',
	// 	'right_text' => $why_section['second_right_text'],
	// 	'right_handwritten_text' => $why_section['second_right_handwritten_text']
	// ));
	?>

	<?php
	process_and_timing_func($ID);
	?>

	<div id="why-important-section" class="container">
		<?php
		$why_important_section = get_field('why_important_section', $ID);
		?>
		<h2><?= $why_important_section['title'] ?></h2>
		<div id="why-important-text">
			<?= $why_important_section['text'] ?>
		</div>

		<div id="why-important-cards">
			<?php foreach ($why_important_section['cards'] as $index => $card) : ?>
				<div class="wi-card">
					<div class="wi-card-image-wrapper">
            <?= custom_image_size($card['image'], "full", $card['title']) ?>
          </div>
					<h3><?= $card['title'] ?></h3>

					<?= $card['text'] ?>

					<a class="swisst2-read-more arrow-down">
						<i class="fas fa-angle-down"></i>
						<?= $why_important_section['read_more_link_text'] ?>
					</a>

					<div class="swisst2-more-text">
						<?= $card['more_text'] ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

	<?php
	big_bc_more_text_section(get_field('what_benefits_section', $ID), "big-bc-what-benefits-section");

	big_bc_more_text_section(get_field('principles_section', $ID), "big-bc-principals-section");
	?>


	<div class="web-dev-big-bc-section-more-text-wrapper container">
		<div id="principles-more-text" class="web-dev-big-bc-section-more-text swisst2-more-text">
			<?= $principles['more_text'] ?>
		</div>
	</div>

	<div id="lets-shape-section">
		<div class="container">
			<h2><?= get_field('lets_shape_title', $ID) ?></h2>

			<?= get_field('lets_shape_text', $ID) ?>
		</div>
	</div>

</div>

<?php
get_footer();
