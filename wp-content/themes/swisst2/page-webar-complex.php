<?php
/**
 * Template Name: WebAR Complex Page
 *
 * Template for the WebAR Complex Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

    <script data-token="demoaccount" src="https://view.seekxr.com/demoaccount/seek.min.js"></script>

	<div id="webar-complex-page" class="swisst2-page">

        <div id="hero-section" data-mobile-bc="<?=get_field('hero_mobile_background_image', $ID)?>">
            <div id="hero-background-images">
                <img loading="lazy"  src="<?=get_field('hero_curve_background_image', $ID)?>" alt="Hero Background Curve" id="hero-background-curve" />
                <img loading="lazy" src="<?=get_field('hero_dots_background_image', $ID)?>" alt="Hero Background Dots 1" id="hero-background-dots-1"/>
                <img loading="lazy" src="<?=get_field('hero_dots_background_image', $ID)?>" alt="Hero Background Dots 2" id="hero-background-dots-2"/>
            </div>
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <h1><?php the_title(); ?></h1>
                    <?php echo apply_filters('the_content', $post->post_content); ?>

                    <div id="hero-hand">
                        <img loading="lazy" src="<?=get_field('hero_hand_image', $ID)?>" alt="Hero Hand Mobile Image" />
                        <img loading="lazy" class="mobile" src="<?=get_field('hero_hand_mobile_image', $ID)?>" alt="Hero Hand Mobile Image" />

                        <video autoplay muted loop controls>
                            <source src="<?=get_field('hero_hand_video', $ID)?>" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>

            <div id="hero-mobile-background" style="background-image: url(<?=get_field('hero_mobile_background_image', $ID)?>);" alt="Hero Mobile Background">
            
            </div>
        </div>

        <div id="no-app-section" class="container">
            <img loading="lazy"  id="no-app-section-image" src="<?=get_field('no_app_image', $ID)?>" alt="<?=get_field('no_app_title', $ID)?>" />

            <div id="no-app-section-content">
                <h2><?=get_field('no_app_title', $ID)?></h2>
                <p><?=get_field('no_app_text', $ID)?></p>

                <a class="try-in-ar-link" data-seek="Flamingo_Lamp">
                </a>
            </div>
        </div>

        <div id="view3d-section">
            <div class="container">
                <div id="view3d-section-content">
                    <h2><?=get_field('3d_view_ar_title', $ID)?></h2>

                    <a class="try-in-ar-link" data-seek="keurigkminiplus">
                    </a>
                </div>
    
                <div id="view3d-images">
                    <img loading="lazy" src="<?=get_field('3d_view_ar_image_1', $ID)?>" alt="3D View or Ar Image 1" />
                    <img loading="lazy" src="<?=get_field('3d_view_ar_image_2', $ID)?>" alt="3D View or Ar Image 2" />
                </div>
            </div>
        </div>

        <div id="next-big-ecommerce-section">
            <video autoplay muted loop id="myVideo">
                <source src="<?=get_field('next_big_ecommerce_background_video', $ID)?>" type="video/mp4">
            </video>

            <div class="dark-layer">
                <h2><?=get_field('next_big_ecommerce_title', $ID)?></h2>
    
                <div id="next-big-ecommerce-list">
                    <?php
                        $items = get_field('next_big_ecommerce_list', $ID);

                        foreach ($items as $item):
                    ?>
                        <div class="nbe-item">
                            <img loading="lazy" src="<?=$item['image']?>" alt="<?=$item['title']?>" />

                            <h3><?=$item['title']?></h3>
                            
                            <p><?=$item['text']?></p>
                        </div>
                    <?php endforeach; ?>
                </div>

                <a class="webar-button-red" href="#contact-us">
                    <i class="fas fa-angle-right"></i>
                    <?=get_field('view_section_button_text', $ID)?>
                </a>
            </div>
        </div>
        
        <div id="view-section" class="container">
            <img loading="lazy" src="<?=get_field('view_section_image', $ID)?>" alt="<?=get_field('view_section_title', $ID)?>" />

            <div id="view-section-content">
                <h2><?=get_field('view_section_title', $ID)?></h2>
                
                <a class="try-in-ar-link" data-seek="Mouse1"></a>
            </div>
        </div>

        <div class="<?=is_user_logged_in() ? 'logged-in' : ''?>" id="purple-section" style="background-image: url(<?=get_field('purple_section_background', $ID)?>);">
            <div id="purple-background-images">
                <img loading="lazy" src="<?=get_field('purple_section_background_curve', $ID)?>" alt="Purple Background Curve" id="purple-background-curve" />
                <img loading="lazy" src="<?=get_field('hero_dots_background_image', $ID)?>" alt="Hero Background Dots 1" id="purple-background-dots-1"/>
                <img loading="lazy" src="<?=get_field('hero_dots_background_image', $ID)?>" alt="Hero Background Dots 2" id="purple-background-dots-2"/>
                <img loading="lazy" src="<?=get_field('purple_section_background_lines', $ID)?>" alt="Hero Background Dots 1" id="purple-background-lines-1"/>
                <img loading="lazy" src="<?=get_field('purple_section_background_lines', $ID)?>" alt="Hero Background Dots 2" id="purple-background-lines-2"/>
            </div>
            <div class="container">
                <?php
                    $media_items = get_field('purple_section_media_items', $ID);

                    foreach ($media_items as $item):
                ?>
                    <div class="purple-section-media-item">
                        <h3><?=$item['title']?></h3>
                        <div class="purple-section-item-img-video">
                            <img loading="lazy" src="<?=$item['image']?>" alt="<?=$item['title']?>" />
                            <?php if ($item['video']): ?>
                                <i class="fa fa-play"></i>
                                <video autoplay muted loop controls>
                                    <source src="<?=$item['video']?>" type="video/mp4">
                                </video>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div id="compatible-section">
            <div class="container">
                <h2><?=get_field('compatible_section_title', $ID)?></h2>
                
                <div id="conpatible-list">
                    <?php
                        $items = get_field('compatible_section_list', $ID);
    
                        foreach ($items as $item):
                    ?>
                        <div class="item">
                            <div class="image-wrapper"><img loading="lazy" src="<?=$item['image']?>" alt="<?=$item['title']?>"/></div>
                            
                            <h3><?=$item['title']?></h3>
    
                            <p><?=$item['text']?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div id="works-section">
            <div class="container">
                <h2><?=get_field('works_section_title', $ID)?></h2>
                
                <p><?=get_field('works_section_text', $ID)?></p>
    
                <a class="webar-button-red" href="#contact-us">
                    <i class="fas fa-angle-right"></i>
                    <?=get_field('works_section_button_text', $ID)?>
                </a>
            </div>
        </div>

        <div id="try-it-section" class="container">
            <h2><?=get_field('try_it_section_title', $ID)?></h2>

            <div id="try-it-list">
                <?php
                    $examples = get_field('try_it_section_examples', $ID);

                    foreach ($examples as $example):
                ?>
                    <div class="example">
                        <img loading="lazy" src="<?=$example['image']?>" alt="<?=$example['data_seek_code']?>" />

                        <a class="try-in-ar-link" data-seek="<?=$example['data_seek_code']?>"></a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

	</div>

<?php
get_footer();
