<?php
/**
 * Template Name: WebAR
 *
 * @package Swisstomato2
 */

get_header(); 

// Site url
$domain_url = "https://{$_SERVER['SERVER_NAME']}";
$actual_url = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$model_path	= $domain_url.'/models/';
$theme_dir 	= get_template_directory_uri();

$video_file = get_field('video_file');
$video_file_thumbnail = wp_get_attachment_url(get_field('video_file_thumbnail'));


?>

<!-- Hero -->
<?php
if ( have_rows( 'hero' ) ) :
  while ( have_rows( 'hero' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $subtitle = get_sub_field( 'subtitle' );
    $lead = get_sub_field( 'lead' );
    $video = get_sub_field( 'video' );
    $button = get_sub_field( 'button' );
?>

<section class="outer--hero js-hero">
  <div class="graphic--hero js-hero-graphic"></div>

  <div class="inner inner--hero c-0 post post--hero">
    <div class="post__header js-hero-header">
      <h1 class="mb-0 mb-1--lg h1 c-0"><?php _e( $title, 'swisst2' ); ?></h1>
  
      <h2 class="h3 mt-0 mb-2 mb-3--lg"><?php _e( $subtitle, 'swisst2' ); ?></h2>
  
      <?php if ( $lead ): ?>
      <p class="lead mt-0 mb-2 c-0"><?php _e( $lead, 'swisst2' ); ?></p>
	  <?php endif; ?>
	  
      <a class="h5 btn btn--basic bgc-5 mb-2 c-0" href="#try-it-now">
        <i class="fas fa-angle-right mr-1"></i>
        <!-- Get started -->
        <?php esc_html_e($button['label'], 'swisst2'); ?> 
      </a>
    </div>

    <div class="post__media js-hero-media">
      <video class="post__media__video" autoplay loop playsinline muted src="<?=esc_url($video_file)?>" poster="<?=esc_url($video_file_thumbnail)?>">
        Sorry, your browser doesn't support embedded videos.
      </video>

      <picture class="post__media__img">
        <source type="image/webp" srcset="<?= $theme_dir ?>/imgs/webar/Hand-phone-2.png"> 
        <img src="<?= $theme_dir ?>/imgs/webar/Hand-phone-2.png" alt="Hand with a phone">
      </picture>

    </div>
  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- Web based -->

<?php
if ( have_rows( 'web_based' ) ) :
  while ( have_rows( 'web_based' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $lead = get_sub_field( 'lead' );
    $image = get_sub_field( 'image' );
	$size = 'full';

	// File name and url path
	$file_name = get_sub_field( 'path' ) ? get_sub_field( 'path' ) : '';
	$src 	 = '';
	$ios_src = '';

	if ( $file_name ) {
		$src 	 = $model_path.$file_name.'/'.$file_name. '.glb';
		$ios_src = $model_path.$file_name.'/'.$file_name. '.usdz';
	}
?>

<section class="outer--web-based js-web-based">
  <div class="inner inner--web-based post post--web-based">
    <header class="post__header js-copy">
      <h2 class="h3 c-1 mb-2"><?php _e( $title, 'swisst2' ); ?></h2>

    <?php if ( $lead ): ?>
      <p class="lead c-2"><?php _e( $lead, 'swisst2' ); ?></p>
    <?php endif; ?>

    </header>

    <div class="post__media js-web-based-media">
      <?php
      if ( $image ) {
        echo wp_get_attachment_image( $image, $size, '', array( 'class' => 'img--web-based' ) );
      }
      ?>
    </div>

    <?php if ( get_field( 'button_label' ) ):
      $button_label = get_field( 'button_label' );
    ?>
	<a onclick="ModelOpenButtonClick(this)" id="<?= $file_name; ?>" class="btn btn--rounded post__btn js-copy popup-open-button" src="<?= $src ?>" data-camera="45deg 60deg 50%" data-ios="<?= $ios_src ?>">
      	<img class="img--ar-icon" src="<?= $model_path ?>Ar-icon.svg" alt="WebAR">
      	<?php if (!wp_is_mobile()) : ?>
		<?php esc_html_e( $button_label, 'swisst2' ); ?>
		<?php else : ?>
		<?php esc_html_e( 'View in your place', 'swisst2' ); ?>
		<?php endif; ?>
    </a>
    <?php endif; ?>
  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- You choose -->

<?php
if ( have_rows( 'you_choose' ) ) :
  while ( have_rows( 'you_choose' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $lead = get_sub_field( 'lead' );
    $images = get_sub_field( 'images' );
	$size = 'full';
	
	// File name and url path
	$file_name = get_sub_field( 'path' ) ? get_sub_field( 'path' ) : '';
	$src 	 = '';
	$ios_src = '';

	if ( $file_name ) {
		$src 	 = $model_path.$file_name.'/'.$file_name. '.glb';
		$ios_src = $model_path.$file_name.'/'.$file_name. '.usdz';
	}
?>

<section class="outer--you-choose js-you-choose">
  <div class="inner inner--you-choose post post--you-choose">
    <header class="post__header js-copy">
      <h2 class="h3 c-1 mb-2"><?php _e( $title ); ?></h2>

    <?php if ( $lead ): ?>
      <p class="lead c-2"><?php _e( $lead ); ?></p>
    <?php endif; ?> 

    </header>

    <?php 
    if ( $images ): ?>
      <ul class="list--you-choose list-no post__media">
      <?php foreach( $images as $image_id ) : ?>
        <li class="list--you-choose__item js-you-choose-item">
          <?php echo wp_get_attachment_image( $image_id, $size, '', array( 'class' => 'img--you-choose') ); ?>
        </li>
      <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <?php if ( get_field( 'button_label' ) ):
      $button_label = get_field( 'button_label' );
    ?>
	<a onclick="ModelOpenButtonClick(this)" id="<?= $file_name ?>" class="btn btn--rounded post__btn js-copy popup-open-button" src="<?= $src ?>" data-ios="<?= $ios_src ?>">
		<img class="img--ar-icon" src="<?= $model_path ?>Ar-icon.svg" alt="WebAR">
		<?php if (!wp_is_mobile()) : ?>
		<?php esc_html_e( $button_label, 'swisst2' ); ?>
		<?php else : ?>
		<?php esc_html_e( 'View in your place', 'swisst2' ); ?>
		<?php endif; ?>
    </a>
    <?php endif; ?>
  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- E-commerce -->

<?php
if ( have_rows( 'ecommerce' ) ) :
  while ( have_rows( 'ecommerce' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $video = get_sub_field( 'video' );
    $button = get_sub_field( 'button' );
?>

<section class="outer--ecommerce">
  <span class="overlay overlay--ecommerce"></span>

  <video class="vid--ecommerce" autoplay loop playsinline muted src="<?php echo esc_url( $video['url'] ); ?>">
    Sorry, your browser doesn't support embedded videos.
  </video>

  <div class="inner inner--ecommerce">
    <header>
      <h2 class="h2 c-0 ta-c mb-2 mb-4--lg"><?php _e( $title, 'swisst2'); ?></h2>
    </header>

  <?php if ( have_rows( 'features' ) ) : ?>
    <ul class="list--ecommerce list-no mb-2 mb-3--lg mt-2">

    <?php while ( have_rows( 'features' ) ) : the_row();

      $title = get_sub_field( 'title' );
      $lead = get_sub_field( 'lead' );
      $image = get_sub_field( 'image' );
    ?>

      <li class="list--ecommerce__item">
        <img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php  esc_attr_e( $image['alt'], 'swisst2' ); ?>">
        <h3 class="h3  c-1 mb-2 mt-2"><?php _e( $title, 'swisst2' ); ?></h3>
        <p class="lead c-2"><?php _e( $lead, 'swisst2' ); ?></p>
      </li>

    <?php endwhile; ?>
    </ul>
  <?php endif; ?>

    <a class="btn btn--basic--2 bgc-1 h5 mt-3 c-0" href="#contact-us">
      <i class="fas fa-angle-right mr-1"></i>
      <?php esc_html_e($button['label'], 'swisst2'); ?>
    </a>
  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- Experience -->

<?php
if ( have_rows( 'experience' ) ) :
  while ( have_rows( 'experience' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $lead = get_sub_field( 'lead' );
    $image = get_sub_field( 'image' );
	$size = 'full';
	
	// File name and url path
	$file_name = get_sub_field( 'path' ) ? get_sub_field( 'path' ) : '';
	$src 	 = '';
	$ios_src = '';

	if ( $file_name ) {
		$src 	 = $model_path.$file_name.'/'.$file_name. '.glb';
		$ios_src = $model_path.$file_name.'/'.$file_name. '.usdz';
	}
?>

<section class="outer--experience">
  <div class="inner inner--experience post post--experience">
    <header class="post__header js-copy">
      <h2 class="h3 c-1 mb-2"><?php _e( $title, 'swisst2' ); ?></h2>

    <?php if ( $lead ): ?>
      <p class="lead c-2"><?php _e( $lead, 'swisst2' ); ?></p>
    <?php endif; ?>
    </header>

    <div class="post__media js-experience-media">
    <?php
    if ( $image ) {
      echo wp_get_attachment_image( $image, $size, '', array( 'class' => 'img--experience' ) );
    }
    ?>
    </div>

    <?php if ( get_field( 'button_label' ) ):
      $button_label = get_field( 'button_label' );
    ?>
	<a onclick="ModelOpenButtonClick(this)" id="<?= $file_name ?>" class="btn btn--rounded post__btn js-copy popup-open-button" src="<?= $src ?>" data-ios="<?= $ios_src ?>">
      	<img class="img--ar-icon" src="<?= $model_path ?>Ar-icon.svg" alt="WebAR">
      	<?php if (!wp_is_mobile()) : ?>
		<?php esc_html_e( $button_label, 'swisst2' ); ?>
		<?php else : ?>
		<?php esc_html_e( 'View in your place', 'swisst2' ); ?>
		<?php endif; ?>
    </a>
    <?php endif; ?>
  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- Compatible -->

<?php
if ( have_rows( 'compatible' ) ) :
  while ( have_rows( 'compatible' ) ) : the_row();

    $title = get_sub_field( 'title' );
?>

<section class="outer--compatible js-compatible">
  <div class="graphic--compatible"></div>
  <div class="overlay overlay--compatible"></div>

  <div class="clip--compatible js-clip-compatible">

    <div class="inner inner--compatible ta-c">
      <header class="js-compatible-header">
        <h2 class="h2 c-0 mb-4 mb-3--lg"><?php _e( $title, 'swisst2'); ?></h2>
      </header>

    <?php if ( have_rows( 'features' ) ) : ?>
      <ul class="list--compatible list-no">

      <?php while ( have_rows( 'features' ) ) : the_row();

        $title = get_sub_field( 'title' );
        $lead = get_sub_field( 'lead' );
        $image = get_sub_field( 'image' );
		$size = 'full';

		// File name and url path
		$file_name = get_sub_field( 'path' ) ? get_sub_field( 'path' ) : '';
		$src 	 = '';
		$ios_src = '';

		if ( $file_name ) {
			$src 	 = $model_path.$file_name.'/'.$file_name. '.glb';
			$ios_src = $model_path.$file_name.'/'.$file_name. '.usdz';
		}
      ?>

        <li class="list--compatible__item js-compatible-item">
          <h3 class="h3 c-1 mb-2"><?php _e( $title, 'swisst2' ); ?></h3>

          <p class="c-0 lead mb-3"><?php _e( $lead, 'swisst2' ); ?></p>

        <?php
        if ( $image ) {
          echo wp_get_attachment_image( $image, $size, '', array( 'class' => 'img--compatible' ) );
        }
        ?>

        <?php if ( get_field( 'button_label' ) ):
          $button_label = get_field( 'button_label' );
		?>
		
		<a onclick="ModelOpenButtonClick(this)" id="<?= $file_name; ?>" class="btn btn--rounded mt-3 mb-3 popup-open-button" src="<?= $src; ?>" data-ios="<?= $ios_src ?>">
			<img class="img--ar-icon" src="<?= $model_path ?>Ar-icon.svg" alt="WebAR">
			<?php if (!wp_is_mobile()) : ?>
			<?php esc_html_e( $button_label, 'swisst2' ); ?>
			<?php else : ?>
			<?php esc_html_e( 'View in your place', 'swisst2' ); ?>
			<?php endif; ?>
		</a>
        <?php endif; ?>
        </li>

      <?php endwhile; ?>
      </ul>
    <?php endif; ?>

    </div>

  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- WebAr -->

<?php
if ( have_rows( 'webar' ) ) :
  while ( have_rows( 'webar' ) ) : the_row();

    $title = get_sub_field( 'title' );
    $button = get_sub_field( 'button' );
?>

<section class="outer--webar">
  <div class="overlay overlay--webar"></div>

  <div class="clip--webar">
    <div class="inner inner--webar ta-c">
      <header>
        <h2 class="h2 c-1 mb-3"><?php _e( $title, 'swisst2'); ?></h2>
      </header>

    <?php if ( have_rows( 'features' ) ) : ?>
      <ul class="list--webar list-no">

      <?php while ( have_rows( 'features' ) ) : the_row();

        $lead = get_sub_field( 'lead' );
      ?>      

        <li class="list--webar__item">
          <i class="fas fa-check"></i>
          <p class="lead c-2"><?php _e( $lead, 'swisst2' ); ?></p>
        </li>

      <?php endwhile; ?>
      </ul>
    <?php endif; ?>

      <a class="btn btn--basic--2 bgc-1 h5 mt-2 c-0" href="#contact-us">
        <i class="fas fa-angle-right mr-1"></i>
        <?php esc_html_e($button['label'], 'swisst2'); ?>
      </a>
    </div>
  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- Try it -->

<?php
if ( have_rows( 'try_it' ) ) :
  while ( have_rows( 'try_it' ) ) : the_row();

    $title = get_sub_field( 'title' );
?>

<section id="try-it-now" class="outer--try-it js-try-it">
  <div class="inner inner--try-it ta-c">
    <header>
      <h2 class="h2 c-1 mb-3 mb-4--lg mt-3 mt-2--lg"><?php _e( $title, 'swisst2'); ?></h2>
    </header>

  <?php if ( have_rows( 'features' ) ) : ?>
    <ul class="list--try-it list-no">

    <?php while ( have_rows( 'features' ) ) : the_row();

	$title 	  = get_sub_field( 'title' );
	$image 	  = get_sub_field( 'image' );
	$size 	  = 'full';

	// File name and url path
	$file_name = get_sub_field( 'path' ) ? get_sub_field( 'path' ) : '';
	$src 	 = '';
	$ios_src = '';

	if ( $file_name ) {
		$src 	 = $model_path.$file_name.'/'.$file_name. '.glb';
		$ios_src = $model_path.$file_name.'/'.$file_name. '.usdz';
	}
    ?>

      <li class="list--try-it__item">
        <h3 class="h4 c-1 mb-2"><?php _e( $title, 'swisst2' ); ?></h3>

      <?php
      if ( $image ) {
        echo wp_get_attachment_image( $image, $size );
      }
      ?>

      <?php if ( get_field( 'button_label' ) ):
        $button_label = get_field( 'button_label' );
      ?>
		<a onclick="ModelOpenButtonClick(this)" id="<?= $file_name ?>" class="btn btn--rounded mt-2 popup-open-button" src="<?= $src ?>" data-ios="<?= $ios_src ?>">
			<img class="img--ar-icon" src="<?= $model_path ?>Ar-icon.svg" alt="WebAR">
			<?php if (!wp_is_mobile()) : ?>
			<?php esc_html_e( $button_label, 'swisst2' ); ?>
			<?php else : ?>
			<?php esc_html_e( 'View in your place', 'swisst2' ); ?>
			<?php endif; ?>
		</a>
      <?php endif; ?>
      </li>

      <?php endwhile; ?>
    </ul>
  <?php endif; ?>

  </div>
</section>

  <?php endwhile; ?>
<?php endif; ?>

<?php include "global-templates/clients.php"; ?>

<?php 

//Detect special conditions devices
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");

$andoid_url = '';
$ios_url 	= '';

if ( $iPad || $iPhone || $iPad ) {
	$android_url = $domain_url . '/models/lamp/lamp.glb';
	$ios_url 	 = $domain_url . '/models/lamp/lamp.usdz';
}

?>

<!-- Model Popup -->
<div id="model-popup">

	<!-- Close Popup -->
	<img loading="lazy" onclick="ModelCloseButtonClick()" src="<?= $model_path ?>exit.svg" alt="Close Popup" id="close-model-popup"/>

	<!-- Model Viewer -->
	<model-viewer id="model-viewer" src="<?= $android_url ?>" ios-src="<?= $ios_url ?>" loading="eager" shadow-intensity="0" camera-orbit="" camera-controls ar ar-scale="fixed" quick-look-browsers="safari chrome">

		<!-- Custom Progress Background -->
		<div class="progress__bg hide">
			<img loading="lazy" class="progress__logo" src="<?= $model_path ?>virtual-tomato-logo.svg">
		</div>

		<!-- Custom Progress Bar -->
		<div class="progress-bar hide" slot="progress-bar">
			<div class="update-bar"></div>
		</div>

	</model-viewer>

	<!-- QR Button -->
	<a id="qr__button" class="hide" href="#qr__code" data-lity>
		<img loading="lazy" class="img--ar-icon" src="<?= $model_path ?>Ar-icon.svg" alt="WebAR">
		<?= __('View in your place', 'swisst2') ?>
	</a>

	<!-- QR View -->
	<div id="qr__code" class="lity-hide">
		<h3><?= __('How to View in Augmented Reality', 'swisst2') ?></h3>
		<p><?= __('Scan this QR code with your phone to view the object in your space', 'swisst2') ?></p>
		<img loading="lazy" src="" alt="QR Code image for Ar scanning" />
		<button class="lity-close new" type="button" aria-label="Close (Press escape to close)" data-lity-close="">Close</button>
	</div>

	<!-- Powered by Text -->
	<div id="powered-by-text">
	<?= __('Powered by', 'swisst2') ?> <a href="<?php the_permalink(); ?>">Swiss Tomato</a>
	</div>
</div>


<!-- 🚨 REQUIRED: Web Components polyfill to support Edge and Firefox < 63 -->
<script src="https://cdn.jsdelivr.net/npm/@webcomponents/webcomponentsjs@2.5/webcomponents-bundle.min.js"></script>

<!-- 💁 OPTIONAL: Intersection Observer polyfill for better performance in Safari and IE11 -->
<script src="https://cdn.jsdelivr.net/npm/intersection-observer@0.12.0/intersection-observer.js"></script>

<!-- 💁 OPTIONAL: The :focus-visible polyfill removes the focus ring for some input types -->
<script src="https://cdn.jsdelivr.net/npm/focus-visible@5.2.0/dist/focus-visible.min.js" defer></script>


<script>

// Constans
const MODEL_VIEWER 	 = document.getElementById('model-viewer'),
	  QR_CODE_BUTTON = document.querySelector('#qr__button'),
	  QR_CODE_IMAGE	 = document.querySelector("#qr__code img"),
	  MODDEL_POPUP 	 = document.getElementById("model-popup");


// Set model-viewer Cache Size to 10
MODEL_VIEWER.modelCacheSize = 10;


// Custom Loading Screen
const onProgress = (event) => {
	let progressBG 	= event.target.querySelector('.progress__bg'), 
		progressBar = event.target.querySelector('.progress-bar'),
		updatingBar = event.target.querySelector('.update-bar');
	updatingBar.style.width = `${event.detail.totalProgress*100}%`;
	if (event.detail.totalProgress === 1) {
		progressBar.classList.add('hide');
		progressBG.classList.add('hide');
		QR_CODE_BUTTON.classList.remove('hide');
	} else {
		progressBar.classList.remove('hide');
		progressBG.classList.remove('hide');
		QR_CODE_BUTTON.classList.add('hide');
	}
};
MODEL_VIEWER.addEventListener('progress', onProgress);


// Detect mobile OS by JavaScript
function getMobileOS() {

  	let userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "windows";
    }
    if (/android/i.test(userAgent)) {
        return "android";
    }
    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "ios";
    }
    return "unknown";
}


// Model Open Button
const ModelOpenButtonClick = (e) => {

	let modelSrc = e.getAttribute('src');

	if ( getMobileOS() !== 'android') {

		let	iosSrc = e.getAttribute('data-ios');

		// Change model-viewer src
		MODEL_VIEWER.setAttribute('src', modelSrc);
		MODEL_VIEWER.setAttribute('ios-src', iosSrc);

		if ( getMobileOS() !== 'ios' ) {
			let camera		= e.getAttribute('data-camera') ? e.getAttribute('data-camera') : '40deg 80deg',
				modelID		= e.id,
				helperUrl	= 'https://' + window.location.hostname + '/models/' + modelID + '/',
				qrCodeUrl	= encodeURI('//api.qrserver.com/v1/create-qr-code/?data=' + helperUrl + '&size=200x200');

			// Open Model
			MODDEL_POPUP.style.display = "block";

			// QR code Image
			QR_CODE_IMAGE.setAttribute('src', qrCodeUrl);

			// Reset Camera View
			MODEL_VIEWER.setAttribute('camera-orbit', '1mm');
			if (typeof camera !== typeof undefined && camera !== false) {
				MODEL_VIEWER.setAttribute('camera-orbit', camera);
			}
		} else {
			MODEL_VIEWER.activateAR();
		}
	} else {
		let fallback = 'https://' + window.location.hostname;
			sceneViewer  = "intent://arvr.google.com/scene-viewer/1.0?file=" + modelSrc + "&mode=ar_preferred#Intent;scheme=https;package=com.google.ar.core;action=android.intent.action.VIEW;S.browser_fallback_url=" + fallback + ";end;"; 
		window.location.href = encodeURI(sceneViewer);
	}

}

// Model Close Button
const ModelCloseButtonClick = () => {
	if ( getMobileOS() !== 'android') {
		if ( getMobileOS() !== 'ios' ) {
			MODDEL_POPUP.style.display = "none";
		}
	}
}

</script>

<!-- Loads <model-viewer> only on modern browsers: -->
<script type="module" src="https://unpkg.com/@google/model-viewer@1.6.0/dist/model-viewer.min.js"></script>


<?php
get_footer();