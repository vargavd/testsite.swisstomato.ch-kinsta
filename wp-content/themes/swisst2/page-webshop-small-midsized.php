<?php
/**
 * Template Name: Webshop Small Midsized
 *
 * Template for the Webshop development for small and mid-sized companies template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="webshop-small-midsized-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('webshop_creation_image', $ID)?>);">
            </div>
            <div class="column text-column">
                <h2><?=get_field('webshop_creation_title', $ID)?></h2>
                <?=get_field('webshop_creation_text', $ID)?>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2><?=get_field('webshop_development_title', $ID)?></h2>
                <?=get_field('webshop_development_text', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('webshop_development_image', $ID)?>);">                
            </div>
        </div>

        <div id="shop-type-section" class="container">
            <div id="shop-type-list">
                <?php 
                    $shopTypeList = get_field('shop_type_list', $ID);
                    for ($i = 0; $i < sizeof($shopTypeList); $i++): ?>
                    <div class="shop-type">
                        <p class="shop-type-title">
                            <?=($i+1)?>)
                        </p>
                        <p class="shop-type-title">
                            <?=$shopTypeList[$i]['title']?>
                        </p>
                        <div class="shop-type-text">
                            <?=$shopTypeList[$i]['text']?>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>

        <div id="shop-type-list-after-section" class="container text-center">
            <div>
                <?=get_field('shop_type_list_after_text', $ID)?>
            </div>
        </div>

        <?php
          $category = get_field('references_section_button_category', $ID);
          
          if (is_numeric($category)) {
            $category = get_term($category, 'reference_category');
          }

          $category_slug = is_object($category) ? $category->slug : '';

          reference_list_masonry_section(
            get_field('references_section_title', $ID), 
            get_field('references_section_references', $ID),
            $category_slug,
            get_field('references_section_button_title', $ID)
          );
        ?>

        <?php
            process_and_timing_func($ID);
        ?>

	</div>

<?php
get_footer();
