<?php
/**
 * Template Name: Whitepaper 20200403
 *
 * Template for the Whitepaper Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

	<div id="whitepaper-page" class="swisst2-page whitepaper-20200403">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                    <a class="button-red">
                        <i class="fas fa-download"> </i>
                        <?=get_field('hero_button_text', $ID)?>
                    </a>
                </div>
            </div>
        </div>

        <div class="container text-center">
            <?=get_field('checklist_section_text', $ID)?>
        </div>

        <?php
            check_list_func(get_field('checklist'), 'type-2');
        ?>

        <div id="form-text-section" class="container text-center">
            <p>
                <?=get_field('form_text', $ID)?>
            </p>
        </div>

        <div id="section-form" class="container image-text-row text-image">
            <div class="column text-column">
                <h2><?=get_field('form_title', $ID)?></h2>
                <?=do_shortcode('[contact-form-7 id="12383" title="Whitepaper 20200403 Form"]')?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('form_background_image', $ID)?>);"></div>
        </div>

	</div>

<?php
get_footer();
