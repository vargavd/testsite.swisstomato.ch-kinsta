<?php
/**
 * Template Name: Whitepaper 20200708
 *
 * Template for the Whitepaper 20200708 Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

	<div id="whitepaper-page" class="swisst2-page whitepaper-20200708">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                    <a class="button-red">
                        <i class="fas fa-download"> </i>
                        <?=get_field('hero_button_text', $ID)?>
                    </a>
                </div>

                <img loading="lazy" class="hero-tomato hero-tomato-1" src="<?=get_stylesheet_directory_uri()?>/imgs/home/parallax/tomato.png" alt="Hero Tomato 1"/>
                <img loading="lazy" class="hero-tomato hero-tomato-2" src="<?=get_stylesheet_directory_uri()?>/imgs/home/parallax/tomato.png" alt="Hero Tomato 2"/>
                <img loading="lazy" id="hero-front" src="<?=get_stylesheet_directory_uri()?>/imgs/whitepaper/20200708/hero-front.png" alt="Hero front">
            </div>
        </div>

        <h2 id="checklist-section-title"><?=get_field('checklist_section_title', $ID)?></h2>

        <?php
            check_list_func(get_field('checklist'), 'type-2');
        ?>

        <div id="form-text-section" class="container text-center">
            <img loading="lazy" id="form-tomato" src="<?=get_stylesheet_directory_uri()?>/imgs/home/parallax/tomato.png" alt="Hero Tomato 2"/>
            <p>
                <?=get_field('form_text', $ID)?>
            </p>
        </div>

        <div id="section-form" class="container image-text-row text-image">
            <div class="column text-column">
                <h2><?=get_field('form_title', $ID)?></h2>
                <?=do_shortcode('[contact-form-7 id="14345" title="Whitepaper 20200708"]')?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('form_background_image', $ID)?>);"></div>
        </div>

	</div>

<?php
get_footer();
