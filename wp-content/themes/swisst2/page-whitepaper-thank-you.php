<?php
/**
 * Template Name: Whitepaper Thank You
 *
 * Template for the Whitepaper Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

	<div id="whitepaper-thank-you-page" class="swisst2-page">
		
        <div id="section-thank-you" class="container">
            <h1><?php the_title() ?></h1>
			<?= custom_image_size(get_field('file_image', $ID), 'full', 'Tomato Download') ?>
            <div id="whitepaper-thank-you-content">
                <?php 
                    $file = get_field('document_to_download', $ID);

                    $content_body = apply_filters('the_content', $post->post_content);
                    $content_body = str_replace('#', $file['url'], $content_body);
                    $content_body = str_replace('rel="noopener noreferrer"', 'data-name="' . $file['filename'] . '"', $content_body);
                    
                    echo $content_body; 
                ?>
            </div>
        </div>

	</div>

<?php
get_footer();
