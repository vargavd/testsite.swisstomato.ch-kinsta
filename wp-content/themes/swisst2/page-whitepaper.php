<?php
/**
 * Template Name: Whitepaper
 *
 * Template for the Whitepaper Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);
?>

	<div id="whitepaper-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" class="container" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer">
                <div id="hero-content">
                    <?php echo apply_filters('the_content', $post->post_content); ?>
                </div>
            </div>
        </div>

        <div id="section-1" class="container image-text-row image-text">
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('section_1_image', $ID)?>);"></div>
            <div class="column text-column">
                <h2><?=get_field('section_1_title', $ID)?></h2>
                <?=get_field('section_1_text', $ID)?>
            </div>
        </div>

        <div id="section-form" class="container image-text-row text-image">
            <div class="column text-column">
                <h2><?=get_field('form_title', $ID)?></h2>
                <?=do_shortcode('[contact-form-7 id="9723" title="Whitepaper En"]')?>
            </div>
            <div class="column image-column bc-img-center" style="background-image: url(<?=get_field('form_background_image', $ID)?>);"></div>
        </div>

	</div>

<?php
get_footer();
