<?php
/**
 * Template Name: WordPress Page
 *
 * Template for the WordPress Page template.
 *
 * @package Swisstomato2
 */

get_header();

$ID = get_the_ID();

$post = get_post($ID);

$floating_websites = get_field('websites_image_parallax', $ID);
?>

	<div id="wordpress-page" class="swisst2-page">
		
        <div id="intro-section" class="container">
            <h1><?php the_title(); ?></h1>
        </div>

        <div id="hero-section" style="background-image: url(<?php echo get_the_post_thumbnail_url($ID, 'full'); ?>)">
            <div id="hero-dark-layer"><div id="hero-content"><?php echo apply_filters('the_content', $post->post_content); ?></div></div>
        </div>

        <div class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('first_section_image', $ID)?>)">
            </div>
            <div class="column text-column">
                <?=get_field('first_section_text', $ID)?>
            </div>
        </div>

        <div class="image-text-row text-image container">
            <div class="column text-column">
                <h2>
                    <?=get_field('second_section_title', $ID)?>
                </h2>
                <?=get_field('second_section_text', $ID)?>
            </div>
            <div class="column image-column" style="background-image: url(<?=get_field('second_section_image', $ID)?>);">
            </div>
        </div>

        <div id="third-section" class="image-text-row image-text container">
            <div class="column image-column" style="background-image: url(<?=get_field('third_section_image', $ID)?>);">
            </div>
            <div class="column text-column">
                <h2>
                    <?=get_field('third_section_title', $ID)?>
                </h2>
                <?=get_field('third_section_text', $ID)?>
            </div>
        </div>

        <div id="why-pick-us-section">
            <div class="container p-68" style="background-image: url(<?=get_field('fourth_section_image', $ID)?>);">
                <div id="why-pick-us-content">
					<?= custom_image_size(get_field('fourth_section_mobile_image', $ID), 'full', get_field('fourth_section_title', $ID)) ?>
                    <h2><?=get_field('fourth_section_title', $ID)?></h2>
                    <?=get_field('fourth_section_text', $ID)?>
                </div>
            </div>
        </div>

        <?php
          $category_slug = !empty(get_field('references_section_button_category', $ID)) ? get_field('references_section_button_category', $ID)->slug : '';
          reference_list_masonry_section(
            get_field('references_section_title', $ID), 
            get_field('references_section_references', $ID),
            $category_slug,
            get_field('references_section_button_title', $ID)
          );
        ?>

	</div>

<?php
get_footer();
