<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Swisstomato2
 */

get_header();
?>

	<div id="swisst2-blogpost-page" class="container">

		<div id="page-content" class="center-1117">
		    <?php
                while ( have_posts() ) : 
                    the_post();

                    $categories_list = get_the_category_list( esc_html__( ', ', 'swisst2' ) );
                    $tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'swisst2' ) );

                    $img_infos = wp_get_attachment_image_src(get_post_thumbnail_id($post), 'full');
                    $img_height = $img_infos[2];
                    $img_url = $img_infos[0];

                    $thumbnail_class = '';
                    if ($img_infos[1] > 1160) {
                        $thumbnail_class = "full-width-img";
                    }

                    if ($img_height > 564) {
                        $img_height = 564;
                    }
            ?>

                <article id="swisst2-post" <?php post_class(); ?>>
                    <!-- <div id="post-categories">
                        <?=$categories_list?>
                    </div> -->

                    <h1><?=get_the_title()?></h1>

                    <div id="post-date"><?=get_the_date("F d, o")?></div>

                    <div alt="<?=get_the_title()?>" id="post-thumbnail" class="<?=$thumbnail_class?>" style="background-image: url(<?=$img_url?>); height: <?=$img_height?>px;">
                            
                    </div>

                    <div id="post-row" class="<?=(empty($tags_list)) ? 'only-1-column' : ''?>">
                        <div id="post-column-1">
                                <div id="post-tags">
                                    <div class="gray-title">
                                        Tags
                                    </div>
                                    <?=$tags_list?>
                                </div>
                            </div>
                        <div id="post-column-2">
                            <div id="post-content">
                                <?php
                                    the_content();
                                ?>
                            </div>

                            <div id="post-sharing">
                                <a id="facebook-share-button" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?=get_current_url()?>', 'facebook-share-dialog','width=626,height=436'); return false;">
                                    <i class="fab fa-facebook"></i>
                                </a>

                                <a id="twitter-share-button" onclick="window.open('https://twitter.com/intent/tweet?url=<?=get_current_url()?>&text=<?=get_the_title()?>', '_blank', 'width=626,height=650'); return false;">
                                    <i class="fab fa-twitter"> </i>
                                </a>

                                <a id="linkedin-share-button" onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?=get_current_url()?>', '_blank', 'width=626,height=650'); return false;">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </div>

                            <?php
                                $next_post = get_previous_post();

                                if (empty($next_post)) {
                                    $next_post = get_next_post();
                                }

                                if (!empty($next_post)):
                            ?>
                                <div id="next-post-wrapper">
                                    <div class="gray-title"><?=__('Read Next', 'swisst2')?></div>
                                    <div id="next-post">
                                        <a href="<?=get_permalink($next_post->ID)?>" class="post-image" style="background-image: url(<?=get_the_post_thumbnail_url($next_post->ID, 'full' )?>)" alt="<?=$next_post->post_title?>"></a>
                                        <div class="post-content">
                                            <a href="<?=get_permalink($next_post->ID)?>" class="post-title">
                                                <?=$next_post->post_title?>
                                            </a>
                                            <div class="post-date">
                                                <?=get_the_date("F d, o", $next_post->ID)?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div> <!-- /#post-column-2 -->
                    </div> <!-- /#post-row -->

                </article><!-- #post-<?php the_ID(); ?> -->
    			
            
            <?php    
    		    endwhile; // End of the loop.
    		?>
		</div> <!-- /#page-content -->

	</div><!-- /#swisst2-blogpost-page -->

<?php
get_footer();
